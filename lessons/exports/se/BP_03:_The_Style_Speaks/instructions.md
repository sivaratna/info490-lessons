# **🐍 Software Engineering/Best Practices**

```
p = lambda x: (+110.99999910447704 * x**0 +12162.796598821811 * x**1 -35646.280324988584 * x**2 +42736.77625882994 * x**3 -28148.17538704171 * x**4 +11424.646768020271 * x**5 -3012.8807264832403 * x**6 +526.3844175438777 * x**7 -60.48075392026485 * x**8 +4.392578810701073 * x**9 -0.1827463623102413 * x**10 +0.0033171997734836277 * x**11)
```

```
v = lambda x: chr(int (x + 0.0005))
```

```
print("".join(list(map(v, map(p, range(0,12))))))
```

# The Elements of _Coding_ Style

 

What makes for well constructed software is difficult to quantify.  In many cases it's hard to know until the code base or application (i.e. app) is used by thousands of people.  Good programming style usually means writing code that is

1. Clear (not obfuscated; intention is clear)
2. Robust (stands up to stress, bad data).
3. Easy to maintain (easy to extend)
4. Easy for others to use
5. Bug free

Of course there are other metrics for quantifying code quality including simplicity, completeness, consistency, and testability.  However, we are addressing only the first point (clear code) which is usually the hardest to quantify.  Clear code not only consists of how the code is written (e.g  being explicit vs relying on hidden state or implicit assumptions), but also about clearly communicating the code's intentions and the aspects that might make it difficult to understand (perhaps addressing how the algorithm differs from a standard implementation or how edge cases are treated).  It's also about the aesthetics that make the code easy to read -- beautiful code is indeed hard to define.

Let's emphasize a point made earlier: we often forget that our code is read by people; it is read hundreds/thousands of times more than written. The computer does not care if you write poorly styled code, but your readers will.

In this lesson we will address a few aspects that make for well written code (in the readability sense): line indentation, spacing, comments, style consistency.

# **Indentation**

With Python, every time you have a nested code block, you must indent (this is not a technical requirement in other languages):

```
def calculate_temperature(measurements):
```

```
    **# 1st level** of indent after function
```

```
    current_temp = 0
```

```
    for m in measurements:
```

```
      **# 2nd level** of indentation
```

```
      current_temp += m.get_value()
```

```
      if current_temp > SOME_HIGH_TEMP:
```

```
        **# 3rd level** of indentation 
```

```
        register_high_mark(current_temp)
```

```
    return current_temp
```

However, what is not required by Python is how much to indent. One of the things you may have noticed is that many of the repl.it lessons use an indentation level of 2 spaces.  However, Python is supposed to be indented with 4 spaces (a recommendation not a requirement).  Because of the importance of fitting code to smaller screens, 2 spaces are used  here.  When you have a new code block (one that ends with a colon), you should indent the amount of the parent block and an additional equal amount for the child.

Just as important is being sure to indent the same amount for each level is making sure peer levels are indented the same amount:

```
def calculate_temperature(measurements):
```

```
    current_temp = 0
```

```
    for m in measurements:
```

```
        current_temp += m.get_value()
```

```
        if current_temp > SOME_HIGH_TEMP:
```

```
          register_high_mark(current_temp)
```

```
    for t in get_high_temperatures():
```

```
     if t%32 == 0:  **# legal, but WRONG** 
```

```
       register_cycle(t)
```

```
    return current_temp
```

Note that the selection statements inside the loops are not indented the same amount.  Each peer indentation level should be indented the same amount.

**Tabs vs Spaces**

Ever since some of the early text editors (ed, emacs, vi) the heated battle emerged as to whether one should use spaces over tabs to indent code.  The biggest reason for choosing spaces is that regardless of what editor is used, it will be formatted consistently since the mapping from tabs to spaces is not needed.  The biggest reason for using tabs is that the resulting source code file is smaller since it uses less key strokes (or characters).  

Because today's editors can easily be configured on how to handle tabs, it's not that big of deal.  Personally I like the control and precision of spaces and shutter at the idea of using tabs (watch the live coding video for more 🙂)

# **Spacing (the use of whitespace)**

Another area of debate is how much whitespace to use for different coding conditions:

**Function definitions**

Some people like separating the definition of the function and the return (or the close of the function) with whitespace:

```
def calculate_temperature(measurements):
```

```
  current_temp = 0
```

```
  for m in measurements:
```

```
    current_temp += m.get_value()
```

```
    if current_temp > SOME_HIGH_VALUE:
```

```
      break
```

```
  return current_temp
```

`vs`

```
def calculate_temperature(measurements):
```

```
  
```

```
  current_temp = 0
```

```
  for m in measurements:
```

```
    current_temp += m.get_value()
```

```
    if current_temp > SOME_HIGH_VALUE:
```

```
      break
```

```
  return current_temp
```

The longer the function, the more whitespace can be used to separate the body from the declaration and the return statement.

The same issue comes up for different block elements (iteration, selection).  The longer the code, the use of additional whitespace help visually separate the different blocks and gives the code some space to breath and be easily read

```
def calculate_temperature(measurements):
```

```
  
```

```
  current_temp = 0
```

```
  for m in measurements:
```

```
    current_temp += m.get_value()
```

```
    
```

```
    if current_temp > SOME_HIGH_VALUE:
```

```
      break
```

```
  return current_temp
```

**Similar groups of variables**

Another area to consider is to treat groups of logically related variables similar with respect to whitespace and spacing:

```
apples = ['mac', 'honeycrisp', 'fuji']
```

```
sweetness = [1,2,3]
```

```
bitterness = [78,120,34]
```

`vs`

```
apples     = ['mac', 'honeycrisp', 'fuji']
```

```
sweetness  = [1,      2,            3]
```

```
bitterness = [78,     120,          34]
```

Depending on the sizes of the lists this is usually not practical, but keeping logically related variable together does help:

```
apples     = generate_apple_names()
```

```
sweetness  = get_sweetness(apples)
```

```
bitterness = get_bitterness(apples)
```

```
cluster_size  = 3
```

```
cluster_error = lambda x,y : x-y
```

**The Magic of the 80's:**

 

A while ago the coding standard was to fit each line of code into 80 columns.  This was a result of IBM's [80 column punched card format (see above).  Your code was the result of which columns were punched out.  It was a time when you could actually get an extension from your teacher if you dropped your program.  ](http://en.wikipedia.org/wiki/Punched_card#IBM_80_column_punched_card_formats_and_character_codes)

[The 80 columns then carried over to when editors were used to write code (rather than punch cards).  As a result of all this, it is still recommended that code should not extend past 80 columns.  You can think of this as aid for reading code.  It's hard to read long lines.  In Python, when you have a long line of code you can use the line continuation character to visually break a single line across many:](http://en.wikipedia.org/wiki/Punched_card#IBM_80_column_punched_card_formats_and_character_codes)

```
if p1 == "paper" and p2 == "rock"  or \
```

```
   p1 == "scissors" and p2 == "paper" or \
```

```
   p1 == "rock"and p2 == "scissors":
```

```
    return p1
```

This is indeed easier to read than a long line that extends past your visual field.

**Parenthetically Speaking**

It's usually common knowledge among experienced programmers that multiplication has higher precedence than addition so 5*5+2 is 27 and not 35 because of operator precedence.  However, beyond simple arithmetic using parenthesis to communicate your intent to the reader (as well as the compiler) is a good rule.  The above code can be made clearer by using parenthesis liberally (you may need to make your screen wider):

```
if (p1 == "paper"    and p2 == "rock")     or \
```

```
   (p1 == "scissors" and p2 == "paper")    or \
```

```
   (p1 == "rock"     and p2 == "scissors"):
```

```
   
```

```
    return p1
```

It becomes much clearer that there are three peer `**and**` statements (a.k.a logical conjunctions) each joined by a boolean `**or**` (a.k.a. logical disjunction). Also, note the visual lining up of the different parts of the logic statements.

# **Comments (reprise )**  

By using some of the other features of good programming (good naming, proper decomposition, indentation and whitespace), you don't have to rely on populating your code with comments -- the code speaks for itself.  However, even the best styled code can be improved by the use of good comments.  Comments help your readers understand what the author was thinking or how a section of code operates.  

The code should speak to what it is supposed to do, **not** tell the reader how it does it (the code already does that).  However, if it's not obvious, always communicate to your reader what the code is doing:

**Wrong**

```
# set accumulator to zero
```

```
acc = 0
```

```
# loop over the list
```

```
for item in elements:
```

```
   # get item's value
```

```
   v = item.value()
```

```
   # add it to the accumulator
```

```
   acc += v
```

**Right**

```
# accumulate the mean temperature measurements
```

```
total = 0
```

```
for m in measurements:
```

```
   total += m.value()
```

`**# vs '''**`

There are two types of comments in Python: single line and multi-line. 

Single line comments start with a '#' :

```
# something that needs to be explained
```

Multi-line comments use two sets of triple quotes:

```
"""
```

```
this is a 
```

```
very long
```

```
comment
```

```
that spans many lines
```

```
"""
```

As a bit of Python Trivia, a multi-line comment is really just a string constant that isn't assigned to a variable.

```
print('''
```

```
apples are red
```

```
violets are blue
```

```
and so goes my sled
```

```
''')
```

The triple quoted string is the recommended way to build docstrings (more below) isn't really a comment (and the, it's more common to use the single line comment multiple times:

```
# this is a 
```

```
# very long
```

```
# comment
```

```
# that spans many lines
```

[Python’s PEP 8 style guide](https://www.youtube.com/watch?v=Hwckt4J96dI) (https://www.python.org/dev/peps/pep-0008/#comments) also favors repeated single-line comments.  

In general most comments belong on there own line.  However, there are situations where an inline comment works well -- usually if the line is short and the comment as well:

```
margin = MAX_WIDTH/2.0 + 1 # adjust for border 
```

**Docstrings**

There's an accepted (standardized, Python conventions) method to associate external documentation with your code.  The Python Docstrings is a form of documentation that uses the triple quoted string (i.e. a string literal) in the class, module, function or method definition.  When we get to designing classes, we will learn more about Docstrings.

You can use a tool to extract these Docstrings from the code and build documentation.  However, we will not be using this in these lessons for now. 

As you begin to use other libraries you will see how valuable good comments are. https://www.python.org/dev/peps/pep-0257/

It's important that as the code adapts the comments need to be in sync with the code.  There is nothing more frustrating than having comments that are wrong or no longer relevant.  Comments are also used to generate documentation.  

**The Bad Side of Comments.**

We all do it.  We write code, it doesn't work.  But rather than removing the broken code, we just comment it out (to help us remember how something did work at one time -- especially as we are learning a new library).  If you do this, always be sure to make it clear that the code never worked or won't work.  Usually code is maintained by source control so the idea of keeping old code isn't necessary -- you can always visit the past.  

You will see other mis-uses of comments:

- draw attention to how good or bad the code is
- blame someone
- to comment out code without explanation
- to explain bad code -- rewrite the code instead

**Style Consistency**

Regardless of what style you adapt, one of the main tenenats is to apply the same style to all the code consistently.  When your job involves designing and building software, your employer will usually have it's own style guide that its employees are expected to follow.

# Style of functions

Since the basic unit of computation block is the function, it requires some additional notes on writing clear functions.

- Write small functions.  If your function becomes too long, look for places to factor out code that could be used by other functions
- Name them properly

        • usually use a verb

        • use `is` as part of the name for any function that returns a boolean (e.g. is_clear(xyz) )

- Document immediately -- while your thoughts are fresh
- Write tests for each function (more later)
- Use default parameters (more later)

**Before you go, you should know:**

- the benefits of good, consistent style
- what factors determine a good comment

# **Lesson Assignment**

NOTHING TO DO! YEA.  Just hit submit.  However, we do expect you to follow these conventions as you write more code for future lessons.


All rights reserved

**References and Readings**

https://docs.python-guide.org/writing/style/

http://carma.astro.umd.edu/nemo/pitp/papers/style.pdf

**comments:**

https://medium.freecodecamp.org/code-comments-the-good-the-bad-and-the-ugly-be9cc65fbf83

