def winner_RPS(p1, p2):
  if p1 == p2:
    return None
  if (p1 == "paper"    and p2 == "rock")  or \
     (p1 == "scissors" and p2 == "paper") or \
     (p1 == "rock"     and p2 == "scissors"):
    return p1
  return p2