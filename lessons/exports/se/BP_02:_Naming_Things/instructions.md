# **🐍 Software Engineering/Best Practices**

# Naming Things

For the most part, all the lessons in the bootcamp had you solve well defined, "simple" problems.  There was not a lot of emphasis (none actually) put on design, reuse, efficiency, testability, readability or complexity (or the lack of). The one thing you had to be concerned about was passing the tests. Even though correctness (i.e. passing the tests) is the most important attribute of a solution, quality code does a lot more than "just pass the tests".  As your coding skills develop and the complexity of your projects increase, the ability to write code that is readable by others, maintainable, extendable, well designed, efficient, and architected for adopting future changes is just as important as passing all the tests.  At some point you may write code that needs to be run in an environment that can't simulate all the possible combinations of potential state and inputs -- hence even "professional' software is constantly being improved and de-bugged.   Well designed and written code lends itself to being fixed without a complete re-write even when new requirements come in.

Up until now, we have focused on writing code that solves the problem at hand.  But we have NOT spent any time or effort on how the code should be written or the 'style' of the code.

 

image credit: https://xkcd.com/844/

**Code is for people too**

Although code is written for the computer to interpret and execute, we often forget that our code is read by people.  Code is read hundreds/thousands of times more than written. The computer does not care if you wrote poorly styled code, but your readers/users will.

Even if the only reader is the author of the code, well written, documented code can make the difference between having to tweak it slightly to completely having to re-write the code because the author either can't understand it (happens a lot) or that it was written so poorly that it can't be adjusted to manage with the new requirements (or pass the current tests).

**Code in poor taste**

Let's look at an extreme case of poor code (although anyone who posts code to piazza, email, etc without formatting is as guilty as this example).  In this case, it's a fictional language, but for many languages similar syntax is completely valid:

```
def doit(l, f, g){
```

```
ptr = l.head
```

```
while(ptr != None){f(ptr)
```

```
if not g(ptr)
```

```
return ptr
```

```
ptr = ptr.next()
```

```
if ptr == None:
```

```
return None
```

```
}return ptr}
```

At first glance, it's really hard to even guess what `doit()` does.  What sticks out to me is a bunch of return statements.  The naming of the function, parameters, and variables is useless.  Even though it would run correctly, it's hard to read what's going on and the author is relying on subtle features of the language (especially if this was a 'C' based language). Its indentation is non existent, the variable and function names offer no clues as to what it's doing.  It's not clear what is being returned -- especially at the end.

**Style guides to the rescue**

As soon as people started writing large programs, it became evident that how the code (style-wise) was written was as important as to what was being written.  Large projects with several engineers would not only suffer from poor coding styles, but from inconsistent style (different engineers had different preferences).  Companies soon adopted best practices to bring consistency to different coding styles to help the situation.

Ever since Python was born, it tried to embrace the idea of having a physiology of style and guidelines.  Go ahead and run this code (you can place it at the top or comment out the all the code in `main.py`):

```
import this
```

You will see the 'Zen' of Python. The idea of putting style ahead of everything else is one of the driving forces as to why Python is one of the few languages where the language itself will force the author to use proper indentation (a style guideline).

There's a lot that we will eventually cover with respect to good style guidelines, but we have enough experience to appreciate and understand some of the following best practices:

**RTF**orking**M**

Let's begin by reading the Python style guides:

- `https://www.python.org/dev/peps/pep-0008/`
- `https://docs.python-guide.org/writing/style/`

Although it has a lot of content, as your skills develop, revisit the manual on style and make sure you are always working towards clarity and consistency.  Almost any software company will have it's own style guide as well that it expects all of its engineers to follow.  Consistency in applying the same style rules helps create a uniform code base.

In the next few lessons we'll discuss some general good coding practices and software engineering principles.  Here's a small out-take of some of the things we will discuss and that you should be ready to apply to all your code:

**Readability matters**

- meaningful variable names function names
- consistent line indentation
- good comments matter; bad ones are worse than no comments
- don't change styles; consistency is important

# **Naming Things Properly**

Not only do you want to use the following conventions for how you type out your variable and function names, you should always use a name that represents the value or action for which you are using it.  In general most variables are either nouns or verbs, begin with a letter and can contain digits and underscores as well.

Remember that in Python the case of a variable matters.  So the variables `my_list` and `my_List` are different. You also want to be sure you aren't naming functions or variables that are imported functions and types (e.g. `len`, `list`, `dict`, `str`, `max`, `min`).

**Variable Names**

Simple variable names like i, e, n are fine (and standard) for looping indices or enumeration, but you usually want to create a descriptive name.  A variable named `temperature` is better than `temp` which is better than `t` which is better than `x` which is better than `office` (has no bearing on the current context).  Also choose names that you can read and are meaningful.  At some point, you should be able to 'read' your code and the comments serve only to clarify certain, tricky situations or edge cases.

As far as how to name things, there are a few Python conventions.  For variables, functions, methods, packages, modules it is standard to use lower case with underscores between words:

- `counter`
- `words_with_vowels`
- `utilities.py`

**Classes and Exceptions**

Although we have to see classes, when you create your own type, it is standard to capitalize them using camel case which start each word with a capital letter:

- SpellCorrector
- TestFramework

**Privacy emphasized**

For protected methods (more on this later), internal functions and variables you intend not to be used outside of the module.  These have a leading underscore.  If you see these (we won't write code this semester that uses them), you know the author has intended for the information to be private:

- `_counter`
- `def _sort_by_sweetness():`
- `_single_leading_underscore(self, ...)`

For private methods (more on this later) they are called dunder methods (for double underscore) and they begin and end with two consecutive underscores:

- `def __get__(self, ...)`
- `def` `__repr__(self):` 

Even though we have not yet written our own types (called classes), when you come across other people's code, you know just by seeing how the item is named what it's purpose/visibility is.

**Constants**

A constant value is one whose value should not change. There are two types of constants.  The first kind holds well known values that won't change because they represent a known fact (like pi, the temperature at which water freezes, etc).  The other kind represents a value that might be changed infrequently but if it did, you wouldn't want to have to search and replace that constant value throughout a code base -- you would just change the value in one place.  In both case constants are written in upper case and with underscores:

```
NASDAQ_APPLE = 'APPL'
```

```
LEVELS_TO_COMPLETE = 3
```

```
ABSOLUTE_ZERO_FAHRENHEIT = 459.67
```

```
MINIMUM_PASSWORD_LENGTH = 3
```

For an example, if you write code that uses just the value there are two main issues:

```
if completed > 3:
```

```
  # do something 
```

1.  Someone (perhaps even the author) will forget why 3 is used (its semantics) when trying to understand the purpose of the code.

2.  If you ever had to update this value, it would require someone to search and decide for every 3 found if it is the one that needs to be updated.  

For the above, it's much better to use the constant name:

```
if completed > LEVELS_TO_COMPLETE:
```

```
  # do something
```

Now if you ever needed to change LEVELS_TO_COMPLETE value, you would change it in only ONE place (part of the DRY principle -- which we will get to in another lesson).

**Avoid the Magic**

When you use numbers like in the above example, it's called a magic number.  If you are using a specific value in your code, you should place that value in a constant and reuse the constant:

**Wrong:**

```
if t > 459.67:
```

```
  # do something
```

**Right:**

```
ABSOLUTE_ZERO = 459.67
```

```
if temperature > ABSOLUTE_ZERO:
```

```
  # do something
```

Notice in the second example, the code reads well.  In the first example, you may have no idea why t is being compared to 459.67. 

**Before you go you should know**

- the basic reasoning behind naming variables
- how to name a constant
- why it's better to use a constant variable rather than the number itself

# **Lesson: RPS revisited and revised:**

 

In the lesson.py tab is one possible version of solving the rock, paper, scissors game.   For this lesson we are going to tidy up this solution by enforcing the following best practices:

1. Create the constants ROCK, PAPER, SCISSORS and update the code accordingly (i.e. there should only be one `'rock'` in your code)
2. Have only a single exit point in the function.  That is there should only be one return statement
3. Properly name the variable that you return (the one that holds the winner of RPS.

**Note:**  The `\` that ends some of the lines in the boolean conditional is a way to continue a single statement across multiple lines -- for easier reading.  Without the line continuation character, it would be much harder to read:

```
if (p1 == "paper" and p2 == "rock") or (p1 == "scissors" and p2 == "paper") or (p1 == "rock" and p2 == "scissors"):
```

The function `winner_RPS` still needs to pass the tests (the ultimate metric), but your focus should be on making the code as clear as possible.

You can tag any question with **SE_Naming** on piazza.


All Rights Reserved

