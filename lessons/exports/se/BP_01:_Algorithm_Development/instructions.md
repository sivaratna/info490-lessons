# **🐍 Software Engineering/Best Practices: Algorithm Development**

Creating an algorithm for a computer to follow to solve a problem (i.e. algorithm development) is **not** something easily learned.  One of the biggest problems with algorithm development is understanding the difficulty that a computer has in following what you think are "simple" instructions.  

As you develop your programming skills the syntax will become more familiar and you will start to focus on how to solve the problem.  The hardest thing about programming is coming up with a design/solution to a problem that can be "easily" translated using a programming language.  Many people enjoy Python because the syntax of the language (the un-fun part) doesn't interfere with solving the problem (the fun part).

The process of creating a solution to a problem is called algorithm development.  It takes time and practice to get comfortable with it.  The only thing you can do to hurt your skill development is to too quickly ask Google or a friend for the solution.  Time, sweat, staring and yelling at the computer are part of the process. Teaching and learning good design is much harder because it relies on having the experience of making bad designs and making bad designs which is part of the learning process.

Even though we know the basic constructs (variables, values, expressions, loops&selection and functions) how we combine them is endless.  You can have loops within loops, selection statements inside of selection statements, loops inside of selection statements, functions inside of loops, etc.  The fun is about to begin!

**PB&J**

There's a classic example used in beginning computer science classes to bring home the point that computers are really not that smart -- they literally just follow the directions given to them.  If the directions are wrong, the computer will follow them blindly.  With that in mind, the exercise given to the students is for them to write down how to make a peanut and butter sandwich.  

Let's do that.  Open the `PB&J.txt` file (i.e. select the tab) and write the directions such that someone could make a good sandwich by following the directions.  You MUST have at least 4 lines (and number them as well, in case you need to refer to other lines. 

Go ahead, write them out (you will also submit a screen shot or separate document to gradescope)

`-------------------------------------`

`--- **Do not pass this line without**` 

    `**doing the above exercise** ---`

`-------------------------------------`

Now watch this video:  

`https://www.youtube.com/watch?v=cDA3_5982h8`

That's a GREAT example of what you are trying to do. When you write code, you are asking something that won't have the assumptions that you have.  You have to be excruciatingly specific AND you have to know what constructs you can use to communicate your directions to the computer.  Even in the PB&J instructions you had to decide for whom you are writing the instructions.

Looking back at your directions, take note of what assumptions you made about the audience.

**Creating Algorithms**

Teaching someone how to write an algorithm is no easy task -- there's no algorithm for that either.  The most simplistic generalization to building algorithms is the following two steps:

1.  Write how you would solve this problem such that the 'thing' that you're instructing is a friend.  The younger the friend, the better.  For example, how would you describe the process to find the smallest number in a set of numbers?  You might get to a point of saying, "now compare the two numbers".  Well that sentence assumes that the person knows what it means to compare two quantities.  If s/he didn't have a good grasp of algebra (or basic math) you would have to write down the steps of the process to "compare" two numbers.  

2. Now that you have the steps written out, you need to translate them to a language that the computer will understand.  That translation can be very difficult for those who are new to programming.  And the more you know about the language (e.g. loops, conditionals, functions, libraries), its interaction with the computer (e.g. how to access memory, persist values, etc), and the relevant (and viable) data structures available  (e.g. arrays, dictionaries, trees, graphs, etc), the better you will become at moving from step 1 to implementation.

**Not So Fast**

Even with this simplistic approach to building algorithms for a computer to use, it has a lot to desire. Think about the answer to the question: "How does Google search work?".  You might be able to do **step 1**:

- you have a bunch of documents (web pages)
- you take the words of the search query as input
- and return those documents that are most relevant.

Well that was easy.  How about **step 2**?  

There's so much to unpack in order to solve that problem (especially efficiently) that it would be unfair to think that we could implement that algorithm.  Think of all the issues we would have to know more about:

What is a document?  How is it stored?  How is it represented?  How do we get access to ALL the documents? How do we fix spelling, disambiguate words (the jaguar car vs the cat), handle slang, abbreviations, acronyms and idioms?  How do we "find" the relevant information, what does it mean to be relevant?  ... and the list goes on.

Even though Google has "solved" search, the solution (or algorithm) isn't perfect.  It still makes assumptions on your intention and expectations.  

However despite the difficulties, we can still give a set of guiding principles that you can use to help become a better algorithm developer.  Our focus is on Python, however the interface being used to communicate to a computer on how to solve a problem is moving (slowly) away from needing to write code to constructing solutions visually.  But underneath it all, is still a machine that operates on instructions.  So the effort you give to become a better algorithm designer will always help you understand "how it works".

# **Flow Charting**

One of the best methods to help breakdown a problem into manageable steps is to create a flowchart.  A flowchart is a way to diagram a process (like a program) in such a way that it helps you break down a problem into manageable steps.  ALSO the end result is something you can 'easily' transform into code.

The following (both links are the same) is a great introduction to creating flowcharts:

- [https://medium.com/@warren2lynch/a-comprehensive-guide-for-flowchart-over-50-examples-785d6dfdc380](https://medium.com/@warren2lynch/a-comprehensive-guide-for-flowchart-over-50-examples-785d6dfdc380)
- https://storage.googleapis.com/uicourse/pdfs/FlowCharts.pdf

You will need to read this to finish the lesson.

# **Principles**

There are several components to becoming proficient at algorithm design.

**Computer Empathy and Subdivision**

Ultimately your goal is to figure out how to solve a problem from the computer's point of view. Of course this assumes you know what constructs a computer understands -- hence this class. 

Perhaps the single most useful guiding principles is to always subdivide a hard problem into smaller ones.  Keep subdividing until the small problem "easily" maps into some set of constructs that the computer understands.  

**Know the Language**

That's what all the lessons (and several more) are about.  Some languages lend themselves to solving the problem a specific way.  You may hear 'that's not very Pythonic' solution -- meaning that you might have solved the problem, but you didn't take advantage of the Python language constructs.  We will cover more topics to get you to be a fully competent programmer.

**Know the Data Structures** 

Data structures are data types that provide specific functionality (like lists and dictionaries) are fundamental in how data is contained, organized and accessed.   The more you know, the more tools you have to quickly solve problems.  The good news is that you rarely have to implement the new data structure (there are so many Python libraries to help you), but you will have to know WHAT is available.  Binary trees, stacks, queues, hash tables, linked lists, graphs, trees, heaps (and the list goes on).  It's a lot to know, but as you slowly solve more problems, you will eventually run into new data structures that can help you solve a problem easier.  However, the good news is that you can solve a majority of problems using only lists and maps (dictionaries).

**Know the Math.**  

Understanding some of the principles behind set theory, complexity theory (are you solving an 'unsolvable ' problem?), combinatorics, linear algebra, discrete math, finite state automata may seem daunting.  If you have a chance to take a course in discrete math or linear algebra, the more tools you will have when it comes time to implement your own algorithms and methods.  If taking a class is impractical, be sure to stop and wonder why something works (e.g. how does the computer solve regular expressions? -- a topic we will soon learn about) and try to find an article that helps explain the logic and math behind a particular method.

**Knowing your Computers.**  

Having a good understanding of boolean algebra, floating point representation, cache design are also topics that can help you.  Once again, be curious (e.g how does the computer know that 01000001 is the number 65 or a letter 'A'?,  how does binary work?).  Many of the questions you come across will lead down a long path to understanding the answer, but it will be worth it.

**Simply Put**

Putting ALL that aside (it seems unnerving doesn't it?) -- the main idea of algorithmic development is the ability to SIMPLY solve a problem.  Occam's razor -- the simpler answer is usually the correct one -- applies mightily.  Always solve the problem in the manner that makes the most sense to you at first.  It's always easy to come back and make things more efficient or "clever" -- but doing that upfront will wreck havoc on your free time.  As you learn more, you will naturally start to solve the problems from a computer's point of view.

**What to do when stuck?**

When you are tasked with a problem to solve, try to figure out the steps necessary to answer the problem -- much like the standard example of creating a recipe or a list of steps to follow.  Describe the problem and your solution to a friend.  If s/he can follow, you're probably on the right track.  It helps to draw pictures and break down the problem into as many discrete units of work as possible.  Talking through your solution with anyone willing to listen, will help you realize some of the issues/points you were taking for granted.

**Before you go you should know:**

- What is an algorithm?
- What strategies can you use to help design a solution?

# **Lesson Assignment:  RPS**

 

# **Part 1:  PB&J and RPS**

Be sure to sign up for a gradescope.com account using your @illinois.edu address.

The invite code for this class is **MP24DP**


**Create a Flow Chart**

The game rock, paper, scissors is a classic tool used to make important decisions among two friends (read https://www.wikihow.com/Play-Rock,-Paper,-Scissors).

- Create a flow chart for the game Rock Paper Scissors.  Be sure to use the appropriate symbols.  You can use an online tool, your own drawing software, or just sketch the flowchart  on a piece of paper.  

**Upload Your Solutions**

You will create a PDF or a set of images to upload to gradescope:

1. the instructions you wrote out in the PB&J.txt tab
2. the flowchart you will create 

# **Part 2:  Code up your flow chart (solution)**


1. Create a function named `winner_RPS`.  The function `winner_RPS` takes two parameters, `p1` and `p2`.  

- It returns `p1` (the parameter) if `p1` wins; `p2` if p2 wins; and `None` if there is no winner.
- Try to write your solution such that the `winner_RPS` has a single return statement.
- This code needs to be in the `lesson.py` module.

2.  p1 and p2 can only be one of the following values:

"rock", "paper", "scissors"

3. Test your code.  You can write your own tests and not rely on the testing framework.  In `main.py` write a test function named `test_RPS` to verify that your code is working. The function doesn't have any parameters and should test your `winner_RPS` at least a few times (more than twice!).  Try to figure out how to return True if it passes all your tests (False otherwise). 

- Use the import statement in main.py (i.e. `import lesson`)


For example the following could be a test:

```
t1 = 'rock'
t2 = 'paper'
if winner_RPS(t1, t2) != t2:
  print('Test FAIL')
```

You can use also random.choice to model the selection part of the game play as well:

```
values = "rock,paper,scissors".split(',')
p1 = random.choice(values)
print(p1)
```

How would you test all possible cases?  Is that even possible?

Once you are finished hit submit.

You can tag any questions you have with SEAD


All rights reserved

