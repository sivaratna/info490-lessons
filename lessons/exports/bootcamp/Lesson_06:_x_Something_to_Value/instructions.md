# **A 🐍 Coder's Journey**

**Lesson 6:  Something to Value**


We now know that a variable can be used to remember values to be re-used throughout our code.  The question to ask is what kind of values can we assign to a variable?


Without values, there would be nothing for computers to do.  All work done by a computer boils down to manipulating values.  However, a computer cannot make sense of a value without knowing its type.  The **type** informs the computer (really the interpreter or compiler) what operations are allowed on the value.  Python has several built-in types including integer, floating point, string, and boolean.


Type in the following code and try to figure out both the value and the type of each variable:

```
a = 2
b = a + 3.0
c = 22.0/7.0
print(a,b,c)
```

**number type**

We have already snuck in some values (and you inferred its type) in the first few lessons.  We have seen 2 and 3 (e.g. 2 + 3).  Both 2 and 3 are values that have the type numeric.  When the Python interpreter sees a number, it knows it can use the operators + - * / on them (and others as we will see).  

Python number types include `int` (whole numbers), `float` (numbers with a decimal point), `long` (big integers), and `complex` (numbers with imaginary parts).

**string type**

Another value we saw was "Welcome Programmers" (e.g. `print("Welcome Programmers"`)).  As you can guess, "`Welcome Programmers`" has type string.  Strings also have operators:  Type the following:

```
s = "Hello"
s = s + " World"
print(s, len(s))
```

The + operator is a short hand way to concatenate strings together.  The function `len` counts the number of characters in a string.  What happens if you pass a number to the len function?  What happens if you attempt to multiple or divide strings (that's a hint to experiment)?

Strings can be defined using both the double quote `"` or the single quote `'` character.  You can also combine them if you need one or the other:

```
ok = "Here's to Python"
```

```
ok = 'Python is "food" for the soul'
```

Sometimes people use double quotes for enclosing multiple words and single quotes for single words:

```
ok = "I love Python"
```

```
ok = 'Python'
```

The empty string is a string without any characters:

```
empty = ""
```

This is not the same as a string with blanks/empty spaces in it.

**boolean type**

Back in the lesson on functions we saw the following:

```
def my_logger(msg):
```

```
    print(msg)
```

```
    return True
```

The value `True` has type boolean.  There are only two values of type boolean: `True` and `False` (capitalization matters).

Note that we do not put quotes around `True` or `False` (even though they may look like they should have quotes).
There are three operators for boolean values: `and`, `or`, and `not`

- The **`or`** operator only returns `False` if both of its operands are `False` (and T`rue` otherwise).
- The **`and`** operator only returns T`rue` if both of its operands are T`rue` (and `False` otherwise).
- The **`not`** operator inverts the boolean value from `True` to `False` or from `False` to `True`. 

Type in the following and try to determine what will be printed before running the code:

```
bb = True
print(bb or not bb)
```

We will discuss boolean expressions (which create boolean values) and their role in programming in the next few lessons.  

**Constants**

We use the word constant to refer to a value that cannot be changed.  By definition, the simple values that we have used to assign to variables are constants (also called _**literals**_).

```
name = "Python"
```

```
age = 20
```

Even though both `name` and `age` are variables (they can hold different values), the values on the right hand side (`"Python"`, `20`) are the constants/literals.  Most programming languages reserve parts of memory to hold the values of constants.  You cannot change the value of a constant:

```
20 = 40 # this is not legal 
```

> **Coder's Log**:  many programming languages allow you to define a variable as constant.  This means once the variable is assigned a value it cannot be changed.  Python does not have this feature but you can use other features of the language to achieve the intended effect.

As a convention, when a variable holds a constant (especially numeric), the name of the variable should be put in uppercase:

```
AGE = 20
```

If there is a chance that the variable's value will indeed change (perhaps based on user input or some computation), the uppercase convention should not be used.

**`None` what?**

We also saw the value `None`.  The value `None` has the type `NoneType`. Same root word, but different meanings. 

Type and run the following:

```
d = None
print(d)
```

The value `None` is reserved for when it's important to mark the absence of a value.  For example, perhaps some function is to calculate a tax rate based on the inputs, however, there may be situations where the tax rate cannot be calculated, the keyword `None` could be used to mark such situations.

> **Coder's Log:** Many languages will classify the types number, string, boolean as **primitive types**.  These are the types provided by the language and have built in operators that work with them.  For technical reasons that we will explore when we discuss objects (as in Object Oriented Programming), Python purists don't consider these types as primitive but will agree to call them built-in types.  

**function type**

We have also seen the type function.  Yes, you can assign a function to a variable (and use it like any other variable)! 

```
def add(a, b):
  return a + b

fn = add
```

```
print(fn(1,2))
print(fn)
```

The `fn` variable "holds" the function `add` (it actually holds the memory location to where the function `add` is stored). Since we can assign functions to variables, we can also pass them as parameters and return them as well.  This is a very powerful feature of Python and makes functions 'first-class' citizens in the type world.


**What type are you?**

With all these types, sometimes it is useful to ask a variable "what type are you?" We can do that with the `type` function.  This function takes one parameter.  The built-in `**type**`**** function **returns a string** indicating its type.  Enter the following and run the code (this assumes that you have been typing in all the previous content):

```
print('a',  a,  type(a))
print('b',  b,  type(b))
print('c',  c,  type(c))
print('d',  d,  type(d))
print('fn', fn, type(fn))
print('s',  s,  type(s))
print('bb', bb, type(bb))
```

Do not continue unless the above code prints out properly. Python also has a function named `isinstance()` to compare two items.  It returns a boolean depending on the values you are comparing:

```
print(isinstance(5, int))
```

> **Coder's Log:**  Booleans are usually implemented using 0 (False) and 1 (True).  You can see this by "dividing" False by True:  `type(False/True)`

In general, you rarely need to use the functions `type` or `isinstance`.  For us, we will use them as a teaching tool to understand how Python treats values and types.  If it's important to use either `type` or `isinstance`, we will be sure to make note of it.

# **Lesson Assignment**

**Part I: You already did it.**

Be sure you typed in everything from this lesson.  The variables must be defined as shown.  These variables (and their types) will be tested when you hit submit.  Make sure you comment out any code that generates an error.

**Part II: Using variables to capture the series.**

In the function lesson when we didn't know about variables, we had a long expression to add the result of a series.  For example to compute 1 + 1/2 + 1/4 we had the following code:

```
def add_series():
```

```
   return add(1, add(divide(1,2), divide(1,4)))
```

Let's repeat that lesson but use variables to hold the result of the 3 divides (e.g.  1 + 1/2 + 1/4 + 1/8):

1. create both `add` and `divide` functions that take two parameters and do the correct math operation on them.

2. create the function `add_series` which calls the `add` and `divide` functions to calculate the sum of 1 + 1/2 + 1/4 + 1/8.  However, you can now use variables to hold the results of each of the divisions (calls to the function `divide`).  It should make your final expression easy on the eyes.  Be sure to define the variables inside `add_series` and do not use global variables.

If you need more help, tag your question on piazza as with **py6**.  Be sure to comment out any errors that occurred during this lesson (or fix them).

**Before you go, you should know:**

- the main types of values in Python
- what a constant is
- what the purpose of `None` is
- what are the two boolean values in Python
- what do the operators `or`, `and`, `not` do in Python



all rights reserved
