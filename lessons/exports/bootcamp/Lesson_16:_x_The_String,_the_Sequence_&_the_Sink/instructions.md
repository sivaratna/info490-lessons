# **A 🐍 Coder's Journey**

**Lesson 16: The String API Exposed**

This lesson provides everything AND the kitchen sink of useful things you can do with sequences: (https://www.theidioms.com/everything-but-the-kitchen-sink/). 

There's a lot of code in this lesson.  It is vital that you run and understand each example.  In upcoming lessons and projects, knowing what you can do with both strings and lists will help your algorithm design.

**Strings**

We learned about the string type in an earlier lesson.  Since many data analysis involves text data, it's important to understand some of the operations that can be done on strings.

**Changing Case**

As we saw in a previous lesson, you can change the case of a string using the string methods `lower` and `upper`:

```
msg = "Hello Python"
```

```
# change to **lowercase**
```

```
print(msg.lower()) 
```

```
# change to **uppercase**    
```

```
print(msg.upper()) 
```

Changing case is usually a data processing step in the data science pipeline where the analysis wants to treat both cases the same (called case insensitive).

**Changing Type**

If a string represents a number, you can convert it to an actual number by using either the `int` or `float` functions:

```
print(int("121"), float("3.33"))
```

**String Concatenation**

```
# **add** to a string  
```

```
msg = "Hello Python"  
```

```
msg = " " + msg + ". " 
```

```
print(msg)
```

As we saw in a previous lesson, you can easily repeat a string value by multiplying it by a number (for easy string repetition):

```
print('H' * 5)
```

**String Cleaning**

There are several methods to clean strings.  

```
# **remove whitespace** at start and end
```

```
msg = "  This is a story " 
```

```
print(msg.strip())
```

The `strip` method also allows you to pass in a character to remove from the ends of a string.

```
msg = "I do love 'food'" 
```

```
print(msg.strip("'"))
```

You can also replace a character with another using the `replace` method:

```
# **replace substrings**
```

```
print(msg.replace('o', 'a'))
```

**String Splitting**

Many times data is encoded as a single string with separators.  You can use the split method to pull out the different components and return a list of the different elements:

```
msg = "Apples are red. Bob likes apples."
```

```
parts = msg.split('.')
```

```
print(parts)
```

In the above example, a period is used to split msg into different parts. A common data format is separating data using a comma (csv - comma separated values):

```
csv = "12,11,10,0"
```

```
parts = csv.split(',')
```

```
print(parts)
```

Note that the character used to split the string is not part of any of the returned strings.  Also if a string **ends with the character** that is being used to split the string, the returned array has an empty string as its last element. You may notice that the empty string was returned in the first split example in the last component.  This is because the string ended (terminated) with the character that was being used to split on.  

Be sure to understand the above and this example:

```
csv = "12,11,10,0,"
```

```
parts = csv.split(',')
```

```
print(parts)
```

If you don't pass in a parameter to `split`, it will use the space, newline, and tabs (called whitespace) as the character (or token) to split on.  You can easily get the words from a string this way:

```
msg = "Apples are red. Bob likes apples."
```

```
parts = msg.split()
```

```
print(parts)
```

**String Joining**

You can also join string elements together to form a single string

```
ssv = ';'.join(parts)
```

```
print(ssv)
```

**Counting Occurrences**

```
msg = "the quick fox ate the hot dog"
```

```
print(msg.count('t'))
```

```
print(msg.count('the'))
```

**Finding Values**

As we have seen, the index method will return the index at which a value is found in a sequence.  However, if it is not found, it will generate an error (called a runtime error).  Strings have an additional method called find that returns -1 if the requested value is not found:

```
name = "Informatics"
```

```
print(name.index('c'))
```

```
print(name.find('d'))
```

**Functions on lists and iterators**

Python provides a wide variety of functions and operations that work on lists (and other types that can be iterated -- called iterators): 

```
l = [9,12,8,2,2,2]
```

```
**# returns the length of l**
```

```
size = len(l)  
```

```
# **count** items == 2 in the list
```

```
print(l.count(2))
```

```
# **reverse the order**
```

```
# the function reversed returns an iterator
```

```
# you can convert this iterator by turning it
```

```
# into a list
```

```
r1 = list(reversed(l))
```

```
print(r1)
```

```
# **reverse** the elements, but not create a new list
```

```
l = [9,12,8,2,2,2]
```

```
l.reverse()
```

```
print(l)
```

**List/String membership**

We already saw the in operator for testing membership

```
print(9 in [1,2,9])
```

```
print('a' in 'hello')
```

**Sequence 'Math'**

There are even some interesting things you can do with some arithmetic operators on sequences. Be sure to understand each example:

```
l1 = [1,2,3]
```

```
l2 = [4,5,6,7]
```

```
l3 = l1 + l2   # Concatenation 
```

```
print(l3)
```

```
print(**min**(l2), **max**(l2))
```

```
print(l1 * 3)
```

```
print(sum(l3))
```

The same operations even work on lists of strings:

```
l4 = ['a','b','c']
```

```
l5 = ['z', 'y']
```

```
l6 = l4 + l5
```

```
print(l6)
```

```
print(min(l4), max(l4))
```

```
print(l4 * 3)
```

Even though string concatenation is like adding strings, the `sum` function does not work on an array of strings.

**The String as a Sequence**

Since a string is a sequence of characters (and is a Sequence type), all the fun stuff you can do with lists can also be done with strings.  Part of this lesson is for you to explore that.

**Before you go, you should know:**

- what does join do
- what does split do
- using join and split how would you remove commas from a string
- what's the difference between find and index
- what's the difference between a function and a method
- what's a sequence

# **Lesson Assignment**

**Center Me**

Create a function **center_me** which takes a list of strings.  It then returns a new list of strings with each string padded with blanks such that if you printed the strings, it would be centered.  You **cannot** use the `center()` method on the string type.  You can assume the incoming argument will be a valid list (and not None).

Below is a screenshot of what the solution should look like:

```
a = ["I LIKE","Dogs","Black Dogs, Green Dogs","Bad Dogs, Mean Dogs","All kinds of Dogs","I like Dogs", "Dogs"]
```

```
for e in center_me(a):
```

```
  print(e)
```

 

Solving this one doesn't require tons of code.  The clue is for you to think about it and describe the process of solving it to a friend without using code -- just simple instructions (called pseudo code) of the steps required.  Try to think this through before reaching out to piazza for clues.  Part of becoming a software engineer is developing the critical thinking skills of building a solution and then translating that solution using what Python provides. 

If you need more help, tag your question on piazza as with **py16**. 


All rights reserved.

