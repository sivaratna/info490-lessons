# **A 🐍 Coder's Journey**

**An Introduction to Repl.it**

 

Welcome to INFO 490! This is the beginning of your journey to learning (and mastering) Python.  Python is a very common programming language for data science.  Although we don't expect you to have much programming experience,  you will need to practice programming daily to be fluent.  These lessons (which you can repeat) are designed to teach you the basics of Python and programming in small and manageable pieces.

Solving problems with Python (i.e. programming) requires a text editor where you write commands (code, statements) for the Python program to execute.  

You have many choices for what editor to use to write your code.  We will use this online editor called repl.it.  At some point you will want (but we will not require it) to install Python and an editor on your own computer so you can program without the limitations.  Some editors also integrate tools to help the development process (called an IDE for integrated development environment).

The editor you see on this page is **repl.it**'s online editor.  REPL stands for **R**ead, **E**valuate, **P**rint, **L**oop. You can pronounce repl.it as 'rep lit'.  It is our interactive environment.  It is an editor with a Python interpreter. 

**The Console** 

The **bottom** left area is called the console (not to comfort someone, nor a gaming system, nor your grandfather's tv cabinet).  Below is a snapshot of a **console window**: 

 

The console is our window (or display) into the Python interpreter.  Go ahead and type the following in the console window:

```
2 + 3
```

After you hit the return (or enter key), the interpreter **R**eads the statement, the statement is **E**valuated and the result is **P**rinted and the process starts over again **(**aka a **L**oop). At the very least, we have a simple calculator.  Go ahead and type the following (followed by enter/return) in the console window:

```
5 * 2+3
```

The answer, 13, might seem strange if you were expecting 25.  The interpreter has rules on how to evaluate mathematical expressions. To get 25, type the following in the console:

```
5 * (2+3)
```

Programming languages put priority on evaluating things in parenthesis first.  We'll have more to say about operators (symbols used to perform operations like '+' for addition,  '*' for multiplication, '/' for division) and operator precedence (the order in which they are evaluated) in later lessons.

The console is a great place to experiment. 

**The Editor**

The **top** left area is where you write code (the editor) that you want to save.  This is where you will write your assignments.  We could write everything in the console window, but your work would not be saved.  

The code you write in the editor will be run when you hit the 'run' button:

 

Go ahead and type the following in the _editor_ window:

```
1+2+3+4+5
```

Hit run ► key to see the result printed in the console.  

**Unlocking Lessons**

In order to do the next lesson, you MUST pass the tests for the current lesson.  The need for this dependency is important and at some point, you may complain loudly about it.  For most classrooms, the lessons will force you to pass the tests before moving to the next lesson.  It serves no purpose to move to another lesson when you haven't mastered the current one.  Some lessons build on each other and its vital that you know how to solve each of the problems.

# Before you go you should know the following:

- The main components of the replit IDE
- The difference between the replit editor and the console
- The words that make up REPL

# Lesson Assignment

You can move to the next lesson by hitting the submit button in the upper right corner:

 

You can tag any question you have on Piazza with **py00**.


all rights reserved
