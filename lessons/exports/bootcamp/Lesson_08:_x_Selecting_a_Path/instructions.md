# **A 🐍 Coder's Journey**

**Lesson 8:  Selecting a path**  
There comes a point in every journey where a decision or rule is made based on some condition:  "I will eat only if I am hungry", "I'll stop reading this journal if I get too tired", "I won't play unless all my lessons are done", "I get a 10% discount on all blue items."


Computer languages provide a construct to help you frame such situations -- where you want to run code but only under certain conditions.  Those constructs are called **if** statements or **if/then/else** statements (also called branching, selection, or conditionals). It allows the computer to execute code based on a condition.

Here's a simple example that encapsulates the logic "I will eat IF I am hungry":

```
def will_eat(hungry):
```

```
**1**.  **if** hungry == True:
```

```
**2**.    return "I will eat"
```

```
**3**.  **else**:
```

```
**4**.    return"I will NOT eat"
```

```
print(will_eat(True))
```

The `if` statement is made up of the following:

- **`if`** is a keyword for a start of the conditional (line 1)
- the statement after the `if` and before the '`:`' is what is evaluated to determine which branch to execute (called a _boolean expression_)
- a boolean expression is used to determine which branch to execute.  In this case you are answering the question is the variable `hungry` equal (the double `==` sign) to `True`? 
- `:` at the end of line 1 starts the 'if true' case
- **`else`** is a keyword for the start of the false case (followed by a '`:`')
- line 4 is the 'else' block of code that will be executed when hungry is not `True`

**Boolean Expressions**

The main part of the `if` construct is the boolean expression that is used to determine which branch gets executed.  A boolean expression can be anything that gets evaluated to be either True or False.  If the boolean expression evaluates to `True` the immediate block of code is run, otherwise, it executes the `else` block.

In most cases, the boolean expression usually involves a test for equality (double equals: == ):

```
if color == 'red':
```

```
  # do something
```

Mathematical inequalities also generate boolean values:

```
if value <= 10.5:
```

```
  # do something
```

When you test against a boolean value:

```
`if is_legal == True:`
```

```
   `# do something`
```

You can drop the comparison altogether:

```
`if is_legal:`
```

```
   `# do something`
```

Also, when you compare a value to `None`, it is recommended (it is Pythonic to do so) that you use the `is` operator:

```
if parameter **is** None:
   # do something
```

Do NOT use the `**is**` operator for testing other values (numbers, strings, booleans, etc).  We will learn the technical reasons why in another lesson.

**Equality is NOT assignment:** 

In most programming languages the symbol for equality (' ==') is NOT the same as the symbol used for assignment ('='). Perhaps the single biggest bug (an error) in programming is caused by confusing assignment (a single =) with equality (a double ==).  However, Python will catch many of these problems for you.

> _**Coder's Log:** For many languages, the compiler will gladly accept an '=' operator even though you meant the '==' operator. This usually results in long hours of debugging and hair pulling.  Python is one of the few languages that helps prevent this 'bug'._

We will explore more complex boolean expressions in the next lesson.

**Indentation Matters (again)**

Also take great care to note how indentation works.  This is the first time we have seen two levels of indentation.  The first one for the function (also see the lesson on functions) and the second for each selection statement.  It's important to always remember to indent the code when the line ends with a '`:`'.  The colon usually marks the beginning of a new indented block.  Typically in Python you indent 4 spaces for each new level.  The examples will always use 2 spaces for compact viewing.

Change the parameter from `True` to `False` on the call to `will_eat` and run again.  Be sure you understand how the **if/else** statement works.  

```
# call the function will_eat
```

```
print(will_eat(True))  
```

**Early Returns**

One thing that might not be clear is that once a function returns, no more code will be executed.  Take a look at the following and try to figure out what will be printed.

```
def will_eat(hungry):
```

```
  
```

```
  if hungry == True:
```

```
    return "I will eat"
```

```
  else:
```

```
    return "I will NOT eat"
```

```
  
```

```
  a = 0
```

```
  a = a + 1
```

```
  print("a", a)
```

Run this code and make sure you understand the output:

```
print(will_eat(True))
```

**Dead Code**

In the above example the code that increments the variable `**a**` will never be reached since every possible branch returns.  The code that is never reached is called dead code.  This is one of the reasons why we try to write functions with a single exit point (that is each function should have a single return statement).  We will discuss this idea in the next lesson.

**Discounted Example**

Here's another code example of selection that encapsulates the logic of offering a 50% discount on 'red' items:

```
def calculate_discount(color):
  discount = None
  **if** color == 'red':
    discount = 0.50  
```

```
  **else:**
    discount = 0    
  return discount
```

Sometimes you will see a selection visualized as follows where the diamond is the boolean condition and the ovals are the code blocks to be executed based on which branch is taken.

 

**Optional else blocks**

The `else` block is optional. Sometimes you can construct your if/else code such that the default case can be left off: 

```
def calculate_discount(color):
  discount = 0
  **if** color == 'red':
    discount = 0.50 
  return discount
```

This only works if the default case is simple and straightforward.

**Multiple If's**

You can also have several '`if`' statements.  For example, using a more complex discounting rules: assume the store owner is giving a 50% discount for green items; a 30% discount for yellow; 20% for white items:

```
def calculate_discount(color):
1.  discount = 0
2.  **if** color == 'green':
3.    discount = 0.50 
4.  **elif** color == 'yellow': 
5     discount = 0.30    
6.  **elif** color == 'white':  
```

```
7.    discount = 0.20 
8.  return discount
```

On lines 2,4 and 6, a separate `if` statement and boolean expression are evaluated to determine which code block will run.  

You can chain as many 'else if' statements as needed. Note that in Python 'else if' is a single keyword `**elif**`**** (pronounced ell-if).  The above code also demonstrates how to write a function with only a **single exit point**.  

If you had common code to run regardless of which branch is selected you could insert it before the first branch or just above the return statement.  Be sure to understand how `calculate_discount` works and the value of having a single return statement.

**One Branch**

The important thing to remember is that ONLY one branch gets executed.  Take a look at this problem:

```
def only_one(age):
```

```
  if (age < 8):
    print("Less than 8")
  elif (age < 10):
    print("Less than 10")
  else:
    print("No idea")
```

```
only_one(7)
```

Even though 7 (the value the parameter `age` will receive) is less than 10, the first boolean expression that evaluates to true is the branch that gets executed.  Be sure to TYPE in this code, determine what the output will be and then run the code.

# **Lesson Assignment**

**Part 1.** Write a function `will_order_dessert`

• has a single parameter: `kind` (as a string), 

• returns `True` for any of these conditions (otherwise return `False`):

- the kind of dessert is "Ice Cream"
- the kind of dessert is "Pumpkin Pie"

You can ONLY use `if` statements (no `else` or `elif`)

You **cannot** use the keywords: `and,` `or` (that's in the next lesson)

**Part 2.** Write a function `will_order_dessert_again`

• has two parameters: `kind` (as a string), `price` (a number, assumed to be in dollars) 

• returns `True` for any of these conditions (otherwise return False):

- the kind of dessert is "Ice Cream"
- the kind of dessert is "Pumpkin Pie"
- the price is less than or equal to 5 dollars

You MUST use at least one `else/elif` 

You **cannot** use the keywords: `and,` `or` (that's in the next lesson)

If you need more help, tag your question on piazza as with **py8**

**Before you go, you should know:**

- the syntax of an `if` statement
- the syntax of an `if/else` statement
- the syntax of an i`f/elseif/else` statement
- the operator used to compare values to `None`


all rights reserved.

Notes: The restriction on not using the `and`, `or` operators is done to help you discover how you can build logic without them.

