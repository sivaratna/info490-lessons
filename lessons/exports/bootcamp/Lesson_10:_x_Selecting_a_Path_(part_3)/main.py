def example1(other):
  a = 10
  if (a < other):
    print('issue')

def example2(other):
    a = 'red'
    if a == other:
        return True
    return False

print(example2(None) == False)