# **A 🐍 Coder's Journey**

**Lesson 10:  Selecting a path (Part 3)**

This lesson is designed for more practice working with selection statements.


**Cleaning Up:  step by step**

The following shows a series of code changes (also called refactoring) that attempts to simplify code without changing its behavior.  Refactoring is an important topic in software engineering and we will get back to it in a later lesson.


Assume the following situation:  

- a discount is given for all red items less than 3 dollars or if the item's color is 'purple'  or if its price is greater than 30 dollars
- the parameters `color` and `price` could be `None`

```
def grant_discount(color, price):
```

```
  # case A
```

```
  if color == 'red':
```

```
    if price is not None:
```

```
      if price < 3:
```

```
        return True
```

```
  # case B
```

```
  if color == 'purple':
```

```
    return True
```

```
  # case C
```

```
  if price is not None:
```

```
    if price > 30:
```

```
      return True
```

```

  return False
```

**Guarded None**

Every variable can hold the value `None`.  In most situations, using a variable whose value is `None` will cause an error (go ahead and run `example1`).  Sometimes you can design your code such that you won't have to worry about this situation.  However, for this example we need to 'guard' the conditions with another selection statement. How to guard code properly is a topic for software engineering; but doing so with a selection statement is an acceptable solution for us.

Another common practice is to assign default values to use for the variables whose values are `None`.  It makes for a more readable solution:

```
def grant_discount(color, price):
```

```
  
```

```
  if color is None:
```

```
     color = ''  
```

```
  if price is None:
```

```
     price = 0
```

```
  # case A
```

```
  if color == 'red':
```

```
    if price < 3:
```

```
      return True
```

```
  # case B
```

```
  if color == 'purple':
```

```
    return True
```

```
  # case C
```

```
  if price > 30:
```

```
    return True
```

```

  return False
```

Since we can use equality check against `None`, you don't need to assign a default value to `color`, a string (run `example2`) but it's good to be consistent.

**Multiple Exits Issue**

The next issue is each function should (not always reasonably possible) have a single exit point.  Having only a single exit point allows you to add common code that would apply to all situations.  Without doing so, you would have to repeat the same code in each and every place a `return` is found.

```
def grant_discount(color, price):
```

```
  
```

```
  if color is None:
```

```
     color = ''  
```

```
  if price is None:
```

```
     price = 0
```

```
  # case A
```

```
  valid_a = color == 'red' and price < 3
```

```
  
```

```
  # case B
```

```
  valid_b = color == 'purple'
```

```
  # case C
```

```
  valid_c = price > 30
```

```

  return valid_a or valid_b or valid_c
```

Be sure you understand why that works.  Notice the use of variables to hold the results of the different situations.


When a function returns a value (in Python all functions return values), we strive for **one exit point**, if the logic isn't too complex we can simplify it even more:

```
def grant_another_discount(c, p):
```

```
  return p is not None and p > 30 or c == 'red'
```

**Nested Boolean Expressions** (a closer look)

The boolean expression used inside `if/elif` statements gets evaluated into a boolean value (either `True` or `False`).  We can build complex logic using nested `if/elif/else` statements.  As we mentioned, a _nested_ if statement is simply another `if` statement inside another.  It has a new level of indention.

We can also set up complex logic with nested `if/else` constructs (remember only ONE branch in each `if/elif/else` statement will be executed):

```
def calculate_tax(age, income):
  tax = 0
  **if** age < 17:
    # only get executed if age < 17
    tax = 0.05
  **elif** age < 21:
    # the first test failed so age >= 17
    # but also < 21
    **if** income > 20000:
       # 17 <= age < 21 AND income >  20000
       tax = 0.15
    **else:**
       # 17 <= age < 21 AND income <= 20000
       tax = 0.10
  **else:**
     # age >= 21
     tax = 0.20
  return tax
```

Be sure to read (and understand all of that logic) before continuing. The statement

```
    **`if`**` income > 20000:`
```

begins a new `if` statement inside its parent `(if age < 21)`

**Again, Indentation Matters**

As a reminder, Python uses space to determine which code belongs to which selection statement.  All the code for the function `calculate_tax` is indented the same amount.  Code belonging to the  '`if`' statement is indented more than the parent element (or block).  Take a close look at spacing and indentation -- it's a critical detail to get correct.

# **Lesson Assignment**

**Part 1: Dessert Time AGAIN**

Write a function `**will_order_dessert**` that takes in two parameters: `kind` (as a string), `price` (a number, assumed to be in dollars) in that order and returns `True` for any of these conditions (otherwise return `False`):

- the kind of dessert is "Ice Cream"
- the kind of dessert is "Pumpkin Pie"
- the price is less than or equal to 3 dollars

**Conditions:**

• you **cannot** use the keywords: `and`, `or` 

• either parameter can be `None`   

• treat `None` as being 0 for `price`    

• treat `None` as the empty string for `kind`; an empty string has no characters

Write a function `**will_order_dessert_again**` with the same signature as `**will_order_dessert**`.  A function signature means its inputs and outputs. Two functions have the same signature if their input types and order are the same and their output types are the same.  This function returns True ONLY if the following conditions hold:

- the kind of dessert is "Ice Cream" **and** the price is less than 5 dollars

**Conditions:**

• you **cannot** use the keywords: `and`, `or` 

• either parameter can be `None`   

• treat `None` as being 0 for `price`    

• treat `None` as the empty string for `kind`; an empty string has no characters

```
print(will_order_dessert(None, None))        # ==> True
print(will_order_dessert("Ice Cream", None)) # ==> True
print(will_order_dessert(None, 3.85))        # ==> False
```

**Hints (for just desserts):**

- try to break up your logic into separate parts and assign default values instead of writing guarded if statements.
- However, **you are NOT allowed** to use the boolean operators `not,` `and,` `or`.  

**Part 2: Tax Season**

Create two functions: `calculate_simple_rate` and `calculate_tax_rate` 

- Both functions have two parameters (age and income).  
- Both will return a number representing the tax rate based on the two parameters.
- You can assume the parameters will only be numbers (no need to worry about `None`)
- **You ARE allowed** to use the boolean operators `not,` `and, or.`

1. Finish the implementation for the function `calculate_simple_rate` 

- anyone under 16 is taxed 5% (0.05)
- anyone 65 or over is taxed 2%
- everyone else is taxed based on income
- less than $10,000, tax rate is 10%
- 10,000 or more but less than $50,000, tax rate is 15%
- 50,000 or more but less than $100,000 tax rate is 20%
- $100,000 and over is taxed at 25%

```
def calculate_simple_rate(age, income):
```

```
    tax_rate = 0

```

```

    # your logic


    # this is the only place a return
    # should happen
    return tax_rate
```

  

2. Finish the implementation `calculate_tax_rate` which returns a number based on the following rules:

- anyone under 16 is taxed 5%
- anyone between the ages of 16 and 19 (inclusive) is taxed at 10%
- anyone over 19 is taxed based on income:
-      less than 10,000:                   15%
-      between 10,000 and 20,000: 20%  [inclusive]
-      over 20,000:                          25%
- anyone making 100,000 or more is taxed an additional 3% regardless of age.  So a 15 yr old making 120,000 would be taxed 8% (5% + 3%).

```
def calculate_tax_rate(age, income): 
    
```

```
    tax_rate = 0

```

```

    # your logic


    # this is the only place a return
    # should happen
    return tax_rate
```

If you need more help, tag your question on piazza as with **py10**

**Before you go, you should know:**

- how to use `or`, `and`, `not` in boolean expressions
- how to write a function with a single exit point (one `return` statement)
- how you can simulate the `or` operator and the `and` operator using `if` statements
- how to guard your variables


All rights reserved
