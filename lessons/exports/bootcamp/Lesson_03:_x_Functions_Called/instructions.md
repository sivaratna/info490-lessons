# **A 🐍 Coder's Journey:** 

**Introduction to Functions (continued)** 

 

As we just saw, we can print the result of calling our `add` function to the console:

```
print("1+2=", add(1,2))
```

> Note that you should type each line, guess it's output, and run the code.

The result of calling `add(1,2)` is being passed as an argument to the `print` function.  The Python interpreter will first evaluate `add(1,2)` to produce 3.  Then this value will be used as the second argument to the function `print`.  We can make use of this idea by passing the result of `add(1,2)` into another call of `add`:

```
print("1+2+3=", add(1, add(2,3)))
```

Let's review what is happening: `add(2,3)` is evaluated (which returns a 5) and that result is then passed to another invocation of `add`.  This time it is being called with the arguments of 1 and 5.  Note: matching each opening '`(`' with a closing '`)`' can be tedious!  The expression to add 1 + (1 ÷ 2) would be 

```
print("1.5:", add(1, divide(1,2)))
```

**Parameter Evaluation Order**

By adding a `print` statement in our `add` function we can reveal how Python evaluates the arguments. 

```
def add(a,b):
```

```
  print(a, b)
```

```
  return a+b
```

```
print(add(1,2), add(9,10))
```

Be sure you understand what you see when you run this code.  Python will evaluate the arguments to a function from left to right.  

# **Lesson Assignment:**

Your challenge: write a single expression to add 1 + 1/2 + 1/4 + 1/8  (hint: **you should get 1.875**). 


1. re-create the `divide(a,b)` function (you did this last lesson) that takes in two positional arguments.  It returns the first argument divided by the second argument.


2. create a function named `add_series` that will use both the `add` and `divide` functions to evaluate the expression 1 + 1/2 + 1/4 + 1/8.  The function `add_series` does not have any parameters; it will essentially call your other functions.  It must also **return** the result of the expression.  It can not use any math operators (it must call the other functions).

3. your function `add_series` will be tested to produce the result 1.875 

**NOTE To those with programming experience:**

If you have experience with programming, you must still solve each of the problems given only the material we have presented.  For example, if you know about variables (which we cover very soon), you **cannot use them** in this lesson.

**HINTS:**

• Don't make it too complicated:  you have 3 adds and 3 divides for the series

• If you are having problems, first do 1 + 1/2 + 1/4 (1.75).  

• If after running your code you see **SyntaxError: invalid syntax**, most likely you need to match your parenthesis.  If you see this, you don't have balanced parenthesis or you might be missing a comma.  

Although the interpreter will consider it to be a single statement, you can write one expression to span multiple lines in the editor. This can help visually line up ')' at the cost of more space.  For example (1 + 1/2 + 1 +  4):

```
def add_series():
```

```
  return add(1, 
             add(divide(1,2),
                 add(1,4)
                )
             )

```

```
print("test it", test_series())
```

There's got to be a better way (several actually). Thankfully there's another chapter. We will simplify this example of computing an infinite series (do you see the pattern?).

> _You might find all of this silly, as we saw in the previous chapter Python already has operators (e.g. + for addition; * for multiplication, etc) for math functions.  We could just do 1 + 1/2 + 1/4 + 1/8 in the console window!  However, we can build functions that go well beyond simple math.  It's just easy to demonstrate complex programming concepts using a familiar tool._

If you need more help, tag your question on piazza as with **py3** 

# Before you go, you should know:

- How Python evaluates the order of parameters to functions.


all rights reserved
