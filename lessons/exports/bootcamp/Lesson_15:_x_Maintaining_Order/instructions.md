# **A 🐍 Coder's Journey**

**Lesson 15: Maintaining Order**

One of the more discussed topics in computer science is sorting.  Thankfully, we won't have to implement a sorting algorithm (although doing so can be fun).  In Python, we can easily sort a list of values by using the built-in `sorted` function.  This function is available to any Python program (it's global in scope).  The function `sorted` takes a list (actually any iterable type) and returns the contents sorted (leaving the input unchanged).

```
print("list",   sorted( [3,2,1] ))
```

```
print("tuple",  sorted( (3,2,1) ))
```

```
print("string", sorted( "info490" ))
```

```
print("range",  sorted( range(10, 0, -1)))
```

**Sorting List Items**

In addition to using the function `sorted`, the list type also has a `sort` **method** that does the same thing but modifies the list it's called on (called an in-place sort).

```
my_list = [3,2,1]
```

```
my_list.sort()
```

```
print(my_list)
```

A few things to notice.  The function `sorted` returns a list regardless of what you pass into it.  The parameter is NOT changed (see previous lesson for why).  The method `sort` on the list type actually modifies the list (it's called an **_in-line_** or **_in-place_** sort). 

> **Coder's Log:** Remember a method is a function that is attached to a specific type (also called a class).  We will learn more about this in another class. `print(type(list))`

**Sorting Strings**

A curious thing happens when you ask `sorted` to sort strings:

```
print(sorted(["2", "10"]))
```

```
print(sorted("Python is fun"))
```

```
print(sorted(['Python', 'Apple']))
```

```
print(sorted([True, False]))
```

So why does 'P' come before 'h', and '10' come before '2' ?

The answer lies in how Python will interpret the values.  Each value in Python has a numeric value that can be used for sorting.  For non number types (like strings and booleans), Python uses rules to determine sorting order.  Specifically for the string type, it looks at each character;  when it compares characters, it uses the unicode value (ascii for older versions of Python) for that character.  You can see this value by using the function **ord**:

```
print(ord('P'), ord('h'))
```

> _**Coder's Log:** At some point you will be creating your own types (called classes) in Python.  If there will ever be a need to compare or sort values of that type, you would also have to tell Python how to compare two items of this custom type._

For the '2' vs the '10' compare.  The character '1' comes before '2' and the comparison stops (even if it was '20' vs '10').

**Sorting in Reverse**

You can easily sort a list by setting the `reverse` parameter to `True` (the reverse parameter is called a named argument because we name the argument in the function call (i.e. `reverse=True`).  

```
jenny = [8,7,6,5,3,0,9]
```

```
jenny.sort(reverse=True)
```

```
print(jenny)
```

We will learn how to create a function that can be called with a named argument.

**Custom Sorting**

In almost all situations,  the default sorted function will not be useful -- your data will be too complex to sort on a single attribute.  Even in a deck of cards, you might want to sort the cards such that ♥︎ comes before ♣︎.  Custom sorting allows you to sort based on a criteria other than the 'numeric' value of the items in the list.  You will learn about this as well.

# **Lesson Assignment:**

Create a function `min_mid_max` which takes a list as its formal parameter and returns a new list with the following characteristics:

- element 0 contains the minimum value
- element 1 contains the middle value  (not the median -- which would require an average of two elements if the list had an even number of elements).  If the length of a list is even (e.g. [1,2,3,4]) the 'middle' value will be the right value of the two possible choices (e.g. 3 rather than 2).  This rule is applied **after** the list is sorted.
- element 2 contains the maximum value

Notes:

- You can only use Python syntax shown so far (even if you know more)
- You **cannot** use any math library (we have yet to see how to do this)
- You **should use** the function **`sorted`**. Remember the function `sorted` does not change its input, it returns a new list.
- You should only need to call `sorted` one time.
- You can use the function **`int`** (another built in function) to help determine the index of the middle value.  This function takes a floating point (or any kind of number) and converts it into an integer (e.g. `print(int(4.32)`).  This will allow you to treat lists with an even number of elements the same as a list with an odd number of elements.
- If `min_mid_max` is called with an empty list, return an empty list.
- If `min_mid_max` is called with 1 item, that item is the min, mid, and the max
- If `min_mid_max` is called with 2 items, the mid and the max should be the same
- No need to worry about the incoming list being the value `None`

For example:

```
# calling min_mid_max with an even number of items
print(min_mid_max([4,2,9,1]))


# would produce the following output
[1, 4, 9]
```

Tip:  See if you can write your code without worrying about the size of the incoming list other than it being empty.  You might want to try a few ways first.

If you need more help, tag your question on piazza as with **py15**

**Before you go, you should know:**

- how to sort lists of things (both forward and reverse)
- does `sorted` modify its parameter
- the difference between `list.sort` and `sorted(some_list)`


all rights reserved 
