# **A 🐍 Coder's Journey**

**Lesson 5: Variables, Part II**


**Visiting variables, again**

As we learned in the last lesson, a programming _variable_ gives us a way to access (or read) a value from memory as well as set (or write) a value to memory. It allows us to _vary the value_ of a memory location. You can think of a variable as a labeled container that holds values.  We learned that functions are containers of code; you can also think of variables as containers of values.  The next lesson will discuss the different types of values you can assign to a variable.


**Two parts to making a variable**

When you create a variable, it is made up of two separate parts: the _declaration_ and the _definition_ (the same words we used for functions). The declaration is just the first part:

```
result 
```

In this case, **result**, is the name of the variable.  All variables in Python must begin with a letter.  They can also contain underscores and numbers.  However, in Python declaring a variable without giving it a value is not allowed. In Python you MUST _define_ the value of the variable when you declare it (this is not true in other languages).  

The second part gives the variable an initial value.  When you give the variable an initial value at declaration time, it's called a _definition_.

```
result = 2 
# = 2 defines the variable result with a value of 2
```

Although the '`=`' sign is used, it is best to say "the variable `result` **is assigned** the value 2."  Many people will say "result equals 2".  That's technically correct, but as we will see shortly, _equality_ (testing that two memory locations hold the same value) is not the same as assignment;  the assignment operator uses the single '`=`' symbol.  Equality uses two `=` symbols (e.g. `==` ).


You can also assign mathematical expressions to variables:

```
p = 2*2   # p holds the value 4
p = p * 2 # p is assigned its value * 2
print(p)
```

You can have variables get their values from other variables:

```
p = 2*2*2
q = p     # assign q to be the same value as p
```

```
r = q + 1 # increment q by 1; assign the result to r
```

```
print("p, q, r", p, q, r)
```

By using a variable, you can re-use the value multiple times rather than recalculating it:

```
result = 22/7
print("this number is curious", **result**)
print(**result**, "seems familiar")
```

We use a variable to access a value multiple times without having to reset or re-calculate it.

> _You should be copying these code blocks and running them.  To get the most practice, actually type all the code (rather than copy&paste).  Copy & Paste is easy, but doing so will short change your learning._

**Using a variable without declaring it**

Two possible questions to ask is what happens if you don't declare or define a variable but just use it?  Let's find out. Go ahead type and run the following: 


```
print("The value of me is", me)
```

Yikes.  You should see '`NameError: name 'me' is not defined`

Basically the Python interpreter is telling us that it doesn't know anything about a variable named '`me`'.  It is unable to reference (or find) something named '`me`'.


**Using a variable without defining it**

Go ahead type and run the following: 

```
me
print("The value of me is", me)
```

We get the same error.  If you want to declare a variable without any specific value, you can use the reserved keyword `None`

```
me = None
```

```
print("The value of me is", me)
```

> _**Coder's Log:**  in JavaScript you can actually declare a variable AFTER it's first use.  Most programming languages do not allow this.  JavaScript **hoists** its declarations to the 'top'.  Relying on this behavior will cause you grief.  Always declare your variables before you use them._

**Variable Scope (a reprise)**

In Python there are two main kinds of variables, local and global.  Both provide the ability to remember a value.  Variables declared inside a function (also called function variables) are 'local' to that function. Local variables are only 'visible' inside the function.  You cannot read (or write) to local variables outside of the function. Global variables are variables **declared outside** of any function and are available (i.e. visible) inside your program including other functions.

> **Coder's Log:** Right now our 'programs' consist of a single module (or file).  We can't see the files yet, but when you type code into the editor it is actually saving it into a file.  Any variable declared/defined in a file/module is visible ONLY to that module.  This will become clearer when we use multiple files (i.e. modules) in Python.

The concept of variable visibility as well as function visibility (what parts of your program can access the variable or the function) is called **scope**.

Type in the following code and understand what you see:

```
a = 10
```

```
def scope():
```

```
  a = 20
```

```
  print(a)
```

```
  return a
```

```
print(a)
```

```
scope()
```

```
print(a)
```

You should see the following when you run the code.

**`10`**

**`20`**

**`10`**

 

> **Coder's Log:** Another word for this type of scope is called **static scope** or **lexical scope**.  The scope is determined statically (how you read the code) and not dynamically (when the code is run and the scope is determined by the order of functions/scopes).  Most languages do not have dynamic scope

Note that even though the function returns the value inside the variable `a`, that return doesn't affect the value of the variable `a`.

**Function Scope**

Parameters for functions are variables locally scoped to that function.  Be sure to type and run the following example.  Try to determine what will be printed.  Run the code and see if you are correct.

```
a = 10
```

```
def scope(a):
```

```
  print(a)
```

```
  a = 20
```

```
  print(a)
```

```
  return a
```

```
scope(30)
```

```
print(a)
```

The important thing to note is that the parameter named `a` is available to the entire function scope and it does NOT refer to the outer scoped variable with the same name.

For almost all of our lessons, your functions will not refer to variables outside the scope of the function.

**Global Scope**

Later, we will discuss the use of the keyword `global`.  This keyword allows you to tell the Python interpreter that you wish to modify an outer scoped variable (you don't need the keyword to read the value of an outer scoped variable). In this class, we will NEVER need to use this keyword.  In general we want to write code that modifies only locally scoped values.  Of course, there are situations when it will be useful to use the global keyword, but with good program design, we can avoid it.

# Before you go, you should know:

- the two parts that make up a variable
- equality vs assignment and the operators used for each
- the meaning of variable scope
- note how the lesson makes use of the difference between parameters and arguments (a concept previously taught and reinforced here)

# **Lesson Assignment:**

**Mad Lib Generator**

Perhaps you remember doing mad libs -- you basically select words (nouns, verbs, adjectives, etc) that will be placed in some text (that you do not get to see when you are selecting your words) to make a silly story.  This lesson will have you build a mad lib so it's important to do the instructions in order (avoid spoiling the fun). See www.madlibs.com for more examples.

We will create several functions to build and fill out a mad lib.

Step 1. create a function named `get_animal` that returns the name of an animal.  You can pick any animal to return.  Do this first (before reading any more). Once that is done, the following should work:

```
`print(get_animal())`
```

Step 2. create a function named `get_person` that returns the name of a person (or a type of person (e.g. grandma).  The following should work.

```
`print(get_person())`
```

Step 3. create a function named `get_game` that returns the name of a game that can be played.

```
`print(get_game())`
```

Be sure to finish the above functions before continuing -- otherwise you will have no fun with your madlib!

Later on we can expand these functions to return different items.  But for now, each of those functions returns the same value every time.

Step 4. Create a function named `mad_lib` that takes three parameters (named `animal`, `person`, `game`).  The function `mad_lib` will print out the following 3 sentences:

`A good vacation involves riding a <animal>
When you vacation with <person> it can be stressful
If you play <game> on your vacation, it will end early`

But with using the names of the variables (e.g. replace <animal> with the variable).  In order to match the output of the above, you need to use only three calls to the print function.  Also match the words, spacing, and punctuation.  The function need not return anything.

Once that is done, you should be able to call it:

```
mad_lib('ox', 'bob', 'checkers')
```

**Test Your Code.**

When everything is complete, you can now call your `mad_lib` function using as inputs the outputs of your three functions:

```
`mad_lib(get_animal(), get_person(), get_game())`
```

Hopefully it's mad fun. Your output should match the madlib above (other than the specific words) exactly.

**Notes:** Spelling and Capitalization counts.  Function names and variable names are case sensitive.  Hence `get_animal` and `get_Animal` are two different things.  Be sure to check your spelling and capitalization.  This is a very common mistake that's hard to track down sometimes (even though there is a misspelling, our minds will correct it for us -- very frustrating).

Also in Python it is a best practice to use underscores to separate different words for both function and variable names (i.e. `get_person`). Other languages use the CamelCase standard where you capitalize each new word (e.g. `getPerson`). 


Once you are done, you can submit this lesson.  If you don't pass, it's most likely that you didn't type it exactly as what is given above.

If you need more help, tag your question on piazza as with **py5**


all rights reserved

