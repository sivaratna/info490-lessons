# **A 🐍 Coder's Journey**

# **Lesson 17: Dictionaries**

_Just as a reminder, it's important (vital to your learning) that you not only read the lesson, but **run the code** in the editor as well.  Make sure you understand the results.  Be sure to add print statements for any variable whose value you want to inspect.  Every lesson is packed with information.  Be sure to read carefully and question your understanding._

As we have seen, in Python you can create lists like this:

```
fruit = ["apples", "pears", "bananas"]  
```

The problem with lists is that if you wanted to associate additional data with each of those values you would have to manage multiple lists. Consider building a shopping list to track what to buy and the quantity you needed AND you could only use Python lists, you would have to create two lists:

```
fruit = ["apples", "pears", "bananas"] 
```

```
count = [5, 2, 12]
```

However, these lists would need to be kept in sync or maintained in parallel.  If you add an item, sort the items, delete an item using the `fruit` list, you would have to ensure that the `count` list is updated accordingly.  To do that would require a lot of code and code "memory".  That is anytime you want to update or use the code, the developer would have to remember the implementation details to make sure both lists are kept in sync.  This problem (and many others) is solved by using a dictionary.

The dictionary (also called a _map_ or an _associative_ array) is another popular data type (i.e. data structure).  Dictionaries are mapped key value pairs.  Let's understand what that means.

The Python dictionary is much like a regular dictionary where the key is the word and the value would be the definition (the combination of the word and its definition is the key, value pair).  This association from key to value (word to definition) is called a mapping.  However, Python dictionaries can hold any type of data that can be associated with a key.

> **Coders Log**: A data structure is a container for data that organizes its data in a specific way in order to make creation, mutation, or access easy.  Python has the built in data structures list and dictionary, but you can create your own types as well.

**A New Way to Shop**

In Python, a dictionary is created using curly braces `{}` (rather than brackets `[]` that are used for lists):

```
shopping_list = {} 
```

```
# creates an empty dictionary
```

However, you can also create a filled dictionary by specifying name/value pairs.  For example, we can model our shopping list easier (and closer to how we think about shopping lists) using a dictionary.  Here's the same data from above using a dictionary:

```
shopping_list = {
```

```
"apples":  5,
```

```
"pears":   2,
```

```
"bananas": 12}
```

The dictionary (named `shopping_list`) is initialized with comma separated items.  Each item contains a string (called the key) followed by a colon and then the value.  For the list, all we had was a collection of values.  For the dictionary we have a collection of keys.  Each key is then mapped to a value.  The keys can be strings or numbers, the values can be any Python object (e.g. number, string, list, another dictionary, even a function).   The key must be something that has a string representation.  The value can be just about anything.

> **Coder's Log**:  the idea of naming a variable that hints towards a specific implementation is usually not a good idea.  In the above example, a programmer might assume that `shopping_list` is a list and not a dictionary.  Perhaps a better name would be shopping_map.  However, most people don't use maps to make grocery 'lists'.  So we will go with `shopping_list`.  When you name your variables, use words (usually nouns) that describe what they hold.

**Dictionary Access**

You can read from a dictionary using the bracket syntax (just like lists and arrays).  In the example below, we use the key "`apples`" to get the value stored in the dictionary for that key.

```
`apples_to_buy = shopping_list["apples"]`
```

```
`print(apples_to_buy)`
```

You can write to a dictionary (and add items) as well:

```
`shopping_list["apples"]  = 3  # update`
```

```
`shopping_list["peaches"] = 3  # new item`
```

Notice that accessing a dictionary looks **exactly** like accessing an array or list; however, the indices can be strings as well as numbers.  This is an important point.  If you saw the following:

`print(m[0])`

There's no way to look at it and know whether it's a list or a map.

**Common Operations**

Dictionaries also support both length and iteration operations:

```
**# length**
```

```
print(len(shopping_list))
```

```
**# iteration**
```

```
for k in shopping_list:
```

```
  print(k, shopping_list[k])
```

**Getting the parts of a dictionary.**

The dictionary has several _methods_ that you can use to access its keys and values or the key-value pairs:

```
print(shopping_list.**keys()**)   
```

```
print(shopping_list.**values()**) 
```

> **Coder's Log:**  if you read the above note very carefully you may have noticed we used the word **method** to describe a function.  The word method is a function that is owned by an object.  The function `len()` is NOT a method since you can use it on many types of values (e.g. both lists and strings).  The method `items()` belongs to the class dictionary. Any object of type dictionary can use the method `items()`

You can also get the key, value mapping (as a list of tuples) by using the `items` method:

```
shopping_list = {
```

```
"apples":5, "pears":2, "bananas":12
```

```
}
```

```
print(shopping_list**.items()**) 
```

Additionally, since `items()` returns an iterator over the tuples, you can access the tuple or the elements of the tuples.  As a reminder, the iterator in Python is just any type whose contents can be sequenced or looped through.  In this case the first item in the tuple is the key and the second item is the value:

```
for t in shopping_list.items():
```

```
  print(type(t), t[0], t[1])
```

```
# unraveling the tuple elements
```

```
for (k,v) in shopping_list.**items**():
```

```
  print(k,v)
```

Note how the iteration returns two items for each item in the `shopping_list` since each item in the list is a tuple with two values. 

The following figure shows an example dictionary and what the methods items(), keys(), and values() returns:

 

However, the dictionary methods `keys()`, `values()`, `items()` **do not return lists** that can be indexed.  Instead, each of these methods returns views of the dictionary that can only be iterated on (e.g looped over):

```
k = shopping_list.keys()
```

```
v = shopping_list.values()
```

```
i = shopping_list.items()
```

```
print(k[0], v[0], i[0]) # ERROR
```

If you do need to index directly into these views, you can convert each to list and then index:

```
print(list(k)[0], list(v)[0], list(i)[0])
```

**No Order**

Unlike lists (or string), dictionaries are **not ordered collections**.  They are not considered a sequence type.  If you ask for `keys`, `values`, `items`, there is no promise in which they will be delivered.  Do not assume that they are ordered.

> **Coder's Log:**  Since Python 3.6, Dictionaries do keep their insertion order, however, it's usually not a good idea to rely on the behavior -- especially if your code may be run using a different version of  Python.

**Dictionary Key Membership**

Dictionaries also support the membership operator (`**in)**` to test if a key is in a dictionary:

```
print("apples" **in** shopping_list.keys())
```

You can also use the get method that allows you to return a default value if the key is not in the dictionary:

```
value  = shopping_list.get("applesauce", 10)
```

In this case, if "applesauce" is not in the `shopping_list`, the value 10 will be returned.

**A Dictionary for Counting**
One of the more useful applications of a dictionary is to count the number of times a certain value (really the key) occurs. A simple example would be to get word counts for a paragraph (or longer) of text.  Here each word is a key into the dictionary and the value is the count.  For example, if we had the sentence "the quick brown fox jumped over the dog", a dictionary could be used to count the number of times a word occurs in the sentence.  In this case, the dictionary would end up looking like:
`{'the': 2, 'quick': 1, 'brown': 1, 'fox': 1, 'jumped': 1, 'over': 1, 'dog': 1}`
The dictionary uses the word as the key and an integer for the count.  The ability to count unique items is used frequently.

# 
**Lesson Assignment**

We are going to build a counting dictionary.  Although there are Python libraries that do exactly this, it's important to learn how to build a simple counter.


**Part 1:**

Create a function named `add_item` that takes two parameters, the dictionary being used and the word to lookup (in that order).

- If the word is NOT in the dictionary add the word with a count of 1
- If the word is in the dictionary, update the count accordingly
- Return the current count for the word (after the word is added)

Create a function named `build_dictionary` that takes a list of words (as a parameter) and returns a dictionary that contains a count for each word from the list of words.  The function `build_dictionary` will call `add_item` for every value in the list.

After you are done, the following should work:

```
sentence = "the quick brown fox jumped over the dog"
```

```
# you should know what this does
```

```
words = sentence.split()  
```

```
d = build_dictionary(words)
```

```
# should be True
```

```
print(d["the"] == 2) 
```

```
print(d["dog"] == sentence.count("dog"))
```

If you need a refresher on `split` or `count` (see the lesson on strings)

**Hints:**

- Use membership notation to test if the key is in the dictionary (see previous lesson).  
- Do not use exception handling (next lesson) or any other viable methods we have not discussed yet (you can ONLY use what has been taught).
- You can use the list of words (in the variable named words) to test your function.
- Do not use any global variables.

**Part 2:**

Create a function named `build_word_counter` that has the same signature as `build_dictionary` (it also accepts a list as input).  This function also returns a dictionary, however, it normalizes each word such that the case of the word is ignored (i.e. case insensitive).  So the words LIKE, Like, like should be considered the same word (much like a regular dictionary would).

- You must use the same function `add_item` again (so don't make any modifications to it)
- Normalize words by using the lower case version of the word

> **Coder's Log:**  As a reminder, the signature of a function is a way to discuss the inputs and outputs of a function.  Two functions have the same signature if their parameters are the same (same order and same type) and their output is the same type.  The idea of creating two different functions that perform the same task with the same signature is how you can easily test two different implementations.

**Part 3:**

Create a function named `build_letter_distribution` that has the same signature as `build_dictionary`.  This function returns a dictionary; however, each key will be a letter and the value will be the count that represents how many times that letter was found.  

Essentially you will have a letter distribution over a sentence (which is a list of words).  The letters should be case insensitive.

- See the lesson on "Working with Lists" on how to get the individual characters from a string
- Normalize letters by using the lower case (see the 'kitchen sink' lesson)


If you need more help, tag your question on piazza as with **py17**.

**Before you go, you should know:**

- what are dictionaries
- what's the difference between a dictionary and a list
- when would you use a list over a dictionary
- when would you use a dictionary over a list
- why dictionaries are useful data types (data structures)
- the difference between keys and values
- how to create a dictionary (empty and with values)
- how to access the value inside a dictionary
- how to get all the keys in a dictionary


All rights reserved.
