# **A 🐍 Coder's Journey**

# **Lesson 19: Exceptional Programming**

In Python it's pretty easy to generate an error:

```
numbers = [1,2,3]
```

```
print(numbers[4])
```

```
data = {'a': 0, 'b': 1}
```

```
print(data['c'])
```

```
zero = 0
```

```
print(2.0/zero)
```

Although in these examples, it's pretty easy to "see" the error (if you don't, look closer).  Also be sure to comment out the above code before moving on.

However, sometimes it's not so simple:

```
def is_odd(x):
```

```
  # the % operator returns the remainder
```

```
  # after dividing by the operand
```

```
  # x % 2 would return either a 0 or 1
```

```
  return bool(x % 2)
```

```
def remove_odd_numbers(l):
```

```
  for i in range(len(l)):
```

```
    if is_odd(l[i]):
```

```
       # this is how you delete an item
```

```
       # from a dictionary
```

```
       del l[i]
```

```
  return l
```

```
numbers = [n for n in range(10)]
```

```
print(remove_odd_numbers(numbers))
```

As a software engineer (part of a data scientist's skill set), it's important to protect yourself from run time errors -- those errors that will cause your program to crash.  However, it is a fine line.  Sometimes it is better to crash than to continue running with bad input or with an incomplete result.  You usually want to guard against errors that are recoverable -- that is you know what the default behavior should be under a specific error condition.

**Guarding against errors**

For many types of errors, we can guard against them.  A common issue is dealing with keys that don't exist in a dictionary:

```
# generate an error
```

```
counts = {'a': 0}
```

```
print(counts['b'])
```

We can get around this by first asking if the dictionary has the key

```
counts = {'a': 0}
```

```
key = 'b'
```

```
if key in counts:
```

```
  print(key, "found")
```

```
if key not in counts:
```

```
  print(key, "not found")
```

Another way we can guard against this kind of error is by using a different access method.  For example, the dictionary type has a `get` method that allows you to pass in a default value if the key is not available:

```
counts = {'a': 0}
```

```
print("a?", counts.get('a', 'is not there'))
```

```
print("b?", counts.get('b', 'is not there'))
```

**Try to except it**

However, sometimes it's easier and more straightforward to deal with an error with a completely separate block of code (like an `if/else` statement in Python), especially if you need to do several things (rather than a single line of code).  Python has a structure called the `try/except` construct to help deal with this situation.

As an example, imagine having a simple function that allows you to update the number of items you need to buy on a shopping trip. The following function `buy_more` increments a counter on a dictionary.

```
def buy_more(basket, key):
```

```
  basket[key] += 1
```

```
# here's your initial list
```

```
shopping = {"apples": 10}
```

```
# buy more apples
```

```
buy_more(shopping, "apples")
```

```
print(shopping)
```

As you can see, `buy_more` just updates the value (the count) to which the key (in this case "apples") maps.  However, what if someone called `buy_more` with an item not in the list?  See what happens when you try to buy more bananas:

```
`buy_more(shopping, "bananas")`
```

We can update the `buy_more` function to deal with this case using `try/except`:

```
def buy_more(basket, key):
```

```
  try:
```

```
    basket[key] += 1
```

```
  except KeyError:
```

```
    # if key is not there
```

```
    # add it to the dictionary
```

```
    # and set the count to 1
```

```
    basket[key] = 1
```

```
shopping = {"apples": 10}
```

```
buy_more(shopping, "apples")
```

```
buy_more(shopping, "bananas")
```

```
print(shopping)
```

**Trapping missing imports**

You can use the `try/except` construct to trap import errors for libraries that don't exist:

```
try:
```

```
  import fancyMath
```

```
except ImportError:
```

```
  print("you can't import this module")
```

**Printing exceptions**

Sometimes while debugging an algorithm, you can use exception handling to get more specific information from the exception that generated it.  Here's the code from the start of the lesson, but with exception handling to give us a clue about the error:

```
def is_odd(x):
```

```
  return bool(x % 2)
```

```
def remove_odd_numbers(l):
```

```
  for i in range(len(l)):
```

```
    try:
```

```
      if is_odd(l[i]):
```

```
         del l[i]
```

```
    except Exception as e:
```

```
      print("Error on delete", e)
```

```
  return l
```

```
numbers = [n for n in range(10)]
```

```
print(remove_odd_numbers(numbers))
```

Note the use of how the `Exception` is given a name, `e`, so that the exception can be printed.  The name  'e' is just a variable name.  It can be any legal variable name.  It holds the exception object that was created with the error that occurred. Without it, all you know is the Exception.  In most cases, you want to use a specific Exception class like ValueError, KeyError so there's no need to assign it to a variable.  In the example given, since Exception is a base class for all Exceptions, assigning it to a variable helps so you know what specific exception was thrown.

> **Coders Log**: As a side note, the issue with the above code is that the array structure is being modified during the loop (items are being deleted).  Hence the size of the list is changing yet the index in the loop is still working with the initial size.

**Else and Finally**

Once you begin to see the usefulness of trapping errors, Python also provides constructs to help you manage different situations cleanly.  You can add the keywords `else` and `finally` to a `try/except`.  The `else` is useful if you are managing several different error conditions and `finally` is useful to put common code that needs to be run after any exception.  This is not a requirement for any of the assignments.

This is given only to show the syntax (it will not run)

```
try:
```

```
  import fancyMath
```

```
  stats[mean] = fancyMath.operation([1,2,3])
```

```
except ImportError:
```

```
  print("you can't import this module")
```

```
except KeyError:
```

```
  print("bad access")
```

```
else:
```

```
  print("unknown exception")
```

```
finally:
```

```
  print("broken code")
```

# **Lesson Assignmen**t

The statistics library throws an exception in many cases where the incoming dataset is empty.  Also older versions of the library generated an error when you calculated the mode on a set of numbers that are evenly distributed.  

```
import statistics as stats
numbers = [1,2,3,4] 
print("mode", stats.mode(numbers))
```

The library still generates an error if the incoming data is empty:

```
numbers = []
print("mode", stats.mode(numbers))
```

Also the same error would be generated if you asked for the variance of a single number:

```
numbers = [1] 
print("var", stats.variance(numbers))
```

Create a function named `worry_free_mode` that takes a list of numbers as its parameter,  returns the value from `statistics.mode` unless there is a `StatisticsError` (see the docs).  If there is an error, return `None`.


Create a function named `worry_free_var` that takes a list of numbers as its parameter,  returns the value from `statistics.variance` unless there is a `StatisticsError` (see the docs).  If there is an error, return `None`.


- You must use `try/except` construct
- You may need to refer to https://docs.python.org/3/library/statistics.html
- You must put your code inside the module `lesson.py`
- Write a test in `main.py` that uses `worry_free_mode` that confirms your implementation
- If you are unsure how to reference or use `StatisticsError` from the statistics module, see the previous lesson on modules.

**Notes:**

We will discuss various statistical functions in another lesson.  Python's standard library includes the statistics module.  There's also the stats module (which we will not use) that provides similar functionality.


If you need more help, tag your question on piazza as with **py19**.


**Before you go, you should know:**

- what an exception is
- how to trap an exception


All rights reserved.

**Additional Reading:**

https://docs.python.org/3/tutorial/errors.html
