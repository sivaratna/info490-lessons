# **A 🐍 Coder's Journey**

**Lesson 11: An Array of Possibilities**


A previous lesson introduced us to the concept of a variable (the idea of having a named entity which refers to a specific value) and some of the values it can hold:

```
count = 10    # a number type
who   = "me"  # a string type
print("the count", count, "doubled", count*2)
print("I am", who)
```

It's also possible to have a variable refer to a list of values rather than a single value.  The Python list is a container (like a variable) that can hold many values. Perhaps the single most useful container in all of programming is the list.  The list is a built in type (or data type) in Python.

> **Coder's Log:** In most programming languages, the list type is called an **array**. Languages with an array type use the same notation that Python uses for lists.  In fact, the word array is used more frequently.  Python also has arrays, but it's not a built in type and the distinction between lists and arrays in Python will be discussed in another lesson.  For now we can use the words array and list interchangeably.

The above example shows that the variable count has been assigned to the specific value 10.  However, we can also have a variable hold a list of values:

```
# first 8 prime numbers
primes = **[**2, 3, 5, 7, 11, 13, 17, 19**]**
```

As you can see, the list syntax is a comma separated list of values within a '`[`' and a '`]`' (called square brackets).

To create an empty list, you just use put nothing between the `[]`'s

```
empty_list = []
```


You can also use the built-in `list` function to create a list:

```
empty_list = list()
```

However, if you want to use the same `list` function, you still need to put the values inside brackets:

```
number_list = list([2,3,5])
```

In an upcoming lesson, we will learn how to modify (update) a list by being able to add or remove values

**The access offsets**

We also use the same brackets to access each of the values in a list.  Having a set of values isn't too useful if we aren't able to access them.  Think of a list as values waiting in a line, where you would describe the location of the value relative to the front of the line or it's offset from the front. 


In our prime number example, the value 2 is the front.  We say it's at offset 0 from the front.  Offset 0 is the front of the line.  Offset 1 would be the value right behind the front (it's one place away (or offset) from the front).  


With that in mind we can access the 8 values as:

primes[0] holds 2;   it's offset 0 spots from the front

primes[1] holds 3

primes[2] holds 5

primes[3] holds 7

primes[4] holds 11

primes[5] holds 13

primes[6] holds 17

primes[7] holds 19  it's offset 7 spots from the front


```
# a list of the first 8 prime numbers
# primes is a variable that holds 
# a set of values
primes = [2, 3, 5, 7, 11, 13, 17, 19]
print(primes[0])
print(primes[1])
print(primes[2])
print(primes[3])
print(primes[4])
print(primes[5])
print(primes[6])
print(primes[7])
```

Since offsets start at 0, the **last valid offset** is the same as the 1 less than the number of elements in the list.  This is an important point to remember. In fact, commit it to memory.


**Length of a list**

We saw earlier that you can get the length of a string using the `len` function (e.g. `len('Python')`).  This same function can be used with lists. 

```
size = len(primes)
```

```
print(size)
```

> **Coder's Log:**  because `len` is a built in function you want to be careful not to create a variable named `len`.  Doing so would prohibit the use of the function. 

**Going beyond the length**

One of the nice features of Python is that it will check the bounds of a list for you before you attempt to access an element that is bigger than the largest offset.

```
size = len(primes)
```

```
print(primes[size])
```

Running this code will give you an **IndexError: list index out of range**.  In other languages, indexing past the last element will not result in the compiler or interpreter stopping your program. Your program may crash or it may just produce wrong results.

The last element in a list is always at 1 less than the number of elements in the list:

```
size = len(primes)
print(primes[size-1])
```

So remember with lists (and arrays) in Python (and almost all languages) is that the first element is always at offset 0 (commonly called an index) and the last element is always at 1 less the number of elements in the array.  Always remember the first element of any array is at index 0 (not 1).  Forgetting this will wreak havoc with your free time.

> _**Coder's Log:** Although all the word offset can be used to describe the relative distance from the front of an array, most programming languages use the word **index**.  The bad news is not all programming languages use index 0 as a way to access the first element.  Fortran, R, MatLab, Scratch, Excel, for example, use index 1 to mean the first element.  Buyer (i.e. Programmer) beware._

**Mixing Types**

A list in Python can also contain items that are not of the same type.  This is one of the distinguishing features of Python lists vs arrays.  Arrays usually contain homogeneous items (items of the same type).

For example to keep track of this class, the credits, where it's located, and the expected GPA, you can use a Python list to hold the necessary information:

```
my_class = ["INFO 490", 3, 'on-line', 4.00]
```

In the next lesson, we will learn how lists and iteration (or looping) go hand in hand.  

# **Lesson Assignment:**

The following variable (`cards`) holds a standard deck of playing cards (those shapes are unicode):

```
**cards =** ['A♢', '2♢', '3♢', '4♢', '5♢', '6♢', '7♢', '8♢', '9♢', '10♢', 'J♢', 'Q♢', 'K♢', 'A♣', '2♣', '3♣', '4♣', '5♣', '6♣', '7♣', '8♣', '9♣', '10♣', 'J♣', 'Q♣', 'K♣', 'A♡', '2♡', '3♡', '4♡', '5♡', '6♡', '7♡', '8♡', '9♡', '10♡', 'J♡', 'Q♡', 'K♡', 'A♠', '2♠', '3♠', '4♠', '5♠', '6♠', '7♠', '8♠', '9♠', '10♠', 'J♠', 'Q♠', 'K♠']
```

```
cards_per_suit = 13
```

A deck is 52 cards:  A,2,3,4,5,6,7,8,910,J,Q,K and 4 suites: ♢♣♡♠

Copy and paste the above so it's the first lines in your program.

Print out the contents of `cards` to confirm. 

**Part One:**

define the following variables (the first one is done for you)

```
`diamond_offset = 0`
```

```
  # holds the index of the first ♢
```

```
`club_offset =` 
```

```
  # holds the index of the first ♣
```

```
`heart_offset =` 
```

```
  # holds the index of the first ♡
```

```
`spade_offset =` 
```

```
  # holds the index of the first ♠
```

Hint: do NOT use hard coded numbers other than for diamonds.  Come up with a mathematical expression to define the value (rather than saying heart_offset  = 34) using the `**cards_per_suit**` variable. For example (this is **not** mathematically correct :) 

```
`club_offset = diamond_offset * (cards_per_suit + 1)`
```

How would you test these offsets before moving on?

**Part Two:**

define the following four functions:

```
first_card_in_deck(deck)
```

- returns the first card in the deck

```
last_card_in_deck(deck)
```

- returns the last card in the deck

```
first_heart(deck)
```

- returns the first ♡ in the deck

```
get_card(deck, suit, rank)
```

- returns the relevant card from the deck
- suit will be one of ♢♣♡♠
- rank will be a number 1 through 13 corresponding to (A, 2, 3 .. J,Q,K)

In your code you need to calculate the appropriate index so you can do

```
  return deck[ calculated_index_value ]
```

For example to get the Q♡ you could use `get_card` like this:

```
card = get_card(cards, '♡', 12)
```

```
print(card) # it should be the Queen of Hearts!
```

NOTES you can assume the following:

- the deck parameter will always be in the A-K and ♢♣♡♠ order

You MUST use the following restrictions:

- do not use any hard coded offsets (e.g. 23, 12)
- you can use the literals 0,1,2,3,4 only (don't use 13, use `cards_per_suit`)
- no hard coded cards (e.g `return 'A♡'`)
- you can reference the variables defined in part one

**Part Three**

write the following function:

```
first_heart_v2(deck)
```

- returns the first ♡ in the deck
- it MUST use `get_card` with the proper parameters

If you need more help, tag your question on piazza as with **py11**

**Before you go, you should know:**

- what a list is
- what's the syntax to create an empty list  (there's two ways)
- how do you find how many elements are in the list
- how do you access the first element
- how do you access the last element


all rights reserved 

