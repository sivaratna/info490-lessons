# **A 🐍 Coder's Journey**

**Lesson 13: Working with Lists**

This lesson has a lot of details.  It is best to re-read the lesson **several times**.  It also contains information that will be used over and over again throughout the lessons and assignments.

Managing and working with lists (or arrays) is one of the most common tasks in programming.  It is so common that Python provides you with multiple "tools" to help you work with lists.  As we have learned, a list is just an **ordered** collection of values.

**List Creation**

All the following syntaxes can be used to produce different kinds of lists in Python:

```
l = [1,2,3]     # matching square brackets
```

```
t = (1,2,3)     # matching parentheses
```

```
s = list("123") # function call
```

```
r = range(1,4)  # function call
```

Be sure to print out the contents of each of those variables before continuing.

However, it's not as simple as it looks.  

• The variable `l` is indeed a value whose type is a `list` with 3 elements (numbers)

• The variable `t` is like a list, but you cannot change the contents of `t`.  It's a list that cannot be modified.  It's called a **tuple** in Python.  We have not discussed modifying lists yet. When something can't be modified it's called _immutable_.  The tuple is an immutable list. You use parenthesis to create a tuple (not square brackets).

• The variable `s` is a list that gets created from using the `list` function.  This function can take different sequences (more on that later) and turn them into lists.  The list, `s`, contains 3 elements, each a string.  Notice that a single string was passed into the list function (`"123"`), but then it 'asked' the string object to iterate over its contents -- which is three separate characters.  This is both useful and confusing at times.

• The variable `r` is a list that gets created as you need it.  If you wanted to create a list of a million values, using the `range` function would spare you memory since it doesn't actually create the elements until you ask for them.  The function `range` (which we will use later) returns a virtual sequence.

> **Coder's Log:**  Both built-in 'functions' `list` and `range` are actually types -- or classes in Python.  Although they look like a function call, they actually create a Python object (or type).  Python provides programmers the ability to build objects that return items as needed called a **generator**.  Technically `range` is not a generator but it does generate numbers on demand, much like the generator concept which we will learn later.

When you use the `range` function (really a class) to create a 'virtual' list, the first parameter is the starting number and the second is the stopping point.  The stopping point is NOT included in the list (we say the end is _exclusive_). Also, the default step value is 1 (which can be changed by adding a third parameter):

```
t = range(1,10)
```

```
print(type(t))
```

```
print(list(t))
```

```
print(list(range(1,10,2)))
```

The range function is used to easily build a defined ordered set of numbers. You can even have a negative step value to count backwards.  When you need a quick sequence of numbers, `range` is your answer.

```
backwards = range(4, 0, -1)
```

```
for n in backwards:
```

```
  print(n)
```

**Sequences**

In Python, the generic family of lists, tuple, range, strings are called sequence types.  Sequence types provide a common set of operations that work for the different types.  It allows you, the software engineer, to design things (e.g. functions) to operate on a family of types rather than assuming a specific type.

**List Membership**

The `**in**` operator for list enumeration can also be used to ask a list if a value is contained within it:

```
l = [1,2,3]
```

```
t = (1,2,3)
```

```
r = range(1, 4)
```

```
print(3 **in** r)
```

```
print('o' **in** 'Python')
```

```
print(4 **in** t)
```

**Element Access**

The nice feature of the sequence types is that you can access elements using the same syntax:

```
print(l[1], t[1], s[1], r[1])
```

**Element Finding**

In addition to knowing that an item exists in a list, sometimes, you need to know where in the list it exists.  The method `**index**` is one of the most useful methods of the 'sequence' type.  It returns (if the item exists) a value from 0 to `len(list) - 1`:

```
l = ["abc", "def", "ghi"]
```

```
print(l.index("def"), l.index("ghi"))
```

Note that the list throws an exception (an error) if you ask for something that does not exist (we will learn how to handle exceptions in another lesson):

```
l = ["abc", "def", "ghi"]
```

```
print(l.index("hello"))
```

However, if you want to avoid dealing with the error, you can guard the `index` method with a membership test:

```
l = ["abc", "def", "ghi"]
```

```
if 'abc' in l:
```

```
   print(l.index("abc"))
```

**Functions vs Methods**

This is the first time the word _method_ has been used.  A method is a function attached to a type (like lists, strings).  When you create your own types (called classes), the functions that work on that type (or class) are called methods.  To invoke a function method you use the `.` (dot) syntax:

```
print("abcdefg"**.**index('c'))
```

**List Equality**

Much like comparing two values in Python to see if they are the same, you can use equality testing on lists.  For two lists to be equal, they must contain the same values and be in the same order:

```
a = [1,2,3]
```

```
b = [3,2,1]
```

```
c = [1,2,3]
```

```
print(a == b, a == c)
```

Another part of list equality is that they have the same type.  

```
a = [1,2,3]
```

```
d = (1,2,3)
```

```
print(a == d)
```

In the above, the types of `a` and `d` are different.

Similarly, you might think that a list with the same characters as a string might be equal since they do share many of the same characteristics of equality:

```
word  = "hello"
```

```
chars = ['h', 'e', 'l', 'l', 'o']
```

```
# same length, same characters
```

```
print(len(word) == len(chars))
```

```
for i in range(0, len(word)): # range to the rescue
```

```
  print(word[i] == chars[i])
```

```
# but they are NOT equal
```

```
print(word == chars)
```

When you create a list of characters using the `list` function, it is the same (although easier to type) as using the bracket notation:

```
s1 = list("abc")
```

```
s2 = ['a', 'b', 'c']
```

```
print(s1 == s2)
```

**Forcing the same type**

Sometimes it's easiest to use the `list` function to force any sequence into the list type (usually for equality testing):

You can force the string into a list and then do equality:

```
print(list(word) == chars)
```

Similarly you can force a tuple into a list as well to check equality:

```
a = [1,2,3]
```

```
t = (1,2,3)
```

```
print(a == **list**(t))
```

You can also convert lists into tuples:

```
a = [1,2,3]
t = (1,2,3)
print(**tuple**(a) == t)
```

**Operations on Lists.** 

There are several operations that you can perform on lists:

```
a = [1,2,3]
```

```
b = ['a','b','c']
```

```
c = a + b
```

```
print(c)      # + is appending 
```

```
print(a * 3)  # * is replication
```

**Iteration**

As we have seen, loops and lists go hand in hand.  Once again Python has generalized the concept of being able to process a set of items with the word _iterable_.  An iterable is anything (or type) that you can loop over (usually with a for loop).   All sequence types are iterable and we will learn of some non-sequence types that are iterable.

We have more to say about operations on lists, but be sure to visit the Python documentation: https://docs.python.org/3/library/stdtypes.html#common-sequence-operations (you can skip the slice operations -- there's a full lesson on list slicing).

For now, we can consider the word 'sequence' to mean a list.  We will discuss the technical details of sequences, lists, and iterators in another lesson.

**Short circuit Evaluation (reprise)**

In the "selecting a path" lesson, there was a short discussion on short circuit boolean evaluation.  This handy construct is useful for list manipulation as well.  For example,

```
if items is None **or** len(items) == 0:
```

```
   # nothing to do
```

In the above example, `len(items)` is protected.  It will not be evaluated unless items is not `None`.  Since it's a logical `**or**` statement, as soon as true is evaluated, there's no need to process more.  The function len will throw an exception if it is passed None.  But using the above will protect that situation.

Similarly you can use the same to insist that a list has at least one item in it:

```
if items is not None **and** len(items) > 0:
```

```
   safe_access = items[0]
```

In the above example, the expression `len(items) > 0` will only be evaluated if `items` is not `None`.  Since it's a logical `**and**` as soon as a false is encountered, the processing can stop.

# **Lesson Assignment**

This lesson provided a lot of mechanics without any interesting reasons for why we might choose a tuple or a list. Hopefully some of the assignments will help you bring together the mechanics.  There's a lot to cover and getting some of the fundamentals down will give you a solid base on which to build your programming career.

For this assignment, we want to create two different functions that return `True` if the parameter, v, is a vowel.  A letter is a vowel if it is one of a,e,i,o,u.  The parameter, v, could be an uppercase or lowercase letter.

**1**. Create the function **`is_vowel`**. It must use an `if` statement(s)

```
def is_vowel(v):
```

```
  # v is a letter
```

```
  # return True if v is a vowel
```

Notes:

- solve the problem using **ONLY** selection statements (e.g. `if,` `elif,` `else`) with boolean expressions (e.g. do not use the **`in`** operator)
- You can use the functions `lower()` or `upper()` in your solution to help manage all the possibilities.  Both `lower` and `upper` are functions on the string type.  When a function belongs to a type we call them _methods_.

```
print('A'.lower(), 'a'.upper())
```

**2**.  Create a function **`is_a_vowel`**. It must use the **`in`** operator to solve the same problem:

```
def is_a_vowel(v):
```

```
  # v is a letter
```

```
  # return True if v is a vowel
```

**3**. Create a function **`count_vowels`** that takes a string and returns the number of vowels in that string.  Your function should use one of the functions you created above to determine if a letter is a vowel.

Test your functions well.  If you need more help, tag your question on piazza as with **py13**

**Before you go, you should know:**

- the different ways to create sequences
- which of the following can hold different types of items: lists, tuples, strings
- when would you use the range function
- how to find elements
- how to create an empty list
- how does Python determine if two lists are the same


all rights reserved

