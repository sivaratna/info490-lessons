# **A 🐍 Coder's Journey**

**Lesson 9:  Selecting a path (continued)**

 



In this lesson we will bring the lessons on boolean expressions and on selection statements together to easily build complex conditional logic.  In the previous lesson, we used simple if/elif statements to build a selection path:

```
if color == 'red':
```

```
  do_something()
```

```
elif color == 'green':
```

```
  do_something()
```

This is essentially equivalent to saying `do_something` if the `color` is red **or** green.  In Python, we can use the boolean `**or**` operator as well to make our selection statements easier to read (and write):

```
if color == 'red' **or** color == 'green':
```

```
  do_something()
```

So it's usually more convenient to use the or operator than to repeat if/then/else logic.  

You might have noticed that it's a bit more complicated to write selection statements when you can't use the boolean keywords `and`, `or`, `not`.  The boolean `**or**` condition is the same as a set of `if/elif` statements:

```
def makes_dog_noise(noise):
```

```
  if noise == "woof":
```

```
    return True
```

```
  elif noise == "bark":
```

```
    return True
```

```
  elif noise == "ruff":
```

```
    return True
```

```
  return False
```

This can be simplified by using the `**or**` keyword:

```
def makes_dog_noise(noise):
```

```
  if noise == "woof" **or** noise == "bark" **or** noise == "ruff":
```

```
    return True
```

```
  return False
```

Note that we need to repeat the name of the variable/parameter each time.  We **cannot** do the following:

```
if noise == "woof" or "bark" or "ruff": # illegal code
```

```
    return True
```

We can also make use of the boolean `**and**` operator.

The boolean `**and**` can be simulated using **nested if statements**:

```
def is_dog(noise, leg_count):
```

```
  if makes_dog_noise(noise):
```

```
    if leg_count == 4:
```

```
      return True
```

```
  return False
```

So `is_dog` will only return true if both `makes_dog_noise` is `True` and `leg_count == 4`. Make sure you understand how that works.  

If you want to implement the logic of an '`and`' you need to _nest_ your if statements.  

As another example if you want to call `do_something` if food is 'Ice Cream' **and** the price is less than 30, you would have to write it like the following:

```
if food == 'Ice Cream':
```

```
  if price < 30:
```

```
     do_something()
```

The first if statement (`food == 'Ice Cream'` will first be evaluated.  If that statement is true, THEN the second if statement will be evaluated (`price < 30`).  ONLY if **both** food == 'Ice Cream' **AND** price is less than 30, will `do_something()` be called.

However, we can simplify this logic using the boolean `**and**` operator:

```
if food == 'Ice Cream' and price < 30:
```

```
     do_something()
```

More examples using nested if statements are in the next lesson.

**Indentation note**:  take close look at how indentation is used to mark a new block of code.  In the above code, there are three levels of indentation.  Notice that the `return` `False` is at the function indentation level.  If you indented '`return False`' two more spaces, what would happen if the function `make_dog_noise` returned `False` ?

The above can be simplified using the `**and**` keyword:

```
def is_dog(noise, leg_count):
```

```
  if makes_dog_noise(noise) **and** leg_count == 4:
```

```
      return True
```

```
  return False
```

The above are examples of _compound conditionals_ -- a condition set up by more than one boolean expression.

**Simplifying Functions**

Each of the above functions (or any function that returns a boolean value) can be simplified even further.  If a function simply returns `True` or `False` based on a condition, you can just return the result of the expression directly:

```
def is_dog(noise, leg_count):
```

```
  return makes_dog_noise(noise) **and** leg_count == 4
```

This is usually the best practice in programming.  Be sure you understand why this simplified logic works without using an `if` statement.

**Making Code Readable**

As you may have noticed, sometimes expressing the necessary logic becomes long.  For example, here's a way to the captures the logic of eating if the price is between 1.01 and 1.99 OR you are hungry:

```
def will_eat(hungry, price):
```

```
  if price > 1.00 **and** price < 2.00 **or** hungry == True:
```

```
    return True
```

```
  return False
```

Reading long lines is difficult.  In addition to using variables, you can use parenthesis to allow your code to span multiple lines (splitting the line with a carriage return (or newline) will result in a syntax error):

```
def will_eat(hungry, price):
```

```
  if (price > 1.00 **and** price < 2.00 
```

```
     **or** hungry == True):
```

```
    return True
```

```
  return False
```

Note in the above code, parentheses are used to allow the line to be split over more than one line.  You can avoid this by using an optional line continuation character `'\'` (the backslash):

```
def will_eat1(price, hungry):
```

```
  if price == 0.25 or \
```

```
     price == 0.50 or \
```

```
     price == 1.00 and hungry == True:
```

```
    return True
```

```
  return False
```

Can you determine the output of the following?

```
print(will_eat1(0.25, False))
```

The above can be simplified by just returning the result of the expression (skipping the entire `if` statement) as well.

**Forcing Evaluation Order**

Parenthesis can be used to force a different evaluation order.  

The keyword **`not`** is evaluated first, then **`and`** and finally **or**.  

However, anything inside `()` is always evaluated first.  Those statements inside the `()` are evaluated using the normal order -- unless additional parenthesis are used. Python doesn't require parenthesis but you should use them if it helps with either clarity or the actual evaluation.  

Take a look at three possible reorderings of the logic inside the following functions.  

Which version will return False if price is 0.25 and hungry is False?  

```
def will_eat2(price, hungry):
```

```
  if (price == 0.25 or \
```

```
      price == 0.50 or \
```

```
      price == 1.00) and hungry == True:
```

```
    return True
```

```
  return False
```

```
def will_eat3(price, hungry):
```

```
  if price == 0.25 or \
```

```
    (price == 0.50 or price == 1.00) \
```

```
     and hungry == True:
```

```
    return True
```

```
  return False
```

```
def will_eat4(price, hungry):
```

```
  if price == 0.25 or \
```

```
     price == 0.50 or (price == 1.00
```

```
     and hungry == True):
```

```
    return True
```

```
  return False
```

This is good example of a possible quiz question.  So make sure you know how to "read" this code and understand how Python will evaluate it based on the arguments passed.

**Short Circuit Evaluation** 

Python uses short-circuit evaluation to help optimize the run-time evaluation of boolean expressions.  For example, if you have the expression `a` `and` `b`, if `a` evaluates to `False`, there's no need to even evaluate `b`.  This can save a lot of time if `b` is a complex expression that involves function calls.

For example:

```
if a is None or b == 'blue':
```

The expression `b == 'blue'` will never be evaluated if `a` is equal to `None`.  Since it's an or statement, as soon as a true statement is evaluated, there's no need to do more.

# **Lesson Assignment**

You will create functions to implement some complex boolean logic.  However, you **are allowed** to use the boolean operators `not,` `and,` `or`.  Also be sure to use parenthesis when it matters.

1. `buy_product_v1(color, price)`

returns `True`:

    • if color is either red or green  AND  the price less than ten (dollars)

2. `buy_product_v2(color, price)`

returns `True`:

    • if color is red and the price less than ten (dollars)

3. `buy_product_v3(color, price, kind)`

returns `True`:

    • color is red, the price less than ten (dollars), kind is 'new'

4. `buy_product_v4(color, price, kind)`

returns `True` if any of the following are true: 

    • color is red

    • the price less than ten (dollars)

    • kind is 'new'

5. `buy_product_v5(color, price, kind)`

• returns `True` if any of the following are true:

     • if color is 'green' and price under five

     • if the price is under 20 and kind is 'used'

6. `buy_product_v6(color, price, kind)`

• returns `True` if any of the following are true:

    • if kind is 'new' AND color is 'green'

    • if kind is 'used' and price is between 15 and 20 (inclusive)

If you need more help, tag your question on piazza as with **py9**

**Before you go, you should know:**

- how to use `or`, `and`, `not` in boolean expressions
- how to write a function with a single exit point (one `return` statement)
- how you can simulate the `or` operator and the `and` operator using `if` statements


All rights reserved.

