# **A 🐍 Coder's Journey**

**Introduction to Functions**


The function is the fundamental way to organize code. Almost all programming languages have functions.  Functions are nothing more than a container for Python code.  They can take input(s), perform a task, and generate an output.  This is essentially the same as when you learned about functions in algebra (... perhaps you remember the words domain and range where the input was called the domain and the output was called the range). 

The familiar 'picture' of a function also applies to Python functions:

 

A function in Python can skip any of those 3 things:  it doesn't need input, it can do nothing (called a _no-op_: short for no operation), and it doesn't have to generate an output.  In this class, we will (almost always) do all three.

There are other code containers as well.  You may have heard of _classes_ (along with objects and instances), libraries (or modules), frameworks, and APIs (application programming interface).  All of these essentially provide a way to organize code.  In this class we will focus almost exclusively on functions and libraries.  However subsequent classes will explore creating classes and objects (called Object Oriented Programming).

**Back to the Example**

Looking at the statement we wrote in the previous lesson:

```
print("Welcome to Python")
```

Here "print" is just the name of a **function**; `print` is given a message (e.g. `"Welcome to Python"`) to write to the console window.


Functions take input (called **parameters**), perform an action and return an output (i.e. a result or value).  Functions can have multiple inputs (parameters) and return multiple values as well.  We will have more to say about this in another lesson.

Formally, we would say we are _calling_ (or invoking) the _global_ function named `print`.  We pass "Welcome to Python" as its _**actual**_ (or calling) _**parameter**_ as input to the `print` function.  Being global means the function is included and is available everywhere within a Python program (we will learn more about how import and use function).  A non-global function is called a local function.

**Creating your own functions**

Almost everything we will do in Python (for this class) will result in creating a function that performs some action.  Creating a Python function looks like the following (**type this into the editor window without the line numbers):**

```
1. def my_logger(msg):
2.     print(msg)
3.     return True
```

There's a lot to unpack in those 3 lines:

- **`def`** is a _keyword_ in Python. Programming language keywords are reserved for specific use. In Python the keyword **def** marks the _definition_ of a function. 
- **`my_logger`** is the name given to the function. Function names must begin with a letter (or an underscore) and contain no spaces.  Python is also case sensitive.  So `my_logger` and `my_Logger` are two different functions.
- **`(`** is the start of the parameter(s) list (also called **_formal parameters_**)
- **`msg`** is the name of the formal parameter.  It represents the **INPUT** to the function.  The parameter msg belongs only to `my_logger`.  Only `my_logger` can "see" the parameter `msg`.
- **`)`** marks the end of the parameter list. 
- **`:`** marks the beginning of the function **_declaration_**
- the _body_ (statements, code) of the function starts on line 2 and ends after line 3.  These lines _**define**_ the function. In this example there are only 2 lines of code (lines 2 and 3) in the body.  The code on line 2 just passes the parameter `msg` to the global Python `print` function.
- **`return`** is a _keyword_ that marks an explicit exit from the function.  Functions in Python can have multiple exit points (although we strive to have one exit point).  Even if a function has multiple return statements only ONE will be executed.
- return **`True`**.  **`True` is the value returned by the `my_logger`.  This represents the **OUPUT** of the function.  We will discuss the meaning of `True` (and it's counterpart `False`) in the chapter on values.**

**Indentation Matters**

One thing you might have not noticed is how Python relies on spacing of the indentation to determine the definition (contents) of a function (and other constructs as well).  Many programming languages rely on using a '`;`' to terminate the end of statements and `{}` (curly braces) to define contents of functions.   Python relies on the end of line character (which is invisible in most editors) and the amount of indentation.  It is a common convention to use 4 spaces to indent. The above syntax of `my_logger` describes a function declaration with the name `my_logger`.  The contents of the body of the function define it.  

**Calling what we created**

Of course, defining a function that is never used is a waste of time.  When we use the function (aka. _invoking_ or _calling_) we pass an _actual_ _argument (aka actual parameter)_ to it.  Type the following into the editor to invoke (or call) the `my_logger` function:

```
my_logger("Hello")
my_logger("World!")
```

> if nothing happens after hitting run ►, read this lesson from the begining and follow all the directions

One problem with `my_logger` is that it can only log one message at a time.  Try the following:

```
my_logger("Hello", "World")
```

What happens? This is an example of calling a function with multiple arguments.  Each argument is separated with a '`,`' ; `print` can process multiple arguments, `my_logger` cannot. We can build a more robust logger which can handle multiple arguments later.

**To Return or Not**

In Python every function returns a value.  If you use the keyword **return** it's called an _explicit_ return.  However, functions in Python still return a value even if there isn't the keyword return.  If a function doesn't return a value, it's sometimes called a _procedure_ or _subroutine_ (depending upon who's in the room) but in Python every function returns a value.

Type in the following:

```
def bad_add(a,b):
```

```
  a + b
```

```
print(bad_add(2,3))
```

What happens after you run this code?

If you don't specify a return, Python returns the value '`None`'.  This keyword indicates an absence of value.  Some languages use the keyword `**null**` or `**undefined**`.  We will learn more about `None` and how it is useful later.  The values `True`, `False`, and `None` are also keywords in Python.

**Fun with math.**

Math and functions work well together. Let's review the following code :

```
# return the sum of it's parameters
def add(a, b): 
  return a + b
```

The **`#`** on the first line is ignored by the interpreter.  We use the **`#`** symbol to add notes to the reader of our code (called **comments**).  Comments in Python start with `#` and anything after the `#` is ignored up until the next line is encountered.  We try to add comments to help explain the logic or the reasoning behind the code.


On the second line, we are declaring a function with the name `add`.  It has two _**formal parameters**_**** (`a` and `b`) also called _**positional arguments**_.  Only the function `add` can use the labels '`a`' and '`b`' to access the two values passed into it. In this example, `a` and `b` are only "visible" inside the function `add`. Since the `add` function has multiple parameters (`a` and `b`), each parameter is separated by a comma.


The third line represents the entire definition of the function. It simply returns the resulting value of adding the two parameters (the OUTPUT of the `add` function):

```
  return a + b
```

**No Returns**

Once a function returns or once Python encounters a return, any code after that return is considered _dead_ code (or unreachable code).  It simply will never be run:

```
def add(a, b):    
```

```
  return a + b
```

```
  print("adding", a, b)
```

```
  return 100
```

```
print(add(1,2))
```

Try to guess the output of the above before running the code.

Here's the first diagram augmented using the `add` function from the lesson:

 

**Test Test Test**

You can test your add function by typing and 'running' this code

```
print("expecting to see 3:", add(1,2))
```

Each time you write a function, you need to test it.  You need to make sure that it behaves the way you intended.  For most of our lessons you will use `print` to write out messages so you can view the results of your tests.  However, Python provides a formal way to test your code which you will learn (and use) later.


# **Lesson Assignment:**

For this lesson you will declare, define and use a few math functions.

**1**.  Define **three more functions**:  `subtract`, `multiply`, and `divide`.  Each has two formal parameters (a and b).  

- for `subtract`, a should subtract b from it
- for `divide`, a should be divided by b.  
- multiplication (ⅹ) operator is '*' 
- division (÷) operator  is '/'.  

Formally we call this process implementing a function.  You are officially programming!


**2.**  Test your 4 functions (`add`, `subtract`, `multiply`, `divide`) with separate print messages.


**3.** Submit your answer. You will also be tested against your `add`, `subtract`, `divide`, and `multiply` functions.   Remember, you can use the Run Tests feature before submitting your code (which also runs the tests as well)

 


If you need more help, tag your question on piazza as with **py2** 

**A note on vocabulary:**

You might have noticed that there is a lot of vocabulary in this lesson.  In general, you won't be tested on vocabulary that we don't consistently use.  However, it's important to know the words so that it becomes easier to read documentation, articles and books on programming as well as participate in technical conversations.  It will serve you well to re-read this lesson after you are done.

# Before you go, you should know:

- The 3 main components of a function
- The syntax to define a function in Python
- The meaning of the `def`, `return`, and the '`:`'
- The difference between parameters and arguments
- What happens when you forget to use the keyword `return` in Python
- What needs to be done to call a function with more than one argument
- What's the difference between defining and calling a function


all rights reserved
