# **A 🐍 Coder's Journey**

**Lesson 7: Boolean Expressions**


We just saw that variables can hold boolean values:

```
a = True
b = False
print(a, b, not b)
```

We can also use boolean operators to create boolean values:

- The **or** operator returns `False` **only if** both of its operands are `False`.
- The **and** operator returns `True` **only if** both of its operands are `True`.  
- The **not** operator returns the inverse of its boolean operand.  So `not` `True` becomes `False` and `not` `False` becomes `True`.

> **Coder's Log:** Many programming languages use different symbols for boolean operations.  In JavaScript, for example, and is **`&&`**; or is **`||`**; and not is **`!`** (pronounced bang).

When you use a boolean operator in an expression, the statement is called a boolean expression (much like a math expression that uses +,-,/,*).  

A less common operation is the xor operator (exclusive or) which is `True` only if one of the operands is `True`.  There is no xor keyword, but rather the `**^**` symbol:

```
print(True  ^ False)
```

```
print(False ^ True)
```

```
print(True  ^ True)
```

```
print(False ^ False)
```

Perhaps the easiest way to think of exclusive or is by thinking of the sentence: "I'll have either a Tea or Coffee".  If you brought me both, it would be wrong.  If you brought me neither that would be wrong as well.  The more common **`or`** is sometimes called an inclusive `or`.

Sometimes it's easier to visualize these operations:

**Logical And (logical conjunction)**

 

The following code will print out the 'and' truth table:

```
def print_and(a, b):
```

```
  print(a, 'and', b, '==>', a and b)
```

```
def and_table():
```

```
  print_and(True, False)
```

```
  print_and(True, True)
```

```
  print_and(False, True)
```

```
  print_and(False, False)
```

```
  
```

```
and_table()
```

**Logical Or (inclusive or)**

 

Can you use the code above to build an 'or' table?

**Xor (exclusive or)**

 

Can you use the code above to build an 'or' table?  Looking at the two rows that have a `True`, **xor** can also be written out as:

```
(p and not q) or (not p and q)
```

**Practice:**

Before you type in the following, see if you can predict what each expression holds (prime testing material).

```
a = True
b = False

c = not b 
d = a or b
e = not a and c
f = a and b or c
```

```

#use print() to check your answers
```

In the above example the variable `f` is assigned a truth value (`True` or `False`) based on if both `a` `and` `b` are true (using the `and` operator) `or` if `c` is true.  

 

**Operator Precedence**

In the welcome lesson, we typed in the following:

```
5 * 2+3
```

Perhaps we were expecting 25.  We use parenthesis to force operations to take place first.  This also applies towards boolean operators. Note the order of evaluation:

- **`not`** is evaluated before **`and`**
- **`and`** is evaluated before **`or`**
- `A and B or C` means: `(A and B) or C`
- use parenthesis to be explicit: `A and (B or C)`

 

So for the following: `A and (B or C)`

We are evaluating `B` `or` `C` first before applying the `and` operator.

**Making boolean expression** 

Every time you use a comparison test between values you are creating a boolean expression.  Even math _inequalities_ (those that are not testing for equality) are boolean expressions (be sure to type these in):

```
num1 = 5
num2 = 7
test = num1 > num2 
```

```
# true if num1 is greater than num2
print(test) 
```

The following are a few examples of operators that compare values and produce a boolean value:

- greater than:              a  **>**   b 
- less than:                   a  **<**   b
- less than or equal to: a **<=**  b
- equal:                        a  **==**  b

> not equal:                  a  **!=**   b

Determine the output of each print statement, before running this code:

```
a = 5
b = 7
print (a > b)
```

```
print (a < b)
```

```
print (a <= b)
```

```
print (a == b)
```

```
print (a != b) 
```

```
a = True 
```

```
b = False 
```

```
print(a, b, b == a, not b == a)
```

**Simple 'boolean' Functions**

You can easily create a function that returns `True/False` based on a boolean expression by just returning the result of the expression:

```
def is_heavy(pounds):
```

```
  return pounds > 2000
```

**Compound Expressions**

A compound boolean expression is one that involves more than one expression. For example:

```
a = 5
```

```
b = 7 
```

```
print(b > 7 or a > 4)
```

One thing to take note of is that when comparing the same variable to different values you need to **repeat the name of the variable**:

```
a = 6
```

```
print(a > 5 or a < 7)
```

```
print(a > 5 and a < 6)
```

**Boolean Power**

Boolean expressions are used throughout programming.  They control how often sections of code is executed (called looping or iteration) and conditional execution (called selection, conditionals or branching). 


**Lesson Assignment**

**1.** The variables `a,b,c,d,e,f` must be defined by following along this lesson in the editor and running the code.  

**2.**  Read the following situation:

There is a town in which people can run for office if they meet the following two qualifications:

- they are not old
- they like a fun color or they are in middle school

A few definitions:

- old means: their age is over 30
- a fun color is either 'red' or 'green' (color is a string)
- middle school is one of the following grades: 6,7,8

We will model this situation using a function for each boolean expression.  Finish each of the definitions (build a boolean expression that conveys the truth value based on the input):

```
def is_old(age):
```

```
  return 
```

```
def fun_color(color):
```

```
  return 
```

```
def is_middle(grade):
```

```
  return 
```

The function **is_eligible** returns True or False based on the input parameters and the rules set up above.  The function **is_eligible** must call the functions **is_old**, **fun_color**, **is_middle**.

```
def is_eligible(age, color, grade):
```

```
  # you can only call the previous functions
```

```
  # and use boolean operators
```

```
  return ???
```

The following people are running for office:

Bob: age 45, likes green and is 7th grade

Pete: age 12, likes white and is in grade 12

Jane: age 14, likes red and is in grade 11

Determine who is eligible to run for office (e.g.  test your code to determine who is eligible to run for office:

```
#testing code
```

```
bob = is_eligible(45, 'green', 7)
```

```
print(bob)
```

Hints:

- The only variables should be the function parameters. There is NO need to use any variables other than in your test code
- There is NO need to use any `if/else` or selection statements (we don't know about them yet).
- Do NOT use the **`in`** operator (we have not seen that as well).

If you need more help, tag your question on piazza as with **py7**

**Before you go, you should know:**

- the difference between a boolean operator and a boolean expression
- what the major boolean operators are 


all rights reserved
