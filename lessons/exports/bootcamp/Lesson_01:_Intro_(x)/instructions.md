# **A 🐍 Coder's Journey**

**An Introduction to Python Programming**


As mentioned in the last chapter, solving problems with Python (i.e. programming) requires you to write commands (code, statements) for the Python program to execute.  Your code (source code, 'human' code) is translated into something a computer can understand and run (machine code).  When the source code is **parsed** (read and divided into smaller parts) and then evaluated it is usually called interpreted.  JavaScript (a language that runs in browsers and helps make web pages like this one be interactive and animated) is interpreted as well.  JavaScript code is read, parsed, and evaluated each time you "run" JavaScript source code.  

Python is a bit more complicated.  Different implementations of Python will first translate the source code using a compiler into a different representation (called byte code) which is then evaluated. However, for our class we will refer to Python as an interpreted language.

> **Coder's Log:** When a language is translated into a different representation it is called compiled.  If it is parsed and evaluated each time, it is called interpreted.  Most people will say Python is an interpreted language but as we will see later many implementations compile it as well.  For this class we will refer to the Python interpreter as the program/process that executes our Python source code.  Although understanding the differences between interpreters, compilers, transpilers, JIT compilers won't make you a better programmer or data scientist, you should know how to describe Python.

**Logging Our Journey**

Our journey to learning Python would not be memorable if we weren't able to log or print messages. One of Python's built in "commands" is the ability to print (or write) to the console from the code being evaluated.  We do this by writing print.  Go ahead and type the following in the **_editor_** window:

```
print("Welcome to Python")
```

You should see the following in the **_console window_** after you hit the **run** ► button

```
Welcome to Python
```

The Python interpreter will execute your source code from the top down, left to right, when you hit the **run** ► button.  

For the print "command" everything between the matching parenthesis is printed to the console.  In the above example, the string (which is marked by matching quotes) "Welcome to Python" is what is sent to print.

**Multiple messages, one log**

The print function can log many things at once:

```
print("The answer to 2*3 is", 2*3)
```

As long as we separate each message item with a comma, print will write each item out.  Each of those items is called an _argument_ to the _function_ print.  As we move through our journey, we will be learning a lot of vocabulary.  The more you program, the easier this vocabulary will be.

**Quick word on comments**

You can keep code from being executed by prefacing the line with a '`#`' character:

```
#print("The answer to 2*3 is", 2*3)
```

This is called commenting code out.  Everything after the '`#`' symbol is ignored.  We'll discuss it more soon.  

**End of the first lesson.  Introduce yourself:** 

You can finish this lesson by logging to the console a message that includes your first name.  Go ahead and type "Hello I'm <insert your name here>". Do NOT include the < and > symbols and make sure you use only letters.  You must comment out any other print lines as well.

```
print("Hello I'm <insert your name here>")
```

After you type in your code (one line), you can hit **run** ► or select 'run tests' from the drop down menu (on the **run** ► button). 


📷

If you think you understand everything in this lesson, run the tests to see if you pass.  You can hit 'Submit' in the upper right corner. 


If you need more help, tag your question on piazza as with **py1** 


# Before you go, you should know:

- for this class, is Python interpreted or compiled?
- how you log messages in Python


all rights reserved
