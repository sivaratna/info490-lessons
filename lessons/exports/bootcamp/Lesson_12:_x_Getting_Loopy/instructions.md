# **A 🐍 Coder's Journey**

**Lesson 12:  Getting Loopy**


You may have noticed throughout our journey, we have come across examples of writing code where several lines were nearly identical.  For example, in the last lesson we introduced the concept of a list and used a sequence of nearly identical print statements to print out each element:

```
primes = [2, 3, 5, 7, 11, 13, 17, 19]
print(primes[0])
print(primes[1])
print(primes[2])
print(primes[3])
print(primes[4])
print(primes[5])
print(primes[6])
print(primes[7])
```

It is very easy to see the pattern of repetition.  Whenever you need to repeat logic or lines of code or nearly identical lines of code, the **loop construct** can redeem your code.  Almost every programming language (e.g. the language Erlang does not) includes syntax to manage looping (aka iteration, enumeration).  

**For Loops**

The first loop construct we will learn is the **for loop**.
The basic format looks like this:

```
**for** item **in** some_list:
  # for every member in `some_list`
```

```
  # the item is assigned a list item
```

Let's go over the parts of a for loop:

- **`for`** is a keyword in Python used for iteration.  
- `item` is the name of the variable you want to use -- it is assigned a member of the list in each iteration
- `item` is still visible outside of the loop -- it will contain the last value assigned to it
- **`in`** is a keyword
- `some_list` is the name of a list or the list itself (or as we will see later any **iterable** value)
- All the statements inside the loop will get executed each iteration (or time) through the loop.

Here is a simple for loop

```
**for** n **in** [1,2,3]:
```

```
  print(n)
```

**While Loops**

Another basic loop construct is the while loop.  It looks like the following:

```
while loop_test:
```

```
   # run this code as long as loop_test is True
```

- **`while`** is a keyword in Python used for iteration.  
- `loop_test` is a boolean expression that gets evaluated to determine if the loop code gets executed.  
- All the statements inside the loop will get executed each iteration (or time) through the loop.

Here is a simple while loop

```
count = 0
```

```
while count < 10:
```

```
  print(count)
```

```
  count = count + 1
```

**Have we seen this before?**

You may recognize that the `while` loop looks very similar to the `if` statement (conditional branching) we saw earlier.  Both have a keyword, both are controlled by a boolean expression, both delineate the code that gets executed if the boolean expression resolves to true.  However, the loop construct will repeat over and over. 


**Loop and Loop again?**

The boolean expression test for loops is used to determine if we enter the loop as well as if we continue to loop.  As soon as Python encounters the last line of code inside the loop (determined by indentation) execution jumps back to the start of the loop and re-evaluates the boolean expression.  If the expression resolves to true again, the loop is run again.  This process repeats itself forever until the expression resolves to false.


**Operation Code Cleanup**

When you spot a repeated pattern of code use, it's time for using a loop.  Let's rewrite (or _**refactor**_) the example at the beginning of printing out each prime number in the list at the beginning of the lesson.

First using a for loop:

```
for p in primes:
```

```
  print(p)
```

That was easy.  How about using a while loop; try to write out the code needed to use a while loop.

```
idx = 0                     # line 1
**while** idx < len(primes):    # line 2
    print(primes[idx])      # line 3
    idx = idx + 1           # line 4
```

```
# note that idx at the end of the loop 
```

```
# is the same as the length of the primes list
```

On line 1, we need to declare and define a variable that will be used to determine if we should loop or not.  As long as idx is less than the length of the primes list, we will run the code inside the loop.  On line 4, we increment the counter and then jump back to line 2 and re-evaluate the boolean expression: **`idx < len(primes)`**  

**When will we stop?**

In almost all while loops, we will have a statement that updates the variable that affects the loop continuation test.  If we never did, our loop would never end; an endless or infinite loop.  Some programs have purposeful infinite loops, we will try to avoid them. In the above code, **`idx = idx + 1`** is the statement that affects the test.  If we never incremented idx, the loop would never end.

> **Coder's Log**:  Be sure you understand why idx is set to zero and why the stopping condition is idx < len(primes) and not idx <= len(primes).  You can print out the value of idx as well.

**Increment Operator**

`idx = idx + 1` is such a common programming pattern (or idiom) that programmers will shorten to `idx += 1`.  

 

> **Coder's Log**:  In many languages (but **not** Python), this idiom is so common there's even a shorter version (to save typing): `++` .  This `++` operator called the auto increment operator provides a special 'increment by 1' operator: `++` as in `idx++`

**Which Loop Should You Choose?**

In general, you will be using more **for** loops than **while** loops.  If you are aware that you will only need to loop a finite number of times, the **for** loop is usually the answer.  It is more 'Pythonic' to use `for` loops for any type of iteration that is finite and use `while` loops for when looping (perhaps forever) until some condition is met.

**Best of both worlds**

If you find yourself needing to loop through items in a list, but also need to know what index you are at, you can use the `**enumerate**` function which returns both the index and the item in the list:

```
my_items = [1,2,3,'a']
```

```
for i, item in **enumerate**(my_items):
```

```
  print(i, item)
```

There's a lot going on in that syntax.  The function `enumerate` returns the index and the item one at a time from a list of items. Each time through the loop the following invariant is always true:

```
`item == my_items[i]`
```

> **Coder's Log:** an **invariant** is something that should always be true. It's something you can count on.  It's used heavily in software design and testing.

**For loops in other languages**

Many languages that are based on the 'C' language, have for loops like the following:

```
# this is **NOT** Python
```

```
for (i = 0; i < len(primes); i++) {
```

```
   print(is_even(primes[i]));
```

```
}
```

Here the for loop is explicitly controlled by a boolean expression (`i < len(primes)`) and the increment is done after each loop iteration (`i++`).  Setting `i=0` is the loop initializer.  You will come across this kind of syntax in C, C++, Java, Javascript, and many more languages.  The lack of semicolons and curly brackets is one of the things Python improved upon.

**Loop Variables**

It is common to use the names `i,j,k` and `idx` for looping variables that are used as integers that represent the index into the list or array.  In enumeration using a `for` or `enumerate`() syntax, item or the singular noun version of the list variable is usually used:

```
for prime in primes:
```

```
  print(prime)
```

# **Lesson Assignment:**

**Prep with Mod.**

The modulus operator (the `%` symbol) is used to return the remainder after division.  It's a very useful operator to know.  Please experiment with it:

```
print(10%5)
```

```
print(11%4)
```

**1.** Create a function **`is_even`**

```
def is_even(num):
```

```
    # return True if num is even
```

```
    # Hint: you can use the modulus operator (%)
```

**2.** Write another function called **`all_even`** that takes a list of integers and returns `True` if ALL the numbers are even. You must use your `is_even` function.

```
def all_even(list_of_numbers): 
```

```
    # write your code here
```

**Notes**

- returns `True` if all numbers in `list_of_numbers` are even
- `all_even` MUST call your `is_even` function
- if `list_of_numbers` is empty, return `True`

**3.** Write a function called **`count_even`** that takes a list of integers and returns the number of even numbers in the list.  You must use your `is_even` function as well.

```
def count_even(list_of_numbers):
```

```
  # counts the number of even numbers 
```

```
  # in list_of_numbers
```

 

**Notes:**

- you MUST call the same `is_even` function you created

**4.** Test your solutions

Notes:  Solving these will require some thought.  The best way to solve this is to describe the steps (called an algorithm) of how you would solve it as if you were talking to a friend who doesn't know how to code.  Then translate your 'pseudo' code into Python.  You have all the tools you need to solve it.  Please refrain from Google and friends who do know how to program.  Learning to break down a problem so a computer can solve it is a skill that takes time.  Even if you spend hours trying to solve this problem, you will have invested that time towards solving future problems. 

You can ask for help on piazza by tagging your question **py12**.

**Before you go, you should know:**

- what is a loop
- why we need loops
- how do you create a for loop
- how do you create a while loop
- when would you use a while loop
- what does the enumerate function do
- how does the modulus operator work


all rights reserved
