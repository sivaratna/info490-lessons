# **A 🐍 Coder's Journey**

**Lesson 14: List Mutation**

In the previous lesson we saw many different ways to create a list or a container of ordered values.  However, just as important as creating a list is the ability to add, remove, or update items in a list.  For the next couple of sections, we are discussing specifically about the Python `list` type (not sequences in general, the `tuple,` or `string` types)

**List Modification**

We saw earlier that you can use the bracket notation to update items in a list:

```
items    = [1,2,3]
```

```
items[2] = 30
```

```
print(items)
```

```
# double each item
```

```
for i, item in enumerate(items):
```

```
  items[i] *= 2  
```

```
  # *= is the same as 
```

```
  # items[i] = items[i] * 2
```

```
  # note that item is the same as items[i]
```

```
print(items)
```

Remember, the **enumerate** function returns both the index and the item in the list.

**List Appending**

Once you have a list, you can add items to the **end of the list** using the append method:

```
items = [1,2,3]
```

```
items.append(4)    
```

```
print(items)
```

```
# empty list append
```

```
items = []
```

```
items.append(4)    
```

```
print(items)
```

> **Coder's Log:**  Remember a method is a function attached to a type (like lists, strings).  When you create your own types (called classes), the functions that work on that type (or class) are called methods.  

Using `append` is very common for building lists.  In the next example, the function `squared` simply returns a new list by multiplying each element by itself.   Although we will come across easier ways to build lists, this is a very common pattern:

```
def square(numbers):
```

```
  out = [] # start with the empty list
```

```
  for n in numbers:
```

```
    out.append( n * n )
```

```
  return out
```

```
print( square([1,2,3]))
```

Note that in the double example above, we are changing the contents of the list and in this example we are creating a new list.

**List Insertions**

You can use the `insert` method to add items to a **specific index location** in the list (the first parameter to insert is the index, the second is the value):

```
items = [1,2,3]
```

```
items.insert(0, 9) # insert 9 to the front
```

```
print(items)
```

```
items.insert(len(items), 4) # same as append
```

```
print(items)
```

Note that if you use `append` or `insert`, you are adding a complete list as a single element  inside of another list:

```
a = [1,2,3]
```

```
a.append([4,5,6])
```

```
print(a)
```

**List Concatenation**

Sometimes you have two (or more) lists that you want to join together.  You can use the + operator to join them:

```
a = [1,2,3]
```

```
b = a + [4,5,6]  # add another list
```

```
print(b)
```

You can also use the `extend` method to accomplish the same thing:

```
l1 = ['French', 'English', 'German']
```

```
l2 = ['Spanish', 'Portuguese']
```

```
l1.extend(l2)
```

```
print(l1)
```

**Element Removal and Deletion**

Python has several ways to remove list elements.  The method `pop` removes the last element in a list:

```
a = [7,8,9,10]
```

```
top = a.pop() # remove the last element
```

```
print(top, a)
```

You can also use `pop` to remove an element at a specific _index_:

```
a = [7,8,9,10]
```

```
val = a.pop(2) # remove item at index 2
```

```
print(val, a)
```

You can use the keyword `**del**` to delete a specific element in a list:

```
a = [7,8,9,10]
```

```
del a[0]      # remove the first element
```

```
print(a)
```

If you want to delete a specific value, the `remove` method will first search for that value and remove it:

```
a = [7,8,9,10]
```

```
a.remove(9)
```

```
print(a)
```

In general, be careful with remove as it does search the entire list looking for the element to remove.  It can be very slow and expensive.

If you request to remove an item not in the list, Python will generate an error.  You can use the `**in**` keyword as a guard:

```
a = [7,8,9,10]
```

```
if 90 in a:
```

```
  a.remove(90)
```

Finally, you can remove all the items in a list by using the clear method:

```
a.clear() 
```

**Immutable: Tuples and Strings**

The above discussion is for the list type only, in fact if you attempt to change a string or tuple (both are called immutable types), Python will generate an error for you:

```
name = "info 490"
```

```
name[0] = 'I'
```

```
values = (1,2,3)
```

```
values[0] = 100
```

Strings in Python (as well as other languages) are immutable to make memory allocation efficient, allow sharing of the same value, and to make programming easier (we will see why in another lesson).  

**Avoiding Mutation**

You might be wondering why the need for the `tuple` type and its immutability?  Imagine you had a dataset and wanted to calculate some statistics on it.  You certainly wouldn't want some function to remove (or add or update) items to your dataset.  Take a few moments to understand the issues with the following implementation of a function that calculates an average from a list of numbers:

```
def find_average(items):
```

```
  count = len(items)
```

```
  total = 0
```

```
  while len(items) > 0:
```

```
    i = items.pop()
```

```
    total += i
```

```
  return total/count
```

```
dataset = [12,0,7,8,9,34,42,37]
```

```
print(find_average(dataset))
```

After you call `find_average`, you would hope that your dataset is still intact.  Print out its value and take a look.

You can avoid this situation (where you want to ensure that your data is not changed), by passing in a tuple.  As we previously discussed, you can convert lists to tuples and tuples to list easily:

```
my_list  = [1,2,3]
```

```
my_tuple = (4,5,6)
```

```
tuple_from_list = **tuple**(my_list)
```

```
list_from_tuple = **list**(my_tuple)
```

```
print("a tuple", tuple_from_list)
```

```
print("a list", list_from_tuple)
```

We can now protect us from functions that change data:

```
dataset = [12,0,7,8,9,34,42,37]
```

```
print(find_average( **tuple**(dataset) ))
```

**Note:**  Why did this cause an exception?  In this case, if our dataset is important, it's good to know that `find_average` was not able to modify it.

# **Lesson Assignment:**

**Part 1: A better Average** 

- Fix the function `find_average` so that its implementation (the code it uses) does not change or modify the input.  You **cannot** use any Python library to solve this (we don't know about those yet).  You will need to re-write the function.
- Update `find_average` to handle the case where the empty list is passed in or if the parameter is None.  In both situations, you will return the value `None` for an average.

**Part 2: To Celsius**

The following function `fetch_temp_from_internet` returns a random temperature (in Fahrenheit):

```
def fetch_temp_from_internet():
```

```
  import random
```

```
  return random.randint(0,100)
```

We will discuss the `import` statement in another lesson -- it's a way to use other Python libraries.  Copy this code exactly into the editor as it is -- you will not modify this code.

Create a function `batch_convert` which takes in a sequence of temperatures (in Fahrenheit) and returns a new list whose values are the celsius equivalent. In order to convert Fahrenheit to Celsius, you need to subtract 32 from the temperature and multiply by 5/9.  

```
def batch_convert(items):
```

```
  return []
```

```
# should be 0 and 100
```

```
print(batch_convert([32, 212]))
```

You can test a batch of temperatures this way:

```
f = []
```

```
for n in range(0, 10):
```

```
  f.append(fetch_temp_from_internet())
```

```
print(batch_convert(f))
```

Make sure you understand the above code.

**Part 3:**  

The following code is part of a project that keeps a history of temperatures. Its current implementation (`get_temperatures`) calls the function (aka a service)  `fetch_temp_from_internet()` to get the current temperature and then it adds the temperature to a list that it is maintaining:

```
history = []
```

```
def get_temperatures():
```

```
  temp = fetch_temp_from_internet()
```

```
  history.append(temp)
```

```
  return history
```

The problem is that a dubious client (a client is code the calls a library, API, or function) could destroy your service:

```
def dubious_client():
```

```
  get_temperatures()
```

```
  print(get_temperatures())
```

```
  get_temperatures().clear()
```

```
  print(get_temperatures())
```

Be sure to call `dubious_client` so you understand the problem.  Do not move forward unless you are comfortable knowing what the issue is.

- Fix `get_temperatures` so that the data it returns is protected from the above situation.
- Be sure to use/keep the `history` variable. The tests expect it to hold the history of temperatures.
- Be sure to copy the `fetch_temp_from_internet` code into the editor.
- DO NOT change `dubious_client`. 
- Once you fix `get_temperatures`, `dubious_client` should no longer work. You must comment out any call to it before submitting your code:

```
# dubious_client()
```

If you need more help, tag your question on piazza as with **py14**

**Before you go, you should know:**

- how to add, delete, update a list
- which Python types are immutable
- why would you want to use an immutable type


All rights reserved.
