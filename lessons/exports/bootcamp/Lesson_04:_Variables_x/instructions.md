# **A 🐍 Coder's Journey**

**Lesson 4: A variable's way to remember our journey** 


Imagine you are writing a children's poem: 

📷

You then realize that "Dogs" doesn't do the poem justice and decide "Bugs" is better.  A quick substitution in your editor and you're done. Easy.


What if you used Python to write the poem?  The function `print_dog_poem` does just that.  Go ahead and run the program to generate the poem: (you will need to remove the comment symbol)

```
print_dog_poem()
```

However, the word 'Dogs' is _hardcoded_ inside the poem. There's no way to change the poem once you call the function `print_dog_poem()`.


Using a construct called a _variable_ we can set up our poem so 'Dogs' can easily be swapped out for any word without having to rewrite the poem every time. The function `print_poem()` uses a variable named `**noun**` to hold a value "`Dogs`".  


```
1. def print_poem():
2.   noun = "Dogs"
```

On line 2, we are creating the variable named `noun`; `noun` is the name given to a container (i.e. memory location) that holds the value "Dogs".  The `=` sign is used for assignment.  Do not say "equals" say "assignment".

> **Coder's Log:** In some programming languages, you will see a keyword in front of the variable (`var`, for example, in JavaScript:  **`var` `noun` `=` `"Dogs"`**).  Some languages even require you to specify the type (more on that soon) of values the variable can hold: **`String noun` `=` `"Dogs"` or `noun:String` `=` `"Dogs`"**.  Python doesn't have these requirements.  

Go ahead and run `print_poem()` (you should comment out the call to `print_dog_poem()` to avoid too much poem.)


```
# print_dog_poem()
print_poem()
```

Now change the variable noun to hold "Bugs";

```
def print_poem():
  noun = "Bugs"
```

And re-run the code:

```
# print_dog_poem()
print_poem()
```

By changing one line of code, you have a whole new poem! Variables allow you to easily change (or vary) the value.

**Variable Scope**

Note that we are creating a variable _inside_ the function `print_poem`.  That variable (`noun`) can only be used **inside** the function body.  A function's variables are not accessible outside of it.  It allows you to create a function with some privacy-- knowing that other functions can't change the value of your variables.  This concept is called _**visibility**_ or _**scope**_.


**Function parameters are variables too**

We can make the poem generator even more flexible, by allowing the person calling `print_poem` to pass in what kind of noun they want to use.  The 'person' calling a function is called the _client_ (the other people or software using your code) or _caller_.

As we saw earlier, functions can have parameters that can be used inside the function.  These parameters are variables to the function as well. 


Let's create a new function named `print_any_poem`. It is the same as `print_poem`, but uses its parameter as the variable instead:

```
def print_any_poem(noun):
  print("I LIKE")
  print(noun)
  # remaining lines are the same 
  # as printPoem 
  # copy them exactly
```

Now for the fun.  You can print out two different poems easily:

```
print_any_poem("Dogs")
print_any_poem("Bugs")
```

Without variables, programming would nearly be impossible to write, change and would be filled with Bugs¹.

> **Coder's Log:** It's important that you actually type in this code and run it.  Some of the tests will test that you followed directions.  You will **not** receive credit for code that you read but didn't actually type into the editor.

**Sequence and Order**

One of the most basic 'constructs' of programming is the idea that code gets executed from top to bottom, left to right (just like reading English).  This idea of sequential order of execution is one of the 3 'big' ideas of imperative languages like Python.  You can see from the code inside **print_poem** how we need to rely on this fundamental rule of how programming languages work.

> **Coder's Log**: An imperative language is one in which you tell the computer how to solve a problem.  If you know SQL, you tell the computer what you want, not how to do it.  SQL is a declarative language.  

# **Lesson Assignment:**

To complete this lesson do the following:

1. create a function named `who_am_i` with parameters named `major` and `cups`.  You should not create any variables inside this function.

2. inside the function print out the following (but using the values contained in the parameters -- the words in bold should be replaced)

`Your major is **major** and you drink **cups** cups of coffee each day.`

3. create **two variables** outside of the function:

- The first variable will be named **my_major** and will hold a string that describes your major.  
- The second variable, named **my_caffeine,**  holds the number of cups/cans of caffeinated coffee, tea, soda you typically drink in a day (e.g 0,1,2, ...).  You should only use whole numbers.

4. call the function created in #1 using the variables created in #3

**Notes:**

To complete this lesson your function MUST be precise in it's printing.  Note the correct spacing and the trailing '.' at the end of the sentence.  Be sure to comment out the printing of any poems.

As shown in the examples, `print()` can print out multiple values by using multiple parameters.

```
num = 37
```

```
`print("Hello", "My favorite Number is", num)`
```

If you need more help, tag your question on piazza as with **py4** 


# **Before you go, you should know:**

- what a variable is 
- what's the purpose of a variable
- what's the operator for assignment
- note how the lesson makes use of the difference between parameters (major and cups) and the arguments (my_major, my_caffeine).


all rights reserved

1. 'bugs' is a common word to describe the situation where your code doesn't run or doesn't run correctly. 

