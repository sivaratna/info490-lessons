# **A 🐍 Coder's Journey**

# **Lesson 20: Review**

**A look back before moving forward.**

Believe it or not you now know enough Python to start working on larger problems including those common in DataScience.  You also have enough of the vocabulary to start reading other textbooks on programming.  We will add more information on everything you learned in each of the lessons in the second half.

Let's do a quick overview of what you have learned:

**Programming Constructs and Control Structures**
Yikes, this is exactly the verbiage (i.e. gobbledygook) we wanted to avoid.  However it's important to be able to organize our programming knowledge using some of the common technical terms (in case you're asked). You now already know everything that goes into building programs (we just need to fill in some details and get some practice).


**Programming Constructs**

Python is called an _imperative_ language -- meaning you tell the computer how to do something (using statements/commands/code).  All imperative languages (e.g Java, C, JavaScript, etc) have three programming _constructs_ (things that the language allows you to do):


1. Sequential Control Flow
2. Selection
3. Iteration

**Sequential** control flow is just the sequence of programming statements or code that is run.  As we know, Python executes each program from top to bottom, left to right -- just like reading in English.  You saw this in action in the `print_dog_poem()`:

```
def print_dog_poem():
  print("I LIKE")
  print("Dogs")
  print("Black Dogs, Green Dogs")
  print("Bad Dogs, Mean Dogs")
  print("All kinds of Dogs")
  print("I like Dogs")
  print("Dogs")
```

Here each line is executed in order, top to bottom (once you call the function).


**Selection** is just using a boolean expression to determine which code (or code block) to execute.  Also called if-then-else, code choice, conditional programming, decision control.

```
if (income > 20000 and age < 16):
```

```
  tax_rate = 0.05
```

 
**Iteration** is just looping or repeating a group of code statements:

```
for word in sentence:
```

```
  print('e' in word)
```


Since all three determine how the code is run, these programming constructs are also called _control flow structures_.


**Structured Programming** 
Everything else we have seen: functions, variables, expressions (things that compute values) and data structures (lists and dictionaries) are called programming structures.  They provide structure to build and organize your programs.  However sometimes they are also referred to as constructs (so reader beware).


**Major Programming Concepts/Structures:**

- Variables (hold values, read/write to memory)
- Values and types (strings, numbers, lists, dictionaries, tuples)
- Expressions (boolean, mathematical)
- Control Flow

          • Sequence: (in order execution, top down, left to right)

          • Loop statements (for, while)

          • Selection statements (if/elif/else)

- Code organization

       • functions 

       • importing modules

       • objects and classes (coming soon)

Many textbooks start with this kind of overview.  However, to a new programmer, it's easy to glaze over and just be confused.  But now that you have some programming experience, it's a bit easier to think about these terms of your own knowledge.

**A note on classes, objects, and instances**

Everything in Python is an object, we just haven't addressed that fact yet.  For now, everything we do will be based on building functions.  A Python _class_ (a keyword) is a mechanism to build new user-defined types.  Objects are created from classes; they are synonymous with the word instance.  You use the class syntax to create instances of a new type.  Time permitting, we will see how we can take a related set of functions and group them into a construct called an object and will get to creating our own objects when we start writing larger programs.  

**A Look Forward**

In addition to a classroom on DataScience (we will focus a majority of our efforts learning techniques to process text), we will continue our Python learning as well as general software design and best practices with additional classrooms.  And finally the 'Challenges and Projects' classroom that we will use to handle some of larger programming assignments

# **Before you go, should MUST know (this is quiz material):**

- how to write a function
- how to call a function
- what is a datatype
- how to create a list
- how to access a list element
- how to create a dictionary
- how to access a dictionary element
- the operations on strings
- how a function is different than a method
- what are parameters
- what does the return statement do
- how to write a selection statement
- how to loop over a list of items
- how to create a variable
- how to access a variable
- how to pass a variable to a function
- how to assign the result of a function to a variable
- what is a module
- what is an exception
- what does it mean to be immutable
- what data types in python are immutable

# Lesson Assignment:

There is no lesson!!!  Just hit submit to finish out the classroom.  Be sure you know all the answers to the above.  

You can tag any questions with **py20** on piazza.

Before you go, you should know:

- as you start to create larger programs, go back and re-read the relevant lessons if you get stuck on syntax or general concepts. 


All rights reserved

