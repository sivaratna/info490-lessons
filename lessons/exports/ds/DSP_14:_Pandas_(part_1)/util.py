import lesson
import pandas as pd
import numpy as np
import math

def generate_dates(size):
  years  = ['1946','1950','1970','1960','1955']
  months = [x for x in range(1,13)]
  days   = [x for x in range(1,30)]
  dates = []
  y = np.random.choice(years, size)
  m = np.random.choice(months, size)
  d = np.random.choice(days, size)
  for i in range(0, size):
    dates.append('{:02d}/{:02d}/{}'.format(m[i], d[i], y[i]))
  return dates


class PandaTester(object):
  
  def __init__(self):
      # print(pd.__version__)
      self.df = pd.read_csv("data.csv", skiprows = 1, delimiter=';')
      
      self.questions = [lesson.q0, lesson.q1, lesson.q2, lesson.q3,
                        lesson.q4, lesson.q5, lesson.q6, lesson.q7,
                        lesson.q8, lesson.q9, lesson.q10]
      
      self.answers = [901.39, 33, 407.98, 5.14, 
                     2.40, 33.0, 35.12, 10, 
                     4, 434.98, 170.93]
    
  def get_df(self):
    return self.df

  def test(self, df=None, answers=None):
    
    if (df is None):
      df = self.df
    if (answers is None):
      answers = self.answers
        
    correct = 0 
    incorrect = 0
    for idx, q in enumerate(self.questions):
      result = q(df)
      print("Testing {}".format(idx))
      if math.fabs(answers[idx] - result) < 0.01:
        correct += 1 
      else:
        print("wanted", answers[idx], 'vs', result)
        incorrect += 1 
    print("Correct:", correct, "Wrong", incorrect)
    return correct