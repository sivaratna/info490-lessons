# **Data Science & 🐍**

# add this code to the top of main.py

```
# REPLIT FIX 
try:
  import pip
  pip.main(["install", "-r", "requirements.txt"])
except SystemExit as e:
  pass
# END of FIX
```

# Pandas (part 1)

 

The Pandas library is one of the more popular Python libraries for doing data analysis.  You can start using it by importing the library.  It's considered best practice to import it as pd:

```
import pandas as pd 
```

```
print(pd.__version__) 
```

```
# Pandas 1.0 was recently released!
```

**Note:**  The Pandas library is not part of the standard repl.it distribution, hence we need to install it.  That will happen on your first 'run'.  After that it will be installed.  If you get disconnected or have to re-load the browser because of an issue, hitting 'run' will re-install it.

Pandas was inspired by R's fundamental data structure, the `data.frame`.  The latest Panda's release has two fundamental data structures: the Series (a 'list' of data), and the DataFrame (a 'table' of data).  We will focus mostly on the DataFrame.

Historically, Pandas had a data structure named Panel where it got its name:  '**Pan**el **Da**ta'.

One of the easiest way to understand the DataFrame is to think of it as a table of data with rows and columns.  You can also think of the DataFrame as a way to use Python to do everything (and much more) you can do in Excel -- but rather than manually selecting rows and columns and entering formulas, you write code to do it for you.   It's your personal Excel minion.

**Data Frame Anatomy**

The parts of a DataFrame are essentially the same as what we would expect.  Rows are zero index based and columns have labels.  We'll look at each of these features in this lesson.

 

**🎶Example**

Let's start with an Excel spreadsheet and move quickly to Python and Pandas.  Here's an Excel document that shows information on different musicians.

 

You can export the data as a csv file and import it using Pandas:

```
#this is conventional way to import pandas
```

```
import pandas as pd   
```

```
# read in the same data
```

```
df = pd.read_csv('music.csv')
```

Note: the first time you run this, the Pandas module will be installed.  Note that the use of `pd` allows you to keep the pandas syntax (keys typed) short and sweet.  The use `pd` is the conventional way to import pandas.

Pandas also supports reading Excel documents directly (skipping the need to export the data as `.csv` formatted data).  Pandas can even read the data using the URL of a file as well.

The returned object from `read_csv` is a Panda DataFrame.  It's Panda's fundamental data structure.  You can think of it as a table of values that you can access rows, columns, cells, and subsets of the data.

To show the contents you can do the following:

```
# print the first 5 records
```

```
print(df.head(5))  
```

This truncates the data to fit the screen size (you may should expand your window/width).  

You can get all the data via to_string():

```
# print ALL of it
```

```
print(df.to_string()) 
```

One thing you might have noticed is that there is an extra column.  That column is called the index (and thankfully, it's zero based).  Since we didn't tell Pandas to create the index explicitly, it's called an implicit index.

 

**General Information**

You can get the number of rows and columns in a DataFrame by using the `.shape` property:

```
size = df.shape
```

```
rows  = size[0]
```

```
cols  = size[1]
```

```
print(rows, cols)
```

The `.size` property gives you the number of row `*` columns:

```
print(df.size)
```

The row and column counts are available using the `count` method:

```
print(df.count(axis='columns'))
```

```
print(df.count(axis='rows'))
```

**Column Cleaning and Renaming**

A majority of analysis using Pandas will be applying a function (or set of functions) to some selection (e.g. columns, rows, and subsets) of the data.  You can change the names of the columns to make selection easier (i.e. less typing).

```
# change specific ones
```

```
df.rename(columns={
```

```
   'age started playing':'asp', 
```

```
   'wiki page words': 'wpw',
```

```
   'firstname' : 'first',
```

```
   'lastname'  : 'last',
```

```
   'instrument': 'instr'
```

```
}, inplace=True)
```

```
print(df.head(7))
```

Our data now looks like this:  (note if you need to come back to this lesson, you will have to re-execute the rename part of this tutorial)

 

You can also use a function to process the columns names:

```
df.rename(columns=lambda x: 'asp' if x == 'age started playing' else x, inplace=True)
```

This is particularly useful when you need to process the column names that need to be cleaned (e.g. remove whitespace, normalize case, etc):

```
# change all of them
```

```
df.rename(columns=lambda x: x.strip(), inplace=True)
```

```
print(df.head(7))
```

You could just change the headings in the actual data set; however, this is a usually a bad idea since the data could be coming from another source and you don't want to manually change column names (and remember what you did) every time the data is refreshed or updated.

**Column Types**

Although you usually don't want to program to specific types (you strive to be type agnostic), you can see what Pandas is using to store your data:

```
print(df.dtypes)
```

You can change the type of a column as well (see the documentation for `astype`).

# **Selection**

Selecting columns and rows is incredibly easy. Pandas provides various ways to get all the rows of certain columns to all the columns of certain rows (and everything in between).

**Selecting Columns:**

To get a single column we use the dictionary access notation:

```
print(df['first'])
```

In this case we are using the column label much like a dictionary key to access the column values.  Each column is a Panda Series object. 

You can also pass in array of columns to select:

```
print(df[ **[**'last', 'asp'**]** ]) 
```

 

In this case the returned type is a DataFrame.

You can also use the name of the column directly:

```
print(df.wpw)
```

**Selecting Rows**

We can select rows just as easy.  The default is to use integer indices (much like a list).  The returned type is a DataFrame:

```
#select the first 2 rows
```

```
print(df[1:3])
```

**Subsets with `iloc`**

There are two ways to do DataFrame slicing.  One uses integers as indices the other uses labels. 

The `**.iloc**` property is used with **integers indices** and its format is the same as Python slices.  It is zero based and the upper range number is **exclusive** (not inclusive) just like slices.

```
**# iloc slicing**
```

```
**# df.iloc[row_selection, column_selection]**
```

```
print(df.iloc[ 0:3 , 3:4])
```

```
print(df.iloc[ ::2 , [1,3,5]])
```

```
print(df.iloc[-5:  , 1:4])
```

Note the importance of the comma, it separates row section and column selection and it is the cause of visual confusion when you see things like this:

```
print(df.iloc[:,:])
```

```
print(df.iloc[4:,:])
```

```
print(df.iloc[:,])
```

```
print(df.iloc[:])
```

Be sure to understand that syntax, you'll see it a lot in tutorials.  Once again please try to figure out what will be printed, then run the code (by typing it in!)

**Subsets with `loc`**

You can also use the **.`loc`** property to do slicing **using labels**; however the range is **inclusive:**

```
**# loc slicing**
```

```
**# df.loc[row_selection, column_selection]**
```

```
print(df.loc[ '1':'2' , 'first':'asp'])
```

If there is a default implicit index on a DataFrame, the `.loc` property can also work with numbers for the rows (it uses the default index):

```
print(df.loc[ 1:2 , 'first':'asp'])
```

The array syntax also works:

```
print(df.loc[1:5, ["genre", "band"]])
```

As does a mixture:

```
print(df.wpw[1:3])
```

**The Index**

As a reminder, the index is the column zero. In most cases you will want an explicit index on the Dataframe.  It helps both in access and performance if the value of the index is unique (so no rows share the same value for the index).

```
rc = df.shape[0] # row count
```

```
df['id'] = [str(x+11) for x in range(0, rc)]
```

```
df.set_index('id', inplace=True)
```

```
print(df.head())
```

Once you have this index set up, you can use labels for both rows and columns with the `.loc` property:

```
print(df.loc['11':'17':2 , 'first':'asp'])
```

You can even use one of the columns as the index column (once again its best that the values are unique):

```
df.set_index('first', inplace=True)
```

```
print(df.head())
```

 

With this index in place, you can use the labels to match for values in the 'first' column:

```
print(df.loc[ ['john','jon'] , 'last':'asp'])
```

Note you can no longer use the index column in the columns selection part:

```
# this will **not work**
```

```
print(df.loc[ ['jon'], [**'first'**,'last'] ])
```

```
# this will though
```

```
print(df.loc[ ['jon'], ['last'] ])
```

**Boolean Selection (i.e. Filtering)**

Just like in Numpy (Pandas uses Numpy), you can set up filtering based on a boolean condition:

```
plays_guitar   = df['instr'] == 'guitar'
```

```
guitar_players = df[plays_guitar]
```

```
print(guitar_players.head())
```

 

You can also combine different boolean filters using the following operators:

- `& (and)`
- `| (or)`
- `~ (not)`

```
plays_guitar = df['instr'] == 'guitar'
```

```
after_8      = df['asp'] > 8 
```

```
rock         = df['genre'] == 'rock'
```

```
who_am_i = df[plays_guitar & after_8 & ~rock]
```

```
print(who_am_i['last'])
```

**`isin`**

In addition, the `isin` method can be used to help create masks as well:

```
gd_player = df['instr'].**isin**(['guitar', 'drums'])
```

```
print(df[gd_player][ ['first', 'last'] ] )
```

**Functions**

When you select a column (e.g. `df['band']`) the returned type is a Pandas Series.  You can see all the possible functions that Series support here:  https://pandas.pydata.org/pandas-docs/stable/generated/pandas.Series.html

To count the actual contents of the column you can use the `value_counts()` method:

```
print(df['band'].value_counts())
```

```
print(df['band'].value_counts(dropna=False))
```

The Pandas Series also provides a count method.

```
print(df['instr'])
```

```
print(df['instr'].value_counts())
```

```
print(df['instr'].value_counts().count())
```

As with Numpy, Pandas also supports the universal functions (those that operate on an element by element basis):

```
pct = df['wpw'] **/** df['asp']
```

```
print(pct)
```

**Aggregation**

The following aggregation functions are available on any series of data (these work on numerical data columns)

 

(from http://pandas.pydata.org/pandas-docs/stable/basics.html#descriptive-statistics)

There will be a full lesson on descriptive statistics including skewness!

So in order to sum up a column of values, it can easily be done:

```
print(df['wpw'].sum())
```

You can also get the unique values of a column:

```
print(df['genre'].unique())
```

The thing to remember is to know with which Pandas datatype you are working.  Then you can easily find what functions work with the respective type.

**Working with Strings**

We saw earlier that dealing with strings and dates in Numpy is not very straightforward.  With Pandas, each value has a `.str` property with which you can use to perform string functions.  For example, a few of the artists have a trailing space in their first name. You can remove those easily using the `.str` property.  However, we will need to reset the index so we can work on the 'first' column:

```
df.reset_index(inplace=True)
```

```
print(df['first'])
```

Now we can go ahead and process the data in the 'first' column  (note we assign the same column to the result of the the function call):

```
df['first'] = df['first'].str.strip()
```

```
print(df['first'])
```

You can also use the same `.str` propter on column names to clean them up:

```
df.columns = df.columns.str.strip()
```

The same property can be used for other string functions (e.g. `contains` and `len` via https://pandas.pydata.org/pandas-docs/stable/generated/pandas.Series.str.len.html):

```
mask = df['first'].str.contains('on')
```

```
print(df[mask])
```

```
print(df[mask]['first'].str.len())
```

```
mask = df['first'].str.contains('tom|jon')
```

```
print(df[mask])
```

**Dealing with Dates** 

In order to show how to work with dates, let's add a column to our data set that represents the birthday of the artist:

```
df['birthday'] = pd.Series(util.generate_dates(df.shape[0]))
```

```
print(df.head())
```

Even though the birthday field is a string/object, we can easily convert this to a date object:

```
df['birthday'] = pd.Series(util.generate_dates(df.shape[0]))
```

```
df['birthday'] = pd.to_datetime(df['birthday'])
```

```
print(df['birthday'].dtypes)
```

The 'birthday' field is now a `datetime` object.  With that you can use the `dt` field (short for datetime properties) to access things like day, hour, month, dayofweek, year, etc.  See the references at the end of this lesson.

```
df['month'] = df['birthday'].dt.month
```

```
is_early = df['month'].isin([1,2,3])
```

```
print(df[is_early][ ['first', 'last', 'birthday'] ])
```

**Creating Columns**

The above example also demonstrates the ease in which new columns can be created.  In combination with the above example of using a universal function, the output can easily be assigned to a new column:

```
df['pct'] = df['wpw'] / df['asp']
```

```
print(df[ ['first', 'last', 'asp', 'pct'] ])
```

**Dropping Columns**

You can also remove columns from a dataframe quickly by using the drop method:

```
df2 = df.drop(columns=['asp', 'wpw'])
```

```
print(df2.head())
```

There's so much more.  But for now, we will take a break before moving on the Part II which discusses grouping among other topics.

**Before you go, you should know:**

- how to read in a csv file 
- how to select rows 
- how to select columns
- how to create columns

# **Lesson Assignment**

The `data.csv` tab contains the same data that used Numpy: amount spent on various merchants.  The questions for this lesson are also the same.  However, you will need to use Pandas to solve them.

All the code will go in `lessons.py` tab.

**q0:** Find the total amount spent on all merchants.answer: already done for you!

```
def q0(data):
  total = df['Total'].sum()
```

```
  return total
```

You can test it like this:

```
tester = util.PandaTester()
```

```
df = tester.get_df()
print(lesson.q0(df))
```

In order to get a feel for the result of each stage, just print each step:

```
series = df['Total']
print(series)
total = series.sum()
print(total)
```

You can test all the functions using the PandaTester:

```
tester = util.PandaTester()
```

```
tester.test()
```

**q1:** How many transactions were on Restaurants?

**q2:** Find the total spent on Restaurants

**q3:** How much was spent on taxes for Groceries?

**q4:** What was the mean (i.e. average) of all transactions whose total was under 3.00?

**q5:** What is the sum of price on transactions where price was a whole dollar amount (i.e. cents is 0) ?

**q6:** How much was spent total on Merchants whose name is less than 7 characters long?

**q7:** Total number of transactions where the Merchant's name had the word 'Café' in it?

**q8:** How many Merchants have the word 'Café' in them? (i.e. unique set Merchants)?

**Extra Credit:**

You can skip extra credit:

```
SKIP_EXTRA_CREDIT=True
```

**q9:** How much was spent in total during the month of January?

**q10:** How much was spent on Technology during the month of February ?

You must get 8/9 correct if you skip extra credit to pass

You must 11/11 if you do extra credit to pass

**Hints:**

- You must hit run first (at least once) before submitting -- so the Pandas module is installed.
- If you find yourself writing loops or using iteration, you should re-think your solution.  All of these answers can be solved using only Pandas.

You can tag any question with **DSPPandas1** on Piazza 

**Readings and References**

- https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf
- [https://pandas.pydata.org/pandas-docs/stable/whatsnew/v1.0.0.html (hot off the presss !)](https://pandas.pydata.org/pandas-docs/stable/whatsnew/v1.0.0.html)
- [https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.to_datetime.html](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.to_datetime.html)
- [https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.dt.html](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.dt.html)
- [https://pandas.pydata.org/pandas-docs/stable/reference/arrays.html?highlight=datetime%20properties#api-arrays-datetime](https://pandas.pydata.org/pandas-docs/stable/reference/arrays.html?highlight=datetime%20properties#api-arrays-datetime)


All Rights Reserved
