# **Data Science & 🐍**: ngrams

**Prerequisites:** 

- **PT: Regular Expressions (part 1)**

You have now done the data science pipeline process from gathering raw data, transforming it to even creating a simple visualization.  This first round is usually called **exploratory analysis**.  For you, it was mostly about learning the nuances of Python and understanding the different pipeline stages.  We purposely required that you do most of the hard work rather than use libraries to do it.  It's critical that you, the data scientist, are competent enough with your programming skills that you don't feel trapped because some library doesn't do exactly what you need it to do -- they usually don't.  Your programming (and critical thinking) skills are much more valuable than your ability to read documentation.  This class is purposely designed not to be a recipe class. Once you have mastered the basic programming concepts, incorporating new libraries in your workflows is the easy part. 

**Recap**

Right now we have a pipeline that will produce a list of the most frequently used words (`top_n`) in a body of text.  This by itself could be useful when comparing different author's writing styles, genres, music lyrics, etc.  The 'technical' term for what we have built is **a term-frequency table**.  Term frequency measures how frequent a term occurs in a document. Since documents are different in length, it is possible that a term would appear much more in long documents than in shorter ones. Thus, the term frequency is often divided by the document length (aka. the total number of terms or tokens in the document) as a way of normalization.  We will visit this concept again when we learn about **tf-idf** (term frequency inverse document frequency) which is used to evaluate how important a word is to a document.  It helps identify which terms 'separate' the documents from each other.

The term frequency table is also the input to tools that build word clouds where the most frequently used words are displayed in a larger font with bolder colors. This simple technique gets a lot of exposure because it communicates the idea quickly and is more interesting to look at (vs our bar chart).  

 

**A BoW Model** 

When your text analysis drops the context (the order, structure, how the word is used) in which the words were used (like in the above word cloud), it's typically called a **bag of words model**.  This model is very useful to extract features where the analysis is only concerned with whether or not a word was used.  The bag of words is simplified to involve only the following:

1. A vocabulary of words (usually normalized to contain only letters, ignoring case)
2. A measure of presence of known words (e.g. a count)

In this model, we say the feature is each count of the word.

**Improvements**

In our second time around we are not only going to make some improvements to each step of the current process by using more powerful techniques and additional analysis but also to offload some of the coding to libraries.  We can incrementally improve our simple model of analysis by making small, but important adjustments.

**N-grams**

What you have accomplished (without perhaps knowing it), is that you built a language model based on single words called **uni**-grams.  You essentially have a distribution model over single word usage.  This concept can be expanded to include co-occurring words (words that follow each other) called **bi**-grams, sequences of three words that occur together (**tri**-grams)-- and so on.  **N-grams** are basically a set of co-occurring words within a given window.  For example, using the following sentence:

> We went to a clump of bushes, and Tom made everybody swear to keep the
> secret, and then showed them a hole in the hill, right in the thickest part of the bushes. 

We can show the bigrams (the variable N, is usually used and for bigrams, N == 2) for the first few windows:

 

Now if you do that and treat each pair as a 'word', you once again have a distribution over pairs of words. Using these pairs of "grams" along with using the same count analysis (unique keys in a dictionary), we get the following (top 5) results:

```
('in the',     2)
```

```
('we went',    1)
```

```
('went to',    1)
```

```
('to clump',   1)
```

```
('clump of',   1)
```

These pairs can be very useful at extracting information from a text (as you will see).  The N-gram method is very powerful at modeling language with applications in optical character recognition (OCR -- if a word is hard to decode, you can use probabilities based on the previous word or following word, text generation that your predictive keyboard may use (e.g. _You are super nice to see if you have any chance of that the first one¹)_, spell checkers, and so on.  You can even create N-grams over sliding windows of characters (not just the words) to build distributions of likely letter/character pairings and use that to help guess what language is being typed by a user.

**Before you go, you should know:**

- the meaning of term frequency table
- what an ngram is and why it's useful

# Lesson Assignment:  Ngrams

We will build a simple pipeline that does the following:

- reads in the contents of Huckleberry Finn
- tokenizes the contents into 'words'
- builds a list of all bi-grams
- find the most common bi-grams in the text

The above pipeline will be used in other lessons and assignments.  It's not only important to pass the tests for this lesson, but to understand the work flow.  We will use this to eventually find characters in literature.  Be sure to **TEST each function** before moving from one stage to the next.  Do NOT attempt to write all the code and hit 'run tests'.  You should build your own tests and make sure you understand each function before moving to the next one.

**Step 1: Read Text**

Create a function `read_text(filename)` that opens the file and returns its contents.  This will be used to get the contents of Huckleberry Finn (e.g. data.txt).  But don't hardcode the file name, use the parameter.  Be sure to confirm you are reading all the data:

```
text = read_text('data.txt')
```

```
print(len(text))
```

**Step 2: Tokenizing**

Now that we have seen regular expressions, we will use them to extract the tokens for this analysis.  The one issue to discuss is how to handle apostrophes.  Look at the following passage that shows some of the various usages of apostrophes:

_With my sisters' 'ax' we are gonna 'chop off her legs!' I'm afraid; 'fraid I am.  I was scout'n' for a place to take the table to shorten it for the kiddos._

The apostrophe can mark contractions (i.e. omission of letters), possession, the marking of plurals (e.g mind your p's and q's), quoting words within a quotation, marking of irony, and more ([https://www.grammarbook.com/punctuation/apostro.asp](https://www.grammarbook.com/punctuation/apostro.asp)).  Even using an NLP (Natural Language Processing) toolkit to mark the different cases (which would require a lot of computation) will not promise error free results. In an attempt to strike a balance, we will use the following two rules for parsing words and dealing with apostrophes:

- **Part A:** Use the following regular expression to tokenize the text: 

   `['A-Za-z0-9]+-?['A-Za-z0-9]+` 

Note that this regular expression will

- skip single letter words (and numbers)
- not match double hyphenated words (Aunt--Poly) (it will be two matches)
- keep single hyphenated words (e.g. iron-will)
- include the apostrophe in all of its possible uses. 

Make sure you understand why the given regular expression has those limitations.  Be sure you can read and understand that regular expression.

- **Part B:** normalize the tokens

Then for each of the returned tokens (from using the regular expression)

- strip off any leading and trailing **apostrophes**
- keep the internal ones
- do NOT change the case of the word
- you will need to use the powerful string methods you learned in the bootcamp

Note that these rules keep contractions together (e.g. ain't, can't).  And quoted words (e.g. 'food' ) will be the same as the non quoted version.

Use the **two rules** above to implement the following function

```
def split_text_into_tokens(text):
```

```
    return []
```

It must return a list of tokens (in the same order as the text).

Be sure your two functions work together.  Print out tokens, is reasonable?

```
text   = read_text('data.txt')
```

```
tokens = split_text_into_tokens(text)
```

**Step 3: Bi-grams**

Create a function named `bi_grams(tokens)` which returns a list of tuples.  

This function moves a sliding window of size 2 over `tokens` (a list), and creates a new list of bigrams tuples: each tuple has two elements, both of them tokens.

```
def bi_grams(tokens):
```

```
    return tokens
```

You must build this algorithm.  Do NOT use any library or the function `zip` to help you.  Those will come in due time.  It's more important to figure out how to do this (i.e. algorithm development) using the constructs of Python. 

Hint: you will need to use a loop.

Your `bi_grams` function should work similar to this example:

```
text = '''
```

```
it was the best of times
```

```
it was the worst of times
```

```
it was the age of wisdom
```

```
it was the age of foolishness
```

```
'''
```

```
tokens = text.split()
```

```
grams  = lesson.bi_grams(tokens)
```

```
print(grams)
```

 The following would be the output: `[('it', 'was'), ('was', 'the'), ('the', 'best'), ('best', 'of'), ('of', 'times'), ('times', 'it'), ('it', 'was'), ('was', 'the'), ('the', 'worst'), ('worst', 'of'), ('of', 'times'), ('times', 'it'), ('it', 'was'), ('was', 'the'), ('the', 'age'), ('age', 'of'), ('of', 'wisdom'), ('wisdom', 'it'), ('it', 'was'), ('was', 'the'), ('the', 'age'), ('age', 'of'), ('of', 'foolishness')]`

**Step 4: Top N**

For calculating top N lists, much like the previous lesson, we can leverage the Python library collections.  It contains many handy utility functions. Please read `[https://docs.python.org/3.6/library/collections.html](https://docs.python.org/3.6/library/collections.html))`

You can use (but not required) the `Counter` class.  The following are two very handy ways to use the `Counter` class:

```
import collections
```

```
words   = "you are the one you are fun".split()
```

```
counter = collections.Counter(words)
```

```
print(counter.most_common(10))
```

If it necessary to use the count map within a loop, you can add the values one by one as well: 

```
import collections
```

```
words   = "you are the one you are fun".split()
```

```
counter = collections.Counter()
```

```
for w in words:
```

```
  counter[w] += 1
```

```
print(counter.most_common(10))
```

Create a function named `top_n(tokens, n)` which returns a list of tuples where each tuple contains the word followed by its count.  The count is the number of times the token occurs in `tokens`. The parameter `n` is used to get the `n` most occurring tokens.

```
def top_n(tokens, n):
```

```
   return tokens
```

After this is done, the following should work (where tokens is from the previous code example):

```
grams = lesson.bi_grams(tokens)
```

```
top   = lesson.top_n(grams, 3)
```

```
print(top)
```

The output should match the following:

`[(('it', 'was'), 4), (('was', 'the'), 4), (('of', 'times'), 2)]`

**Tom Sawyer**

Before you submit, test your process by processing the text of Tom Sawyer which is in the data.txt tab.  The top 5 bi-grams are:

  `bi-grams     Count`     

`('in', 'the'),  421`

`('of', 'the'),  333`

`('and', 'the'), 310`

`('it', 'was'),  290`

`('to', 'the'),  233`

That's not too useful.  We will fix that in the next lesson.

You can tag any question on Piazza with **ngrams**.


All Rights Reserved

_¹That is literally the sentence that was created by selecting the suggested word after typing You._
