import matplotlib.pyplot as plt

#
# simple charting library
#

def name_generator(preface='ex'):
  import os 
  count = 1
  while(True):
    name = "{}{}.png".format(preface, count)
    if os.path.isfile(name):
      count = count + 1
    else:
      print("file is ", name)
      return name
  
def make_simple(x,y):
  fig, axes = plt.subplots()
  axes.axhline(y=2.0)
  axes.axvline(x=10.0)
  plt.savefig(name_generator('simple'))
  
def make_scatter(d):
  # since it's a collection of points
  # order does not matter
  x = d.keys()     # 2,3,4 .. 11, 12  (unordered)
  y = d.values()
  fig, axes = plt.subplots()
  axes.scatter(x,y)
  plt.savefig(name_generator('scatter'))
  
def make_lines(x,y ):
  # need the x,y pairs in order
  fig, axes = plt.subplots()
  axes.plot(x,y)
  plt.savefig(name_generator('line'))
  
def make_bar(x,y):
  # need the x,y pairs in order
  fig, axes = plt.subplots()
  axes.bar(x, height=y)
  plt.savefig(name_generator('bar'))
  
  # VS axes.bar(x,y) ???
  #show barh(x,y)

def make_histogram(elements, centered=True):
  fig, axes = plt.subplots()
  low  = min(elements)
  high = max(elements)
  bins = high - low + 1
  
  if centered:
    bins = [i-0.5 for i in range(low,high+2)]
  
  axes.hist(elements, density=True, bins=bins)
  plt.savefig(name_generator('histogram'))
  
  
def make_area(x, y):
  # need the x,y pairs in order
  fig, axes = plt.subplots()
  axes.fill_between(x,y)
  plt.savefig(name_generator('area'))
  