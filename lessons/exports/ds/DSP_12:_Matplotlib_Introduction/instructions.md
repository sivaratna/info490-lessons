# **Data Science & 🐍: Matplotlib, the basics, Part 1**

 

# **Matplotlib: The Basics**

Matplotlib is one of the oldest and most complete libraries for building data visualizations in Python.  Knowing the basics of matplotlib will help you use many of the other libraries that use it directly or wrap convenience functions around it (e.g. seaborn, pandas, ggpy, scikit-plot).  At some point the libraries that use matplotlib won't give you enough control and you end up needing to know how to use matplotlib anyway.

The big problem with matplotlib is that it is so BIG and so flexible that it's hard to learn -- there's plenty of excellent tutorials available but the issue is that each tutorial presents a different way to build charts and graphs.  A previous lesson introduced two different styles for creating visualizations, this lesson will continue along the path of using the object-oriented (OO) approach where there is not a reliance on state sharing (called the matplotlib pyplot interface)  This comes at the cost of needing to write a bit more code; however, the code is more explicit (you're not relying on hidden state) and presents a uniform approach to building visualizations. Even matplotlib advises using the OO interface:

 

source: https://matplotlib.org/tutorials/introductory/lifecycle.html and https://matplotlib.org/api/api_overview.html

**Static vs Interactive**

Perhaps one of the least appealing aspects of using standard matplotlib is that the visualizations are static.  You can't interact with them; there's no animation -- the library was built to create publish-quality graphics.  However, the popularity of online notebooks has pushed the development such that there are now options to convert a matplotlib figure into HTML/Javascript as well as create animations.  

Since matplotlib's API is so popular, other web-based animation libraries support a matplotlib-like interface as well.  There's plenty of web-first visualization libraries (e.g. D3, React-vis, Raphael, Chart.js, ) that we can explore later.  Some libraries like mpld3, Plotly, bokeh bring the worlds of Python, matplotlib, and Javascript together.  The effort required to learn matplotlib will be worth it even if you end up creating only web-based visualizations.

**Starting From One.**

Matplotlib was written by someone who used MATLAB and wanted similar functionality in Python (the `pylab` module was built to mimic the MATLAB global style -- which we are trying to avoid).  Another carry over is that MATLAB uses 1-based indexing so many things in matplotlib start at one and not zero (this fact only affects those using the pyplot, shared state interface).

Since matplotlib is a graphics library there is a separation of the user created code that uses matplotlib (called the frontend) from the actual user interface (e.g. GUI) called the backend -- the output.  The backend is responsible for taking the matplotlib internal representation of a graph to different platforms (windows, unix, jupyter notebooks, etc) to actually render the plots.  In some cases, the backend will provide a toolbar to work with the created visualization.  For the most part, you will not need to reconfigure the backend.

You can see what the backend is being used by calling the following:

```
import matplotlib
```

```
print(matplotlib.get_backend())
```

If you need to change or modify the backend, you must do it before you do anything else (including importing other matplotlib modules).

> **Coder's Log:** When you write client/server code, the user-facing product (i.e. client-code, a GUI, a user interface, web-page) is usually called the frontend and the backend refers to the server code.  

To bring matplotlib into your codebase you usually just import matplotlib's pyplot library:

```
import matplotlib.pyplot as plt
```

It is standard convention to alias `matplotlib.pyplot` to `plt`.

**A Hierarchy of Objects**

Every graph starts out by creating a figure:

```
fig = plt.figure()
```

```
print(type(fig))
```

A matplotlib **figure** is the user interface.  It's a container that can hold multiple plots.  You can create multiple figures, but the usual convention is that you build multiple subplots inside a figure.

The figure function can take different parameters to configure it's size (among other things see: https://matplotlib.org/api/_as_gen/matplotlib.pyplot.figure.html):

```
fig = plt.figure(figsize=(6, 4), facecolor='green')
```

The figure holds one or more subplots.  However these subplots are called **axes**.

 

source: https://realpython.com/python-matplotlib-guide

The axes object (a plot) holds all the elements of a visualization including the x and y axis, lines, labels, legends (you can also attach a legend to a figure), etc:

 

source: matplotlib.org

Since it's so common to need to access the figure and subplots (i.e. axes) there is a convenience function to create both using the `plt.subplots` function:

```
fig, axes  = plt.subplots()
```

```
print(type(fig), type(axes))
```

If you need multiple subplots within a figure you can do the following:

```
fig, axes = plt.subplots(nrows=1, ncols=2)
```

```
print(type(axes))
```

```
fig, axes = plt.subplots(ncols=2, figsize=plt.figaspect(1.0/2.0)) #twice as tall 
```

However, now `axes` is a list/array of axes.  Specifically it is an NumPy `ndarray` of AxesSubplot objects.  Like many libraries, matplotlib also uses Numpy for many of implementation details.  Numpy is not needed for this lesson, but be aware that knowing Numpy will help you with matplotlib.  Pandas is built on top of both matplotlib AND additional numpy features.  So mastering these libraries will make you a very effective Pandas user and developer.

We will look at using multiple subplots in a future lesson.  Each axes (yes it's plural) is a plot that you can access the x and y axis and build your visualization.  

You can also create the figure and the axes independently using the `add_subplot` function:

```
fig, axes  = plt.subplots() 
```

```
# IS SAME AS
```

```
fig  = plt.figure()
```

```
axes = fig.add_subplot(111)
```

The reasoning behind the `111` parameter will be discussed in further detail in the second part (111: 1 row, 1 col, subplot position 1).

We will focus on a single unified way to build a figure via the OO API.  There are also a multitude of functions that use the active state to create and manipulate figures (e.g. `plt.subplot()`). The flexibility of the library is what makes learning it so difficult -- there's so many ways to solve a problem!  The decision to use the OO API and a few of the specific functions will allow you to write explicit code and more or less follow the recommended/best-practices way to use matplotlib

Also, as a reminder, writing explicit code is better than relying on implicit state.  If you see `plt.<SOMETHING>` (other than for creating subplots and showing them), you're seeing the **non**-object oriented (pyplot) interface.  

**Showing a Figure**

Once you are finished building your figure, you do the following:

```
plt.show() 
```

```
# make a figure window appear (shows all figures)
```

But you will notice (well you should have noticed), that in the repl.it environment nothing happens (the backend's implementation doesn't do anything).  Instead, we will save the figure to a file (called the non-interactive mode of matplotlib) using the `plt.savefig()` method:

```
fig, axes = plt.subplots(nrows=1, ncols=2)
```

```
plt.savefig('ex1.png')
```

Once we start using interactive Python (e.g. notebooks), `plt.show()` will indeed display the figure.  However, in a notebook environment, matplotlib is usually configured to be in interactive mode, so there's no reason to call show.

Since the `savefig` has it's own set of parameters, you may need to set some of the parameters explicitly (rather than assume `savefig` will use the information from the figure) see https://matplotlib.org/api/_as_gen/matplotlib.pyplot.savefig.html

```
fig, axes = plt.subplots(nrows=1, ncols=2)
```

```
fig.set_facecolor('green')
```

```
plt.savefig('ex2.png', facecolor=fig.get_facecolor())
```

**Simple Plots By Example**

Let's go over some very basic chart types.  Mastery of the simple allows you to build more complex visualizations.  You will begin to see some patterns emerge.

In the tab `data.py`, is a custom type that contains a simple data set:  a roll of two dice done multiple times.  You can access this data as follows:

```
import data
```

```
DICE = data.Dice(10000) # roll it 1000 times
```

We will use the same dataset for all the visualizations.

**Scatter Plot**

The simplest graph to produce is a plot of x,y values.  

 

The following creates a scatter plot:

```
import chart
```

```
def make_scatter(d):
```

```
  x = d.keys()     # 2,3,4 .. 11, 12  (unordered)
```

```
  y = d.values()   # times that sum was rolled
```

```
  fig, axes = plt.subplots()
```

```
  **axes.scatter**(x,y)
```

```
  plt.savefig(chart.name_generator('scatter'))
```

To create a scatter plot of the dice rolls:

```
make_scatter(DICE.roll_map)
```

**Repl.it Issues**

One thing to note, is that since repl.it won't write over existing files, we created a simple function to automatically return a new name to use.

```
chart.name_generator('scatter')
```

If your repl.it session creates so many new visualizations (i.e. new tabs),  you can just do a full refresh of the page.

We will soon learn how to add labels and make the graph more visually appealing, but for now we are going to focus on the basics.

**Line Plots**

For line plots, it's important that your data is ordered -- since the lines are drawn connecting adjacent points.

 

For line graphs, you need two parallel arrays: one that represents the x values and another for the y values:

```
def make_lines(x,y):
```

```
  # need the x,y pairs in order
```

```
  fig, axes = plt.subplots()
```

```
  **axes.plot**(x,y)
```

```
  plt.savefig(chart.name_generator('line'))
```

```
make_lines(DICE.x, DICE.y)
```

**Vertical and Horizontal lines**

To make simple vertical or horizontal lines there are the convenience functions `axhline` and `axvline`.  See `charts/make_simple` for an example.  These are perfect for augmenting a chart.

**Bar Charts**

 

Bar charts use a similar type of data as the line chart, however, you need to specify the width of the bars.  By default,  matplotlib will do this for you.  In the case of showing dice rolls, the y values (the number of times the sum (represented by x)) will be the height values.

```
def make_bar(x,y):
```

```
  fig, axes = plt.subplots()
```

```
  **axes.bar**(x, height=y)
```

```
  plt.savefig(chart.name_generator('bar'))
```

```
make_bar(DICE.x, DICE.y)
```

**Histograms**

Closely related to bar charts are histograms.  A histogram typically visualizes a distribution of numerical data -- usually an estimate of a probability distribution for a continuous variable.  You need to specify either the number of bins you want to divide your data into or a list of bin 'edges'.

 

For making histograms, you need the raw elements that you will be plotting

```
def make_histogram(elements):
```

```
  fig, axes = plt.subplots()
```

```
  low  = min(elements)
```

```
  high = max(elements)
```

```
  bins = high - low + 1
```

```
  
```

```
  **axes.hist**(elements, bins=bins)
```

```
  plt.savefig(chart.name_generator('hist'))
```

```
make_histogram(DICE.rolls)
```

To make it into a probability density function, you specify density to be `True`:

```
  axes.hist(elements, density=True, bins=bins)
```

```
  plt.savefig(chart.name_generator('hist'))
```

Note that the argument for `bins` is an integer in this case.

In order to center the data, you can specify a **list of bin edges** to use:

```
  bins = [i-0.5 for i in range(low,high+2)]
```

```
  axes.hist(elements, density=True, bins=bins)
```

```
  plt.savefig(chart.name_generator('hist'))
```

Here bins is a list of values.  We simply subtract 0.5 from each bin to essentially move the x axis labels.

**Area Charts**

There's not much difference between a line chart and an area chart other than the space under the line is filled in.  It helps communicate trends rather than specific values.

 

```
def make_area(x, y):
```

```
  # need the x,y pairs in order
```

```
  fig, axes = plt.subplots()
```

```
  **axes.fill_between**(x,y)
```

```
  plt.savefig(name_generator('area'))
```

```
chart.make_area(DICE.x, DICE.y)
```

Each plot type has unique configurations (formal parameters).  The only way to harness matplotlib is to always consult the documentation for each chart type.  For example, documentation for the histogram method on the axes is available via

https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.hist.html

Be careful when you google 'matplotlib histogram'.  There's many different ways to generate a histogram.

**Choosing Jupyter?**

This lesson is intended to be done inside of repl.it.  However, you can also do the coding inside a new Jupyter (Google collab) notebook if you want.  You will need to do the following:

- copy all the code inside `data.py` into a new cell and run it
- copy all the code inside `chart.py` and run it
- replace `plt.savefig( .... )` with `plt.show()` in the notebook.  The `show` method may not do anything, but it will suppress a bunch of printing
- any reference to `chart.<something>` you can remove the prefix
- any reference to `data.<something>` you can remove the prefix

**Before you go you should know:**

- what a figure is
- what an axes is
- the basic chart types

# **Lesson Assignment.**

All this great knowledge will be used in a notebook project rather than a replit assignment.  Be sure you understand the code and read some of the basic matplotlib resources.  Simply hit submit when you are comfortable with matplotlib basics -- there are no tests for this lesson.

You can tag any question with **MatPlot** on Piazza.

All Rights Reserved


**Readings and References**

- https://matplotlib.org/Matplotlib.pdf
- **cheatsheet:**  Warning this is the non object oriented API but similar functions exist using the OO API: https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Python_Matplotlib_Cheat_Sheet.pdf 
