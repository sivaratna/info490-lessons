
import numpy as np
import sales

def question0(sales_data):
  # how much A3 Paper was sold in the NorthEast
  
  # get the NE sales data
  north_east = sales_data[sales.Location.NE]
  
  # get the totals for all the products
  ne_sales_total = np.sum(north_east, axis=0)
  
  # just return the A3 total
  return ne_sales_total[sales.Product.A3PAPER]

  
def question1(sales_data):
  return None

def question2(sales_data):
  return None
  
def question3(sales_data):
  return None


#
# extra credit
#
def question4(sales_data):
  return None

def question5(ds, jh):
  return (None, None)

def question6(ds, jh):
  return None