# **Data Science & 🐍: n**umPy: Part 2

Numerical Analysis: The higher dimensions

**Prerequisites:**

- NumPy Part 1

This is the second part of the NumPy series.  

**Not so Slow**

When we first learned about Python list comprehensions we saw that we could easily extract the even numbers from a list:

```
py_list  = list(range(0,10))
```

```
py_evens = [x for x in py_list if x%2 == 0]
```

```
print(py_evens)
```

The same thing in Numpy would be:

```
np_num  = np.arange(0,10)
```

```
np_even = np_num[np_num%2 == 0]
```

```
print(np_even)
```

So that may seem like a lot to learn for a small syntactical gain.  However, we are still only in one dimension.  As the array's size and/or dimension increases, Python will require you to write more looping code, become harder to read, and the execution time slows down. NumPy was written to offer fast execution to complex calculations.

**Dimensional Goodness**

The power of NumPy comes when you need to handle arrays of multiple dimensions.  A single dimensional array is just a list of numbers:

```
x1 = np.array([1,2,3,4,5,6])
```

```
print(x1)
```

A two dimensional array is just a list of lists of numbers.

```
x2 = np.array([ [1,2,3], [4,5,6]])
```

```
print(x2)
```

Numpy uses the `axis` parameter to give you control over how you want to use manipulate the data.  By default the axis parameter is usually set to zero:

```
print(np.sum(x1))
```

```
print(np.sum(x1, axis=0))
```

See if you can figure out (guess at this point) what the following will do:

```
print(np.sum(x2))
```

```
print(np.sum(x2, axis=0))
```

```
print(np.sum(x2, axis=1))
```

Note that the axis parameter cannot go beyond the dimensions of the data. In the x1 example, the following will generate a runtime exception;

```
print(np.sum(x1, axis=1))
```

Here's the same shown in a diagram:

 

 **Note** 

- you can think of **axis 0** as collapsing the rows to get the sums
- you can think of **axis 1** as collapsing the columns to get the sums

**Dimensional Functions**

In addition to the sum function, the same aggregate functions shown before (e.g. mean, min, max, etc), also are available for arrays in higher dimensions. You need to specify which dimension (using the `axis` parameter) you want the aggregate to be applied to.  

Just to reinforce this concept, here's another example to consider:

```
data = [
```

```
  [1,  2, 3],  # row sum: 6
```

```
  [4,  5, 6],  # row sum: 15
```

```
  [7,  8, 9],  # row sum: 24
```

```
  [10,11,12]]  # row sum: 33
```

```
# -----------
```

```
# 22, 26, 30   column sums
```

To get the sums for all the **_rows_**:

```
print("row sums", np.sum(data, **axis=1**))
```

 

The `axis` parameter is used to tell the function on **which dimension it should collapse** so it can then operate on the remaining vector of values.  The row dimension is `axis` `0` and `axis` `1` is the column dimension (for 2D matrices).

Based on that and the example above, it seems unintuitive.  However, you pass in the `axis` that you want to collapse into the functions.  Since we want row sums, we want the column axis (e.g. `**axis=1**`) to 'collapse':

To get the sums for all the _**columns**_, you need to collapse the row dimension:

```
print("col sums", np.sum(data, axis=0))
```

 

**Thinking in 3D**

Let's do the same but now in **_three_** dimensions:

```
x1 = np.array([ [1,2,3], [4,5,6], [7,8,9]])
```

```
x2 = x1 * 2.0
```

```
x3 = x1 * 3.0
```

```
x = np.array([x1, x2, x3])
```

```
print(np.sum(x, axis=0))
```

```
print(np.sum(x, axis=1))
```

```
print(np.sum(x, axis=2))
```

Before running that code, see if you can determine the answers --- it will help you understand what's going on.

 

When working in three dimensions, `**axis=0**` will 'cut' through the space as if the matrices are stacked on top of each other. Be sure you understand what is going on before moving on.

For `**axis=1**`, numPy will sum by collapsing the rows of each dimension:

 

For `**axis=2**`, numPy will sum by collapsing the columns of each dimension.  Take note of how the columns become the rows of the resulting matrix:

 

As you can see, it becomes a bit complicated how to interpret the axis parameter in high dimensional space (more than two).  None the less, you can see the power of how the same function can be applied to different interpretations of a dataset.

**More Axis**

To further illustrate the `axis` parameter, let's look at the NumPy function to create cumulative sums of array elements:

```
a = np.array([[1,2,3], 
```

```
              [4,5,6]])
```

```
print(np.cumsum(a))
```

```
# [ 1  3  6 10 15 21]
```

sum over rows for each of the 3 columns

```
print(np.cumsum(a, **axis=0**)) 
```

```
# array([[1, 2, 3],
```

```
#        [5, 7, 9]])
```

sum over columns for each of the 2 rows

```
print(np.cumsum(a, **axis=1**))      
```

```
# array([[ 1,  3,  6],
```

```
#        [ 4,  9, 15]])
```

These are incredibly useful functions, so it's important to understand how the `**axis**` parameter works.

**Operators**

The universal functions also work in multiple dimensions as well:

```
data = np.array([
```

```
  [0,1,2], 
```

```
  [3,4,5],  
```

```
  [6,7,8],  
```

```
  [9,10,11]]) 
```

```
print(2 ** data) # power of 2 table
```

For index operations (shown in the previous lesson) on higher dimensional arrays, the array is flattened down to a single dimension first:

```
even_large = data[(data%2 == 0) & (data>5)]
```

```
print(even_large)
```

If that's incomprehensible, please go back and read through the previous lesson.  You can start to imagine the difficulty (and slowness) of doing that using only Python lists and loops.

**Reshaping**

We saw in the single dimensional case, a `np.array` is just a list of values.  However, you can **shape** this list into either a column vector or a row vector:

```
d43 = np.arange(1,13)   
```

```
print(d43.size, d43.shape)
```

```
print(d43.reshape(1,12)) # 1 row,   12 columns
```

```
print(d43.reshape(12,1)) # 12 rows, 1 column
```

In theThinking in 3D example, using both `arange` and `reshape`, it's easy to create numPy arrays:

```
x1 = np.arange(1,10).reshape(3,3)
```

```
x2 = x1 * 2.0
```

```
x3 = x1 * 3.0
```

You can use the reshape method to create complex arrays easily:

```
#old way
```

```
data = np.array([
```

```
  [1,  2, 3], 
```

```
  [4,  5, 6],  
```

```
  [7,  8, 9],  
```

```
  [10,11,12]]) 
```

```
#easier
```

```
d43 = np.arange(1,13).reshape(4,3)
```

```
print(np.array_equal(data, d43))
```

When you reshape a lower dimensional array to a higher one, you need to make sure you have the right number of elements:

```
a9   = np.arange(1,10) 
```

```
m3x3 = a9.reshape(3,2) # ERROR !!!
```

**Misc Numpy Utility**

Numpy provides many utility functions to help manage and manipulate arrays.  If you need a specific data manipulation, chances are there's one available.

For example, sometimes its easier to grab components from a flattened (1-D) array.  You can always use the `np.ndarray.flatten` method:

```
`a =` `np.array([(1,2,3,4),(3,1,4,2)])`
```

```
`print(a.flatten())` 
```

If you ever need to operate on all the values in a high dimensional array, `flatten` makes things easy without needing to worry about using the axis parameter.  

```
d43 = np.arange(1,13).reshape(4,3)
```

```
print(d43)
```

```
print(np.sum(d43.flatten()))
```

If you see a reference to `ravel` it's similar to `flatten`, but is more space efficient.  

**There's SO MUCH to learn!!**

 

Each of the libraries we will learn (numpy, pandas, matplotlib, scipy, etc) have a huge number of APIs (function calls) to know that it's literally impossible to know them all.  You usually remember the ones you end up using over and over.  However, always take time to read (and search) the documentation to see if there's a function that exists that covers what you are trying to do.  Don't get demoralized in thinking you need to memorize everything.  Every time you use something, it becomes part of your repertoire and you slowly build on your knowledge base. 

The power of numPy lies beneath matplotlib, pandas and many machine learning libraries.  It's worth your time to understand the syntax and semantics of the library.

**Before you go you should know:**

- how to reshape data
- what the axis means in Numpy
- how the axis parameter is used in NumPy functions (np.cumsum, np.sum, etc)
- how to flatten a high dimensional array and why its useful

# **Lesson Assignment**

 

**Analyzing Sales Data**

In the `sales.py` tab is the sales data for a paper company over the past quarter.  

There are 4 sales regions:                    NorthEast, NorthWest, SouthEast, SouthWest

There are 5 people of the sales team:  Dwight, Jim, Todd, Andy, Michael

There are 4 products that are sold:      A3 paper, A4 paper, A3 Card Stock, and Copy Machines

You can view this data by the following:

```
import sales
```

```
print(sales.sales_data.shape)
```

 

As you can see the sales data is stored in a numpy array that has 4 regions (slices), 5 people (rows), 4 product types (columns).  Be sure to understand both the data in `sales.py` and the above diagram.

Each 'item' given above is indexed starting at 0.

So to see northeast sales data you would slice off the first dimension of the data set:

```
north_east = sales.sales_data[0]
```

```
print(north_east)
```

However, using numbers as indices can be a bit hard to read, so inside `sales.py` are some custom data types to help make the code more readable:

```
north_east = sales.sales_data[**sales.Location.NE**]
```

```
print(north_east)
```

**For example, let's answer the question: In the north east region, how many sales did Dwight make?**

```
dw_sales = north_east[sales.People.DWIGHT]
```

```
print(np.sum(dw_sales))
```

Make sure you can visualize why that works.  The diagram above should help.

**How much A3 paper was sold in the north east?**

```
print(np.sum(north_east, axis=0)[sales.Product.A3PAPER])
```

Of COURSE, that last line is bit tough to parse (especially if you didn't do all the examples in the lesson).  

Unfortunately much of the code you will come across will look like the following:

```
print(np.sum(sales.sales_data[0], axis=0)[0])
```

```
print(np.sum(sales.sales_data, axis=1)[0][0])
```

The ONLY way that's ever going to make sense is for the following to happen:

1. you understand the format of the data (the size and shape of `sales_data`)

2. you understand how numpy `axis` and aggregate functions work

3. you understand how to access into arrays, matrices, columns and rows

4. you're willing to draw and diagram it out.

**STOP**

If you go through each of the above examples using the diagrams as a guide, you can see how this very obtuse looking syntax becomes manageable.   Don't guess, understand.  

This assignment will be unsolvable unless you understand (well, it's not that bad) how the numPy syntax works.

In `lesson.py`, fill out each question.  

- You should only need numpy statements, not much python.  
- You SHOULD solve each using a few lines of code rather than one giant statement.
- Do NOT return hard coded numbers
- All questions can be done (even the extra credit) using a sequence of Numpy functions.  So no need to use loops, selection statements (if/else). 

**Q0: How much A3 paper was sold in the north east?**

Yea, it's already done for you.  

Try to model your solutions after this one.  The data being past in will be slightly modified to what is in `sales.py.`  You can easily test your functions by doing the following:

```
import lesson
```

```
import sales
```

```
ans = lesson.question0(sales.sales_data)
```

```
print(ans)
```

In no case, should you just return a number.  Your code must use numpy statements.

**Q1:  How many total units were sold by the sales team (all products, all regions)?**

```
def question1(sales_data):
```

```
  return None
```

**Q2:  How many total units were sold by Jim in the North East region?**

```
def question2(sales_data):
```

```
  return None
```

**Q3:  How many copy machines did Dwight sell (all regions)?**

```
def question3(sales_data):
```

```
  return None
```

**Extra Credit:**

If you want to skip the extra credit add these lines:

```
skip_extra_credit = True
```

```
skip_bonus = True
```

**Q4:  In the south, which product was sold the most?** 

Your answer will be a number that represents a product index (e.g. 0 is the same as `sales.Product.A3Paper)`.  

```
def question4(sales_data):
```

```
  return None
```

Remember, no loops or selection statements.

**Q5: How many sales calls did it take for each to hit 25 products sold?**

There was a contest between Dwight and Jim for who could sell the most units.  

Each was given 100 sales calls to sell any product.  

That data is given in `sales.py` as well (see the variables `dwight100` and `jim100`).  

You can use Numpy's `tolist` function (to convert a numpy array to a Python list) OR you can (and should) use Numpy's `where` function (see previous Numpy lesson) for a complete Numpy only solution.

Your function will return a tuple.

- The first value of the tuple is the number of calls it took Dwight
- The second value of the tuple is the number of calls it took Jim

```
def question5(ds, jh):
```

```
  return (None, None)
```

You can test it like this:

```
ans = lesson.question5(sales.dwight100, sales.jim100)
```

```
print(ans)
```

**Bonus:** (just for the sake of fun and learning)

You can skip the bonus question by adding the following code:

```
`skip_bonus = True`
```

Now that you know who won the sales contest.  Find the point at which the winner gained the lead in the contest and never lost it again.  

For this question, you ARE welcome to solve it using both Python and Numpy (no restrictions) if you want.

```
def question6(sales_a, sales_b):
```

```
  return None
```

```
ans = lesson.question6(sales.dwight100, sales.jim100)
```

```
print('At sales call number', ans, ' the contest was over')
```

Note that the answer should be the same regardless of the order of arguments.

```
ansA = lesson.question6(sales.dwight100, sales.jim100)
```

```
ansB = lesson.question6(sales.jim100, sales.dwight100)
```

```
print(ansA == ansB)
```

Congratulations! 👏 You can now use numpy operations to 'easily' do data analysis.   I hope you can see the cost of learning the library and its syntax balance out the huge time gains when you need to automate data analysis. You can tag any question with **NP2** on Piazza. 


All Rights Reserved

**Readings and References:**

- https://hackernoon.com/introduction-to-numpy-1-an-absolute-beginners-guide-to-machine-learning-and-data-science-5d87f13f0d51
- https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Numpy_Python_Cheat_Sheet.pdf

