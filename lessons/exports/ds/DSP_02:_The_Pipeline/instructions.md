# **Data Science & 🐍** 

# **Introduction to the Data Science Pipeline**

The data science process starts with a problem to solve, a question to answer, an insight or a trend to discover within a specific context (e.g. the domain).  It is necessary to understand how data is transformed in order to be useful as it moves from its raw state into a usable format.   The transformation usually occurs in steps and each step requires code to be written (or the use of libraries). The different stages or steps of this transformation is known as a pipeline.  For this class you will be introduced each of the stages of data science pipeline using an example or inquiry to drive our learning.  You will also use the pipeline framework to introduce new programming tasks and Python mechanics as well to practice the fundamentals of programming.

In summary, here are the pipeline steps (or stages) for each data science problem (the five C's): 

Step 1: Understand the **Context** of the problem (i.e. what is the problem?, who's involved?)

Step 2: **Collect** the data (i.e. acquire, locate, fetch/read)

Step 3: **Clean** the data (i.e. prepare it for computation and analysis)

Step 4: **Compute** the data (e.g. explore, visualize, analyze, etc)

Step 5: **Communicate** the results; build an action plan; make a decision

The steps are highly cyclical, you may have to go back to any step based on your findings in the current step.  Sometimes, the initial round (or entire pass through the pipeline) is named **exploratory** data science (a visualization is a simple example of exploratory data science) and subsequent rounds, **data modeling**. Data modeling is an overloaded word with many definitions.  You can think of a data model as a representation (i.e. model) of the data it is supposed to be describing.  Data modeling is an attempt to build an algorithm that captures the essence of the problem being solved.  The simple line equation (y = mx + b) is a data model that models (in this case perfectly) a line (the output y) with a specific slope (m) given an input (x) and offset (b).  In other areas (like machine learning), the model is so complex that it's nearly impossible (or indeed impossible) to explain how (and sometimes why) it works.

For each example presented, we will describe what needs to be done at each stage; we won't spend too much time on the first stage other than to recognize that in complex situations it will be necessary to do additional research on the domain so that you understand the perspectives and people involved (and perhaps know who's funding the research and the potential conflicts of interests).  Sometimes you will drive this process, other times it will be your job to accept the task as given to you.  For this class, we will state the problem or inquiry to solve.  However, you will still need to recognize the overall goal and the context in which the problem lies.  

Below is a simple (very simple) picture of the data science pipeline. Other classes, blogs, or textbooks might exclude or have different names for the stages, but the five 'C's gives you an easy way to learn (and talk about it) and cover almost everything that's needed to move raw data to information and action.

**The Five C's Data Science Pipeline:**

 

When you think of some of the Python tools and libraries that will be used, here's how some of them map to the three main stages (for our purposes):

**Collect:**

This stage is mostly about reading or fetching (remote reading) the data.  It's all about being able to **access the data**.  For some examples we can just set a variable to the dataset, in others we can use the native open and read functions in Python. At some point we will use libraries like pandas and numpy that provide reading capabilities as well.

**Clean:**

This stage is taking the raw data from the previous stage and parsing it into a usable, reliable and valid format for downstream processes.  It can include tokenization (splitting up the data), normalization (standardizing input, correcting for errors, etc) and validation as well.  Unfortunately this stage usually consumes the most time and has been given the names of **data preparation**, wrangling, munging.  You will spend a significant amount of time on understanding and using regular expressions to process (and analyze) the data.

**Computation:**

This is the stage where all the fun is. You will be using visualization libraries like matplotlib, seaborn, altair and others. But even before creating the 'output', you may have to do a wide variety of numerical or statistical computations and modeling.  If the previous stage did all the hard work, this stage can always make assumptions about its input, making development go faster.

Although understanding the **Context** and **Communicating** the results don't necessarily use Python, being familiar with knowing what you can do will inform the context stage and being able to explain the results will provide a better communication stage. 

**Before you go, you should know:**

- What are the 5 steps to the Data Science Processing Pipeline
- What is the main characteristic(s) of each stage/step

# **Assignment:**

Let's mimic the pipeline with a very simple problem:  given any sentence, your task is to find which letters are missing from the sentence.  For example the sentence, "The quick brown fox jumped over the lazy dog" is missing the letter 's'.

The following functions

- `fetch_data()                     [Collect]`
- `normalize()                      [Clean]`
- `find_missing_letters()           [Compute]`
- `find_missing_letters_algorithm() [Compute]`
- `visualize()                      [Compute` 

                             `and Communicate]`

correspond to some step in the pipeline (other than the first).  Other than `fetch_data()` and `visualize()` all of them need to be implemented.  All the code should go in the `lesson.py` module.  Use `main.py` to test your code (and don't rely on just the built-in tests).

**1.**  Implement the following functions 

`def normalize(input_string):`

- input:  a string of text characters. Each character is called a token.
- output: an array of 'letters'.  Each item in the array corresponds to a letter in `input_string` (in lowercase)
- for example, the input `"O.K. #1 Python!"` would generate the following list:  `['o','k','p','y','t','h','o','n']`
- take a look at the Python documentation for string functions (url given below); **don't** try to replace, delete, or substitute -- if the 'token' is a letter it passes the test.

`def find_missing_letters(sentence):`

- the input sentence is an array/list (created from `normalize()`)
- returns a sorted list of letters that are NOT in the sentence
- use the built in Python type `**set**` to solve (see previous lesson)

 

`def find_missing_letters_algorithm(sentence):`

- the input sentence is an array/list (created from `normalize()`)
- returns a sorted array of letters that are NOT in the sentence
- you must **NOT** use the `set` type

Hints for `find_missing_letters_algorithm:`

- create your own algorithm and use **only** the data types (other than `set`) that we have discussed
- the `set` datatype is removed during the running of the tests (so don't use it)
- the output should be the same as that from `find_missing_letters` (easy to test)
- if you find yourself writing over 30 lines of code, you are probably on a path that's making things more complicated than necessary. Think first about how you would solve this problem and how you would describe solving the problem to a friend.  Once you think you know how to explain the steps to solve the problem, your task is to write the Python syntax to implement your solution.  There are many ways to solve the problem.
- `https://www.digitalocean.com/community/tutorials/an-introduction-to-string-functions-in-python-3`
- `http://www.datasciencemadesimple.com/remove-spaces-in-python/`
- `https://docs.python.org/3.6/library/string.html` has some useful constants (but not required for the solution)

Pipeline example:

```
ex = "Bob went to the market and bought an zaxaphone"
```

```
print(find_missing_letters(ex))
```

```
# output:
```

```
['c', 'f', 'i', 'j', 'l', 'q', 's', 'v', 'y']
```

```
ex = "The quick brown fox jumped over the lazy dog"
```

```
# the output:
```

```
['s']
```

**2.** implement the function **`pipeline()`,** which pipes the output of each step in the pipeline process into the next stage.  For example, if you had the functions a1, b2, c3, pipeline would just do something like `return c3(b2(a1()))`.  

- The answer should read (inside out) and be a single line of code.  
- You can use either `find_missing_letters_algorithm` or `find_missing_letters` since both return the same output.
- Your final stage should be calling `visualize` (already done)

Notes:

- `pipeline` returns the result of running the pipeline.  Hence, every stage in the pipeline needs to return data.  If the stage doesn't modify the data, it just returns the input.  This is a fundamental principle of many data processing frameworks.

Once you pass the tests, move to the next lesson.  You can tag any questions with **DSPipeline** on piazza


All rights reserved

