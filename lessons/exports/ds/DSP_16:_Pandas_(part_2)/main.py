# REPLIT FIX for SPRING 2020
try:
  import pip
  pip.main(["install", "-r", "requirements.txt"])
except SystemExit as e:
  pass
# END of FIX

import numpy as np
import pandas as pd 
import string
import matplotlib.pyplot as plt

import dateutil
pd.options.display.float_format = "{:,.2f}".format

print(pd.__version__)
df = pd.read_csv("data.csv", skiprows = 1, delimiter=';')