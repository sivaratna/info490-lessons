# **Data Science & 🐍:** Pandas (part 2)

# add this code to the top of main.py

```
```

```
try:
```

```
  import pip
```

```
  pip.main(["install", "-r", "requirements.txt"])
```

```
except SystemExit as e:
```

```
  pass
```

```
# END of FIX
```

# **Data Analysis: GroupBy and Aggregation**

 

This lesson continues our learning of the Pandas library. For this lesson we will use the same data set (`data.csv`: which shows amount spent on various merchants) as the previous lesson.

With Pandas we can read in the same data file easily changing the delimiter, skipping any initial rows that may have comments:

```
df = pd.read_csv("data.csv", skiprows = 1, delimiter=';')
```

```
print(df.head(5))
```

**Grouping**

We also saw the following aggregation functions in the previous lesson:

 

As an example:

```
print(df['Type'].count())
```

```
print(df['Total'].mean())
```

These provide a quick summary of all the rows in the DataFrame.  However, sometimes you want to get sub totals of different sets or groups of data.  Pandas has a groupby method (much like an SQL Group By statement) that produces a **`DataFrameGroupBy`** object.  You can treat this very much like a DataFrame:

```
grp = df.groupby(['Type'])
```

```
print(type(grp))
```

To print out this group, you _**could**_ do this:

```
for key, item in grp:
```

```
    print(grp.get_group(key), "\n\n")
```

However it's much easier to use one of the aggregates that you want information about the group:

```
print(grp.mean())
```

 

This creates a very quick pivot table in Pandas (although there is way to create detailed pivot tables in Pandas -- which we can discuss in a subsequent lesson).  However, the type is actually a Series:

```
print(type(grp.mean())
```

**Resetting the Index**

If you look carefully at the result, you notice that the header consists of two rows

```
         `Price       Tax     Total
Type`                                     
```

Also, the 'Index' is now

`['Apparel', 'Grocery', 'Restaurant', 'Technology']`

The columns are:

`['Price', 'Tax', 'Total']`

You can convert the result of the operation to a DataFrame, by resetting its index using the method `reset_index` that assigns a proper index and column headers.

 

```
`g_f2 = grp.mean().reset_index()`
```

```
`print(g_f2)`
```

```
`print(g_f2.index)`
```

```
`print(g_f2.columns)`
```

Note: you can format the output to 2 decimals with this cryptic line of code:

```
pd.options.display.float_format = "{:,.2f}".format
```

When we go over string formatting, that command will be readable.

**Which columns?**

Take a look at these two statements and the columns in the resulting dataframe:

```
print(df.groupby(['Type']).mean())
```

```
print(df.groupby(['Type']).count())
```

Some of the functions work only on columns whose values are numeric.  Pandas is smart enough to filter out the non numeric columns for you

What's really great is that the type returned from an aggregation on a group is another DataFrame.  So you can do all things you already know:

```
df_mean = df.groupby(['Type']).mean()
```

```
print(type(df_mean))
```

```
print(df_mean['Price'])
```

Note that order of how you select the columns and apply the aggregate does not matter:

```
a = df.groupby(['Type']).mean()['Price']
```

```
b = df.groupby(['Type'])['Price'].mean()
```

```
print(a.equals(b))
```

**Group Size**

Once you have a group, you can get the sizes of each of the groups using the size() method:

```
grp = df.groupby(['Type'])
```

```
print(grp.size())
```

The returned value from `.size()` is a Pandas Series.  So you can get the number of elements in the group using the `.size` property (see previous lesson):

```
grp = df.groupby(['Type'])
```

```
print(grp.size().size)
```

**Back to DataFrames**

As mentioned previously, you can convert this series to a DataFrame by resetting the index.  You can also name the index so you can access the column values using labels (as we saw earlier):

```
a = df.groupby(['Type']).size()
```

```
b = df.groupby(['Type']).size().reset_index(name='count')
```

```
print(type(a), '\n', a)
```

```
print(type(b), '\n', b)
```

However, if you `reset_index` with `inplace=True` and set `drop=True`, it remains a series (setting a name on a Series index is not necessary or valid):

```
sa = df.groupby(['Type']).size()
```

```
sa.**reset_index**(inplace=True, drop=True)
```

```
print(type(sa))
```

Of course, once you have your DataFrame, you have full access to all its great features:

```
c = df.groupby(['Type']).size().reset_index(name='count')
```

```
more_than_30 = c['count'] > 30 
```

```
is_tech      = c['Type'] == 'Technology'
```

```
print(c [ more_than_30 | is_tech ])
```

**Sorting and Top n**

Getting the top N values of a DataFrame is also a matter of finding the right method(s) in the documentation:

```
c = c.**sort_values**('count', ascending=False)
```

```
c = c.**nlargest**(2, 'count')
```

```
print(c)
```

**Apply(ing)**

Pandas makes it easy to apply a function to different parts of the data within a DataFrame.  The tricky part is knowing whether you're dealing with the DataFrame, a Series, or Group.  How you apply to those objects will be slightly different.

Let's creates some subsets to work with the examples (you could also use `.iloc` property to grab specific rows and columns (e.g. `subset = df.iloc[0:11]`):

```
# dataframes
```

```
usd   = df.drop(columns=['Type', 'Date', 'Merchant'])
```

```
names = df.drop(columns=['Price', 'Tax', 'Total'])
```

```
# series
```

```
total = df['Total']
```

```
# groupby
```

```
grp   = df.groupby(['Type'])
```

```
print(type(usd), type(total), type(grp))
```

**Apply to the DataFrame** (by columns)

With the DataFrame apply, you can apply a function to each of the values in the dataset.  Now let's convert the three dollar-related columns from USD to CAD:

```
def usd_to_cad(x_s):
```

```
  return x_s*1.33
```

```
cad = usd.apply(usd_to_cad)
```

```
print(cad)
```

The above essentially applies the function to the column series.  There's a bit more going on here.  The parameter to **`usd_to_cad(x_s)`** is a Series (once for each column [and an additional time to test if an optimization can be done] ).  Each Series is then using a universal function (the `*` in this case) to each value in the series.  

**Apply to the DataFrame** (by rows)

The default of apply is to operate on a Series of column values, however, if you want to apply a function to rows, you need to set the `axis` parameter:

```
def together(row):
```

```
  m_type = row[0] # row['Type'] will work too
```

```
  merc   = row[1] # row['Merchant']
```

```
  
```

```
  name = m_type[0:5] + ':' + merc[0:5]
```

```
  # convert to title case
```

```
  return name.title()
```

```
df['abbr'] = df[['Type','Merchant']].apply(together, **axis=1**)
```

```
print(df['abbr'])
```

Note: if `axis=0`, the function would be called twice, once for each column of values.

Remember if you wanted to use the new 'abbr' column label, you would need to reset the index.

Here's the comment from the documentation: ([https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.apply.html](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.apply.html))

`_DataFrame.apply(func, axis=0, broadcast=None, raw=False, reduce=None, result_type=None, args=(), **kwds)[.](http://github.com/pandas-dev/pandas/blob/v0.23.4/pandas/core/frame.py#L5837-L6014)_`

> Apply a function along an axis of the DataFrame. Objects passed to the function are Series objects whose index is either the DataFrame’s index (`axis=0`) or the DataFrame’s columns (`axis=1`).

This may seem a bit confusing, until you read the specific comment for the axis parameter:

`_axis : {0 or ‘index’, 1 or ‘columns’}, default 0_`

> Axis along which the function is applied:
> 0 or ‘index’: apply function to each column.
> 1 or ‘columns’: apply function to each row.

**Apply to a Series**

In a Series, apply is passed the individual values:

```
def floor(x_s):
```

```
  return int(x_s)  # each item
```

```
  
```

```
total = df['Total']
```

```
print(total.apply(floor))
```

Here's the comment from the documentation:

> `Series.apply(_func_, _convert_dtype=True_, _args=()_, _**kwds_).` Invoke function on values of Series. Can be ufunc (a NumPy function that applies to the entire Series) or a Python function that only works on single values

**Apply to a Group**

For group apply, the function is passed a DataFrame:

```
df = pd.read_csv("data.csv", skiprows = 1, delimiter=';')
```

```
def simple(x_df):
```

```
  # x_df is a dataframe being passed in
```

```
  # that represents each group
```

```
  return x_df.size
```

```
 
```

```
grp = df.groupby(['Type'])
```

```
print(grp.apply(simple))
```

Here's the comment from the documentation:

> `GroupBy.apply(_func_, _*args_, _**kwargs_).` Apply function `func` group-wise and combine the results together. The function passed to `apply` must take a dataframe as its first argument and return a dataframe, a series or a scalar. `apply` will then take care of combining the results back together into a single dataframe or series. `apply` is therefore a highly flexible grouping method.

**Aggregation**

We already have used aggregation functions, however, there is a `.aggregation` method (also `.agg` for short) to which you can pass an aggregation function.  Once again, it's important to know on what Pandas object you are aggregating:

**Aggregation on DataFrames:**

```
print(usd.sum())
```

This is equivalent to using the `agg` method

```
print(usd.agg('sum'))
```

**Aggregation on Series:**

```
total = df['Total']
```

```
print(total.agg('sum'))
```

**Aggregation on GroupBy:**

```
print(grp.agg('mean'))
```

You can even do aggregation on multiple columns:

```
print(grp.agg(['sum', 'mean']))
```

You can rename the output columns as well: 

```
#drop Date and Merchant columns 
```

```
#only to make printing it managable
```

```
`df1 = df.drop(columns=['Date', 'Merchant'])`
```

```
`grp = df1.groupby(['Type'])`
```

```
`r1 = grp.agg(['sum', 'nunique'])`
```

```
`r2 = r1.rename(columns={'nunique': 'uniq', 'sum': 'tot'})`
```

```
`print(r2)`
```

```
           Price        Tax       Total     
              tot uniq   tot uniq    tot uniq
Type                                         
Apparel    238.72    4 21.49    4 260.21    4
Grocery     57.13    2  5.14    2  62.27    2
Restaurant 374.29   30 33.69   27 407.98   30
Technology 156.82    2 14.11    2 170.93    2
```

You should visit the relevant documentation for more details:

- https://pandas.pydata.org/pandas-docs/stable/generated/pandas.**DataFrame.agg**.html
- https://pandas.pydata.org/pandas-docs/stable/generated/pandas.**Series.agg**.html
- http://pandas.pydata.org/pandas-docs/stable/generated/pandas.core.groupby.**DataFrame`GroupBy.agg`**.html

Sometimes it's difficult to figure out if you should use an apply or an aggregation.  Usually, `aggregation` is used to reduce a set of values to a single entity and `apply` is used when you need to touch the individual values.

**Chaining methods**

As you solve problems and research Pandas, you may come across code like this (and this is a short one):

```
answer = df.groupby(['Type', 'Merchant']).size().nlargest(5).reset_index(name='top')
```

```
print(answer)
```

Each of the Pandas methods returns an object and that returned object can be operated on as well.  When you come across code like the above, be assured that the author spent a lot of time with the individual pieces before gluing them together in confusing an almost incoherent (yet supposedly impressive) chain. Don't be impressed -- they're hard to read and reason with unless you're well versed in the api. 

**Filtering Groups**

We have seen how to use masks to filter a DataFrame:

```
is_tech = df['Type'] == 'Technology'
```

```
print(df[is_tech])
```

The following example was used earlier showing how to use masks with groups and do more advanced filtering:

```
c = df.groupby(['Type']).size().reset_index(name='count')
```

```
over_30 = c['count'] > 30 
```

```
is_tech = c['Type'] == 'Technology'
```

```
print(c [ over_30 | is_tech ])
```

The GroupBy object has a `.filter()` method that you can use with a custom function.  The function receives a DataFrame and must return a single boolean indicating to keep or not keep the group.  The two methods `.any()` and `.all()` can be used to produce a boolean value based on the intermediate arrays that are created with the boolean masks:

```
def pass_me(x):
```

```
  over_30 = x['Type'].count() > 30
```

```
  is_tech = x['Type'] == 'Technology'
```

```
  return (over_30 | is_tech)**.all()**
```

```
result = df.groupby(['Type']).filter(pass_me)
```

```
print(result.groupby(['Type']).size())
```

Both DataFrames and Series have a `.filter` method, but each works very differently (you don't pass it a function).

**Recap:**

There's a lot to know in this lesson, but grouping is one of the fundamental tasks of using Pandas. The main thing to remember is that Pandas has three primary objects: DataFrames, Series, Groupby.  And what you can do will depend on those objects.  Visit the documentation often:

- [https://pandas.pydata.org/pandas-docs/version/1.0/reference/frame.html](https://pandas.pydata.org/pandas-docs/version/1.0/reference/frame.html)
- [https://pandas.pydata.org/pandas-docs/version/1.0/reference/series.html](https://pandas.pydata.org/pandas-docs/version/1.0/reference/series.html)
- [https://pandas.pydata.org/pandas-docs/version/1.0/reference/groupby.html](https://pandas.pydata.org/pandas-docs/version/1.0/reference/groupby.html)

**Before you go, you should know:**

- how groupby works
- how aggregation works
- realize that learning Pandas has a _shallow_ learning curve (e.g. it takes a long time)¹

# **Lesson Assignment.**

For this semester, you can breath a little easy; we removed all the questions for this lesson.  But it is important to know how Pandas operates. 

HOWEVER, the Pandas project will use many of the concepts taught in this lesson.


All Rights Reserved

¹[https://difficultrun.nathanielgivens.com/2014/07/16/steep-learning-curve-doesnt-mean-what-you-think-it-does/](https://difficultrun.nathanielgivens.com/2014/07/16/steep-learning-curve-doesnt-mean-what-you-think-it-does/)

Be sure to download the cheat sheets and print them out to study over the summer

**Readings and References**

- https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf
- https://www.webpages.uidaho.edu/~stevel/504/pandas%20dataframe%20notes.pdf
- https://s3.amazonaws.com/assets.datacamp.com/blog_assets/PandasPythonForDataScience.pdf
- https://ugoproto.github.io/ugo_py_doc/Pandas_DataFrame_Notes.pdf

# **Addendum**

**Display Options**

- to print out the full width of columns:

```
pd.set_option('display.expand_frame_repr', False)
```

**Plotting**

Since we don't have time to see how visualizations work, here a quick glimpse into the ease in which you can plot data with Pandas.  Pandas puts a simpler API on top of Matplotlib.  You can also mix using the two libraries with no issues.

Plotting a graph is just as easy:

```
# plot the average transaction Price 
```

```
df = pd.read_csv("data.csv", skiprows = 1, delimiter=';')
```

```
pt = df.groupby(['Type']).mean()
```

```
chart = pt['Price'].plot(kind='bar', 
```

```
                         fontsize = 8, rot=-45)
```

```
chart.get_figure().savefig('price.png')
```

 

However, there's no need to think "why in the world did we spend so much time with matplotlib?" 

Almost ALWAYS, you end up needing to go down to the matplotlib level to adjust the graph.  But to get a quick graph done (exploratory data science), pandas is great.

