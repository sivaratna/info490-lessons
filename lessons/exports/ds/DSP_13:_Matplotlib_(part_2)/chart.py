import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def show():
  if matplotlib.get_backend() == 'agg':
    # replit
    plt.savefig(name_generator())
  else:
    #IPython
    plt.show()
  
def name_generator(preface='ex'):
  import os 
  count = 1
  while(True):
    name = "{}{}.png".format(preface, count)
    if os.path.isfile(name):
      count = count + 1
    else:
      print("file/tab is ", name)
      return name
