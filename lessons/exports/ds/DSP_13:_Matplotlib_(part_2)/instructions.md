# **Data Science & 🐍: Matplotlib, Part 2**

# **Annotations and Subplots**

**Prerequisites:**

- Matplotlib Part 1
- NumPy Part 1 and 2

This lesson will work in both replit and in a Jupyter/Colab notebook.  The assignment is to hand in a notebook on gradescope that has the exercises in it, but you can run all the examples here as well.

For this lesson, you should create a new collab notebook (name it matplotlib2)  And add the code inside `chart.py` to the first code cell and run it.  For each example, create a new code cell to easily follow along.  Feel free to experiment as well.  And remember, actually typing in the code will push it in your memory -- copy & paste will not.

# **Annotating Charts: Plotting x¹ x² x³**

In the previous matplotlib lesson, we went over the basic chart types and built bare minimum graphs.  Let's start simple and keep adding to it

**The Data**

For the next section we will use numpy to generate 21 values from 0 to 5 (equally split) and generate x¹, x² and x³ graphs for each of the 21 values.   The y values -- the output of the function (i.e. _**f**_x¹, _**f**_x², _**f**_x³) are x, x squared, and x cubed:

```
# the x values
```

```
x = np.linspace(0, 5, 21) 
```

```
# the y values
```

```
fx1 = x
```

```
fx2 = x ** 2
```

```
fx3 = x ** 3
```

Setting up the data using Numpy makes it very efficient and user friendly (numpy is so great!).

**Annotating Plots**

Let's start out with a very basic chart (f(x) = x) but force the line color to be blue and add a title to the graph:

```
def chart_v1(title, x, y):
```

```
  
```

```
  fig, axes = plt.subplots(nrows=1, ncols=1)
```

```
  axes.plot(x, y, **color='blue'**)
```

```
  axes.**set_title**(title, fontsize=12)
```

```
chart_v1("x¹", x, fx1)
```

```
show()
```

You can see (by clicking on the newly created tab) that the line is colored blue and has a title with a 12 point (pt) fontsize.  Also, the title is actually being set on the subplot/axes.  If you want to set the title on a figure use the `suptitle` method (see references).

The `plot` method is very versatile.  You can plot multiple graphs with one call:

```
def chart_v1b(title, x1, y1, x2, y2):
```

```
  
```

```
  fig, axes = plt.subplots(nrows=1,ncols=1)
```

```
  axes.plot(x1,y1, x2,y2)
```

```
  axes.set_title(title, fontsize=12)
```

```
chart_v1b("x¹ x²", x, fx1, x,fx2)
```

```
show()
```

We will strive to make the plotting function more generic by allowing the caller to pass in any number of plots.  You can plot multiple lines on the same axes (i.e. subplot), by either calling `plot` multiple times, or by passing in an array of values.  However, when you pass in an array of values, **each column** is considered the data set.  We can use `np.transpose` to get the data in the correct shape:

```
# Data Transposition:
```

```
dataset = [fx1, fx2, fx3]
```

```
f_x     = np.transpose(dataset)
```

```
all_x   = np.transpose([x,x,x])
```

```
def chart_v2(title, x, y):
```

```
  fig, axes = plt.subplots(nrows=1,ncols=1)
```

```
  axes.plot(x, y, color='blue')  # with explicit x values
```

```
  #axes.plot(y, color='red')     # without x values
```

```
  axes.set_title(title, fontsize=12)
```

```
chart_v2("x¹ x² x³", all_x, f_x)
```

```
show()
```

A few notes, the x axis values can be dropped but matplotlib may not get the x-axis correct.  Also, if the same x values are used for all the y graphs, you can just pass in a single array:

```
chart_v2("x¹ x² x³", x, f_x)
```

```
show()
```

**Different plots, Different colors**

If you want a different color for each line, you could plot each line separately and use the color parameter.  In `chart_v1b`, the colors are chosen by a default cycle of colors.  You can build your own property `cycler` to set a custom cycle of colors for your plots:

```
def chart_v3(title, x, y):
```

```
  fig, axes = plt.subplots(nrows=1,ncols=1)
```

```
  
```

```
  colors = ['red', 'green', 'blue']
```

```
  axes.set_prop_cycle(color=colors)
```

```
  axes.plot(x, y)
```

```
  axes.set_title(title, fontsize=12)
```

```
chart_v3("x¹ x² x³", all_x, f_x)
```

```
show()
```

**Adding a Simple Legend**

 

For making a legend, you need to get a handle to the elements that you want in the legend.  All the graphing commands return the elements created -- which can be used by the `legend` method.  You also need to provide labels as well (a simple array will do):

```
def chart_v4(title, x, y):
```

```
  fig, axes = plt.subplots(nrows=1,ncols=1)
```

```
  
```

```
  colors = ['red', 'green', 'blue']
```

```
  axes.set_prop_cycle(color=colors)
```

```
  **plots** = axes.plot(x, y)
```

```
  # legend
```

```
  axes.set_title(title, fontsize=12)
```

```
  lbls = title.split()
```

```
  **axes.legend(handles=**plots**, loc=**'upper left'**, labels=**lbls**)**
```

```
  # add a grid background
```

```
  axes.grid(True)
```

```
chart_v4("x¹ x² x³", all_x, f_x)
```

```
show()
```

In the example, it assumes the title can be split into an equal number of parts.  You can also attach a legend to the figure as well.  The example also shows how to add a simple background grid to your graphs.

**Axis Labels and Tick Marks**

As a small touch you can easily add labels to each of the x and y axis using the `set_[xy]label` methods.  Also, you have complete control of how the tick and label markers appear.  In the example below, we have double the tick marks, set at 45 ° angle:

```
def chart_v5(title, x, y):
```

```
  fig, axes = plt.subplots(nrows=1,ncols=1)
```

```
  
```

```
  colors = ['red', 'green', 'blue']
```

```
  axes.set_prop_cycle(color=colors)
```

```
  plots = axes.plot(x, y)
```

```
  # legend
```

```
  axes.set_title(title, fontsize=12)
```

```
  lbls = title.split()
```

```
  axes.legend(handles=plots, loc='upper left', labels=lbls)
```

```
  # add a grid background
```

```
  axes.grid(True)
```

```
  axes.set_xlabel('x')
```

```
  axes.set_ylabel('f(x)')
```

```
  
```

```
  # add tick marks at each interval + 1/2 interval
```

```
  x_count = int(np.ma.size(x, axis=0))
```

```
  x_min = np.amin(x)
```

```
  x_max = np.amax(x)
```

```
  ticks = np.linspace(x_min, x_max, int(x_count/2.0))
```

```
  labels = ["{:0.2f}".format(i) for i in ticks]
```

```
  axes.set_xticks(ticks)
```

```
  axes.set_xticklabels(labels, fontsize=12, rotation=-45)
```

```
chart_v5("x¹ x² x³", all_x, f_x)
```

```
show()
```

# **Multiple SubPlots**

With matplotlib, its also easy to work with different subplots within a figure to create very flexible visualizations.  We are going to generate a figure with 3 subplots, all in the same 'row':

```
fig, axes = plt.subplots(nrows=1, 
```

```
                         ncols=3, figsize=(10, 4))
```

 

It's important to note that axes is an numpy array of `AxesSubplot` and you can access each axes/subplot by using standard index notation.

```
print(type(axes), type(axes[0]))
```

**First Chart:**

```
def chart_s1(chart, x, y, title=''):
```

```
  chart.plot(x, y, label=title)
```

```
  chart.set_title(title, fontsize=12)
```

```
chart_s1(axes[0], x, fx1, 'x¹')
```

```
show()
```

You can see (by clicking on the newly created tab) that the figure now has 3 subplots.  We only drew into the first one.  

# **=== Jupyter/Colab Notebook Note ===**

Once a plot is shown in a notebook, the figure is no longer active.

```
print(plt.fignum_exists(fig.number))
```

So you need to **repeat the creation** of the figure each time:

```
**fig, axes = plt.subplots(nrows=1,** 
```

```
                         **ncols=3, figsize=(10, 4))**
```

```
chart_s1(axes[0], x, fx1, 'x¹')
```

```
show()
```

In this lesson, we will not explicitly repeat that line of code.  But you will NEED to before each chart generation.  Once `show()` executes, you need to create a new figure for the next chart.

# **=== Jupyter/Colab Notebook Note ===**

You can also plot multiple charts in the same subplot:

```
# don't forget to re-create the figure (notebook only)
```

```
chart_s1(axes[0], x, fx1, 'x¹')
```

```
chart_s1(axes[1], x, fx2, 'x²')
```

```
chart_s1(axes[2], x, fx3, 'x³')
```

```
show()
```

Note that we calling the same function, but are passing in a different subplot each time.  By default matplotlib will choose different colors for each plot and attempt to make sense of the range for each axis (x and y).  

**Second Chart:**

You can force an axis to have a specific range by using the `set_ylim` and `set_xlim` methods to set limits on the x and y axis:

```
def chart_s2(chart, x, y, title=''):
```

```
  chart.plot(x, y, label=title, color="red")
```

```
  chart.set_title(title, fontsize=12)
```

```
  **chart.set_ylim([0, 60])
  chart.set_xlim([2, 5])**
```

```
**chart_s1**(axes[0], x, fx1, 'x¹')
```

```
**chart_s2**(axes[1], x, fx2, 'x²')
```

```
show()
```

Note that we are now composing charts!  Two different functions are used to build different parts of the visualization. There is also a generic `set` method that you can use to set properties.  However, it is less flexible and provides less power/functionality than the specific method used to set an attribute:

```
`chart.`**set**`(ylim=[0,60], xlim=[2,5], title='apples', xlabel='x')`
```

**Third Chart:**

The last chart has a legend for each line chart.  In order for this to work, you need to label the plot.  We also use a **log scale for the y axis** and use a grid background:

```
def chart_s3(chart, x, y, title=''):
```

```
  chart.plot(x, y, label=title, color='lightblue')
```

```
  **chart.set_yscale("log")**
```

```
  chart.set_title(title, fontsize=12)
```

```
  chart.grid(color='b', # b == blue
```

```
            alpha=0.5, 
```

```
            linestyle='dashed', 
```

```
            linewidth=0.5)
```

```
  chart.legend(loc='lower right')
```

```
**chart_s1**(axes[0], x, fx1, 'x¹')
```

```
**chart_s2**(axes[1], x, fx2, 'x²')
```

```
**chart_s3**(axes[2], x, fx3, 'x³')
```

```
fig.legend(loc='upper left')
```

```
show()
```

Adding a legend to the figure works because each plot was labeled.

**TwoByTwo and More**

We also set up a figure with a 2x2 grid of subplots.  Since there is more than one row, we can access the individual subplots using a 2 dimensional notation (which we first saw in the bootcamp lesson 'The String, the Sequence and the Sink')

```
fig, axes = plt.subplots(nrows=2, ncols=2)
```

```
chart_s1(axes[0][0], x,fx2)
```

```
chart_s2(axes[0][1], x,fx3)
```

```
chart_s1(axes[1][0], x,fx2)
```

```
chart_s2(axes[1][1], x,fx1)
```

```
show()
```

**Numpy Data!**

Since the axes is a numpy array, You can use Numpy index notation: 

```
axes[0,0].set(title='upper left') 
```

```
axes[1,1].set(title='lower right') 
```

 

Or simple looping:

```
for ax in axes.flat:
```

```
  ax.set(...)
```

**111 ?**

In the previous lesson, we used the value 111 as a parameter into `add_subplot`:

```
fig = plt.figure()
```

```
axes = fig.add_subplot(111)
```

As mentioned, `111` is processed as 1 row, 1 column, with the subplot at position 1.  In this case, since there is only one row and one column the only available position is 1.

We can use this notation to build more complex graphs:

```
def complex_1():
```

```
  fig = plt.figure()
```

```
  a = fig.add_subplot(1, 2, 1)  
```

```
  b = fig.add_subplot(2, 2, 2)  
```

```
  c = fig.add_subplot(2, 2, 4)
```

```
  a.set_title('top and bottom left')
```

```
  b.set_title('top right')
```

```
  c.set_title('bottom right')
```

```
  a.grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
```

```
  b.grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
```

```
  c.grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.5)
```

```
  
```

```
  fig.tight_layout()
```

```
  return fig
```

```
complex_1()
```

```
show()
```

 

You can add the following (before show()) to give the labels the necessary padding so they don't overlap:

```
fig.tight_layout()
```

```
axes.axis('tight') # a specific subplot
```

```
def complex_2():
```

```
  fig = plt.figure()
```

```
  fig.add_subplot(221, title='top left')
```

```
  fig.add_subplot(222, title='top right')    
```

```
  fig.add_subplot(223, title='bottom left')    
```

```
  fig.add_subplot(224, title='bottom right') 
```

```
  fig.tight_layout()
```

```
  return fig
```

```
complex_2() 
```

```
show()
```

**More Axis Labeling:**

Take a look at the Y axis for x³ graph.  It's not very reader friendly.  

 

We can change the scales on the plots:

```
import matplotlib.ticker as ticker
```

```
def chart_s3b(axes, title, x, y):
```

```
  chart_s3(axes, title, x, y)
```

```
 
```

```
  # now update the axes
```

```
  axes.xaxis.set_major_locator(ticker.MaxNLocator(integer=True)) 
```

```
  axes.yaxis.set_major_formatter(ticker.ScalarFormatter())
```

Note that the formatting is done after the drawing.  Here's how we would build a new chart:

```
fig, axes = plt.subplots(nrows=1, 
```

```
                         ncols=3, figsize=(10, 4))
```

```
chart_s2(axes[0], x, fx2, 'x²')
```

```
chart_s3b(axes[1], x, fx3, 'x³')
```

```
show()
```

The function `chart_s3b` calls `chart_s3` to create the same plot (this is where the object oriented interface shows its power).  Be sure you see the differences.

You can even specify a custom formatter.  There will be a special lesson on using Python string's format method later:

```
def chart_s3c(axes, title, x, y):
```

```
  chart_s3(axes, title, x, y)
```

```
  def custom(x,pos):
```

```
    if x < 1:
```

```
      return '{0:0.2f}'.format(x)
```

```
    else:
```

```
      return '{0:d}'.format(int(x))
```

```
  
```

```
  # now update the axes
```

```
  axes.xaxis.set_major_locator(ticker.MaxNLocator(integer=True)) 
```

```
  axes.yaxis.set_major_formatter(ticker.FuncFormatter(custom))
```

```
chart_s2(axes[0],  x, fx2, 'x²')
```

```
chart_s3(axes[1],  x, fx3, 'x³')
```

```
chart_s3c(axes[2], x, fx3, 'x³')
```

```
show()
```

**Styles**

It's also possible to define style sheets (much like CSS's role in HTML) to change the aesthetics of the visualizations.  You can play with different styles (run them before any graph generation):

```
`print(plt.style.available)`
```

```
`plt.style.use('ggplot')`
```

```
plt.style.use('fivethirtyeight')
```

```
plt.style.use('classic')
```

**Before you go, you should know:**

- what is a matplotlib axes
- how to create subplots
- how to title, label a plot

# **Lesson Assignment**

**Step 0:** be sure you typed in all the examples from this lesson into your notebook.

**Step 1:** x² vs x³

Create a function named `plot_x2_x3` that does **at least** the following:

- use the OO API explained in this lesson
- plots x² and x³ as two separate subplots from -20 to +20 using at least 100 data points
- the 2 subplots are arranged horizontally (i.e. side by side)
- x² is colored blue
- x³ is colored green
- x² is titled x² (12 pt font) and its y axis is from 0 to 500
- x³ is titled x³ (12 pt font) and its y axis is from -1000 to +1000
- use your first name for the title of the figure (14 pt font)
- all data must be internal to the function (i.e. no global variables)
- return the figure created
- `plot_x2_x3` has no arguments

 

**Step 2:** Share your notebook you created for this lesson

Hit Share, Get shareable link.  Make sure it works by testing using another browser in which you are not logged into Google.  

**Step 3**: Paste the full url for your shared notebook in `lesson.py` as the return value for the function jupyter

**Step 4:**  Download your notebook as `.py` file and upload to Gradescope (be sure to save the file as `solution.py` before uploading it). You should comment out any calls to show() that are inside of `plot_x2_x3`.

The Gradescope assignment is named **Matplotlib Part 2**

**Step 5:** Hit submit to receive credit for the replit

You can tag any question you have with **pyMat2** on Piazza


All Rights Reserved

**Readings and References:**

**Titles**

https://matplotlib.org/api/_as_gen/matplotlib.pyplot.suptitle.html

https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.set_title.html

**Legends**

https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.legend.html.

https://jakevdp.github.io/PythonDataScienceHandbook/04.06-customizing-legends.html

**Formatting Scales**

https://matplotlib.org/api/ticker_api.html

**Tight Layout**

https://matplotlib.org/users/tight_layout_guide.html

**Styles**

https://tonysyu.github.io/raw_content/matplotlib-style-gallery/gallery.html

