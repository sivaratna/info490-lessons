# **Data Science & 🐍**

Prerequisites:  

- Python Bootcamp
- University Python: Local I/O 

# Cliff Note Generator  

Imagine that you had a Python function (or program) that accepted the text of a book as input and returned a summary of that book: the characters, the plot, the sentiment, etc. chapter by chapter.  I always dreamed of having that machinery in 9th grade.  Our first data science _project_ will be just that (well a modest version).  A full version would need to use machine learning and advanced NLP (natural language processing) and text analysis.  However, we will start with what we currently know and continue to build on it as we get new ideas and more experience and knowledge -- it's how all great software gets built.

We will use this text analysis example to introduce concepts and techniques in data science to make sense of data (and to learn a lot of new Python libraries and techniques).

# The Pipeline

The previous lesson introduced the idea of a data processing pipeline where each stage depends on the previous stage to move raw data to information.  We will briefly discuss the first three stages within this example.


**Pipeline Stage 1: Understanding the context** 

The main focus of the first stage is to understand the problem being solved and the goals of the entire project.  The initial scope of our **C**liff **N**ote **G**enerator (**CNG**) will be to generate word counts for a given body of text.  

For our first example we will use the text (freely available) of The Adventures of Huckleberry Finn and generate a simple "table" with two columns: word and count.  For example, our goal is to produce the following table (the words and numbers are factitious):

```
word,  count 
```

```
the,   400 
```

```
Tom,   305 
```

```
Polly, 206
```

This 'table' can then be converted based on how we want to communicate the results (raw data saved in a file or database, a spreadsheet or comma separated values (csv), a visualization, etc).  In many situations you won't necessarily know ahead of time what format your results need to be in until you start working with the data and understand the analysis and the needs of everyone involved.  

This table will be a first attempt to find the most popular words in a novel.  Perhaps we might hypothesize that we would be able to identify the characters this way.  Is this a reliable method to find the characters of a novel?  What if we removed all the common words like 'the', 'a', 'an', etc. ?  We will have more to say about this, but you need to critically think about the potential issues of the method being used for the desired goal in any data science problem.


**Pipeline Stage 2: Collecting the data (finding, accessing and reading the data)**

Once you know the problem being solved, the next step is to figure out what data is available to help answer the question being asked or solve the problem, and to determine how to access it (if possible).  

There are many ways to get the text of a book.  Project Gutenberg (https://www.gutenberg.org) and HaitiTrust (https://www.hathitrust.org) are two sites (among many) that offer open source versions of many classics (and even more that never made it to that level of aspiration).  You might also find pdfs (Portable Document Format) of books which can be converted to text (or you can just copy and paste the text from the document itself sometimes).  For some, you might have images of a book that had OCR (optical character recognition) applied to each page.  The OCR process is another data science pipeline where the computation part involves machine learning.  The final product is essentially a program that takes an image as input and generates 'text' from that image.  The resulting text is usually never perfect, but it is much better than hand transcribing (another option).

Another potential issue at this stage is understanding the access rights of the data.  Although not legal advice, in general, personal use of text that is not to be distributed and is used for research and analysis falls under fair use of copyright owned material.  Please read the following:

- https://www.copyrightuser.org/understand/exceptions/text-data-mining/
- https://guides.nyu.edu/fairuse
- https://www.nolo.com/legal-encyclopedia/fair-use-rule-copyright-material-30100.html

Since the text we are using is indeed in the public domain, we don't have to address the topic of fair use but understanding these issues is necessary.

One of the biggest _issues_ of building a generalized data processing framework is that the structure of every book is slightly different.  You can't rely on a way that consistently (and perfectly) finds the start of the book's contents (e.g. there's preface material, table of contents, illustrations, etc).  Take a look at these two books: 

- Moby Dick (via http://www.gutenberg.org/files/2701/2701-0.txt)
- Huckleberry Finn (via http://www.gutenberg.org/files/76/76-0.txt)

Although they come from the same repository the two books have very different characteristics.

For now, we are going to just focus on ONE document from Project Gutenberg.  To keep things even more focused,  the first chapter of Huckleberry Finn is isolated into a separate file -- just to keep us from getting EASILY side tracked and dropping down a rabbit hole of trying to build rules to capture the text properly.  In general when you first start out developing an algorithm for data analysis, starting with a small, simple, cleaned version of the data will help you stay focused.  Once you think you have things working, you can then expand the data set until you end up working with the rawest version of the data.  Eventually the CNG will take the"raw" text from a Project Gutenberg book; but for now we aren't going to figure out how to get this data programmatically.  It is an issue we will come back and solve (a Python Topics lesson -- remote I/O).

In this case, the data will be coming from a website.  Other situations will require you to read databases or process documents. Once you start thinking how you will parse and convert your data, you're actually in the third stage.  This stage is about finding the right data (or at least what you think is the right data) and accessing it.

**Pipeline Stage 3: Cleaning the data (i.e. data preparation)**

In this stage you ask how to best prepare and clean the data for analysis?

Take a look at the Huckleberry Finn data via the above URL (use another browser tab or window).  What do you see?  Where is the actual text for the book's contents? (yes go ahead and find it).  How would you separate the useful data from the unusable programmatically?  How do we break the text into parts (e.g. chapters, paragraphs, sentences, phrases, words, letters, etc) that can be used for our specific analysis?  More questions that you will need to know how to answer.  

In addition to deciding how to parse the data into the necessary parts to make the analysis, this step is also concerned with cleaning or normalizing the data.  Cleaning the data involves making decisions on how to handle missing values, incorrect values (like OCR text or misspellings).  There are several options to discuss, but we don't have to worry about missing, invalid or bad data in this case -- we have it all.  

Another part of this stage is getting the data ready for whatever the analysis stage will require.  For this project, the analysis is to simply count the words and put them in descending order (those with the highest word counts will be 'first').  So the task at hand is to figure out how to take the text and break it into a list of words.

Data normalization (or standardization) is about preparing the data such that processes further down the pipeline work on a standardized version of the data.  As an example, the process (or function) that attempts to finds the names of people in a body of text needs to make some assumptions of the incoming text (the language, what token(s) are used to separate individual words, etc).  The process that builds a visualization also makes some assumptions about the incoming data as well.  Each process should document what assumptions are made regarding the format and expectations of the incoming data.  At the same time/chate, when you write a data processing component, the fewer restrictions you require by writing robust code, the more reusable you are making that component so that it can be used in other pipelines.

However as you will see, moving the data from what's available at Project Gutenberg to what we have here did take some processing.  Some of the text is pasted into the repl.it tabs for you.  At some point copy&paste will not work or scale for a project.   However, it's a perfectly viable strategy for now:  get your analysis to work on cleaned and normalized data.  You can always add additional steps to move data into the format that's required by your code.

**Before you go you should know**:

- How each stage in the pipeline depends on the previous stage
- What stage usually involves the most amount of work

# **Lesson Assignment**

All of your code will be written in the `lesson.py` tab.

**Part 1**

Define the body of the function `read_data(filename)`.  

- If `filename` is `None`, return the `sentence` (already defined for your),
- Otherwise return the contents of the file (whose name is in the parameter `filename`) as a single string
- If there is any error during the opening or reading of the file, `print` out a warning and return `sentence`.  If you haven't completed the lesson on Python local I/O, do so now.

**Part 2**

Define the function `parse_text_into_words_v1(text):`

- takes a string of words and returns a list of words.  
- Do not make assumptions regarding what text is.  The text could a single sentence, a paragraph of text or the contents of an entire book. 

Consult the Python documentation for strings to learn/remember how do to break apart a single string of text into a list of words: `https://docs.python.org/3/library/stdtypes.html#str` and read how `split` works.

Do NOT worry if the results of your simple parser leave a lot to be desired (for example, some of the words will still have punctuation in them).  We will fix the output in the next lesson (and subsequent lessons).  Do not attempt to remove or replace characters, that's for another lesson.

**Part 3**

Write another function named `parse_text_into_words_v2` that has the same parameter but uses `split` with a single space as the separator.


**Part 4**

Implement the function `determine_difference(a_list, b_list):`

This function will return a **list** of items found in `a_list` that are not in `b_list`.

 

You can solve this using any technique and Python data type (without needing to import a library).

**Part 5 (nothing to submit, but be sure to know)**

Use the above function to determine the difference between your two versions of `parse_text_into_words`. 

This idea of doing an A/B test between two different techniques is very useful.  Based on your observations, what would you choose as the best implementation for `parse_text_into_words`?

Fill out the comment in `main.py` regarding any issues you found using the `split` command.

Hit submit when you are finished.  Tag any question on Piazza with **DSP3**.

  


All rights reserved
