# REPLIT FIX for SPRING 2020
try:
  import pip
  pip.main(["install", "-r", "requirements.txt"])
except SystemExit as e:
  pass
# END of FIX

# main.py
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import util
import scipy.stats
import statistics as stats


rolls = util.perfect_dice()