# **Data Science & 🐍:** Estimating Location

 

# add this code to the top of main.py

```
try:
  import pip
  pip.main(["install", "-r", "requirements.txt"])
except SystemExit as e:
  pass
# END of FIX
```

# Descriptive Statistics (central tendencies)

Statistics can be broadly split into two different fields:  **Descriptive** and **Inferential**. Descriptive statistics are used to organize and summarize data from both populations (having the complete dataset that includes all possible cases/instances) and samples (those values drawn from a population in an attempt to learn (i.e. infer) something about the population).  Inferential Statistics is about making predictions or generalizations (educated guesses or inferences) about a population and putting bounds of uncertainty on those predictions.

For descriptive statistics, the focus of this lesson, we'll discuss how to calculate various summary statistics using Python libraries. In addition to the familiar mean, median, mode and variance, we'll discuss other 'statistics' that help summarize a dataset.  There will be a separate set of lessons on inferential statistics.

# **Welcome SciPy**

 

In addition to Numpy, Matplotlib and Pandas, we will also use **SciPy**  (pronounced “Sigh Pie”).  It's a library for mathematics (especially linear algebra), science and engineering.  We will go over the statistics package inside SciPy.  The standard statistics module for Python also has many useful functions.

# **Where are We?  Estimates of Location**

Given a dataset, it's nice to know what a typical value might look like -- an estimate for where most of the data is located.  You probably have heard of these, but let's do a quick review of each and show you how to use Python.

We'll use a dataset of 36 rolls of two statistically perfectly thrown dice:

```
rolls = util.perfect_dice()
```

```
print(rolls)
```

```
print(len(np.unique(rolls))) # 11 unique rolls
```

**Replit Note:**  The first time you run this, a few libraries will need to be installed.  If you leave your session unattended, it's possible, that you will need to reload the browser as well.

If you plotted this dataset, it would look like the following:

 

**Mean**

The mean (or average) is just the sum of the values divided by the number of values:

```
total = sum(rolls)
```

```
count = len(rolls)
```

```
ave = total/count
```

```
print(ave)
```

It's formula is given below:

 

You can use Python's `statistics` library and Numpy:

```
print(stats.mean(rolls))
```

```
print(np.mean(rolls))
```

It's also easy to use Pandas to get the mean.  Take note that both Series and DataFrames, have a very similar API (i.e. functions and methods) for doing analysis:

```
r_s  = pd.Series(rolls)
```

```
r_df = pd.DataFrame({'roll': rolls})
```

```
print(r_s.mean())
```

```
print(r_df['roll'].mean())
```

As a reminder you can also use Pandas `agg` or `aggregate` method to pass in a function.  Although a bit redundant here, these simple examples allow you to learn alternative ways to process data using different APIs.

```
print(r_s.agg(np.mean))
```

**Weighted Mean**

For calculating a mean, each data item contributes equally to the resulting statistic.  However, you can also calculate a weighted mean (formula given below) where each item in your data has a different contribution or weight.  

 

For example, the sum of two dice throws (36 possible outcomes) can be any number from 2 to 12.  But the probability or weight of each sum occurring is not equal.  The average function in Numpy can also take a weight array:

```
x = np.unique(rolls)
```

```
w = np.array([1/36,2/36,3/36,4/36,5/36,6/36,5/36,4/36,3/36,2/36,1/36])
```

```
print("{0:.2f}".format(np.average(x, weights=w)))
```

In this example the weighted mean is the same as the standard mean.  But it's a very useful calculation, especially if you have relative proportions of your data and not the entire dataset.

**Trimmed Mean**

Using a trimmed mean is a good way to drop off a fixed number of values from both the low and high end.  You can use a trimmed mean to remove the influence of extreme values.  The `scipy.stats` library has a trimmed mean function.   

The dice example doesn't lend itself for a useful scenario, but imagine you had a list of the amount people spent to buy a ticket.  That data is in the dollar.csv tab/file:

```
df = pd.read_csv('dollar.csv')  # dataframe
```

```
dollars_s = df['dollar']        # series
```

```
print(dollars_s.head())
```

Let's look at the mean:

```
d_ave = np.mean(dollars_s)
```

```
print("mean {0:.2f}".format(d_ave))
```

Of course the question is how representative is the mean, $33.40, of the

dataset?  We can get some useful values by looking at the max and min of this dataset:

```
print("low  {0:.2f}".format(min(dollars_s)))
```

```
print("high {0:.2f}".format(max(dollars_s)))
```

So given that the mean is so much closer to the minimum than the maximum, it tells us the data is a bit skewed (discussed in detail soon).  One of the best ways to get a view of the data is to simply visualize it:

 

It's clear the high values are influencing the mean.  However, if you trim off just 5% off both ends, what happens to the mean?

```
t_m = scipy.stats.trim_mean(dollars_s, 0.05)
```

```
print("trim {0:.2f}".format(t_m))
```

Another way to trim off extreme values is to cut off the some percentage of the top (or bottom end).  Usually, you will want to visualize this data first before doing so (coming up soon).  But here's a quick way to do it:

```
s2 = sorted(dollars_s)
```

```
cut_off = int(0.90 * len(s2)) # keep lower 90%
```

```
lower90 = s2[0: cut_off]
```

```
m = np.mean(lower90)
```

```
print("trim top {0:.2f}".format(m))
```

So we went from $33.40 (overall average) to $24.65 (trimmed both ends) to $19.75 when just trimming the top end.  What does this tell you about the data?  If you're not sure, ask on Piazza.  We'll have a separate lesson that discusses distribution skew.

**Geometric and Harmonic Means**

Two means that get little attention are the geometric and harmonic means.  The library `scipy.stats` has functions to do each.

**Geometric Mean**

The **geometric mean** is taking the nᵗʰ root of each item **multiplied**.  It indicates central tendency using the product of values.  It is used when working with compound growth rates, investment returns, area and volume.  It's general formula is shown below:

 

**When to use**

- appropriate when the data contains values with different units of measure (e.g. some measure are height, some are dollars, some are miles)
- does not accept negative or zero values (e.g. all values must be positive)

**Geometric at the Movies**

 

A good situation of using the geometric mean is when working with different scales of data.  For example, let's say you are trying to decide between two movies (Avengers End Game and Beyond Forgiving Chore) using IMDB and Rotten Tomatoes.  Both use different scales to rate movies. IMDB uses a 100 point scale and Rotten Tomatoes uses a 5 point scale.

Avengers End Game

`4.5  RT score`

`68   IMDB score`

Beyond Forgiving Chore

`3.0  RT score`

`75   IMDB score`

Using simple mean calculation:

```
aeg = [4.5, 68]
```

```
bfc = [3.0, 75]
```

```
pick_a = np.mean(aeg) > np.mean(bfc)
```

```
print(pick_a) # pick Beyond Forgiving Chore
```

Using geometric mean calculation:

```
pick_a = scipy.stats.gmean(aeg) > scipy.stats.gmean(bfc)
```

```
print(pick_a) # pick Avengers End Game
```

**Harmonic Mean**

The **harmonic mean** is the appropriate mean if the data is comprised of rates.

As a quick reminder,  [rate](https://en.wikipedia.org/wiki/Rate_(mathematics)) is the ratio between two quantities with different measures (e.g. speed, acceleration, frequency, etc.)

A good example is calculating average speed.  If you drove 10 miles @ 60 MPH (hence 10 miles in 10 minutes of driving) and then got stuck behind a bus going 30 MPH for 10 more miles (10 miles in 20 minutes of driving), you drove a total of 20 miles in 30 minutes.  Thats 40 miles per hour.  Correct?  

Let's see what the means say:

```
MPH = [60, 30]
```

```
print(np.mean(MPH))
```

Since we are dealing with rates, the normal mean doesn't make sense.  The harmonic mean uses the inverse of the values:

  

```
print(scipy.stats.hmean(MPH))
```

**When to Use**

- appropriate for rates
- does not take rates with a negative or zero value (all rates must be positive)

**The Middle: The Median**

The next measure of central tendency is the median, or the middle value. It's the value that's in the middle of the data set.

 

You can get the median by using numpy's or panda's `median` function or pa

```
rolls = util.perfect_dice()
```

```
print('med {0:.2f}'.format(np.median(rolls)))
```

```
df = pd.read_csv('dollar.csv')
```

```
dollars_s = df['dollar']
```

```
print(dollars_s.median())
```

Note that this median is much closer to the trimmed mean you calculated before.  

If the dataset has an even number of items, you average the two 'middle' values.  The median has a hidden cost of calculating it in that the data needs to be sorted first.

 

**Mode**

The mode is the value(s) that occur the most in the data set.  It's possible to have multiple modes (i.e. multi-modal) or no modes (e.g. if every value occurs once).

 

The `scipy.stats` package has a `mode` function:

```
m = scipy.stats.mode(rolls)
```

```
print('mode', m)
```

This returns a '`ModeResult`' which can be thought of containing a values array and a count array:

```
value, count = scipy.stats.mode(rolls)
```

```
print('mode', value, count)
```

To get a better sense of this measurement, let's remove the 7's from the data and recalculate the mode:

```
no_7 = [d for d in rolls if d != 7 ]
```

```
v,c = scipy.stats.mode(no_7)
```

```
print('mode ', v,c )
```

Note that it returns '6' with a count of 5.  BUT this dataset has 2 modes (6 and 8).  Pandas has a better mode version which returns all the modes of a dataset.

However, Pandas has version of mode that returns all the modes in a dataset:

```
s = pd.Series(no_7)
```

```
print(s.mode())
```

# Review

The following shows a quick summary of the different statistics discussed:

 

**Mean:**

Pros:

- It works well for simple lists of numbers/measurements.
- Easy to calculate: just add and divide.
- It’s intuitive — it’s the number “in the middle”, pulled up by large values and brought down by smaller ones.

Cons:

- The average can be skewed by outliers — it doesn’t deal well with wildly varying samples. The average of 100, 200 and -300 is 0, which is misleading.

**Median**

Pros:

- Handles outliers well — often the most accurate representation of a group
- Splits data into two groups, each with the same number of items

Cons:

- Can be harder to calculate: you need to sort the list first
- Not as well-known; when you say “median”, people may think you mean “average”

**Mode**

Pros:

- Works well for exclusive voting situations (this choice or that one; no compromise)
- Gives a choice that the most people wanted (whereas the average can give a choice that nobody wanted).
- Simple to understand

Cons:

- Requires more effort to compute (have to tally up the votes)
- “Winner takes all” — there’s no middle path

**Describe Me**

One of the great features of Pandas is that you can use the describe method to see all the relevant statistics of a dataset:

```
df = pd.read_csv('dollar.csv')
```

```
dollars_s = df['dollar']
```

```
print(dollars_s.describe())
```

The `describe` method returns a tuple which holds the count, mean, standard deviation (next lesson), min, max and the values at different percentiles (next lesson).

**A Look Ahead.**

The location of the mean, median and mode relative to each other can hint towards the distribution of values.  This will be discussed in more detail in the next lesson.  But do take a look at how the order of the mean, median, and mode in both l_eft_-_skewed_ distributions  (also called _negatively_-_skewed_ distribution), _right-skewed_ distributions (also called _positively_-_skewed_ distribution) and normal distributions.

 

**Before you go, you should know:**

- how to find the mean, median, mode using Python
- what a trimmed mean is and why it's useful
- the values returned by Panda's `describe` method

# Lesson Assignment

We removed all the tests for this lesson.  You can receive credit by just hitting submit. 

04.20.20

All Rights Reserved
