# **Data Science & 🐍**

**Cliff Note Generator (computing continued: visualizations)**

Initially, the goal of this project was to create a table that has the number of times each word appeared in the text.

The output of the `build_table` from the previous lesson is a dictionary whose keys are the words and whose values are the counts for each word.  We can easily convert the dictionary (by using the `items()` method) to a list of tuples (which essentially is a table). 

The output of `top_n` from the previous lesson is indeed a list of tuples but sorted and filtered to include only the top N (i.e. the most common N) tuples in the list.

Take a look inside the data.py tab.  These tuples are essentially what the variable '`sorted`' contains.  It's an array of tuples.  Each tuple has 2 elements, the first is the word and the second is the count.  

**Visualizing Data**

Perhaps building data visualizations is one of the most rewarding parts of the data science pipeline.  If built properly, a good visualization should easily convey something interesting about the data that might have been overlooked had it been buried inside a table or a spreadsheet.

An easy way to visualize data in Python is to use the library `matplotlib`.  It's an old library that's a bit clunky to use, but it is very popular and learning it will help you with other libraries that use `matplotlib` (as well as understand and debug the billions of `matplotlib` lines of code out there).

There will be several tutorials and readings on using matplotlib, but for now we will do a gentle introduction to making a bar chart.   When you have data to visualize, always choose the clearest way to convey what information you are seeking.  For a table of word counts, it's not fancy, but a bar chart gets the job done.

 

As you know, a bar chart just needs to display a value on the x axis (usually the label) and a value on the y axis (usually the height of the bar).  Color could be used for another dimension, but for now we will focus on visualizing our table of words.

 

Take a look inside the module named `viz.py` (a tab in the editor).  This file contains a function named `simple_vis()`:  it is probably the least number of lines of code to create a chart using matplotlib and Python.

> The last line of the function creates a png image file.  This version of repl.it doesn't support an interactive mode for matplotlib.  This means you can't just display a graph and have it appear in another window on your laptop/computer.  When this functionality is added, you will be able to call `fig.show()` which is the standard way to show a graph without needing to create an image.

**Experiments In Order**

There are a few experiments we want to try to demonstrate some of the issues with creating visualizations with matplotlib.

**1.**  Run the following code as is (that is don't remove any other comments).  The current code creates a file named `simpleBar.png`. 

```
viz.simple_vis('simpleBar.png')
```

Take a look at the image (the tab `simpleBar.png`) (NOTE: you may have to click on another tab and then on the newly created tab).  Seems fine.

**2.** Now run the code as follows (comment out the previous code):

```
#viz.simple_vis('simpleBar.png')
```

```
top_10 = data.sorted[0:10]
```

```
viz.visualize(top_10, 'top10A.png')
```

Take a look at the file `top10.png`.  Seems fine (and it clearly communicates what word is the most popular).

**3.** Now run the code as follows:

```
viz.simple_vis('simpleBarB.png')
```

```
top_10 = data.sorted[0:10]
```

```
viz.visualize(top_10, 'top10B.png')
```

That is we are running the same code but creating two visualizations.

Now look at both `simpleBarB.png` and `top10A.png`. There are problems. The colors and the Y axis are messed up. The issue is that matplotlib shares state when creating graphs (or figures). In other words when you create a graph, any subsequent graph is affected by properties you set creating the first graph. This can be a feature or a pain and it's one of those issues (of many) that you will run into when using libraries -- you really need to read the documentation and understand how the library works and how it is expected to be used.

**NOTE**:

As you run these examples, it may be necessary to change the name of the file that is being written.  Repl.it will not write over files that exist.

In our case, we can fix this by uncommenting the lines in the file `**viz.py**`**:**

```
plt.figure(0) # inside of simple_vis
```

```
plt.figure(1) # inside of visualize
```

We are essentially telling matplotlib to keep the two graphs separate (do not share state).

Rerun the code such that both graphs are created and both graphs generate the correct image.

```
# make the code changes above
```

```
viz.simple_vis('simpleBarC.png')
```

```
top_10 = data.sorted[0:10]
```

```
viz.visualize(top_10, 'top10C.png')
```

**A better approach**

The issue with the above solution is that you have to make sure each visualization built in a session does not share state.  At one point it was assumed that you would build one visualization (called the active state) and rely on this state.  However, as people started to build more complex sets of visualizations in a single session, this sharing of state became an issue.

Take a look at `visualize_better(tuples, filename`) (inside of `viz.py`).  It uses something called the 'object oriented interface' abbreviated OO. It's a way of designing a solution using separate classes and types.  Using this API, building visualizations is not only easier but there is also a clear separation of state between visualizations.  We will only use this API moving forward.  However there are lots of matplotlib tutorials that only show the shared state way.

**Round One Wrap-up.**

It's not a great visualization is it?  But it is a start.  We proved that we can take the text of a book (well a subsection of one) and process the words to make a bar chart of the top most frequently used words.  A few weeks ago you didn't know any Python (and perhaps didn't know how to program).  So you have actually accomplished a great deal.

We certainly can make a lot of improvements to not only the final output, but to each step in the pipeline.  We will be addressing some of the enhancements in the next set of lessons.

**The Mountain that is Matplotlib**

Learning how to build visualizations using matplotlib will involve a lot more learning than just showing you some code that builds a bar graph.  It's important to know how graphics are composed.  For this lesson, we are just starting our journey and we will have a dedicated lesson that describes the library in more detail.

**Before you go you should know:**

- Which version of the matplotlib API we will ONLY use in this class

# **Lesson Assignment**

Take a look at the documentation for `pyplot.bar` (`https://matplotlib.org/api/_as_gen/matplotlib.pyplot.bar.html`).  It returns a container of the bars created.  Your task is to color the highest bar red.  That is the bar that represents the word with the highest count should be colored red.

Inside the **lesson.py** tab:

- Create a new function named `visualize_high(tuples, filename):`
- It will generate a graph named using `filename`.  
- This plot will look very similar to `top10.png` but the word with the highest count will be colored red.
- For this assignment you must use the object oriented API for building your visualization -- You must (and should) look at the code and see the differences otherwise, this step will not make any sense.
- Be sure to return the figure
- see `visualize_better` for the template

Hints:  

- https://matplotlib.org/api/_as_gen/matplotlib.pyplot.bar.html
- https://matplotlib.org/2.0.2/api/colors_api.html
- You should not need to add a lot of code to do this
- You can color the other bars any way you see fit, but the one bar needs to be 'red'

Once your graph looks great (feel free to augment based on the documentation), hit submit.

You can tag any question with **CNGViz** on Piazza.


All Rights Reserved

