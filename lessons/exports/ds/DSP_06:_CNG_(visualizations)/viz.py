import matplotlib as mpl
import matplotlib.pyplot as plt


def show_matplot_info():
  #mpl.use('Agg')
  plt.ioff() #force to use plot.show()
  print(plt.rcParams['interactive'])
  # Get current size
  fig_size = plt.rcParams["figure.figsize"]
  print ("Current size:", fig_size)


def simple_vis(filename):
  # Make a fake dataset:
  height = [ 3,  12,   5,  18,  45]
  bars   = ('A', 'B', 'C', 'D', 'E')
  x_pos = range(len(bars)) # simple 
  
  #plt.figure(0) # uncomment line per directions
  
  # Create bars
  plt.bar(x_pos, height)
   
  # Create names on the x-axis
  plt.xticks(x_pos, bars)
   
  # 'Show' the graphic  
  # plt.show() will not work inside repl.it
  plt.savefig(filename)
  
  print("created", filename)
  
  return plt


def visualize(tuples, filename):
  labels = []
  counts = []
  for t in tuples:
    labels.append(t[0])
    counts.append(t[1])
    
  x_pos = range(len(labels))
  
  #plt.figure(1) # uncomment line per directions
  
  bars = plt.bar(x_pos, counts)
  
  plt.xlabel('word',  fontsize=12)
  plt.ylabel('count', fontsize=12)
  plt.xticks(x_pos, labels, fontsize=8, rotation=30)
  plt.title('Word Usage')
  plt.savefig(filename)
  
  print("created", filename)
  
  return plt
  
  
def visualize_better(tuples, filename):
  
  labels = []
  counts = []
  for t in tuples:
    labels.append(t[0])
    counts.append(t[1])
    
  x_pos = range(len(labels))
  
  # use the object API
  fig = plt.figure()
  ax  = fig.add_subplot(111) 
  #ax = fig.add_subplot(1,1,1) # same as above
  
  # create the bars
  bars = ax.bar(x_pos, counts)
  
  ax.set_xlabel('word',  fontsize=12)
  ax.set_ylabel('count', fontsize=12)
  
  ax.set_xticks(x_pos)
  ax.set_xticklabels(labels, fontsize=8, rotation=30)
  ax.set_title('Word Usage')
  
  #fig.show()
  fig.savefig(filename)
  
  # return the figure
  # allows the caller to customize
  # do NOT return plt
  return fig
