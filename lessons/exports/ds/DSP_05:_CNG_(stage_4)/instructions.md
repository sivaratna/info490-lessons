# **Data Science & 🐍**

**Pipeline Stage 4: Compute the data**

The computation stage within the data science pipeline is where you can finally do something useful to the data that you have been carefully preparing.  Although it can involve visualizations, this stage is also about building models and doing analysis including pattern discovery, machine learning, etc.  This stage usually requires the most creativity and hence the most fun.  

Unfortunately for this example, the end goal is not too exciting -- it's only to build a 'table'.  As we saw earlier,  our goal is to produce the following table (words and numbers made up):

```
word,  count 
the,   400 
Tom,   305 
Polly, 206
```

However, once this table is built, we can easily use it for other analysis including visualizations (coming soon).

**Collections Module**

Python has a set of useful containers to manage data in the `collections` module.  Specifically, the Counter type is a great way to keep track of counts of unique items.  For example, here is a simple example:

```
import collections
```

```
counter = collections.Counter()
```

```
words = ['apple', 'pear', 'apple']
```

```
for w in words:
```

```
   counter[w] += 1
```

```
print(counter.most_common())
```

Read that code carefully and make sure you understand it.  It's a common pattern.  See https://docs.python.org/3.7/library/collections.html#collections.Counter for more details.

**Before you go, you should know**

- what the `collections.Counter` type is and why it's useful

# **Lesson Assignment**

There's not much content to this lesson other than to create the table of words and counts (which will be a list of tuples).  The words are already parsed out for you (same as previous lesson).

Build the following three functions:  `clean`,  `build_table`,  `top_n`

`1.` `def clean(words):` 

- normalizes the words so case is ignored
- returns an array of 'cleaned' words

`2.` `def build_table(words):` 

- builds a dictionary of counts
- returns the dictionary or `collections.Counter` type

`3.` `def top_n(table, n):` 

- returns the `n` most frequent words(keys) in table
- the return type is an array of tuples
- the tuple's first value is the word; the second value is the count

**Notes:**

the function `top_n` does not have to worry about the order of items for those words that have the same count.  This feature is called stable sorting -- where the items after the sort will always be in the same order (more discussion in the extra credit).  You can use `collections.Counter` to help you with this lesson, but it will NOT return a stable order.

Be sure to test your pipeline on multiple texts.  Each 'run' should not affect others:

```
v1 = list(lesson.pipeline(['a','b','c'], 5))
```

```
v2 = list(lesson.pipeline(['a','b','c'], 5))
```

```
print(v1 == v2)
```

**Extra Credit:**

You can skip extra credit. But you need to add the following line of code (`lesson.py`)

```
skip_extra_credit = True
```

Solve the entire solution **without** using the collections module (specifically the Counter type).  Use a regular dictionary for all parts of the pipeline.

You will change your implementation or `top_n` to do the following:

 `def top_n(table, n):` 

- sorts the table (a dictionary) in reverse order.  
- the sorting criteria is the count (the 2nd value in the tuple)
- returns a subset of the sorted data (use array slicing)

```
# this is what you want to print out the top 20 words:
```

```
print(top_n(build_table(clean(parsed), 20))
```

```
[
```

```
('the', 6), ('book', 4), ('is', 4), 
```

```
('that', 4), ('and', 3), ('aunt', 3), 
```

```
('but', 3), ('he', 3), ('or', 3), 
```

```
('polly', 3), ('told', 3), ('was', 3), 
```

```
('a', 2), ('about', 2), ('by', 2), 
```

```
('i', 2), ('mainly', 2), ('mary', 2), 
```

```
('of', 2), ('truth', 2)]
```

```
]
```

It's important to NOTE that the sort from this must be stable.  A stable sort is one in which items are always returned in the same order.  You wouldn't want one run of a sort to return a different order than another run.

In this case, the words with the highest counts come first.  If there is a tie (e.g. '`book`' has 4 and '`is`' has 4) the two are returned in alphabetical order.  The c`ollections.Counter` type does not do this (your function is actually going to be better!)

**Extra Credit Hints:**

- Your helper function that you will pass to sort will accept a tuple.  You will need to use both parts of the tuple to determine the sort order
- https://www.peterbe.com/plog/in-python-you-sort-with-a-tuple shows a good example of tuple sorting

Once your output matches, hit submit and move on.

You can tag any questions on Piazza with **pyCNG4**


All rights reserved
