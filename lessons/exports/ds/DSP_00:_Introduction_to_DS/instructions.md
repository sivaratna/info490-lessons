# **Data Science with 🐍: Introduction to Data Science**

Data science is essentially the process of moving raw data into information where one can explore, gain insights, make inferences and even predications.  Being a data scientist involves understanding the techniques of statisticians, computer scientists and domain experts (see Venn diagram below). 

 

**Statistics** is large part of data science; but rather than knowing the inner workings of methods like regression, sampling, hypothesis testing and formulas (which a statistics student/expert might know), the data scientist knows what methods, models, tools and techniques to use and when to apply them.  

**Computer Science** is also a large part of data science; but rather than knowing the inner workings of algorithms, operating systems, compilers, language theory (which a computer science student might know), the data scientist knows how to build software to acquire, clean or wrangle the data in order to use the appropriate analysis techniques. ** **

**Domain Knowledge** (or access to a domain expert) is another part of being a good data scientist.  The domain scientist must have a good grasp of the problem being solved and the type of data available to help uncover the truth.  

Much like the overloaded term data, data-science can be just as difficult to define.  Based on who you talk to, you'll get a different viewpoint of "what is data science".  Someone from statistics may view data science as simply a rebranding of techniques that they have been doing all along.  A computer scientist may feel the same way -- after all the whole field of data mining and knowledge discovery seem very similar to current definitions of data science.

Others believe that data science is indeed a new emerging field that attempts to integrate different aspects of fields that work with data -- truly an integrative career path.  The ability to manipulate and understand data is increasingly critical to discovery and innovation. As a result, the emergence of a new field —that focuses on the processes and systems that enable us to extract knowledge or insight from data in various forms and translate it into action — is starting to happen. In practice, data science has evolved as an interdisciplinary field that integrates approaches from such data-analysis fields as statistics, data mining, and predictive analytics and incorporates advances in scalable computing and data management. 

Let's take a closer look at each of these areas.

# **Statistics**

There are two main branches of statistics: descriptive and inferential. **Descriptive statistics** is the collection, exploration, and presentation of the data.  It's essentially about describing the characteristics of a dataset using some metric that conveniently summarizes some aspect of the data.  Although convenient, descriptive statistics often over simplify the data and don't tell the entire picture.  It's part of how statistics can get a bad rap for 'lying'.  As an example, think of how the GPA, batting average, an index to measure air quality or economic health really don't give you a complete (and possibly inaccurate) picture of a student, baseball player, the environment or economy.

Visualizations, standard deviation, frequency tables, mean, medium, mode are all examples of descriptive statistics (measures of frequency, central tendency, dispersion, position).  They are convenient for measuring characteristics and allow comparisons to be made, but at the same time don't give the complete story.

**Inferential Statistics** is all about making conclusions from incomplete information. Inferential statistics plays an important part of the scientific method -- the systematic approach to building knowledge (more about this later).  Sampling, prediction, examining relationships (regression, correlation) and using probability and randomness are all within the realm of inferential statistics. Some statistical methods like hypothesis testing include a bit of each: It uses some descriptive statistics but also p-values, confidence intervals which are more inferential.  

You may have heard that you can use statistics to lie but you **need** statistics to also tell the truth.

# **Computer Science**

As soon as you hear about data mining, artificial intelligence and machine learning you are moving away from statistics and into fields that depend heavily on algorithmic development -- a large field of computer science.  For our purposes, we will focus more on understanding how to use Python to both read and build algorithms but also on using many of the available statistical and data processing modules.  As soon as you become fluent in both statistics and programming you can start authoring your own techniques.

# **Domain Knowledge**

One of the great things of being a data scientist is that you get to immerse yourself inside a domain you know nothing (perhaps) about.  It's a great way to learn about topics you had no idea others were so passionate about.  In many cases, you may not be the domain expert, but knowing what to ask and who can answer your questions will be vital to your success.  

More than anything, your ability to think critically and your willingness to become interested in the domain will be as important as your technical skills.  No one wants to work with someone who doesn't care about the problem being solved.

Sometimes, when data science becomes a specialty in another domain (e.g. biology) it's sometimes given a new name (e.g. bio-informatics):

 

# **Building Knowledge, The Scientific Method, and Sherlock Holmes**

 

As a small diversion from the above discussion, let's discuss a point on terminology.  It can sometimes be confusing trying to understand how data science, mathematics, statistics, etc fit together in the bigger picture of pushing knowledge forward.  Some refer to inferential statistics as _inductive_ statistics and machine learning as _inductive_ machine learning since one is making inferences from specific observations or examples to a general population.  The word inductive is used to emphasis the type of logic (i.e. inductive logic) or reasoning used.  

The scientific method relies on **inductive reasoning** (e.g. generalizing from a small sample of observations) and **deductive reasoning** (e.g. applying a general rule to a specific instance, mathematical proofs) for making inferences.  For completeness, a third type named **abductive reasoning** tries to find the mostly likely explanation from incomplete data (e.g. hypothesis building, cause and effect models).  All three types of reasoning (a.k.a. inferential reasoning) form the base for the scientific method. 

Some fields and disciplines align themselves with a specific type of reasoning.  Statistics and _inductive reasoning_ are almost synonymous as is mathematics and _deductive reasoning_ -- even they, confusingly, have 'inductive' proofs.  Although Sherlock Holmes might be a master of 'deduction' in his quest to solve mysteries, his methods usually involve a high degree of _abductive reasoning_**¹**.

# Assignment

Looking at the first Venn diagram, some authors have gone to great lengths to define the overlapping parts (for example Computer Science ∩ Statistics could be Machine Learning).  The darkest shaded intersection (∩) between the three domains is "data science".  

For this assignment, we will learn to use Python's built in `set` type.  It's a great collection class that allows you to get intersections, unions, differences, etc between different sets of values or objects.  A set is just an unordered collection of items.  One could claim that the keys in a Python dictionary are essentially a set. 

All your code should be in the `lesson.py` module and put any testing code in `main.py`.

1. Create a function named `common_letters(strings)` that returns the intersection of letters among a list of strings as a set.  The parameter is a list of strings.

For example, you can find the common letter in the domains/words **statistics, computer science, and biology**.  You might easily see it, but you need to write Python code to compute the answer.  In order to pass the tests **you must use the `set` api** (see  `https://www.programiz.com/python-programming/set`).  

Notes: 

- You should READ the entire document.  It provides useful knowledge for dealing with `set` collections
- https://docs.python.org/3/library/stdtypes.html#set-types-set-frozenset is another great resource
- This is a departure from the bootcamp.  It's up to you to experiment with the `set` type in the repl.it window.  Try to figure out how to use it.
- You can (not required to solve or pass the tests) create a helper function that creates a set of letters given a word (e.g. `s = build_letter_set("dog")`)

2. Once you pass the test, hit submit to continue.  You can tag any questions on piazza with **DSP1**

**Before you go you should know**

- How you would describe data science to someone not in the field?
- What operations are allowed on the set data type?
- When would you use a `set` over a `list`?


All rights reserved

1. [https://storage.googleapis.com/uicourse/pdfs/The_Abduction_of_Sherlock_Holmes.pdf](https://storage.googleapis.com/uicourse/pdfs/The_Abduction_of_Sherlock_Holmes.pdf)
