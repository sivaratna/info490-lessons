# **Data Science & 🐍:** Jupyter Notebooks

 

**Prerequisites:** Assumes the following have been completed:

- Remote I/O

You may have heard of Jupyter Notebooks (sometimes referred to as IPython -- for Interactive Python).  They provide another way to write, organize and to share Python code.  They are very popular because of their nature to help linearly tell a story using both computation and data.  

The name Jupyter is an indirect acronym of the three core languages it was designed for: **JU**lia, **PYT**hon, and **R**.  Despite some of Jupyter's issues (all technology has issues), which we can go into later, learning how to write, read, and share Jupyter notebooks is a useful exercise -- some data science classes are taught exclusively using Jupyter notebooks. 

As a Python developer (I'm talking to you now), you have a few choices on how you will write your code.  You can use your own IDE (integrated development environment) like PyCharm (no affiliation, but at some point you should try to install it on your laptop/computer) or a text editor like Sublime Text installed on your computer.  You can even use on-line editors like Repl.it (there are many others too) which literally get better every day.  Jupyter notebooks are another on-line/browser based development environment that can be used.

You can also run a Jupyter server on your laptop so you don't necessarily need to be connected (i.e. on-line).  We are going to learn about Jupyter notebooks in this lesson.  At first it may seem strange to have a lesson on a development environment inside of another.  However, repl.it works well for introducing a new technology in that the information needed can be presented in small digestible pieces.

**From the beginning**

You can visit the main Jupyter website via: http://jupyter.org and try it in a browser ([just go to http://jupyter.org/try and click 'Try JupyterLab or Try Classic Notebook' ](https://mybinder.org/v2/gh/ipython/ipython-in-depth/master?filepath=binder/Index.ipynb)or install it on your own machine.  

However, we will use Google's version of 'Jupyter' notebooks -- which allow for both learning the technology but not needing an IT department to manage a cluster of computers.  Google also gives you access to free compute resources and it manages your notebooks like any other document in your google drive.

 

For this assignment, you will need to log into Google.  You can associate your @illinois.edu address or use your `**@**`**gmail** address (you can create one if necessary).  At some point we will use gradescope.com to grade the notebooks.

Once you are logged in, you can visit this URL:

`https://colab.research.google.com`

You can select 'Welcome to Collaborator' notebook which is a nice tutorial but for this lesson we will start with an empty notebook (select 'New Python 3 notebook').  Your starting point will look like the following:

 

# **Basics**

Once you have your notebook, rename it to INFO490.  It gets saved to your Google drive account.  The main toolbar has the following options: **+ Code**   **+ Tex**t  

**The Code Cell**

Hit the **+ Code** to add a 'code block' -- something that you can put Python code into and execute it. Type in the following: 

 

This creates an executable 'cell'. Hit the ▶︎ to run the code inside the cell.  Once the cell is done running, any output is shown immediately beneath the cell: 

 

You can hover your mouse over the output icon on the left side and clear/delete the output.

 

So basically, you add Python code (just the same as we have been doing) in a Code Cell.  

**The Text Cell**

The nice thing about Jupyter style notebooks is the ability to add 'mark**up**' (using Mark**down**) .  This essentially allows you to document your code and format it in style that's much easier to read than inline Python comments.  Go ahead and hit a + Text Cell and add the following: 

 

 

You can view markdown syntax here: [https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

**Cell Moves**

After the new cell is created (usually after the current active cell), you can move/reorder the cell using the  **ꜛ**  and **ꜜ** icons on the cell's toolbar.  

 

Move the text above the code cell:

 

You can delete a cell by using the 'trash' icon:

 

The mixing of formatted text with code is Jupyter's main selling point.  You can nicely tell your story mixing narrative and code and allow your readers to run the code and confirm the results.

**Getting Help**

Sometimes it's useful to get help on the internal documentation for Python.  We haven't discussed this because

1. It's not very user friendly -- especially inside Repl.it

2. It's usually easier just to use Google

BUT you can use the help function to get the API and documentation for almost any Python module/library:

```
s = set(['a', 'b', 'c', 'a'])
help(s) # or help(set)
```

  

**Visual Fun and Magic**

Notebook code cells support HTML as well.  You need to type `%%html` in the first line of a code cell that contains the HTML markup:

```
%%html 
```

```
<div style="display:inline-block; border:0.062em solid #ccc; padding:10px 40px 10px 80px">
```

```
  <h1 align="center; left-padding:20px">INFO 490 Can be Fun</h1>
```

```
</div>
```

```
<div>
```

```
<marquee style='width: 30%; color: blue;'><b>FuN</b></marquee>
```

```
<br>
```

```
<img width=400 src="https://i.imgur.com/REpK0gd.jpg">
```

```
</div>
```

When you execute this cell you get the formatted HTML:

 

The `%%` is a way to tell the notebook that the following is a special 'magic' command and to not interpret the cell as Python code but to use a different run-time to execute the command.  To see all of the magic commands available type the following in a new cell block.

```
%lsmagic
```

You can research the utility of the `%%` commands.  Some of the commands can be either in a code cell or a text cell (e.g. %%html), others will only work in a specific cell type. Can you figure out which cell type the following works in?

```
%%latex
\begin{equation}
\int_{-\infty}^\infty f(x) \delta(x - x_0) dx = f(x_0)
\end{equation}
```

**Matplotlib**

You can even use Matplotlib (and many other charting libraries) to draw charts in your notebook:

```
from matplotlib import pyplot as plt
import random

labels = ['Apple', 'Oranges', 'Bananas']
yrs_to_plot = 50
start_year  = 1950
yr_range    = list(range(0,yrs_to_plot))

# generate sets of random sales data from 0 to 10
# force an upward trend (i + random number)
apple_sales   = [i + random.randint(0,10) for i in range(0, yrs_to_plot)]
orange_sales  = [apple_sales[i]  + random.randint(0,10) for i in range(0, yrs_to_plot) ]
banana_sales  = [orange_sales[i] + random.randint(0,10) for i in range(0, yrs_to_plot) ]

```

```
years = [start_year + yr for yr in range(0, yrs_to_plot)]  # x Axis
sales = [apple_sales, orange_sales, banana_sales]          # y Axis 

```

```
# build the chart
fig, ax = plt.subplots(figsize=(5, 3)) # 5 by 3 inches
ax.stackplot(years, sales, labels=labels)
ax.set_title('Combined sold over time')
ax.legend(loc='upper left')
ax.set_ylabel('Total Sold')
ig=ax.set_xlim(left=years[0], right=years[-1])
```

The last line (`ig=`) is an ignored assignment so the notebook doesn't print the result (the default behavior) of the last line executed.  

When you run this cell, you should see the following graph:

  

Note that there's no need to call `plt.show()`or `save_figure()`.  If you put matplotlib into non-interactive mode, you would have to call `show()`.  However, by default inside a notebook matplotlib is in interactive mode.  Don't worry about understanding the actual code, we will discuss more matplotlib soon.

**Shell Commands**

Like the magic commands, there's also the ability to run commands on the computer that is hosting the Jupyter notebook. Although you may have never worked with a command line -- it's essentially a very crude interface to the operating system.  You need a terminal or command window and then you issue (i.e. type and hit enter) commands that you want to run. In a Jupyter notebook you preface the command you want to run using an exclamation point. If we had more time we would do several lessons on 'data science from the command line'.  You can do a lot of data analysis and data wrangling without ever needing to write any code! 

One of the easier ways to get an external data source into your notebook is to use the `wget` command (there's even a reference to `wget` in the movie _**Social Network**_ --- `https://www.youtube.com/watch?v=odOzMz-fOOw` -- watch first 15 seconds).  The `wget` command line tool is used to download remote resources.

 

Try this in a code cell and run it:

```
def command_line_demo():
```

```
   import os
```

```
   SRC = 'https://drive.google.com/uc?export=download&id=**13F68-nA4W-0t3eNuIodh8fxTMZV5Nlpp**'
```

```
   os.environ['SRC_FILE'] = SRC
```

```
   **!wget** --no-check-certificate -r $SRC_FILE -O "book.txt"
```

```
command_line_demo()
```

That block of code is using the `**wget**` command to fetch a remote resource and save it to a file named `book.txt`.  There's also a Python library named PyDrive that you can install in your notebook to read files from your Google drive.  You could just issue a single `wget` command and skip the `os.environ` nonsense, but it makes for easier reading.  The major issue with relying on ! commands, is that they are **not portable**:  if you wanted to convert a notebook to a pure Python code, those commands would not work. The `wget` command is an easy 'hack' but it is not native Python. 

You can get more information using `--help` flag to `wget`

```
!wget --help
```

Now that you fetched and downloaded the remote resource, you can then open the _local_ file (`book.txt`) and print out the first 100 characters:

```
print(open('book.txt', 'r').read()[0:100])
```

Note that `book.txt` is local to the VM that is running the notebook.  Even though it's not on our computer, we still refer to it as a local resource.  Also, the `wget` command is a great tool to get access to remote resources without a lot of coding -- perfect for experimenting and getting quick results.  However, your notebook is now no longer portable with respect to running in a native Python environment. The Python interpreter would complain when it attempts to parse the wget command.

**Fair Warning: This is NOT Python!**

Take note that the previous sections showed how to include non Python code and commands inside a notebook.  The bad news is this notebook can't be run in a pure Python environment.  You must comment out any code that uses non Python commands.   When you export your notebook as a .py file none of the contents in a text cell is saved, but everything in a code cell is.

**Being Disconnected**

If you leave your browser window open and it remains active, the browser may detach from the VM.  In this case you will see the following notice:

 

If this happens you need to reconnect; However you will have to **re-run** any cells that downloaded resources to the VM.  For example, in the cell that uses `wget` to fetch the remote resource, you would have to re-run that cell and all others that download or install resources.  You can see the options under the Runtime menu.

**Saving to Python**

You can also save your notebook to your laptop/computer using the File menu:

 

You will need to do this for many of the assignments in order to grade the notebook.

That's it in a nutshell.  There are a few gotcha's and corners for which to watch out, but we can address them when it's necessary.

**Before you go, you should know:**

- the difference between code and text cells
- what happens when you download your notebook as a .py file

# Lesson Assignment: 

**Step 1: Log into Google**

Be sure you are logged into Google using your **@gmail address** so your notebook will be saved to your drive. If you don't have a google account, you will need to create one.  If you have your university account already linked to google, that will work as well.

**Step 2:** **New Notebook**

Create a new Python 3 notebook and name it INFO490-RemoteReading

The first cell should be a text cell that has your first name in it.

**Step 3: Reading a Google Doc.**

Storing data in your google drive is a great way to be able to access it from various locations.  The trick is to make sure you can access it remotely.  This requires you to generate a sharing link for the document you want to read.  If it's important to keep access restricted, you can use other tools that allow you to authenticate first and then you can read the resource (research PyDrive for another way). 

Once you share your document, you can copy the link.  For example, a copy of Huckleberry Finn is at https://drive.google.com/file/d/**13F68-nA4W-0t3eNuIodh8fxTMZV5Nlpp**/view?usp=sharing.  The part that is in bold is the id that we can use to access the document with Python.

**Paste the following code into a code cell in your notebook:**

```
def build_google_drive_url(doc_id):
```

```
  DRIVE1  = "https://docs.google.com/uc" 
```

```
  DRIVE2  = "https://drive.google.com/uc"  

  baseurl = DRIVE1 # DRIVE2 works as well 
  params = {"export" : "download",
            "id"     : doc_id}
 
  url = baseurl + "?" + urllib.parse.urlencode(params) 
  return url
```

**Create the following function in your notebook:**

```
def read_google_doc(doc_id): 
```

```
   # build a URL for the document with doc_id
```

```
   # read the remote document (see remote IO lesson)
```

```
   # save the contents to a local file 
```

```
   # close the local file
```

```
   # return the contents of the document
```

The function `read_google_doc` does the following:

- reads the contents of the remote document that has the id, `doc_id`
- writes the contents to a file named `doc_id.txt` (where `doc_id` is the actual google doc id).  So if the id was 2112, the file would be named "2121.txt".
- you can and should use the `build_google_drive_url` function.
- return the text (the result of reading a remote resource)
- use Python, not `wget`

**Step 4: Test it**

Once it is done the following should work:

```
def test():
```

```
  HUCK_ID = "13F68-nA4W-0t3eNuIodh8fxTMZV5Nlpp"
```

```
  text = read_google_doc(HUCK_ID)
```

```
  print(len(text), text[0:200])
```

```
  text = open(HUCK_ID + ".txt", "r").read()
```

```
  print(len(text))
```

```
test()
```

The length should be 566232 (characters) long.

**Step 5: Share Your Notebook**

For this step, you need to locate the notebook on your google drive and share it using the 'Get sharable link' menu option.

 

 

In this example,  you would use 1zjANMuAK_rAlPnbo6HJ_Lm_q0R2i5xT_

**Test It.**

You can also test it by pasting the link with a browser **where you are NOT logged into google.com**.  

The permissions should be that anyone with the link can view.

```
def test2():
```

```
  MY_ID = # FILL THIS IN WITH THE ID
```

```
  url = build_google_drive_url(MY_ID)
```

```
  print("try this url in a browser", url)
```

You can also use the same workflow to make sure your document is indeed readable.

```
def test3():
```

```
  MY_ID = # FILL THIS IN WITH THE ID
```

```
  text = read_google_doc(MY_ID)
```

```
  print(text[0:100])
```

Once you have the shared Google URL, extract the ID part and return it in the function that is inside this repl.it (`lesson.py` tab)

```
def get_google_docid():
```

```
  # share your notebook
```

```
  # return the id part of the URL
```

```
  return '18BcaFBJRh4olDrMVKthS6N-RbWyHJOAG'
```

That code needs to be in **lesson.py**

The testing framework will download your notebook and run the code inside of it.  You can tag your questions on Piazza with DSPJupyter


All rights reserved

**Readings and References:**

- https://colab.research.google.com/notebooks/welcome.ipynb
- https://research.google.com/colaboratory/faq.html

Many assignments will use the above workflow to get data into your notebook.  Once you have the data, you can save it to a 'local' file and just use that file while you test your algorithms.  You can use the following logic in `read_google_doc(doc_id)` (not required for this assignment):

```
import os
```

```
# pseudo code for read_google_doc
```

```
filename = doc_id + ".txt"
```

```
if not os.path.exists(filename)
```

```
  • the local file does not exist
```

```
  • read the remote resource
```

```
  • save it locally
```

```
• now return the contents of the file
```

**Confessions in the addendum:**

I do have a confession to tell you and I feel I should tell you _my_ opinion.  I don't _love_ Jupyter notebooks.  Despite my reasons, my goal is to present them in the best light possible. And if you end up loving them, it's not my fault.  My background is in software engineering and most of my complaints have to do how notebooks do **not** encourage good software engineering practices.  The following are just a few of the shortcomings that keep me from giving them a full endorsement 😀

_My_ Issues with Jupyter:

**Portability:**

When you write Python code, it should work on any computer running the same version of Python.  However, the code downloaded from a notebook can be anything but portable: Both the %% magic and ! commands are not portable -- they may not even work across different version so notebook implementations.

**Hidden State:**

It's hard to know what cells are 'stale' .. those cells that have either changed content and not run OR those cells that are un-run (especially after a reconnect is needed).  When a cell is run it pushes state into the runtime environment.  It's hard to know what the exact state of a notebook is at any given time.  Also, as you are creating a notebook, it becomes a pain to scroll back through the cells and figure out which ones need to be re-run for your current cell to work properly.

Here's a very simple example:

 

Now that works.  But if I edit it:

 

That code is wrong.  And I MAY forget to run that cell and if there's no test for it, the previous version is in memory and will be used.  It may be months before the error is noticed.  The problem grows with the number of code cells in the notebook as well.  You can help alleviate this by always including tests in the notebook.

 

**Modularity**

It's difficult to have a notebook separate the different parts of a software design -- they all need to live in the same notebook.  There are ways to import different notebooks, but it's not as easy as building a Python module and importing it.

**IT department needed**

Running a Jupyter server requires a lot of resources (both staff and computers).  If you're in a situation where you need to manage several users (hundreds),  Hosting your own Jupyter cluster is not an easy task to fulfill.

That all being said, the use of a Notebook to tell a story using computation and data is appealing and useful.  However, as soon as you want to share the code development of that story, the process breakdowns into a software development problem and notebooks are not a good tool for doing that.

