HUCK_URL= "https://raw.githubusercontent.com/NSF-EC/INFO490Assets/master/src/datasets/pg/huckfinn/huck.txt"

def read_remote(url):
    import requests
    with requests.get(url) as response:
      response.encoding = 'utf-8'
      return response.text