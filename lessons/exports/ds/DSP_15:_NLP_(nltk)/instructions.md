# **Data Science & 🐍: NLP**

Much of what we have been doing in this classroom is within the domain of **N**atural **L**anguage **P**rocessing (i.e. NLP).  It's a field that is very popular and important since a majority of what people create in terms of historical record is in the form of written text.  NLP is about writing algorithms to help organize, process and understand text.  

Two of the more popular Python libraries for NLP are NLTK (Natural Language ToolKit) and SpaCy.  We will explore SpaCy in the advanced class.   Another good tool that we will not explore is the Pattern library and you should look into once your time frees up: (https://www.clips.uantwerpen.be/pages/pattern)

Jupyter notebooks are useful here since repl.it does not work well (it can take a long time to execute) for these libraries.  This is a bit of a 'chicken and egg' problem for teaching NLP as well.  We could give you a completed Jupyter notebook and ask you to run it and hope you glean what's important -- it's hard to sequentially expose bits at a time inside a notebook.  So that's where repl.it is useful.  It's much easier to unveil sections at a time with adequate explanations. The rest of this lesson will have you write code using these libraries to do some natural language processing.

**Building a better NLP Processor**

We will attempt to find characters in a novel by coding a _heuristic_ (see the lesson on finding characters).  However, most NLP libraries have models (a system of rules built by an algorithm using lots of data) that were created using machine learning techniques for processing text -- for example, finding entities like people, places, and organizations is essentially a classification problem.  The "models" (i.e. heuristics) you built were small and very fast.  The models inside libraries like NLTK and SpaCy are large and were computational expensive to build (and sometimes to run).  However, they _should_ perform better.  The one issue is that if a model was trained using data that is not similar to the data on which it will be fed or tested, the accuracy can be very poor.  For example, a language model built on Project Gutenberg text would perform very poorly on Twitter streams.  That's also an issue with NLTK -- some of its models are outdated.  

**Jupyter Coding**

As we give code samples to put inside a Jupyter notebook, it is expected that you will **type in the code** rather than a simple copy and paste (copy and paste would work, but at the cost of learning). 

> **Learner's Log:** There is a rigor to typing it all by yourself, line by line that makes you understand the tutorial better. When you copy&paste, you almost always take a macro view and conclude with 'ah, I get the primary logic'. When you type it in line by line, you are seeing each line and are forced to inspect it more. 

For the love of Python (and your brain), and a grade for this lesson, type in the code; line by line.  It will be easiest to manage if you use two tabs in the same browser.  Go to https://colab.research.google.com and create a new Python 3 notebook, rename it INFO490NLP and follow along.

# **NLP with NLTK**

The NLTK (natural language toolkit) library is one of the most popular natural language processing libraries for Python.  The project started in the early 2000's and supports multiple languages.  You can visit the 'How To' page (http://www.nltk.org/howto/) to see all of its capabilities.  We will do some quick demonstrations.

**Tokenizing**

One of the basic functionalities of NLP is to tokenize text into sentences and words.  In our examples, we used a regular expression to decide how to split the text into tokens.  For NLTK, it's based on a more elaborate set of rules.  Let's add the following code into a new Jupyter Cell block:

```
def nltk_tokenize_demo(text):
```

```
  for sentence in nltk.sent_tokenize(text):
```

```
    tokens = nltk.word_tokenize(sentence)
```

```
    print(tokens)
```

```
    
```

```
demo = "This is a simple sentence. Followed by another!"
```

```
nltk_tokenize_demo(demo)
```

Now run that code block:

You should see `"NameError: name 'nltk' is not defined"`. That tells us that the NLTK module is not installed.  So add another code block (and move it to the top) to add the import.  Your notebook should look like this:

 

Now you need to execute the cell with the import statement and then execute the cell block with the function `nltk_tokenize_demo`.

You will now get a new error about a model name in the `Punkt` corpus.  So you need to download and install that model as well.  Create a **new** cell block (and move it to the top) and add the following:

```
import nltk
```

```
nltk.download('punkt')  # sentence tokenizer
```

Run that block followed by the code block with the demo.  You should now see the tokens for each of the sentences.  Note how the tokens include punctuation.  You should think about how you would use a regular expression to build a sentence parser -- it's actually very difficult.  You can do it, but as we will see later, it's easier to use a 2-pass algorithm.

**Part of Speech Tagging.**

NLP also includes the ability for determining the parts of speech within a sentence.  This includes finding  nouns, pronouns, adjectives, verbs, adverbs, prepositions, conjunctions and interjections within a sentence.  With NLTK, it's very simple to get these tags.  Put the following in a new code cell:

```
def nltk_pos_demo(text):
```

```
  for sent in nltk.sent_tokenize(text):
```

```
    tokens = nltk.word_tokenize(sent)
```

```
    tagged = nltk.**pos_tag**(tokens)
```

```
    for t in tagged:
```

```
      print(t)
```

```
demo = "This is a simple sentence. Followed by another!"
```

```
nltk_pos_demo(demo)
```

When you run that cell, you get the following error:

`Resource averaged_perceptron_tagger not found`

You need to add another download:

```
`import nltk`
```

```
`nltk.download('punkt') # sentence tokenizer`
```

```
`nltk.download('averaged_perceptron_tagger') # pos tagger`
```

**Note:** It's best to gather all your imports in the first cell, even though they are shown in-line.  It will make it much easier when you get ready to submit the notebook for grading.  When you add a new import to that code cell, you will have to re-run the cell.

Run the above code cell again, and then run the block that uses `nltk_pos_demo`.  The output should look like the following:

```
('This', 'DT') 
```

```
('is', 'VBZ') 
```

```
('a', 'DT') 
```

```
('simple', 'JJ') 
```

```
('sentence', 'NN') 
```

```
('.', '.') 
```

```
('Followed', 'VBN') 
```

```
('by', 'IN') 
```

```
('another', 'DT') 
```

```
('!', '.')
```

In this example, the word 'simple' is an adjective (marked by the `JJ` tag). You can look up the meaning of each of the tags (e.g. `JJ,VB`, etc) here: https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html.

If you were interested in doing sentiment analysis (discussed later) using only adjectives and adverbs, getting the POS tags would be good start.

**Named Entity Recognition (NER)**

Getting labeled entities like People, Location, Organizations, Money, Dates from text is also a common use for NLP.  Basically it's a classifier (machine learning technique for labelling data). In the NLTK we just extend the pos demo:

```
def nltk_ne_demo(text):
```

```
  for sent in nltk.sent_tokenize(text):
```

```
    tokens = nltk.word_tokenize(sent)
```

```
    tagged = nltk.pos_tag(tokens)
```

```
    for chunk in **nltk.ne_chunk**(tagged):
```

```
      print(chunk)
```

```
demo = 'San Francisco considers banning sidewalk delivery robots'
```

```
nltk_ne_demo(demo)
```

When we run that we get another error: `Resource maxent_ne_chunker not found.` So we need to download that as well:

```
import nltk
```

```
nltk.download('punkt')              # sentence tokenizer
```

```
nltk.download('averaged_perceptron_tagger') # pos tagger
```

```
**nltk.download('maxent_ne_chunker')**           # NE tagger
```

When you run both again, we need one more resource:

```
import nltk
```

```
nltk.download('punkt')               # sentence tokenizer
```

```
nltk.download('averaged_perceptron_tagger')  # pos tagger
```

```
nltk.download('maxent_ne_chunker')           # NE tagger
```

```
**nltk.download('words')**
```

After you run that cell again, and the cell that contains `nltk_ne_demo`, you should now see:

```
(**GPE** San/NNP) 
```

```
(**PERSON** Francisco/NNP) (
```

```
'considers', 'NNS') 
```

```
('banning', 'VBG') 
```

```
('sidewalk', 'NN') 
```

```
('delivery', 'NN') ('robots', 'NNS')
```

As a shortcut, you could just add `nltk.download()` to download everything.  But sometimes it is good to be minimalist and know what the basic requirements are for each NLTK task.

You should experiment with different sentences to get a feel for the accuracy of the model that finds named entities:

```
s1 = 'San Francisco considers banning sidewalk delivery robots'
```

```
s2 = 'In San Francisco, Aunt Polly considers paying sidewalk delivery robots $20.00.'
```

```
nltk_ne_demo(s2)
```

Note that in the s1, it thinks Francisco is a PERSON.

The following shows the common NLTK entity types (GPE: **G**eo-**P**olitical **E**ntity):

 

To get a specific named entity, you have to work with the chunk entity:

```
def nltk_find_people_demo(text):
```

```
  for sent in nltk.sent_tokenize(text):
```

```
    tagged = nltk.pos_tag(nltk.word_tokenize(sent))
```

```
    for chunk in nltk.ne_chunk(tagged):
```

```
      if hasattr(chunk, 'label') and chunk.label() == 'PERSON':
```

```
        name = ' '.join(c[0] for c in chunk)
```

```
        print(name)
```

```
s3 = 'In San Francisco, Aunt Polly considers paying sidewalk delivery robots $20.00.'
```

```
nltk_find_people_demo(s3)
```

Just to make sure you are in sync, the NLP part of your notebook should now look like the following:

 

**Fair Warning Reminder**

As a reminder from the previous lesson, If you leave your browser window open and it remains inactive, the browser may detach from the VM.  

 

If this happens you need to reconnect; However you will have to re-run any code cells that download resources for doing NLP.

**Stopwords**

NLTK provides a list of stopwords to use as well:

```
nltk.download('stopwords') 
```

```
from nltk.corpus import stopwords
```

```
def nltk_stop_word_demo():
```

```
  stop_words = stopwords.words('english')
```

```
  print("nltk", stop_words)
```

**Ngrams**

The `ngrams` function from `nltk` allows you to create any sized n-grams:

```
import collections
```

```
from nltk import ngrams
```

```
def nltk_ngram_demo(text):
  tokens = text.lower().split()
  grams = ngrams(tokens, 2)
  
  c = collections.Counter(grams)
  print(c.most_common(10))

text = "We went to a clump of bushes, and Tom made everybody swear to keep the secret, and then showed them a hole in the hill, right in the thickest part of the bushes. "
nltk_ngram_demo(text)
```

**Sentiment Analysis**

Sentiment analysis is about extracting an opinion (positive, negative) from text.  It can be done at different levels of granularity (i.e. document vs sentence). Although the topic of building our own sentiment analyzer would be fun, the details of doing so would be an advanced topic (i.e. involves machine learning).  However, in the meantime, we can use NLTK's implementation:

```
nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer

def nltk_sentiment_demo():
  
```

```
  sentiment_analyzer = SentimentIntensityAnalyzer()
```

```
  
```

```
  # helper
  def polarity_scores(doc):
      return sentiment_analyzer.polarity_scores(doc)
  doc1 = "INFO 490 is so fun."
  doc2 = "INFO 490 is so awful."
  doc3 = "INFO 490 is so fun that I can't wait to take the follow on course!"
  doc4 = "INFO 490 is so awful that I am glad there's not a follow on course!"

  print(polarity_scores(doc1)) # most positive
  print(polarity_scores(doc2)) # most negative
  print(polarity_scores(doc3)) # mostly positive, neutral
  print(polarity_scores(doc4)) # mostly negative, a little positive too

nltk_sentiment_demo()
```

**Stemming and Lemmatization.**

One of the underlying issues with the exercises that involved word frequency counters or dictionaries is that a word is separately counted even though it may exist in the dictionary but in a different form.  For example, argue, arguing, argues, argued would all be distinct keys in our counter even though they essentially are the 'same' word.  Both word stemming and word lemmatization are attempts to solve this issue.  

- **[Stemming** uses an algorithm (and/or a lookup table) to remove the suffix of tokens so that words with the same base but different inflections are reduced to the same form. For example: ‘argued’ and ‘arguing’ are both stemmed to the form: ‘argu’.  The goal is to arrive at a common root form of the word.  The two common algorithms are **porter** and **lancaster**.  The lancaster algorithm is more aggressive than porter. A good discussion on the complete Porter algorithm is given here: http://snowball.tartarus.org/algorithms/porter/stemmer.html and its implementation: https://tartarus.org/martin/PorterStemmer/python.txt.](https://en.wikipedia.org/wiki/Stemming)

- **[Lemmatization** reduces tokens to their lemmas, which is the canonical dictionary form. For example, ‘argued’ and ‘arguing’ are both lemmatized to ‘argue’.  The process uses the part of speech of the word and then applies different rules based on its usage ](https://en.wikipedia.org/wiki/Lemmatisation)(e.g. The house is our home **_vs_** We will house them) in order to figure out the base form.

So the major difference between the two is that a [lemma](http://en.wikipedia.org/wiki/Lemma_(linguistics)) is a canonical form of the word, while a stem may not be a real word -- stemming can often create non-existent words, whereas lemmas are actual words.

NLTK provides an implementation of the different stemming algorithms:

```
def nltk_stem_and_lemm_demo():
```

```
  words = ["game","gaming","gamed","games","gamer","grows","fairly","nonsensical"]
```

```
  ps  = nltk.stem.PorterStemmer()
```

```
  sno = nltk.stem.SnowballStemmer('english')
```

```
  lan = nltk.stem.lancaster.LancasterStemmer()
```

```
 
```

```
  for word in words:
```

```
    base  = ps.stem(word)
```

```
    sbase = sno.stem(word)
```

```
    lbase = lan.stem(word)
```

```
  
```

```
    s = ''
```

```
    if (sbase != base):
```

```
      s += "(or {})".format(sbase)
```

```
    if (lbase != base and lbase != sbase):
```

```
      s += "(or {})".format(lbase)
```

```
  
```

```
    print("{:11s} stems to {:s} {}".format(word, base, s))
```

NLTK's Lemmzer is based on WordNet (`https://wordnet.princeton.edu`) which is an open source dictionary that closely _resembles_ a thesaurus_._  It is a database of English words that are linked together by their semantic relationships.

```
nltk.download('wordnet')
```

```
def nltk_wordnet_demo():
```

```
  lemma = nltk.stem.WordNetLemmatizer()
```

```
  print(lemma.lemmatize('dogs'))
```

**Accuracy**

All of these models (both NLTK and SpaCy) will not give you perfect results.  However, given a large enough sample and assuming the text you are analyzing is somewhat similar to the text that was used to train and build the underlying models, accuracy can be in the 80% - 95% range (see https://spacy.io/usage/facts-figures).

If accuracy is an issue for your particular analysis, you can even build your own models (either for a new language or from a new source of text) and integrate them with both spaCy and NLTK.  Model building is an advanced topic that will be addressed in the DM&🐍 class (the sequel).

You're essentially done.  Please make sure you appreciate all the power you learned.  You will put it to good use soon.

**Before you go, you should know:**

- the main use cases of nlp
- how to use `nltk` for tokenizing, ngrams, entity, part of speech tagging 

# **Lesson Assignment**

**NLP and Finding Characters**

You will use the power of nltk to find characters in a novel.  Write the function `find_characters_nlp`.  It will use `nltk`

```
def find_characters_nlp(text, topn):
```

```
  # returns top n characters found in text
```

Be sure to test this first on small samples of text first.  Once this is done, the following should work:

```
text = read_remote(HUCK_URL)
topn = find_characters_nlp(text, 50)
```

```
print(topn)
```

You should see these numbers:

('Jim', 336) ('Tom', 150) ('Huck', 41) ('Tom Sawyer', 37) ('Aunt Sally', 37) ('Buck', 32)

Notes:

- starter code in `starter.py`
- note that with `nltk`, there's no need for stopwords, ngrams
- most of the code is already done for you
- what did you notice about the running time of how long it takes to find the characters using nlp.  

# 
**Notebook Preparation**

The notebook you created to follow along the above lesson is what you will turn in for grading.  

**Step 1:** Share your notebook you created for this lesson

Hit Share; Get shareable link.  Make sure it works by testing using another browser in which you are not logged into Google.  

**Step2**: Paste the full url for your shared notebook in `lesson.py` as the return value for the function `jupyter`

**Step 3:** Comment out the following code:

- code that uses the `!` syntax or move it into a function named `install`.  As we mentioned this syntax is not Python.  
- move all the download into it's own function

```
def do_downloads():
  nltk.download('punkt')
  nltk.download('averaged_perceptron_tagger')
  nltk.download('maxent_ne_chunker')           
  nltk.download('words')
```

The autograder has all of these installed. These downloads will either slow down the auto grader or cause it to fail

- comment out calls to the demo functions to prevent them from running (for example):

```
# nltk_stem_and_lemm_demo()
```

**Step 4:**  Download your notebook as `.py` file and upload to Gradescope (be sure to save the file as `solution.py` before uploading it).  The assignment is Intro2NLP.

If you see errors regarding the autograder failed, it's most likely due to a timeout error where your code took too long to run.  Reminder: be sure to comment out any calls to the demo functions.

The grader already has everything installed that you will need.  Also if you get the following message, it most likely means that there is a syntax error in your code (most likely from commenting out your code).  Just be sure that your notebook runs before you download it.

 

**Step 5:** Hit submit to receive credit for the replit

Notes:  if the grading process takes too long, one thing you can do to speed it up is to place all the download statements into the function (named `do_downloads`) that you created in the previous step (see above).

You can tag any question you have with **pyNLP** on Piazza


All rights reserved

