
def parse(input):
  # input is a string
  # returns a list of tokens
  return []
  
def pre_clean(t):
  # t is a string
  # remove all punctuation from the parameter t
  # however, keep single quotes.
  # so contracted words have the quote still
  # this comes at a cost of single quoted words
  # will stay:  I love fast 'food'.
  # this could be dealt in a subsequent cleaning
  # stage if necesary
  return t

def normalize(t):
  return t
  
# you can define more functions as well  
def pipeline(text):
  # call the different stages of
  # cleaning, parsing, normalizing
  # you can add additional helper functions too
  # return the results as a list
  return []
  
sample_text = "YOU don't know about me without you have read a book by the name of The Adventures of Tom Sawyer; but that ain't no matter.  That book was made by Mr. Mark Twain, and he told the truth, mainly.  There was things which he stretched, but mainly he told the truth.  That is nothing.  I never seen anybody but lied one time or another, without it was Aunt Polly, or the widow, or maybe Mary.  Aunt Polly--Tom's Aunt Polly, she is--and Mary, and the Widow Douglas is all told about in that book, which is mostly a true book, with some stretchers, as I said before."


