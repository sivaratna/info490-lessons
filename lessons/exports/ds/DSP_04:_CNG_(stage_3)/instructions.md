# **Data Science & 🐍**

# Cliff Note Generator (continued)

**Pipeline Stage 3: Cleaning the data .. continued**

In the previous lesson we wrote a very simple parser for dealing with a string of text.  Our first attempt to split the text into words had a lot to be desired.  In the last lesson, you were asked to list all the issues with the method used to split the text.  Here's my list:

- some words had punctuation: e.g. 'widow,'
- 'double words: e.g. 'is--and'
- blank words: e.g.  ' '  (if you used the `split(' ')` method)
- the words 'YOU' and 'you' (not a problem with the splitting, but a problem that needs to be addressed) should be treated as the same word

The list becomes longer if you parse the entire book -- which we will get to soon.

**Tokens**

The word token is used to represent a sequence of characters that have some kind of meaning.  For doing text analysis, a token is usually a word, but punctuation and whitespace -- which usually separate words -- are also tokens.   Since we are interested in getting words (as opposed to punctuation or currency or phrases), tokens are synonymous with words for our discussion.

 

**Fixing the parser.**

The results of the parser we used to split the words can be fixed by one of three methods:

1. preprocess the input
2. post process the output
3. build a more robust parser

**Pre-process the input**

If the parser can't be changed (it will use `split()`) we can help that functionality by replacing all the punctuation with spaces (so "Hi! Tom, how are you?" becomes "Hi Tom how are you " and splitting that works fine.  However, you need to be careful.  For words that are contracted (e.g. ain't) or hyphenated (e.g. well-water, can-not, etc), you will have to make the decision on how to handle those.  Some analysis will want to keep the punctuation others may remove or replace the contracted words with several words or a single word.

**Post-process the output**

In some cases, it might be easier to clean up the output of the parser.  In this example, you could loop through the tokens and remove, replace, or fix the issues.  You will have to decide which solution is more robust or easier to implement.  Since some tokens were really two words (e.g. 

is--and) separating them and then adding each component back into the token list is a bit of more work.

Usually you will want a generalized solution such that you don't have to make modifications to your code as your process more text.   For example, you could have a giant selection statement that does something like the following:

```
output = parse(text)
```

```
for word in output:
```

```
  if word == 'is--and':
```

```
    # do something
```

```
  if word == 'book,':
```

```
    # do something 
```

This is called a hard coded solution.  It might work for one chunk of text, but it surely won't work for a different book.  It's not a scalable solution.  You could 'fix' this by testing for different patterns (does the word have a '--' in it?, does the word end in a ','.   This is an improvement, but you still are left with a bunch of specific rules, what you want is to come up with a general set of rules that would apply to almost all text documents.

**Building a Better Parser**

Our simple parser could be improved by splitting the text using both spaces (called whitespace -- tabs, newlines, spaces) and punctuation.  If the token is a space or punctuation, then the input is split into two parts.  When you want to perform an action based on a pattern match, you need to use a tool called regular expressions.

Regular expressions allow you to find, replace, delete, and to test for text that matches a pattern.  For example, if you define a word as only containing letters, then the regular expression for that patter is `[A-Za-z]+`.  The square brackets denote a set of character classes; `A-Z` is the set of uppercase letters, `a-z` is the set of lowercase letters.  The `'+'` sign at the end means a sequence of 1 or more of the preceding pattern.  If you wanted to match numbers you could use the regular expression (regex for short)  `[0-9]+`  (you would read this as 1 or more characters from the range 0 through 9).

However, mastering regular expressions will take us on a journey that would detract from our main goal: getting through this example.  We will revisit regular expressions in an upcoming lesson.  

**Cleansing Data**

A closely related topic to cleaning the data in preparation for analysis is the idea of clean**s**ing the data -- detecting and correcting data _quality_ issues.  The focus here is on fixing **corrupt data**, **inconsistent data** (merging data from two different sources that use different data management schemes), **inaccurate data**, **irrelevant data** (remove useless or redundant attributes in the data), **dirty data** (where data was wrongly ingested), **typographical errors**, **incomplete data**.   Many of these issues are relevant when the data is coming from people (think survey data -- free form response).  People may be saying the same thing, but if it doesn't get coded in a consistent way, your analysis will suffer.

**Normalizing Tokens**

Once we are done with parsing and cleaning, we need to further process the data for our specific analysis (word counts).  For many text analysis projects it's important to normalize the data.  That is put the data into a format that removes differences that would affect our analysis.  In this case the words 'You', 'you', and 'YOU' should really be considered the same word.  Not only are you trying to get an accurate analysis, but you can also speed up the project by not having to consider so many different versions of the same 'word'.  Sometimes this is called data standardization.

Additional normalization may include stemming -- which attempts to take different forms of a word and reduce them to their common root.  For example, work, working, worked, works could all map to the word work.  Other normalizations techniques include spell correcting, spell normalization (e.g. changing can not to cannot), removal of common words (called stop words or stop lists).  Also expanding contractions might be necessary (e.g. changing can't to cannot).  Each of these has a cost (time, money, inaccuracy) and a benefit (time, money, accuracy). The question you need to ask yourself is what normalizations would be useful and at what cost.

For our purposes, the only normalization you should do is to ignore differences in case.  So the words 'You' and 'YOU' are treated as the same word. This includes single letters capitalized in the middle of a word (e.g. You bET).  

**When to normalize**

Moving the text to a normalized form can be done before, after, during the parsing of the information.  It's really up to the data scientist to understand the tradeoffs of when to normalize the data (the same situation we discussed for cleaning).  

Don't get too hung up on whether this process is actually _cleaning_ the data or is it _normalizing_ the data.  If the process of cleaning the data, puts it into a format that further processes down the pipeline can assume, then that cleaning is part of data normalization.

**Before you go, you should know:**

- When and why do we have to parse the data?
    - What is cleansing and normalizing the data?

# **Lesson Assignment**

All your code will go in the `lesson.py` tab.  

**1.** Finish the definitions for all the functions in `lesson.py` such that you can take the text inside the variable `sample_text`, clean, and parse it into a list of tokens.

**2.** Implement the function `pipeline(text):`

Note the order of the stages:  

1. cleans (`pre_clean`),  (see comments in code)
2. parses (`parse`)
3. normalizes (`normalize`) its input from parse (the text of this lesson specifies what to do).

So `pre_clean` can help the parser (that's a hint). Be sure to have pipeline return the list of processed tokens (see comments).  You should have each stage of the pipeline saved into a variable (no need for nested function calls).

Hints for cleaning/normalizing the text:

- look at `string` module (`https://docs.python.org/3.6/library/string.html`) it has some of the common character sets (including punctuation). You can modify those strings as well to suit your needs.
- Do NOT use any regular expressions for parsing, cleaning, etc. 
- After you are done, if you send `lesson.sample_text` through your `pipeline()`, you should get 108 total tokens
- As a bonus, can you figure out (using Python) how many unique tokens there are? (hint: 66)

You can tag any questions with **DSP4** on Piazza


All Rights Reserved

