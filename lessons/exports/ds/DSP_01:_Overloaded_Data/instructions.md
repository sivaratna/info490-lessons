# **Data Science with 🐍:**  Overloaded Data

Before we tackle our first data science example, let's address an issue that is (or will be) recurring: the _meaning_ of data.  One of the problems with being a data scientist is knowing that how people use and think about the word 'data' will depend highly on their experience.  **A computer scientist** thinks of the representation, transfer, access, storage, retrieval issues surrounding the data.  **A statistician** will be concerned about what analysis is appropriate and possible as well as what modifications or additional collections need to take place to make an accurate analysis.  **A domain expert** may think of data as a specific collection of measurements to meet her/his needs as well as the surrounding issues of ownership, legality, management, location, cost, and provenance (among a few).  Even though they each may have some different views, there are some common aspects (like data ethics and privacy) that span all the viewpoints.

# **A CS View of Data**

You might also hear the words _unstructured_ vs _structured_ (and even _semi-structured_) to describe the data as well.  These words are usually from a computer scientist -- whose task is to figure out how to read and parse (split it up into manageable components) the data.

The first example we will use (as are many problems in data science) involves processing text information.  Text is considered **unstructured data**.  You may think that, for example, the text of an email message or a book has some structure to it, but to a computer, there's no universal set of rules that can take any collection of words and attach structure and semantic meaning to each component.  A human can; but not a computer.  

Most data is unstructured.  Video, images, text (e.g. chat, tweets) and audio cannot be understood without a lot of processing.  Each of those data sources has well defined rules on how to parse (divide or break up into separate units) and interpret the bytes (e.g. the first four bytes is the version number, the second 4 bytes is a date, etc) that make up the data (so it has architectural structure), but you can't, for example, query the **raw data** of a video stream or a jpeg image and ask if there are cats in it.  If there's room for interpretation, the data is most likely unstructured.

For an email, you could parse it into the separate fields (like columns in a spreadsheet) for the sender, receiver, subject, and the message itself. However, the subject and message fields (or columns) would still contain unstructured text that would not reveal any specific semantics (however, the sender and receiver fields do). Email stored this way could be considered **semi-structured**.  Both XML and JSON (more on this later) are semi-structured since they either tag, label, or identify various parts of the data -- removing some of the interpretation.  For example, if a field is labeled temperature, then you will know how to interpret the value.  But you would still need to know the units of measurement (e.g. fahrenheit, celsius, kelvin).  So it has _some_ structure, but not enough.   

**Structured data** has specific rules on how to parse the data.  For example, a sensor may record and send information on its id, location, date/time, and measurement.  Each of those data items is well defined on how to parse and interpret it. You might be able to fully convert an email into structured data by replacing the message and text fields with simple booleans (e.g. `is_spam`) or calendar events. Other examples of structured data include bank transaction data, financial data and inventory data. Most csv (comma separated values - spreadsheet, table data with rows and columns) is usually structured, however, columns in that data could be unstructured (e.g. free response to a question asked).

When you categorize data as either structured or unstructured, you need to know what's the form of the data being considered. Almost all assume it's the raw data that's being categorized.  It takes a lot of computer science (and data science) to move raw data into having a meaningful structure. For our example, the text of a book is indeed unstructured.  All we know is that each word is either separated or delineated by white space (line feeds, newlines, spaces, tabs) or punctuation.

**Formats,  encoding and decoding**

One of the key issues for a data scientist is understanding how to parse (and eventually clean) the different formats that data might be delivered in.  Understanding how data is encoded (translated into something with which a computer can use for either transmission or storage) and decoded (translated into something a program or human can use) are key issues to understand.

In this class, we will first focus on text (sentences from a book, email, etc) and then look at data formatted in csv (Comma Separated Values), json (Javascript Object Notation), html (web pages) and xml (Extensible Markup Language -- like html but with user defined tags) format.  Each type requires its own parser (how to split and interpret the different parts).  Luckily Python has many libraries to help us avoid the painful details.

# **A Statistician's View of Data**

In order to know what processes, tools and analysis can be done with the raw data, we need to understand the nature of the data from a statisticians' point of view.  You can't multiply words nor can you find the sentiment of a financial statement.  Data can be categorized as either **qualitative** (data that doesn't lend itself to 'measurement') or **quantitative** (data or information that can be measured).  

A statistician might also discuss how the data can be organized.  Qualitative data is also **categorical** data that can be put into mutually exclusive categories (coffee roasting techniques).  For categorical data there usually is no logical ordering of the categories but others may (coffee bean roast types: light, medium, dark, espresso) have an order.

Quantitative data can be classified as either **discrete** or **continuous**.  Discrete data (or variables) have a countable number of values between any two values and continuous data (or variables) have an infinite number of values between them. 

Additionally, for those data types that can be measured, statisticians use the four measurement scales: **nominal**, **ordinal**, **interval** and **ratio** (please read `https://www.mymarketresearchmethods.com/types-of-data-nominal-ordinal-interval-ratio/` for a quick review).  In summary, both nominal and ordinal are categorical while interval and ratio are continuous, quantitative measurements.

Take a look at the following diagrams which summarize the four measurement scales:

 

 

(image credit: see references)

There's also the idea of discussing data (or variables within the data) of being **independent** or **dependent** and other aspects of the data like **dimensionally**, unary or univariate, binary or bivariate, and on and on.

**Not so Fast**

The above discussion can become a bit tricky because it depends on your reference and definitions.  There's even a slight inconsistency in the above standard definitions.  The _measurement_ scales include both nominal (no inherent order) and ordinal (arrange the categories logically, although there is no meaning for the difference between two categories). Yet the classic definition of qualitative data (which includes nominal and ordinal data) is data that can't be measured.  So sometimes the word measurement is restricted to those things that can be numerically measured and sometimes it's a broader use of the word.

Sometimes the same type of variable can be classified into two different ways.  If you encode a person's eye color into a variable, you would say that data is categorical (you can't measure it) even if you could measure it (by using the RGB values of an eye image), there's no inherent order between the categories (hence it's nominal). However, if you have a variable that represents the number of respondents with blue eyes, then 'eye color' is now a discrete and countable (i.e. measurable) entity.  

You could also take a continuous entity (like height) and bin it into a discrete number of ranges.  A similar issue is with age.  Age is indeed continuous, however, if we measure in years AND round off the values to the nearest year, then our values become discrete.  Also, if you confine your measurements to the accuracy of some instrument or computer, then everything could be argued to be a discrete value.  For our purposes, we will consider the inherit data type of the value, not how the value is being used, wrangled or reported.

# Machine Learning

 

In the field of machine learning (ML), sometimes considered a combination of both statistics and computer science, there is discussion of data being **labeled** or **unlabeled**.  This is usually with respect to a variable or column in the data that identifies the row (a.k.a an instance) of the dataset.  That variable (a.k.a. target, output) could be a classification (e.g. 'dog', 'cat', 'rat') or an unknown continuous value (e.g. future price, expected grade).  

Labeled data can be used for a class of problems known as **supervised** ML where the goal is to learn the label (or target variable) from relevant features (independent variables) in the data. Types of supervised ML include regression (predicting a numeric value) and classification (predicting a category).

Unlabeled data (data without a target variable) can be used in **unsupervised** ML where the goal is to discover hidden relationships, trends, or patterns in the data.  Types of unsupervised ML include clustering and association rules.

# **A Domain Expert's View of Data**

The meaning of data to a domain expert will depend not only on the person's professional background but also the people or institutions that s/he represents.  It's important for the domain expert to communicate `h[er|is]` (a regex reference that you will soon appreciate) personal and professional interests with respect to the outcome of the data science research or project.

# **A MetaData View**

In addition to the data itself, the metadata (information about the data) is often just as important as the data itself.  Information regarding the collection (who, what, when, how, and why or purpose), storage (size, quality, compression used), structure (relationships, data types) and ownership need to be managed and maintained as much as the data itself.  For example, if you hosted a survey to collect people's input on a product, the browser a respondent used to take the survey might be meaningful metadata to the answers you are gathering.  A detailed analysis from your data science pipeline will always be enhanced if you can also provide interesting correlation (and perhaps, causation) data with metadata. 

# **Lesson Assignment**

In the data tab is a table of data and some measurements including:

- **#Children**: number of children
- **Class**: economic class (I-V are labels for those classes)
- **Happy**: answered on a scale from Strongly Disagree to Strongly Agree
- **ID** is an assigned unique identifier

For this lesson, we are using a new Python type.  The `Enum` type.  It allows you to create categorical values for a variable.  

Fill out the answer dictionary in `lesson.py` such that each question will contain a set of answer(s) for the following questions.  Each answer is an array of enum types.  Question 0 is already done so you can see the format of an answer.  Be sure to hit run to confirm you have no syntax errors.

Question 0:  What columns begin with a vowel (answer already given)?

Question 1:  What variable(s) are classified as **quantitative** variable(s)?

Question 2:  What variable(s) are classified as **qualitative** variable(s)?

Question 3:  What variable(s) are classified as **continuous** variable(s)?

Question 4:  What variable(s) are classified as **discrete** variable(s)?

Question 5:  What variable(s) are classified as **ordinal** variable(s)?

Question 6:  What variable(s) are classified as **nominal** variable(s)?

Question 7:  What variable(s) are classified as **binary** variable(s)?

You can submit your answers.  The answers are tested in order so if, for example, you fail Question 5, that would mean 0 through 4 passed.

Of course, it's easy to keep trying different answers until you pass the tests, but what you really want to do is reason why for each variable.  The goal is to learn, not just to pass the tests. Since the nature of data is anything but objective (as we have learned), some of the tests have multiple accepted solutions. 

Tag any questions with **DSData** on piazza

**Before you go, you should know:**

- why the word data is so hard to define
- the different levels of measurement


All Rights Reserved

**more reading:**

http://www.psy.gla.ac.uk/~steve/best/ordinal.html

[https://www.graphpad.com/support/faq/what-is-the-difference-between-ordinal-interval-and-ratio-variables-why-should-i-care/](https://www.graphpad.com/support/faq/what-is-the-difference-between-ordinal-interval-and-ratio-variables-why-should-i-care/)

[https://www.theanalysisfactor.com/level-of-measurement-not-obvious/](https://www.theanalysisfactor.com/level-of-measurement-not-obvious/)

[https://medium.com/@rndayala/data-levels-of-measurement-4af33d9ab51a](https://medium.com/@rndayala/data-levels-of-measurement-4af33d9ab51a)

