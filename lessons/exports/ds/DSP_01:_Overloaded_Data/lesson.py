from enum import Enum

# do not change this
cols = Enum('columns', ['ID', 'Age', 'Gender',  'Height', 'Blood', 'Happy', 'Children', 'Smoke', 'Class'])

# put your answers here:
answers = {
  "q0": [cols.ID, cols.Age],
  "q1": [],
  "q2": [],
  "q3": [],
  "q4": [],
  "q5": [],
  "q6": [],
  "q7": []
}
