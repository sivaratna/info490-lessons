import numpy as np

import csv

def get_cereal_data():
  with open("data.csv", 'r') as f:
    data = list(csv.reader(f))

    # pop of the first column (the name)
    header = data[0]
    header.pop(0)
    names = [i.pop(0) for i in data[1:]]

    # ignore the header
    matrix = np.array(data[1:], dtype=np.float)
    # convert all -1 to np.nan
    for row in matrix:
       mask = row == -1
       row[mask] = np.nan
    return names, header, matrix


class CerealData(object):
  names, header, data = get_cereal_data()
  @staticmethod
  def get_cereal_data():
    return (CerealData.names,CerealData.header,CerealData.data)