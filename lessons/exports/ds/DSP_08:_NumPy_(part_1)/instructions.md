# **Data Science & 🐍:  Numerical Analysis with NumPy**

**Prerequisites:**  

- **Array Slicing and Dicing**

**Welcome to NumPy!**

One of the more popular libraries for working with arrays of data is NumPy (Numerical  Python and pronounced num pie).  You could use the `list` type for all your array needs; however, NumPy makes working with arrays (both single and multiple dimensions) easy by allowing efficient syntax and a large number of utility methods and functions to handle just about any numerical need.  NumPy is also very fast.  The library is built in C (a language Python is also implemented in) and has great execution time.  For easy and fast array and matrix manipulation, NumPy comes to the rescue.  

For doing numerical linear algebra and working with vectors and matrices, NumPy is the standard library (it's also at the core of most machine learning algorithms).  Other popular libraries like Pandas and SciPy (Scientific Python) uses NumPy as well.  Learning how to effectively work with NumPy makes the other libraries easier to learn.  Much of what you will learn in Pandas comes directly from NumPy.

In the same spirit of this class, where we are more interested in understanding the concepts rather than learning recipes on how to use an API, we will learn the basics of NumPy within the context of helping us work with data.

To use NumPy you must import it; and as a convention, it's imported as `np`:

```
import numpy as np
```

**Python List vs Numpy Array**

As we have seen, a Python list can hold a set of heterogeneous items (e.g. strings, numbers, booleans, functions, etc).  However, NumPy arrays can only hold multiple items of the same type.  Numpy arrays are **statically typed** (vs dynamic) and **homogeneous**. The type of the elements is determined when the array is created --it's why it can be memory efficient and fast.

NumPy also makes a distinction between numbers that have no decimal points (aka. integers) and those that do (aka. floating point  - since the decimal point is allowed to 'float').  Not only can you specify different types in NumPy (e.g. integer, float, boolean) but also how much storage you want those values to occupy.  If you are only dealing with small numbers (e.g. -128 to 127) you can manage by using a single byte (8 bits) of data for each number; however, if you need to hold very large numbers you can specify up to 64 bits (8 bytes).  You can see the type by using the `.dtype` property  For now, we won't need to worry about selecting the most suitable size.

> **Coder's Log:**  You don't have to master bits and bytes for this class, but you should be curious why everything in computer's seems to be a power of 2 (2,4,8,16,32,64,etc).  A bit (the smallest unit capable of holding a value can either be on (i.e. hold a 1) or off (i.e. hold a 0).  So one bit can hold 2 states.  Two bits could hold 4 states (on, on; on, off; off, on; off, off).  Eight bits (a byte) can hold 2⁸ == (2*2*2*2 * 2*2*2*2) == 256.  So 8 bits can hold 256 unique values.  One range would be -128 to +127.

Let's take a quick look at both the Python list and the NumPy single dimension array:

```
import numpy as np
```

```
py_list = list(range(0,10))
```

```
# numpy's version of range
```

```
np_num  = np.arange(0,10)
```

```
print(type(np_num))
```

```
print(py_list)
```

```
print(np_num)
```

What did you notice about how each gets printed?

**Array Creation**

There are several functions to create arrays (and more than one dimension, as we will see):

```
# create an array from a Python list
```

```
data = np.array([1,2,3])
```

```
print(data)
```

 

In addition, there's convenience functions to create arrays/vectors/matrices filled with 1's, 0's, and random numbers:

 

 

**Linespace**

Numpy provides a way to easily divide a range into equally sized intervals.  This is very useful when you need to create grid spacing or equally split up a discrete range.

```
x_axis = np.linspace(0,10, 100)
```

This becomes super useful when you need to 'spin' around a portion of a circle:

```
x_axis = np.linspace(-np.pi, np.pi, 100)
```

**Size and Shape**

You can find out how many elements are in the array using `.size` property.  This is the preferred way (but the `len` function does work -- for one dimensional arrays).  

Each NumPy array also has a `shape` attribute.  The shape is returned as a tuple that describes the size of each dimension For single dimension arrays, the shape[0] is the number of elements in the array:

```
np_num = np.arange(0,10)
```

```
print(np_num.size, np_num.shape)
```

Numpy will return `(10,)` for the above single dimension array with 10 elements.

**Operator Overload**

As we have seen some of the mathematical operators work with lists:

```
print(py_list * 3)      # concatenation 
```

```
print(py_list + py_list)# concatenation 
```

```
print(py_list + 2)      # will **not** work
```

These operators (* and +) are defined on the `list` class as methods. This is one of the powerful features of Python.  When you create your own class (we have not done that yet), you can also define how some binary and unary operations will work.  We have seen that the `set` class defines the '`-`' operator for doing set difference between two sets.

Since NumPy knows all the elements are the same type (usually numbers), the mathematical operators work on the individual elements:

```
print(np_num * 3)      # multiply each item by 3
```

```
print(np_num + np_num) # add the corresponding items together 
```

```
print(np_num + 2)      # add two to each item
```

```
print(np_num % 2)      # modulus/remainder
```

These are called **element-wise** operations since the mathematical operation is performed on each element in the array.

```
data = np.array([1,2])
```

```
ones = np.ones(2)
```

```
print(data + ones)
```

```
print(data * data)
```

It's very easy to build vectors/matrices of answers without using any loops:

```
miles = np.array([1,2,3])
```

```
kilometers = miles * 1.6
```

```
print(kilometers)
```

This can be visualized much like:

```
[1] * [1.6] = [1.6]
```

```
[2] * [1.6] = [3.2]
```

```
[3] * [1.6] = [4.8]
```

These operations that are performed on each element in an array are called _vectorized_ operations and also named universal functions (UFuncs for short) in NumPy.

Some other functions that work on an element-wise basis:

`np.abs`    Calculate the absolute value element-wise.

`np.sin`    Trigonometric sine, element-wise.

`np.log`    Natural logarithm, element-wise.

`np.square` Element-wise square of the input

If you need to perform an operation, element-by-element, most likely NumPy has a function to do it for you.

**None in NumPy**

Numpy uses a special constant to indicate a missing value or a value that can't be represented by using `numpy.nan` or `numpy.NaN`, `numpy.NAN` (**N**ot **a** **N**umber).  There are several functions that can be used to determine if a value is NaN:

```
data = np.array([1,2,3, np.nan])
```

```
print(data[3])
```

```
print(np.isnan(data[3]))
```

**Simple Functions**

There's also aggregation functions (functions that return a single value like `max`) that work with NumPy arrays as well:

 

`np.sum`  Compute sum of elements

`np.prod` Compute product of elements

`np.mean` Compute mean of elements

`np.std`  Compute standard deviation

`np.var`  Compute variance

`np.min`  Find minimum value

`np.max`  Find maximum value

`np.argmin`  Returns the index of minimum value

`np.argmax`  Returns the index of maximum value

`np.median` Compute median of elements

`np.percentile` Compute rank-based statistics of elements

`np.any`  Evaluate whether any elements are true

`np.all` Evaluate whether all elements are true

`np.ptp`  Calculates the range (np.max - np.min)

```
x = np.array([0,1,2,3,4,5,6])
```

```
print(np.**sum**(x))
```

```
print(x + 2*x)
```

Also, most aggregates have a `NaN`-safe counterpart that computes the result while ignoring missing values, which are marked by the `np.NaN` value.

```
x = np.array([1,2,0,3,6,5,np.NaN,4])
```

```
print(np.**nan**sum(x))
```

```
print(np.**nan**argmin(x))
```

```
print(np.**nan**argmax(x))
```

**2D and beyond**

We mentioned a few times that the power of Numpy comes into play as soon as we extend beyond a simple series of numbers (i.e. a 1 dimensional array).

 

Using NumPy to manage 2 dimensional arrays is just as easy:

```
data = np.array([
```

```
  [1,  2, 3], 
```

```
  [4,  5, 6],  
```

```
  [7,  8, 9],  
```

```
  [10,11,12]]) 
```

`np.ones`, `np.zeros`, and `np.random.random` work in multiple dimensions as well:

 

We will explore higher dimensions in the next lesson.

**Size, Dimension, Shape**

The method `size` returns the number of elements, and `ndim` returns the number of dimensions.  

Try to find out what the `len` function returns on data:

```
print(data.size, data.ndim)
```

You can ask a Numpy 2D array its shape as well:

```
print(data.shape)  # 4 rows, 3 columns
```

Each element of the returned tuple describes each dimension.

**Indexing**

Standard array/list indexing works as well:

```
one_d = np.array([1,2,3])
```

```
two_d = np.array([[1,2,3],[4,5,6],[7,8,9]])
```

```
print(one_d[1])
```

```
print(two_d[1])
```

```
print(two_d[1][1])
```

**Slicing And Dicing**

Just as with Python lists you can slice (see Python Topics), NumPy arrays using the same syntax:

`[start : stop : step]`

```
x = np.arange(100).reshape(20,5)
```

```
print("Rows 2,3,4 ", x[2:5])
```

```
# Advanced slicing
```

```
print("First 5 rows\n", x[:5])
```

```
print("Row 18 to the end\n", x[18:])
```

```
print("Last 5 rows\n", x[-5:])
```

```
print("Reverse the rows\n", x[::-1])
```

However, for multiple dimensions, this syntax is extended to each dimension -- where each dimension is separated by a comma:

```
data = np.arange(1,21).reshape(4,5)
```

```
print(data)
```

```
print(data[:2  , :3])     # two rows, three columns
```

```
print(data[:3:2, :4:2])   # ??? 
```

So it's incredibly easy to extract an attribute (column) from all the rows in a dataset:

```
print(data[:, 2]) # all rows, column '3'
```

Do not go past this point unless you understand what is happening.  Most of the lesson (and NumPy and Pandas) depends on understanding this compact syntax.

 

 

 

**Operator Indirection and Masking**

What's even more powerful is that you can use the result of one NumPy array to extract values from another NumPy array.  In this example, we build an array of the integers that represents an index for values we want to extract.  In the example below, we will extract the values at index 0, 5, and 9:

```
np_num = np.arange(0,100,10) 
```

```
# [0,10,20,30,40,50,..,90]
```

```
np_idx = np.array([0,5,9])   
```

```
# want first sixth and last 
```

```
print(np_num[ np_idx ])      
```

```
# [0, 50, 90]
```

If the index array contains boolean values, we extract the values whose index value is True.  This is called a boolean mask. For example the following code creates a boolean array (from a Python List) that is filled with alternating True, False values (10 total):

```
np_num  = np.arange(0,10)
```

```
np_bool = np.array([True, False]*5)
```

```
print(type(np_bool))
```

Here is a quick illustration of the above situation

`[**0**, 1, **2**, 3, **4**, 5, **6**, 7, **8**, 9]  np_num`

`[T, F, T, F, T, F, T, F, T, F]  np_bool`

We can then use this boolean mask to extract the values inside of `np_num`.  Those values that are True inside `np_bool` will "filter" those in `np_num`:

```
np_num  = np.arange(0,10)
```

```
np_bool = np.array([True, False]*5)
```

```
np_wow  = np_num[np_bool]
```

```
print(np_wow) 
```

The above situation:

`[**0**, 1, **2**, 3, **4**, 5, **6**, 7, **8**, 9]  np_num`

`[**T**, F, **T**, F, **T**, F, **T**, F, **T**, F]  np_bool`

`--------------------------------`

`[0,    2,    4,    6,    8   ]  np_wow`

**Now for the Fun**

With that background information, we can easily create boolean arrays based on a condition and then use that array to extract the values:

```
np_num  = np.arange(0,10)
```

```
np_bool = np_num < 5
```

```
print(np_num[np_bool]) 
```

That's an efficient way to get all the values inside `np_num` whose value is less than five. We can even combine the syntax to make for a reading friendly version:

```
print(np_num[np_num<5])
```

See the appendix for yet another way to mask with Numpy.

**Working With Strings**

Even though Numpy is meant for heavy duty processing of numeric data, it also can be used to process strings, unicode, and character data (https://docs.scipy.org/doc/numpy/reference/routines.char.html).

```
fruit = np.array("apples,pears,bananas,oranges".split(','))
print(fruit)
lens = np.char.str_len(fruit)
print(lens)
```

You can do masking as well:

```
short = (lens <= 5)
print(fruit[short])
print(np.char.count(fruit, "a"))
```

The function `np.char.count` works with multiple dimensions as well:

```
data = ["apple pear pizza",
        "apple lemon bagel milk lemon",
        "pizza soda apple"]
trips = np.array(data)

print(np.char.count(trips, 'apple'))
print(np.char.count(trips, 'lemon'))
print(np.char.count(trips, 'pizza'))
```

You should see the following output:

```
[1 1 1] <= bought an apple on all three trips
[0 2 0] <= bought 2 lemons on trip 2
[1 0 1] <= bought a pizza on trip 1 and trip 3
```

**A word on words**

All of the following will be used at times when dealing with numPy data: lists, arrays, vectors, columns, rows, matrices (and a few others).  In general, a list, an array, a vector all describe a one dimensional ordered set of numbers.  The word vector could mean a row vector or a column vector.  Both are components inside a matrix -- which is usually a 2 dimensional grid of values:

 

**Before you go, you should know:**

- numpy array creation
- how to index an array
- how operators (e.g. +, -, *) work on numpy arrays
- how masking works

# Lesson Assignment

In the tab, `data.csv` is the cereal dataset you worked on before.  This is a very popular dataset of 77 cereals although it is from 1993. You are going to use Numpy to do some simple data analysis.  

You can use the CerealData type in inside `util.py` to get the dataset: 

```
names, header, data = CerealData.get_cereal_data()
```

```
print(names[0:3]) # the names of the first 3 cereals
```

- `CerealData.names`: the cereal names (as a Python list)
- `CerealData.header`: the column names (as a Python list)
- `CerealData.data`: the 2D matrix of values (numpy array)

The arrays (names and the rows in data) are parallel arrays.  That means each cereal is represented by the same index:

```
idx = 10
```

```
print(names[idx])
```

```
for c_idx, c in enumerate(header):
```

```
  print(header[c_idx], data[idx][c_idx])
```

You can use this data to find out simple information:

"How much protein is in Quaker Oatmeal?"

```
r_idx = names.index('Quaker Oatmeal')
```

```
c_idx = header.index('protein')
```

```
row = data[r_idx]
```

```
print(row[c_idx])
```

Some of the data is missing.  Missing values have `np.nan` for them:

```
r_idx = names.index('Almond Delight')
```

```
c_idx = header.index('potass')
```

```
row = data[r_idx]
```

```
print(row[c_idx])
```

With NumPy slicing, you can easily get a single attribute for all the cereals:

```
c_idx = header.index('weight')
```

```
weight = data[:, c_idx] # all rows, a single column
```

```
print(weight)
```

**Common Ground**

For each of the attributes in the cereal (i.e. calories', 'protein', 'fat', 'sodium', 'fiber', 'carbo', 'sugars', 'potass', 'vitamins') they are per serving.  The biggest issue with this dataset is that not all cereals are using the same serving size.  Although most are based on a single ounce (seems small), some are not.  We can find these by using the masking ability:

```
mask = weight != 1.0
```

This creates a mask (print it) that shows which servings are not equal to 1 (an ounce).  We can then use this mask to print out the actual serving sizes of those non-standard cereals:

```
non_standard = weight[mask]
```

```
print(non_standard)
```

```
print(len(non_standard))
```

There are 13 cereals that are not using a one ounce reference.  If this syntax looks bewildering, re-read the lesson.  It does take some time for the slice notation (and masking) to feel 'normal'.

Your first task is to put all the cereals on a 'level playing field'.  Also, let's be realistic about the serving size. For this lesson, you need to figure out what we should multiply each of the cereal attributes by so that valid comparisons can be made.

Let's assume the following:

A standard serving will consist of `40.0` grams of cereal

An ounce is `28.3495` grams

Create a vector (an NumPy single dimensional array) that represents a series of numbers.  Each number corresponds to a cereal.  So the first number is for '100% Bran', the 2nd is for '100% Natural Bran', and so on.

Here's how you know you have the right answer:

```
print(28.3495 * conversion * weight) 
```

```
print(np.all(28.3495 * conversion * weight == 40.0))
```

So if you take your vector and multiply it by the weight (in ounces) per serving, you should get 40.0 for all the cereals.  The function `np.all` returns True if all the elements in the array are True.

You can create the solution with a SINGLE line of numpy code.  If it helps to use more, you can, but the idea is to let numpy do all the work for you. 

```
`conversion = **TYPE IN CODE HERE**`
```

- If you start using loops, you're on the wrong path

**Step 1: Same Servings**

Once that is working, create a function inside `lesson.py` that does the same thing.  But now the function returns this conversion array (i.e. vector) 

```
def build_conversion_vector(cereal_data):
```

```
  return np.nan
```

- cereal_data is a CerealData object.  So `cereal_data.names`, `cereal_data.header`, `cereal_data.data` are all valid
- build a single dimensional array that represents the conversion factor for each cereal
- be sure to use good programming practices (use a variable to hold the constants, use proper case)
- you can put all your constants near the top in lesson.py

The following should work:

```
vector = lesson.build_conversion_vector(CerealData)
```

```
idx    = CerealData.header.index('weight')
```

```
weight = CerealData.data[:, idx]
```

```
print(vector*weight)
```

Once again, be sure you understand what you are doing and seeing.

**Step 2:  Most Calories**

Which cereal has the most calories per 40.0 gram serving?  That is which cereal will give you the most calories if you ate 40.0 grams of it?

```
def most_calories(cereal_data):
```

```
  return None
```

- return the index of the cereal that has the most calories per 40gram serving
- use your function from step 1

Once done, the following should work:

```
idx = lesson.most_calories(`CerealData`)
```

```
print(CerealData.names[idx])
```

**Step 3:  Least Sugar**

Write a similar function to return the index of the cereal that if you ate 40.0 grams, it would contain the least amount of sugar among the 77 cereals.

```
def least_sugar(cereal_data):
```

```
  return None
```

Once done, the following should work:

```
idx = lesson.least_sugar(`CerealData`)
```

```
print(CerealData.names[idx])
```

**Note**: There could be several cereals that offer the same low calories.  The tests assume you keep the data in the exact same order.  Numpy will return the first matching row -- which is the row the tests expect.

**Step 4:**  **How Many Bowls**

Assume that you need 71 grams of protein in a day and your bowl holds 1.25 cups of cereal.  If your favorite cereal is 'Apple Jacks', how many bowls would you need to eat to get that amount of protein?

Create a function that returns the number of bowls (as a floating point number) you will need to eat in order to get 71 grams of protein.  This function will work for any cereal in the database and any amount of protein:

```
def bowls_of(cereal_data, name, protein_needed):
```

```
  return None
```

  

After it is done the following should work:

```
bowls = lesson.bowls_of(`CerealData,` 'Apple Jacks', 71)
```

```
print(bowls)
```

**Notes:**

- You can assume that `name` is a valid cereal.
- If you're not sure about your answer, do the calculation by hand and see if you are getting the correct result.
- One of my favorite cereals is Cocoa Puffs (and Cocoa Pebbles), how many bowls do I need to eat to get 71 grams of protein?

If you know Pandas, you may ask why are we doing all of this -- if in Pandas it will be so much easier?  

- There are times when you need to use Numpy with Pandas
- This gives you a sense of how Pandas actually works (it relies on Numpy)

You can tag any question with **np1** on Piazza.

Notes on the grader.

- The only constants allowed are `1.25`, `28.3495` `and` `40.0` (be precise)
- The constants must be named using UPPER CASE LETTERS (a Python best practice).
- Try to name the variables for what they hold semantically (e.g. first_name, idx_of_cereal) rather than things like n1, n2, idx1, idx2, etc.  
- All the solutions can be done using numpy (no loops, comprehension, etc)


All Rights Reserved

**Readings and References:**

- https://hackernoon.com/introduction-to-numpy-1-an-absolute-beginners-guide-to-machine-learning-and-data-science-5d87f13f0d51
- https://s3.amazonaws.com/assets.datacamp.com/blog_assets/Numpy_Python_Cheat_Sheet.pdf
- http://jalammar.github.io/visual-numpy

**Cereals**

• https://www.consumerreports.org/cro/news/2014/12/cereal-portion-control-matters/index.htm

• https://www.nestle-cereals.com/global/en/healthy-breakfast/what-is-a-healthy-breakfast/cereal-serving-sizes

**Appendix: (but still very useful)**

Here's yet another way to use masking in Numpy.

**Using the Where Clause**

Numpy allows similar masking using the where function.  You pass in a condition to where function to get back an array that can be used for masking.  For example, let's find all elements of an array whose value is 5:

Here's the code to generate an array of 10 numbers:

```
`np.random.seed(101)`
```

```
`values = np.random.randint(0,7, (10,))`
```

```
`print(values)`
```

Now let's create a condition:

```
`mask = values == 5`
```

```
`print(mask)`
```

And finally pass that condition/mask to the `where` function:

```
indices = np.where(mask)
```

```
print(len(indices), indices)
```

The `where` function returns a set of tuples.  Each tuple is an index.

You can iterate over the first element of the tuple to get the values:

```
for idx in range(0, len(indices)):
```

```
  i = indices[idx]
```

```
  print(values[i]) # should be a 5!
```

**One more time, in 2D**

Let's do the same process, but now with a two dimensional array:

```
np.random.seed(55)
```

```
values = np.random.randint(0,7, (2,3))
```

```
print(values)
```

```
mask = values == 5
```

```
print(mask)
```

```
rows, cols = np.where(mask)
```

```
print(rows)
```

```
print(cols)
```

```
# NOTE len(rows) == len(cols)
```

A few important notes:

- `np.where` still returns a tuple, but we unravel (assign the different parts of) the tuple to different variables
- since values is 2D, `np.where` will return indices for both rows and columns 

We can now iterate over these indices:

```
for idx in range(0, len(rows)): 
```

```
  r = rows[idx]
```

```
  c = cols[idx]
```

```
  print(values[r][c]) # should be a 5!
```

That's a lot to learn. Be sure you understand the different parts of all of this.  You don't need to memorize anything, just understand how numpy works at the syntax level.

