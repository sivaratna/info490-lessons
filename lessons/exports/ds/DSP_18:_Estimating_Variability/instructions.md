# **Data Science & 🐍: Descriptive Statistics (variability)**

 

# add this code to the top of main.py

```
try:
  import pip
  pip.main(["install", "-r", "requirements.txt"])
except SystemExit as e:
  pass
# END of FIX
```

If the average for a test was 80%, what would you know about the individual test scores?  

If half of the students scored 100% and the other half scored 60%, then that 80% average is misleading.  However, if everyone got 80%, that mean provides a good representation of the data.

This is why it's best to ask for as many descriptive statistics as possible. The mean, median and mode can tell you a lot about how the data is distributed. Unfortunately, it's still not enough -- but there's more in our statistical toolbox.

# **How Different Are We?**  

**Dispersion, Distribution and Variability**

In addition to location the next "dimension" of descriptive statistics is to look at the distribution of the data, that is how spread out or concentrated the data is. The essence of statistics is trying to understand variability and knowing what part is 'random' and what part can be determined.

**Variance**

The variance is the average squared deviation.  A deviation is the distance of each point from the mean. In the formula below the deviation is the numerator.  The deviation is squared so that both positive and negative deviations don't cancel each other out.  Dividing by n, get's the average.

 

Taking the square root of the variance is the **standard deviation**.  This value is sometimes easier to interpret since its value has the same magnitude (relative distance to zero) of the dataset.  A small variance means the data is close to the mean or its spread is low.

Let's look at an example using the dice roll dataset:

 

You can use Numpy to calculate both the variance and standard deviation.

```
rolls = util.perfect_dice()
```

```
print('var {0:.2f}'.format(np.var(rolls)))
```

```
print('std {0:.2f}'.format(np.std(rolls)))
```

The standard deviation, 2.42 is an indicator of how much spread there is in the dataset.  So, on average (think of throwing two dice, many times), the sum of two dice thrown will be about 2.42 away from 7 (the mean) in either direction.  However, as we will see later, if you know more about the underlying distribution, the standard deviation carries a lot more information. Without knowing about the distribution, it's simply a measure of the average deviance of the observed values from the mean; an indication of how closely the values are spread about the mean. A small standard deviation would indicate that the values are all similar.

**Range/Percentile/Quantile**

The range of a dataset is the difference between the largest and smallest values:

 

The range is also a very handy measurement used to rescale the data (discussed later).

A more useful range looks at the difference between the **median** values of the first 25% and the last 25%.  This value is called the interquartile range (IQR) and is defined as difference between the Q3 and Q1 values.  It's a measurement of the spread that is less susceptible to outliers.

 

For the dice example (shown below), Q1 is 5, Q2 is 7, and Q3 is 9.  The IRQ is 4.

 

Rather than skipping quickly over the above graph, be sure you understand where those values come from.

The `scipy.stats` module has an IRQ function:

```
rolls = util.perfect_dice()
```

```
print('IQR', scipy.stats.iqr(rolls))
```

**The Box Plot**

The Box Plot (sometimes called box and whiskers) graph essentially visualizes the IRQ data.  The following diagram highlights the interesting parts of a box plot:

 

You can easily create box plots in Pandas (i.e. matplotlib):

```
rolls = util.perfect_dice()
df    = pd.DataFrame(rolls)
axes  = df.boxplot()
axes.get_figure().savefig('b.png')
```

 

For the `dollar.csv` data, the boxplot looks like the following:

 

You can show outliers (by passing `showfliers=True` to `boxplot`)

 

This indeed confirms that the dollar data has a number of significant outliers.

**Percentiles**

A percentile indicates the value below which a given percentage of observations in a group of observations falls.  You can use numpy to easily get the different percentiles. For example, to get the percentile of a distribution of data is the median value of the data at 25%, 50%, 75%.  

```
print(np.percentile(rolls, 25))  # Q1
```

```
print(np.percentile(rolls, 50))  # median
```

```
print(np.percentile(rolls, 75))  # Q3
```

Similarly, you can find out what the percentile of rank a specific data value is.  For example, if you rolled an 11 what percent rank is that?

```
rolls = util.perfect_dice()
```

```
print('11', scipy.stats.percentileofscore(rolls, 11))
```

# Distribution

Probably the most interesting (yet the most elusive to numerically quantify into a meaningful summary statistic) attribute of a dataset is its distribution -- that is how are the measurements spread out from the minimum to the maximum.  Usually, you will see either histograms or probability density functions (pdf) used to visualize a distribution.

 

Histograms (left graph) are also called a probability **mass** functions (pmf) when the height of the bars is expressed as a percentage.  The characteristic or variable being measured is considered discrete. In this context, discrete means the ONLY possibilities are those listed and are countable (you could not get a 3.5 in a dice roll, for example).

The probability **density** function (pdf) (the right graph) is concerned with graphing a continuous set (infinite) possibilities.  A PDF is more of a theoretical/mathematical concept.  Even if you measured/sampled a continuous variable (like age, height, weight), the measurements are discrete -- at some point no device can truly measure to an infinite level of accuracy.  The PDF graph shows the distribution if you were able to sample or plot every possible value.

In order to determine the probability of a specific item in a continuous distribution, you will need to use calculus over a very small range to find the "area under the curve" (if you had calculus, you may remember that the probability of any specific value is zero).  The word probability is used (i.e. **p**mf, **p**df) as these refer to **random variable** whose value is the outcome of a random event -- but that is indeed outside the scope of this course¹.

**The Normal Distribution**

 

There are many types of well studied distributions (discrete, binomial, geometric, poisson, continuous, uniform) but if the field of statistics had to offer a poster distribution, it would be the normal distribution.  The normal distribution has many known properties.  

You have probably seen the graph below that describes the properties of a normal (continuous) distribution.

 

As mentioned, one of **_THE_** central ideas in statistics is understanding the metrics and characteristics of a "normal" (a.k.a Gaussian) distribution.  Even when the data is discrete, a distribution can be said to be deemed normal if 95% of the data falls within 2 standard deviations of the mean.  Discrete distributions can exhibit 'normality' but a normal distribution usually refers to the continuous (PDF) model.

 

Although the word 'normal' is used, you should know that other than for an artificially generated dataset, almost all natural datasets are not 'normal'.  However, a key fact in statistics is that from proper sampling, the distribution of sample [means](https://statisticsbyjim.com/glossary/mean/) for a variable will approximate a normal distribution regardless of that variable’s distribution in the [population ](https://statisticsbyjim.com/glossary/population/)(i.e the central limit theorem).  Many of the underlying statistics rely on the characteristics of normality.  We will look at how to use Python to generate proper random samples in another lesson.

**Fitting a PDF to a dataset.**

The data inside `normal.csv` is the measurements of the wing span of common house flies.  It just so happens, that the wing length is normally distributed.  We can create a graph of the data and plot a PDF to it as well:

```
df = pd.read_csv('normal.csv')
```

```
h = df['hws'].values
```

```
# both of these generate a normal
```

```
fit = scipy.stats.norm.pdf(h, np.mean(h), np.std(h)) 
```

```
kde = scipy.stats.gaussian_kde(h)
```

```
#plot both series on the histogram
```

```
fig, axes = plt.subplots(nrows=1, ncols=1)
```

```
# pick one
```

```
axes.plot(h, fit,'-',linewidth = 2)
```

```
#axes.plot(h, kde.pdf(h))
```

```
axes.hist(h, density=True, bins=20)      
```

```
fig.savefig('n.png')
```

By now, you should be able to read that code with alacrity.

 

**The Variability of  Normality** 

 

Although the normal is nice (see picture of normal corn), in reality, the actual distribution of your data is most likely abnormal (see not so nice corn picture). There's two more summary statistics:  skew and kurtosis.  One important point is that both skewness and kurtosis are impacted by the sample size. 

**Skew**

If you plot out the distribution and also mark the mean, median and mode, the relative location can tell you information about the skew (how distorted or asymmetric the data is).

Take a look at the figure below to understand the order of the mean, median, and mode in l_eft_-_skewed_ distributions (also called _negatively_-_skewed_ distribution), _right-skewed_ distributions (also called _positively_-_skewed_ distribution) and normal distributions:

📷

**negative skew: (mean < median < mode)**

The left tail is longer; the mass of the distribution is concentrated on the right of the figure. The distribution is said to be _left-skewed_, _left-tailed_, or _skewed to the left_, despite the fact that the curve itself appears to be skewed or leaning to the right; _left_ instead refers to the left tail being drawn out. A left-skewed distribution usually appears as a _right-leaning_ curve.

 

**positive skew: (mode < median < mean)**

The right tail is longer; the mass of the distribution is concentrated on the left of the figure. The distribution is said to be _right-skewed_, _right-tailed_, or _skewed to the right_, despite the fact that the curve itself appears to be skewed or leaning to the left; _right_ instead refers to the right tail being drawn out, A right-skewed distribution usually appears as a _left-leaning_ curve²

 

**Kurtosis**

The other characteristic to discuss is kurtosis. If skewness differentiates extreme values in one tail, kurtosis measures extreme values in either tail. Distributions with large kurtosis exhibit tail data exceeding the tails of the normal distribution (e.g., five or more standard deviations from the mean). Distributions with low kurtosis exhibit tail data that are generally less extreme than the tails of the normal distribution.  Kurtosis describes the **degree to which values cluster in the tails of a distribution**.

**Measuring Skew & Kurtosis** 

We won't go into the math behind measuring skewness or the kurtosis, but it is useful to know how to interpret the values.  Here's the normal pdf drawn (red) on top of the dice graph for reference:

 

`Scipy.stats` has both functions to measure skewness and kurtosis.

Let's take a quick look at skewness:

```
data = util.perfect_dice()
```

```
print("skew","{0:.2f}".format(scipy.stats.skew(data)))
```

For the dice example, it has skew 0 (it's symmetric) and has a kurtosis of 2.37 

- A symmetrical dataset will have  `skew == 0`
- A negative distribution will have `skew < 0: (mean < median)`
- A positive distribution will have  `skew > 0: (mean > median)`

- If the skewness is between -0.5 and 0.5, the data are fairly symmetrical
- If the skewness is between -1 and – 0.5 the data are moderately negatively skewed
- If the skewness is between 0.5 and 1, the data are moderately positively skewed
- If the skewness is less than -1 or greater than 1, the data are highly skewed

Kurtosis values range from 1 to positive infinity and are centered around 3 or 0 (Fisher-Pearson adjusted) if the distribution is normal.

Here's how to interpret kurtosis values:

• Normal distribution: kurtosis = 3 (Fisher: =~ 0)

• Kurtosis value **greater than 3** (Fisher: > 0)

- It has **more outliers** than a normal distribution.
- A distribution that is more peaked and has fatter tails than normal distribution 
- The higher kurtosis, the more peaked, heavier, fatter the tails.
- Called _leptokurtic_ or _leptokurtotic_. (Peaked Looking)

• Kurtosis value **between 1 and 3** (Fisher: < 0)

- It has **less outliers** than a normal distribution.
- A distribution that is less peaked and has thinner tails than normal distribution
- Called _platykurtic_ or _platykurtotic_. (Flat Looking) 

 

For the dice example, it has a kurtosis of 2.37 (or -0.63)

```
data = util.perfect_dice()
```

```
print("kurt","{0:.2f}".format(scipy.stats.kurtosis(data)))
```

```
print("kurt","{0:.2f}
```

Looking at the graph, the 2.37 indicates that there is less data in the tails compared to a normal distribution.

**Skew and Kurtosis By Example**

Let's take a look at some example distributions and their respective skew and kurtosis numbers:

The Normal Curve

 

Right (positive) Skewed

 

Left (negative) Skewed

 

Uniform

 

**The Problems with Skew And Kurtosis**

Unfortunately, having multi-modal distributions (or multi skewed) won't be revealed in the resulting skew and kurtosis values.

Bi-Modal

 

 

Just as you wouldn't compare the mean of the human weights to the mean of human heights (even if you controlled for gender, age, etc) -- it just doesn't make sense to compare these averages, comparing the skew and kurtosis of two different distributions isn't wise either.

Both skew and kurtosis are providing information compared to a normal distribution. 

The diagram below shows the same distribution (having the same mean and variance) but with different levels of kurtosis (using the Fisher variant of 0 being normal)

 

As previously mentioned, both skewness and kurtosis are not perfect.  For multi-modal examples, you can see the skewness is not a reliable indicator.  Also, since the sample size affects these values, it's not helpful to compare the kurtosis of one distribution against another.  You can see this in some of the above graphs.

# Statistical Moments

In statistical theory, the word moments is used to describe parameters to measure a distribution.  We have now covered the first four:

- The First Moment: location (mean)
- The Second Moment: variability (variance, standard deviation)
- The Third Moment: skewness
- The Fourth Moment: kurtosis 

**Before you go, you should know**

- the meaning of variance, range, skew, kurtosis (not mathematically, but intuitive)
- the difference in skewed distributions (left, right, negative, positive)
- how to interpret kurtosis

# Lesson Assignment

All tests have been removed in an attempt to balance work load and content.

Hit submit, but understand you still need to take the time to learn the material presented.


All Rights Reserved

**Readings**

• https://towardsdatascience.com/probability-concepts-explained-probability-distributions-introduction-part-3-4a5db81858dc

**References**

¹ If there's one class that should be taken by every data scientist, it is statistics.  

² wikipedia Entry

image credit:

https://www.datavedas.com/measures-of-shape/

