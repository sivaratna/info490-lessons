# **Data Science & 🐍**: 🛑 Words

**Prerequisites:** 

- **Ngrams**
- **Local I/O**

In the previous lesson on ngrams, we were trying to find the most frequent word combinations.  Unfortunately, our results left a lot to be desired.  Is it really useful to know that  'in the' and 'and the' are some of the most occurring two word sequences?  Fortunately, with a very simple technique, we can make a substantial improvement: removal of stop words.

**Stop** (considering these) **Words**

Usually one of the first things done with text analysis is to remove common words that frequently occur across all genres of writing (i.e. across all documents).  These words are called **stop words.**   Stop words have very little informational content.  Examples include _and, the, of, it, as, may, that, a, an, off_, etc.  You can think of these words as being stopped or dropped from further consideration and freeing resources from waiting time with them.  

There is no definitive list of what constitutes a complete or proper list and it usually depends on your situation and needs.  In this case, our word analysis (and subsequent word clouds) would be improved by removing stop words.  It's not that interesting to point out that 'the' is the most popular word in the text.  

In the tab `stop-words.txt` is a list 174 common stop words (source: https://www.ranks.nl/stopwords).  Take a look at these words.  Are there any words you would remove from this list?  Are there words you would add?

For some tasks, this list would have to be changed.  For example, in **sentiment analysis** (identifying and categorizing opinions -- another popular NLP (natural language processing) task), it's important **not to remove stop words** (mostly adjectives) that might be used to convey some kind of emotion (e.g. well) as well as words that convey negation (can't, don't, etc).  There are methods (e.g. tf-idf) that actually identifies stop words -- that is words that offer no discerning information.  The **t**erm **f**requency **i**nverse **d**ocument **f**requency technique will be discussed in another lesson.

**Hand in Hand with Data Cleaning**

It's important that whatever cleaning and normalization is done to the data, that **the same process is applied to stop words as well**.  Otherwise, it's very possible that you will not remove words that should be removed.  For example, if the words in stopwords.txt were capitalized, but in your normalization process you made everything lowercase, no stop words would be removed.  Similarly, if you removed all apostrophes, stop words like we'd and aren't would never be removed.  Had we decided to remove all the apostrophes, you would also have to remove all the apostrophes in the stop words (a viable decision).  Otherwise, you will see words like 'ive' and 'im' in your output.  

The handling of apostrophes is very tricky can be tricky as well.  There is also an issue that _could_ present itself as well:  There are stop words that after you remove the apostrophe becomes a different word (e.g. she'd becomes shed, we'll become well, he'll becomes hell, others include wed, shell, were).  You could either remove these from your stop word list or decide just to leave in the apostrophes.  It's important to know how you tokenize and clean your text could affect other areas of your analysis.  This is one of the reasons why we are leaving internal apostrophes alone -- we don't have to clean the stop words as well.

For some types of text analysis the words that convey negation (aren't, can't, didn't, doesn't, don't, hadn't, not, can't, cannot, don't, etc) play an important part.  Many stop word lists include these words.  If your analysis never sees negation, you might be building a faulty model. For example, if we were doing sentiment analysis or topic modeling, we would leave these words in.

**Slippery Slope of Stop Words**

The caution for having a list of stop words, is that it's easy to get in situation of having better analysis by tweaking the stop words.  However, this is not a good practice if you're trying to build a generic model or pipeline.  It's simply not scalable to adjust the stop words for every possible text document you are processing.  

There is a temptation to look at your results and add in stop words to get 'better' results.  For example, the word 'chapter' could be a stop word for this analysis.  It only occurs as a token to separate chapters so it would be harmless to remove.  The issue is that if you build a pipeline that processes data automatically, you won't be able to customize the stop word list for each input.  Similarly, the word well appears 427 times and we'll appears 40 times.  You would have to decide if the connotations and usage of well (interjection vs an adjective, etc) affect your analysis before deciding to either remove internal apostrophes (and we'll becomes well) or add either as stop words.

**Tokenizing and Cleaning Tom Sawyer**

In the previous lesson on ngrams, we had a very specific data cleaning stage where we removed leading and trailing apostrophe (e.g. 'food' becomes food) but left internal apostrophe's alone (e.g. I'm).  Luckily this decision will not impact the stop words too much. But we will still run the stop words though the same data preparation step.

Since you will be comparing tokens to stop words, you **must also normalize the stop words** to treat words with apostrophes (e.g. I've, I'll, etc) the same as you do in `split_text_into_tokens`.  Had we decided to remove all the apostrophes, you would also have to remove all the apostrophes in the stop words (a viable decision).  Otherwise, you will see words like 'ive' and 'im' in your output.  

**Before you go you should know**

- what stop words
- why you want to remove them
- why it's important to process them the same as you data
- understand the issues with stop word removal

# Lesson Assignment:  Better Data, Better Ngrams

For this lesson, you will use a lot of your code from the ngrams lesson and build two additional functions

We will build a simple pipeline that does the following:

- reads in the contents of Huckleberry Finn
- tokenizes the contents into 'words'
- removes all the stop words
- builds a list of all bi-grams
- find the most common bi-grams in the text

As mentioned in the ngram lesson and repeated here:

The above pipeline will be used in other lessons and assignments.  It's not only important to pass the tests for this lesson, but to understand the work flow.  We will use this to eventually find characters in literature.  Be sure to **TEST each function** before moving from one stage to the next.  Do NOT attempt to write all the code and hit 'run tests'.  You should build your own tests and make sure you understand each function before moving to the next one.

**Step 0.**  Using another browser tab, go to the **ngrams** lesson and copy over your working code.  If you want more programming practice, now's a great time to just reimplement the same functions.  You might even find a tighter, cleaner solution.  All that code goes in lesson.py tab (the empty functions are already there).

**Step 1: Stop Words**

Create and define a function named `load_stop_words`.  

This function builds a list of stopwords from a filename.  

- You can assume that the file contains one stop word per line.  
- If you forgot how to read lines from a file, see the Local I/O lesson.

We will use the stop words that are given in the tab `stop_words.txt`

```
def load_stop_words(filename):
```

```
  # read filename and return a list of stopwords
```

Note this function will also clean each stop word using the exact same process that was done inside your `split_text_into_tokens` function.

NOTE:  you should also remove surrounding whitespace from each stop word.  Although stopwords.txt should not contain any whitespace before or after the words, depending on how you read the words in, you could introduce this artifact.  It's better to be safe and have normalization remove surrounding whitespace as well.

**Step 2: Cleaning and Refactoring code**

You should now refactor your code so you can reuse the data normalizing step(s) you used in `split_text_into_tokens`.  What does that mean?  You should create a function called `normalize_token` that takes in a word and returns the word normalized according to the rules specified in the ngrams lesson (i.e remove leading and trailing apostrophes and whitespace).

You can now use this call function in two places!  Once in 

`split_text_into_tokens` and again in `load_stop_words`.  This is how software development works.  When you discover a place where code can be reused, you can 'refactor' that code so you don't have to duplicate it.

- remove the cleaning code from `split_text_into_tokens` and place it into the `normalize_token` function.  This function takes in a string and returns the normalized, cleaned string.
- in both `split_text_into_tokens` and `load_stop_words` call the function you just created.
- once you are done, you should see the following (or something similar) in two places:

```
   n = [normalize_token(t) for t in tokens]
```

**Step 3:  not stop words**

Create a function named `remove_stop_words(tokens, stoplist)`:

- `tokens` is a list of words
- `stoplist` is a list of words
- return a new list of words (in the same order as tokens), but only if the token is not a stop word
- now's a good time to break out a comprehension to solve this using one line of Python.

**Step 4: Test**

Once this is done the following pipeline should reveal the top 25 bigrams in Huckleberry Finn.  

```
text = read_text('data.txt')
```

```
stop = load_stop_words('stopwords.txt')
```

```
tokens  = split_text_into_tokens(text)
```

```
cleaned = remove_stop_words(tokens, stop)
```

```
grams = bi_grams(cleaned)
```

```
print(top_n(grams, 25))
```

If the top bi-gram is 'He said', you are most likely done!!!   

A few points to ponder:

- Take a look at the returned bi-grams.  Do you see anything interesting?
- Note how you built the functions in such a way that you can reuse this code (which you will) in future lessons but on different sources of text.
- Note that in this lesson we removed the stop words from the text BEFORE we processed the text into ngrams.  There are many situations (which we will see), where you will use the stop words to determine if you want to consider an item for further processing.  Removing the stopwords like we did in this lesson, actually modifies the text order.
- What two word phrase is the most occurring if `normalize_token` also did case folding (that is made the case of the word the same for all words)?  Be sure to remove this additional code before you submit.

You can tag any question on Piazza with **PyStop**.


All Rights Reserved

