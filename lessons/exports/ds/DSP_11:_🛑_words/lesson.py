'''
# feel free to add other functions 
# to help you decompose the problem
'''

def read_text(filename):
  # same solution as Ngrams
  return ""
  
def split_text_into_tokens(text):
  # same solution as Ngrams
  return [] 
  
def bi_grams(tokens):
  # same solution as Ngrams
  return []

def top_n(tokens, n):
  # same solution as Ngrams
  return []


#
# NEW for this LESSON
#
def normalize_token(token):
  return token
  
def load_stop_words(filename):
  # reads in the contents of filename
  # applies normalization rules to each stop word
  # returns a list of normalized stop words
  # see instructions
  return []

def remove_stop_words(tokens, stoplist):
  return []