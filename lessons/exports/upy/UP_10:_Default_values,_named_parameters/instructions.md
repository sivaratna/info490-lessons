# **University 🐍:  Functions with default values, named parameters**

This is a short lesson (with no tests!) BUT you need to know the contents and future lessons will require you to know and use what is being taught.

**Getting the verbiage correct**

There is usually some confusion when you read about or discuss function _parameters_ (and its first cousin _arguments_):

- **Parameters** are defined by the names that appear in the function definition.
- **Arguments** are the values actually passed to a function when calling it.  

Parameters are assigned the values of the arguments.  The confusing part is that _some_ refer to arguments as _actual parameters_ -- and the parameters in the function definition as _formal parameters_ (I don't recommend using these terms but you may see them).

Python uses the name **positional parameters** or required parameters as well:

```
# parameters
```

```
# formal parameters
```

```
# positional parameters 
```

```
# required parameters
```

```
def bmt_1(filename, stop, separator):
```

```
  print(filename, stop, separator)
```

```
# arguments
```

```
# actual parameters
```

```
# positional arguments
```

```
bmt_1('a', 10, ',')
```

But the story doesn't stop there.  You can also specify **default values** for function parameters.  Once you do this, the parameters are now referred to as **named parameters** and they have, by default, a value to use if the caller doesn't provide one.  Hence, sometimes they are referred to as _optional parameters_.  Note that since the default value is part of the formal argument specification, they are also referred to as formal parameters.

From the _caller point of view_, named parameters are sometimes called **keyword arguments** and the order does not matter when using keyword arguments.  So with named parameters, you can call the functions using any order for the keyword arguments:

```
# adding default values to the parameter names
```

```
def bmt_2(filename, stop=10, separator=' '):
```

```
  print(filename, stop, separator)
```

```
  
```

```
# when we call a function, we use arguments
```

```
# using named arguments (order can be different)
```

```
bmt_2('a', separator='*', stop=2)
```

```
bmt_2('a', stop=5)
```

```
# using the default values
```

```
bmt_2('a')
```

Note in the above, `filename` MUST be provided.  Also you can't specify a named before an unnamed parameter; all the named parameters must come after required parameters.  As you might imagine, Python uses an internal dictionary to manage function parameters (hence order of named parameters doesn't matter).

**Moving Forward**

As a software engineer, it's always best to provide reasonable default values.  You want to create functions that don't require every parameter to be specified -- only the necessary ones.  This gives flexibility to your software and allows the client to customize the call as needed.

**Before you go, you should know:**

- what a named parameter is
- why a named parameter is useful
- how defaults are set for named parameters
- the difference between an argument and a parameter

# **Lesson Assignment**

There is nothing to write to test.  Be sure to submit for credit.  

Remember for assignments that don't specify a specific function signature, be sure to use named parameters to your benefit.

You can tag any question you have with **UPParameters** on Piazza


All Rights Reserved

**References:**

https://www.python.org/dev/peps/pep-0570/

[https://docs.python.org/3/tutorial/controlflow.html#keyword-arguments](https://docs.python.org/3/tutorial/controlflow.html#keyword-arguments)

