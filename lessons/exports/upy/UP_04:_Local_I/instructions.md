# **University 🐍: Local I/O**

Perhaps you already know that I/O (pronounced eye-oh) is just shorthand for Input/Output or how data/information is read or written by the computer.  You have already been using it, but now you will learn how to manipulate different I/O streams (think of a stream as a channel for communication) to access data.

Up until now you have used variables to set the data directly:

```
my_list = [1,2,3,"apples"]
```

However, at some point, you will want to read data from either the user or from a local file.  When you install Python on your laptop the idea of reading a 'local' file will mean reading a file that is on your laptop.  However, since you are running Python code on one of repl.it's machines (which is actually run on Google's hardware), 'local' really means the computer that is hosting the VM (virtual machine -- a program that has access to a subset of the host's actual resources).  

**Standard Input**

The way to read from the user's keyboard (or prompt the user to enter input) is with the `input` function:

```
data = input("Please enter something, anything")
```

```
print("You entered ", data)
```

> **Coder's Log:**  In Python version 2, The input function, actually evaluates the data that you typed in via `input()`.  So if you typed in `[x*x for x in range(0,5)]`, the `input` function would evaluate that input and return: [0, 1, 4, 9, 16].  That can be a very dangerous thing -- evaluated code from a user.

The reading from the 'console' window via the keyboard is called standard input (**`stdin`** for short) -- it's a term from the early days of computers.  It usually means input from the keyboard.

You can also use the `sys` (system) module to read from standard input (no need to do this unless you have a specific reason).  See the documentation for `sys.stdin(https://docs.python.org/3.6/library/sys.html)`

```
import sys
```

```
print("enter something")
```

```
data = sys.stdin.readline()
```

```
print("You entered ", data)
```

**Standard Output**

You have already been using Python's `print` function which writes to the console window or display (also called standard output and abbreviated `**stdout**`).  

```
`print("Hello Python")`
```

Just as you can read from `sys.stdin`, you can also write using the `sys` module and simulate what the `print` function is doing:

```
import sys
```

```
sys.**stdout**.write('Hello Python')
```

```
sys.**stdout**.write('\n')
```

`**'\n'**`** What?**

One thing you may have noticed is that the `print` function will print a newline (the character that forces a newline) after it is done:

```
print("Hello Python")
```

```
print("Here we are")
```

You can tell the `print` function to not print the newline at the end of it:

```
print("Hello Python", end='')
```

```
print("Here we are")
```

Essentially any control characters inside a regular Python string will get replaced when evaluated.  This newline character will become important to consider when you read and write (and parse) data.  But it can serve as a token to separate different lines in a file.  A line can be any length.

**Standard Error**

There's another type of output called standard error (`**stderr**` for short).  You can think of standard error as another output stream.  It's one way to separate the output sent to standard out and output that is meant to bring attention to an error situation.  However, the default output for standard error is the console/display.  So it's really hard to show the difference.  When you get to the lesson on learning Unix commands, you will see the power of being able to separate the output of a program into two streams or categories: normal output and errors.

```
import sys
```

```
for x in range(0,10):
```

```
  sys.**stderr**.write('*')
```

```
sys.**stderr**.write('\n')
```

```
# will look the same in the console window (stdout)
```

**Getting Data**

Other than simple examples and the smallest of datasets, you will need to get data from some location -- either a local file, remote file, or a service (http web page request, SQL request, etc).  For now you will focus on reading and writing to local files. 

In the repl.it editor window, each 'tab' can be considered a file that has data in it.  With Python you can actually read the contents of this tab/file and process it. 

**Local File Reading**

When you want to read a file that has data in it, it's usually a three step process:

1. Open the file for reading
2. Read the data (either in chunks or in its entirety)
3. Close the file

**Opening a File**

We will use the open function which returns a file I/O object.

```
fd = open('data.csv', 'r')
```

Conventionally, the object returned by `open` is usually referred to as a file descriptor (sometimes referred to `fd` for short) or a file. The second parameter tells the `open` function, how you will be using the returned file (i.e. for reading, writing).  You can even specify if you want to read or write the data in 'binary' mode.  That usually means you are interested in encoding and decoding the raw bytes that make up the file directly.  You will not be doing that in this class.

**Reading a File**

Once you have a file object, you can then read from it.  You can request to read from the file incrementally or to read all the data at once.  The two main methods on the file object are `readline` (which reads a single line) and `read` (which reads as much as possible).  

```
fd = open('data.csv', 'r')
```

```
print("1-->",fd.readline())
```

```
print("2-->",fd.read())
```

If you are just interested in the raw contents of the file, use `read`

There is also a `readlines` method that returns all the lines from the file into a list:

```
fd = open('data.csv', 'r')
```

```
lines = fd.readlines()
```

```
print("read", len(lines), "lines")
```

One important note is that `readlines` method KEEPS the `\n` (the newline) character in the returned lines.

Note that you could simulate the same using the `split` method

```
fd = open('data.csv', 'r')
```

```
lines = fd.read()**.split**('\n')
```

```
print("read", len(lines), "lines")
```

Since in repl.it every tab is a file.  You can actually read the contents of the editor window as well:

```
fd = open('main.py', 'r')
```

```
print(fd.read())
```

One thing to note and remember is that even if a file is filled with numbers, the read functions will treat the contents as text.  You will need to convert it if the contents of the file will be processed differently.

**Too large to hold**

For really large files, you won't be able to read ALL the data --- you may run out of memory to do that.  However, for this class, you will not run into that problem (yet).  However you should be aware of a few methods that you can use to determine if you read all the data.  The `os` module provides a way to get the size of a file (using the path object)

```
import os
```

```
size = os.path.getsize('data.csv')
```

The file object also has a `tell` method that returns the number of bytes it has read (you can also think of it as the current position of the where the next read will take place).

```
fd = open('data.csv', 'r')
```

```
data = fd.read()
```

```
print(fd.tell())
```

If you compare the result of `tell` to the `size`, you will then know if there's more data to be read:

```
import os
```

```
size = os.path.getsize('data.csv')
```

```
fd = open('data.csv', 'r')
```

```
data = fd.read()
```

```
print("read all the data?", fd.tell() == size)
```

> **Coder's Log:**  Another useful method is `seek`.  This method allows you to move the file pointer to a specific location in a file (see https://docs.python.org/3.6/tutorial/inputoutput.html).  But you can use `fd.seek(0,2)`  (`2` means: goes to the end of the file; `0` means and move back zero bytes)  effectively setting the current position at the end of the file.  You can then issue a `tell()` and find the file's length.

If memory is an issue, you can process a file line by line:

```
fd = open('data.csv', 'r')
```

```
line = fd.readline()
```

```
while(len(line) > 0):
```

```
  print(line, end='')
```

```
  line = fd.readline()
```

That sequence of code is a very common idiom that there's a Pythonic way to do this is as follows:

```
fd = open('data.csv', 'r')
```

```
for line in fd:
```

```
  print(line, end='')
```

**Good Housekeeping**

After you are done with a file, you should `close` it.  For reading, it will clean up any resources dedicated to reading the file.  For writing, it will do the same but also flush any data that's in the buffer out to the file.

```
fd.close()
```

So as a complete example of reading a file, line by line would be:

```
fd = open('data.csv', 'r')
```

```
for line in fd:
```

```
  print(line, end='')
```

```
fd.close()
```

**Handling newlines** 

The newline character `'\n'` is something that can trip up new programmers.  The  `readline` function will use it to determine where a new line begins but it will also return that character in the read.  You can strip it off with string's `strip` method (which removes preceding and trailing whitespace from a string):

```
fd = open('data.csv', 'r')
```

```
line = fd.readline()
```

```
line = line.strip() # remove the newline character
```

```
print(line, end='<')
```

```
fd.close()
```

See how that code behaves with the `line = line.strip()` commented out.

**Local File Writing**

Writing to a file is very similar to reading.  You `open` the file, and call `write` or `writelines`, followed by a `close`.

```
fd = open('output.csv', 'w')
```

```
fd.write("header_a, header_b, header_c\n")
```

```
fd.writelines(["1,2,3\n", "4,5,6\n"])
```

```
fd.close()
```

For writing, the second parameter tells open how to write to the file: `"w"` means overwrite the file, `"a"` means append to the file.  You should visit the documentation for additional flags, but those are the most common.

Also note, that you need to add the `'\n'` newline character if you want the file to have line feed\carriage return (words from typewriter days) -- so each line will appear on a new line in the file.

**`with` What ?**

One of the issues is that if a `read` (or `write`) causes an exception or the processing of the data causes an error, it's possible to have an open file (a hence a memory leak -- or memory that is unable to be reclaimed).  So it is standard Python to wrap the `open` command inside a `with` statement.  The `with` statement helps guard against any errors and exceptions and guarantees to close the file regardless if there is an error or not:

```
with open('data.csv', 'r') as fd:
```

```
  for line in fd:
```

```
    print(line, end='')
```

```
# fd is closed at this point 
```

```
# (outside the with statement)
```

You can even nest `with` statements:

```
with open('data1.csv', 'r') as fd1:
```

```
  # fd1 is open at this point
```

```
  with open('data2.csv', 'r') as fd2:
```

```
    # both fd1 and fd2 are available
```

Note that you can/should still wrap your code with a try/except statements in order to provide robust error handling.  The `with` statement only ensures the file handle gets closed.

**Buffering**

As mentioned previously, it's possible to process a file in chunks rather than a complete unit.  You can read the Python docs on reading and writing files.  If you're in a situation where you want to process big chucks of it at a time, there's a lot of buffering (reusing memory to read into) capabilities.

**Repl.it caveat.**  Sometimes when you call `write()` followed by a `close()` the contents of the new file will **not** appear in your editor.  You may need to click on `main.py` and then on the newly created tab (i.e. file).

**Before you go, you should know**

- the difference between stdin, stdout, stderr
- how to open a file for reading and writing
- how to use the `with` statement
- how to read a file once it's open
- the difference between read and readline
    - how to write contents to a file

**Lesson Assignment**

The tab `cereal.scsv` contains the nutritional information for over 70 cereals. However it's in a format that is rarely used (semi-colon separated values).  The reason it's in this format is that a few cereals have commas in their name (e.g. Fruit & Fibre Dates, Walnuts, and Oats).  

For this lesson you will convert this format to the more common csv file format (comma separated values).  There are two standard ways to treat values with commas in them when writing out a csv file:

1. Put the entire value in quotes (e.g. "Fruit & Fibre Dates, Walnuts, and Oats")
2. Use a different separator (e.g. Fruit & Fibre Dates; Walnuts; and Oats)

We will chose the latter and use semicolons for those values that have commas in them.

3. Write a function named `convert_table`.  It has two parameters a filename to be read (the scsv file) and a filename to be written (the csv file). Name them `filename_in` and `filename_out`.
4. Open, read, and parse the scsv file (i.e. `filename_in`)
5. Write out the same contents to `filename_out` but separate the values (columns) with commas.
6. For those values that have commas in them, replace the commas with semicolons.
7. Be sure to end each line (except the last line) with a newline.
8. Return True from the function.  Normally you would return a boolean indicating success or failure
9. do NOT use any helper libraries (e.g. pandas, csv, etc).

Once again the Python documentation on strings will be useful: https://docs.python.org/3.6/library/stdtypes.html#string-methods.  

Test your code and submit.

You can tag any questions you have with **PyLocal** on piazza


All Rights Reserved
