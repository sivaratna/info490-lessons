# **University🐍:  Fancy String Formatting**

For printing information to the console, we have been using string concatenation:

```
ans = 37
sol = 42
print("Debug " + str(ans) + " vs " + str(sol))
```

or using multiple parameters to `print`:

```
ans = 37
sol = 42
print("Debug", ans,"vs", sol)
```

Trying to keep track of how to properly format a string with many parameters can be tedious.  However, Python provides a few methods that makes formatting strings easy[ier].


**The format method**

The string type has a method named `format` that can help you build strings that involve multiple parameters:

```
ans = 37
sol = 42
print("Debug {} vs {}"**.format**(ans, sol))
```

The advantage is that you can easily format a string to hold different dynamic (values that come from variables) at runtime.  Each parameter passed to `format` gets matched with a placeholder in the string marked with matching brackets {}.  


**Changing Positions**

You can even change the order of parameters by referencing the parameters with a number called the index number:

```
ans = 37
sol = 42
print("Debug {1} vs {0}".format(ans, sol))
```

However, I don't recommend that you do this. It can get out of hand and become hard to read (and track) as your parameters list grows.

**{0:1.2} what?**

What makes the `format` method powerful is what you can do between the braces.  With the proper specifier, you can specify alignment, pad numbers, round off floating point numbers and even truncate the output. 

**String, Decimal or Float.**

You can specify whether you're formatting a **s**tring, **d**ecimal or **f**loat using the proper character (if none is given, string is assumed):

```
print("{:s}".format("2")) 
```

```
print("{:d}".format(2))
```

```
print("{:f}".format(2))
```

By default format assumes you are formatting a string:

```
print("{}".format("help"))
```

# String Formatting (`:s)`

**Left Alignment (no padding)**

Printing is left aligned (the default) with no padding:

```
print('123456789012')
```

```
print('{:s}#'.format('python'))
```

The above will produce the following output:

```
`123456789012`
```

```
`python#`
```

the '`s`' is optional as well

```
print('123456789012')
```

```
print('{:}#'.format('py'))  
```

```
123456789012
py#
```

**Left Alignment with padding**

You can specify the amount of space to use with a number after the colon.  In the next example, the output will be padded (if necessary) to a minimum of 10 characters:

```
print('123456789012')
```

```
print('{:10}#'.format('python'))
```

will produce:

```
`123456789012`
```

```
`python    #`
```

the number is a minimum:

```
print('123456789012')
```

```
print('{:3}#'.format('python'))
```

```
123456789012
python#
```

**Left Alignment with Truncating (no padding)**

```
print('123456789012')
```

```
print('{:.2}#'.format('python'))
```

```
123456789012
py#
```

**Left Alignment with Truncating and Padding**

```
print('123456789012')
```

```
print('{:10.2}#'.format('python'))
```

```
123456789012
py        #
```

**Pad with Style:**

When you want to pad with a character other than a space, you must specify the alignment explicitly: '`<` ' for left; '`>`' for right; '`^`' for center.

The following will left align python, padded to 10 spaces, truncated to 5 characters and padded with the character '`*`'

```
print('123456789012')
```

```
print('{:*<10.5}#'.format('python'))
```

```
123456789012
pytho*****#
```

**Right Alignment:**

Just as we saw with padding, if you want to use non-left alignment (the default) you must specify the alignment using '`<`' for left; '`>`' for right; '`^`' for center.

The following with right align 'python' using 10 places

```
print('{:>10}'.format('python'))
```

```
123456789012
    python
```

**Right Alignment with Truncation**

```
print('{:>10.5}#'.format('python'))
```

```
123456789012
     pytho#
```

**Right Alignment with Style**

```
print('123456789012')
```

```
print('{:*>10.5}#'.format('python'))
```

```
123456789012
*****pytho#
```

**Center Alignment:**

```
print('123456789012')
```

```
print('{:^10}#'.format('python'))
```

```
123456789012
  python  #
```

**Center Alignment with Truncation**

```
print('123456789012')
```

```
print('{:^10.5}#'.format('python'))
```

```
123456789012
  pytho   #
```

**Center Pad with Style**

You can even choose a character to use for padding (space is the default)

```
print("{:*^20}".format("DOGS"))
```

```
print("{:*^20}".format("I LIKE DOGS"))
```

```
print("{:^20}".format("DOGS"))
```

```
print("{:^20}".format("I LIKE DOGS"))
```

# **Number Formatting**

For printing numbers (either decimal or floating points) the format command can help you nicely format strings.  You will use '`d`' for decimal and '`f`' for floating point numbers.  

However for numbers, the **default is RIGHT alignment** and for floating point numbers the default is to use 6 places for precision and right align the number.  However, if no padding is specified, it will appear that the numbers are left aligned.

**Align left**

```
print('123456789012')
```

```
print('{:d}#'.format(100))
```

```
print('{:d}#'.format(10))
```

```
print('{:d}#'.format(1))
```

```
print('{:f}#'.format(math.pi))
```

```
print('{:<d}#'.format(100))
```

```
print('{:<d}#'.format(10))
```

```
print('{:<d}#'.format(1))
```

```
print('{:<f}#'.format(math.pi))
```

```
123456789012
100#      deceiving
10#       deceiving
1#        deceiving
3.141593# deceiving
100#
10#
1#
3.141593#
```

**Align left, with padding** (if you don't use the '`<`', it will be right aligned)

```
print('123456789012')
```

```
print('{:<10d}#'.format(100))
```

```
print('{:<10d}#'.format(10))
```

```
print('{:<10d}#'.format(1))
```

```
print('{:<10f}#'.format(math.pi))
```

```
123456789012
100       #
10        #
1         #
3.141593  #
```

**Align Right (need to specify padding to 'see' the default)**

```
print('123456789012')
```

```
print('{:10d}#'.format(100))
```

```
print('{:10d}#'.format(10))
```

```
print('{:10d}#'.format(1))
```

```
print('{:10f}#'.format(math.pi))
```

```
print('{:>10d}#'.format(100))
```

```
print('{:>10d}#'.format(10))
```

```
print('{:>10d}#'.format(1))
```

```
print('{:>10f}#'.format(math.pi))
```

```
123456789012
       100#
        10#
         1#
  3.141593#
       100#
        10#
         1#
  3.141593#
```

**Align Center (with padding)**

```
print('123456789012')
```

```
print('{:^10d}#'.format(100))
```

```
print('{:^10f}#'.format(math.pi))
```

```
123456789012
   100    #
 3.141593 #
```

**Floating Point Precision** 

It's also very easy to specify how to truncate floating point numbers:

**Floating point precision (3 places) (the default is 6)**

```
print('123456789012')
```

```
print('{:f}'.format(math.pi))
```

```
print('{:.3f}'.format(math.pi))
```

```
123456789012
3.141593
3.142
```

**Precision and width: (use 10 spaces)**

```
print('123456789012')
```

```
print('{:**10**.3f}'.format(math.pi))
```

```
123456789012
     3.142
```

**Precision and width (and pad with zeros)**

```
print('123456789012')
```

```
print('{:**0**10.5f}'.format(math.pi))
```

```
123456789012
0003.14159
```

You can even mimic the `zfill` method (how we built the multiplication table)

```
print('123456789012')
```

```
`print('12'.zfill(4))`
```

```
print('{:04d}'.format(12))
```

```
123456789012
0012
0012
```

**Exponent format:**

```
print('123456789012')
```

```
print('{:e}'.format(math.pi*123456789))
```

```
123456789012
3.878509e+08
```

**Tuple Indices**

Going back to the first example, the number before the colon, can be used to specify which parameter you want to print:

```
print('{**3**}'.format(10,11,12,13))
```

```
print('{**3**:04d}'.format(10,11,12,13))
```

```
13
0013
```

**Summary**

Here's a summary of what we have learned in this lesson.  The main point is that with the format function you can easily specify how strings are to be formatted and make for easy printing that involves multiple values.

**General Formatting:**

```
print("{0:0>3}".format(1))
```

```
print("{0:0>3}".format(10))
```

```
print("{0:0>3}".format(100))
```

```
{0 : 0 > 3}
```

```
 │   │ │ │
```

```
 │   │ │ └─ Width of 3
```

```
 │   │ └─ Align Right  (use ^ for center)
```

```
 │   └─ Fill with '0'
```

```
 └─ Element index
```

 

**Floating Points:**

```
              {:6.1f}
```

```
                ↑ ↑ 
```

```
                | |
```

```
# digits to pad | | # of decimal places to display
```

**More String formatting.**

In addition to the format method, Python provides string templating and f-strings for even more possibilities for formatting strings (not required, but good to know):

- https://cito.github.io/blog/f-strings/
- https://docs.python.org/3.6/library/string.html#template-strings

**Before you go, you should know:**

- how the format function works
- the specifiers for integer, string, floats

# **Lesson Assignment**

# **[NOTE: see piazza -- this lesson should be uploaded to gradescope for grading]**

# **Finish your code, submit to gradescope AND then hit submit on repl.it (even though on repl.it, it won't pass the tests)**

All code should go in lesson.py

Fill in the `{}` such that `pi` is formatted and using 15 spaces and 5 decimal places, in left, right, center aligned respectively:

```
def left_pi():
```

```
  left = "Pi is fun to 5 places:{}<-".format(math.pi)
```

```
  return left
```

```
def right_pi():
```

```
  right= "Pi is fun to 5 places:{}<-".format(math.pi)
```

```
  return right
```

```
def center_pi():
```

```
  center= "Pi is fun to 5 places:{}<-".format(math.pi)
```

```
  return center
```

**Extra Credit: Table of Fun**

To skip extra credit add this line to `lesson.py`

`SKIP_EXTRA_CREDIT = True`

Define the function `table_of_fun(end)`.  

Using `format`, return a string that contains the multiplication table shown below.  

You will be passed in the end value of the table.  

So below is the table from `print(table_of_fun(10))`

```
   `1   2   3   4   5   6   7   8   9  10
   2   4   6   8  10  12  14  16  18  20
   3   6   9  12  15  18  21  24  27  30
   4   8  12  16  20  24  28  32  36  40
   5  10  15  20  25  30  35  40  45  50
   6  12  18  24  30  36  42  48  54  60
   7  14  21  28  35  42  49  56  63  70
   8  16  24  32  40  48  56  64  72  80
   9  18  27  36  45  54  63  72  81  90
  10  20  30  40  50  60  70  80  90 100`
```

So you will need to pad the columns to right align numbers and make sure you use the minimum amount of padding such that the maximum value in the table will 'fit'.

Below is the output for `print(table_of_fun(5)):`

```
  1  2  3  4  5
  2  4  6  8 10
  3  6  9 12 15
  4  8 12 16 20
  5 10 15 20 25
```

If you had a wide screen, `table_of_fun(124)` would produce a table that would look like the image below:

📷

Note:  there is a final newline in the output of `table_of_fun`.

You can tag any question on Piazza with pyFormat


All Rights Reserved
