import re
import lesson

STORY = 'story.txt'
HUCK = 'huck.txt'

def get_uniq_set(t):
  return sorted(set([x.lower() for x in t]))
  
def read_file(filename):
  with open(filename, 'r') as fd:
    txt = fd.read()
  return txt

def read_huck(): 
  return read_file(HUCK)
  
def read_story():
  return read_file(STORY)

sample0 = "He pulled the lever all the way down to where it said full steam ahead. A bell rang. The motors made a grinding sound and the ferry began to move. The passengers were surprised because the captain was still on deck taling to the Man in the Yellow Hat. Who was running the boat? It was George!!!\nI had 20 dollars and george, $25.00!!! Even CHAPTERS can help us"

sample1 = '''
Miss Watson would say,"Don't put your feet up there, Huckleberry;" and "Don't scrunch up like that, Huckleberry--set up straight;" and pretty soon she would say, "Don't gap and stretch like that, Huckleberry--why don't you try to behave?"  Then she told me all about the bad place,
'''

sample2 = 'Apples--app_les! I love apples'
sample3 = "Richmond............... Mr. Kean."

class AutoGrader(object):
  
  def __init__(self, name):
    self.name  = name
    self.tests = []
    
  def run_pattern(self, pattern, data, flag=None, do_uniq=False):
    
    if flag != None:
      regex = re.compile(pattern, flag)  
    else:
      regex = re.compile(pattern)
      
    result = regex.findall(data) 
    
    if do_uniq:
      result = set([x.lower() for x in result])
      
    return result
    
  def test_one(self, num, show_result_count=5):
    return self.run([self.tests[num]], show_result_count)
    
  def test_all(self, show_result_count=25):
    return self.run(self.tests, show_result_count)
    
  def run(self, tests, show_result_count=25):

    pass_count = 0
    fail_count = 0
    
    for i, tup in enumerate(tests):
      print("--"*5)
      ok = True
      if (len(tup) == 4):
        (label, reg_ex, result, answer_set) = tup
      else:
        (label, reg_ex, result, answer_set, ok) = tup

      if ok and len(result) in answer_set:
        print("Test {} PASS ({} found)".format(label, len(result)))
        pass_count += 1
      else:
        found = len(result)
        print("Test {} FAIL".format(label), end=' ')
        if (len(answer_set) == 0):
          print("(no answer key)")
        else:
          print("")
        print("Test {} returned {} values (wanted {})".format(i,found, answer_set))
        print("Test {} regex {}".format(label,reg_ex))
        fail_count += 1
        
      n = min(show_result_count, len(result))
      if n > 0:
        print("Top {} found:".format(n))
        print(result[0:n])
    
    print ("\n{} correct; {} incorrect".format(pass_count,fail_count))   
    return (pass_count,fail_count)
  

class LessonAutoGrader(AutoGrader):
  def __init__(self):
    super().__init__('demo')
    self.story = read_story()
    
    self.tests = [
      ('s0', lesson.s_q0(), 
            lesson.s_q0().findall(self.story),
            [3]),
            
      ('s1', lesson.s_q1(), 
            lesson.s_q1().split(self.story),
            [4]),
            
      ('s2', lesson.s_q2(), 
            lesson.s_q2().findall(self.story),
            [14]),
    ]

    
    