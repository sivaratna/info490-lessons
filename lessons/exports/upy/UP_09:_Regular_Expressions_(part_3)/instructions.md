# **University 🐍:  Regular Expressions Part 3**

This is the third part of using regular expressions to easily find and use patterns in text. The following is a summary of the new topics we will cover in this lesson:

- The greed-less qualifier
- Splitting, Replacing, Searching text

# **Greedy vs Lazy** 

As we mentioned previously, having a greedy qualifier can yield unexpected results.  It's always important to remember that the regular expression engine will always eat up as much text as possible that matches a pattern.  However, you can make the default greedy operators to be non-greedy (or lazy) by appending a `?` to them:

```
Greedy  | Lazy  |        Description           
```

```
*       | *?    | Star Quantifier: 0 or more   
```

```
+       | +?    | Plus Quantifier: 1 or more   
```

```
?       | ??    | Optional Quantifier: 0 or 1  
```

```
{n}     | {n}?  | Quantifier: exactly n        
```

```
{n,}    | {n,}? | Quantifier: n or more        
```

```
{n,m}   | {n,m}?| Quantifier: between n and m  
```

For example:

```
text = '<H1>title</H1>'
```

```
greedy = re.findall(r'<.*>', text)
```

```
print(greedy)
```

Note that the greedy .* will match everything up to the closing tag '<'.  If we just wanted to get the text inside the tag, we can qualify the greedy with a '?':

```
text = '<H1>title</H1>'
```

```
lazy   = re.findall(r'<.*?>', text)
```

```
print(lazy)
```

# **Splitting on Regular Expressions**

Besides using `findall` another useful method on the regular expression object is the split method.  We have used the strings `split()` method to either split text on a set of characters, but with regular expressions you can split text based on patterns. 

```
def split_chapters(text):
```

```
  pattern = r'^CHAPTER\s[A-Z\s]+\.?$'
```

```
  regex   = re.compile(pattern, re.M | re.IGNORECASE)
```

```
  return(regex.**split**(text))
```

```
chp = split_chapters(helper.read_huck())
```

```
print(len(chp))
```

Note that this returns 44 even though there are 43 chapters.  One thing you should note (i.e. remember) is that if the text you are splitting starts with the regular expression, then split will return an empty string for the first item returned (this is the same behavior as string's `split`):

```
text = "apples are red"
```

```
print(text.split('a'))
```

```
print(re.split(r'a', text))
```

**Capturing the Pattern:**

One nice thing about the regular expression splitting is that you can group the expression using parenthesis so that the pattern used to `split`, is also returned in the returned token list:

```
text = "apples are red"
```

```
print(re.split(r'(a)', text))
```

# **Replacing on Regular Expressions**

You can also replace text that matches patterns for easy text cleaning.  For example, we can normalize all whitespace (including tabs, multiple spaces, newlines) to be a single space.  This makes finding multi-word tokens (e.g. Tom Sawyer that may span a new line in the text) simpler:

```
def cleaner(text):
```

```
  pattern = r'\s+'
```

```
  regex   = re.compile(pattern)
```

```
  return regex.**sub**(' ', text)
```

```
text = helper.read_huck()
```

```
t1_count = text.count('Tom Sawyer')
```

```
t2_count = cleaner(text).count('Tom Sawyer')
```

```
print("TS", t1_count, t2_count)
```

Note the differences of finding 'Tom Sawyer' across newline boundaries.  How would you figure out which ones span across lines?

# **Searching on Regular Expressions**

One of most useful methods on the string type is find which returns the index of the search string

```
text = 'I own both app99 and app9'
print( text.find('app9') )
```

When we use regular expressions, the `re.findall` returns the found strings. 

```
import re

text = 'I own both app99 and app9'
regex = re.compile(r'app[0-9]+')
print(regex.findall(text))
```

We can also use regular expressions to get the index of the location of the search strings but it is a bit more complicated which involves using the match object (see https://docs.python.org/3.6/library/re.html#match-objects).  The `finditer` method returns an iterator of match objects:

```
text = 'I own both app99 and app9'
regex = re.compile(r'app[0-9]+')
for i in regex.finditer(text):
  print(i.start(), i.end())
```

You can convert the iterator into a list as well:

```
text = 'I own both app99 and app9'
regex = re.compile(r'app[0-9]+')
items = list(regex.finditer(text))
print(items[0].start())
```

A simpler (but less flexible) options is to use the search method that also returns a match object:

```
text = 'I own both app99 and app9'
regex = re.compile(r'app[0-9]+')
m = regex.search(text)
print(m.start(), m.end())
```

The ability to search using regular expressions is especially powerful when you don't want to deal with how the text was created with respect to newlines, tabs, carriage returns, line feeds, and spaces -- all which can be equally treated using the whitespace (`\s`) search character.  


In case you're interested, below is a quick explanation to the differences in how you might see different combinations of newlines/linefeed/formfeed (`\n`) and carriage returns (`\r`) (the seldom used `\f` is for page breaks) in text documents:

📷

You can view whitespace using the `repr` function:

```
text = """
this
is a long  


sentence.


"""

print(**repr**(text))
```

You can then see how newlines and whitespace are used.  However, when you find yourself coding like this:

```
idx = text.find('\r\n\r\n')
```

You should think carefully about using specific whitespace sequences since those characters are very specific to the computer that the data was originally saved on.  It's possible that your code could be used with the same data, but whitespace was saved differently.  It is in these cases that using a regular expression, will make your code more resilient.


# **`.*`**

Once you have mastered the basics of building the correct patterns, there's even more power to explore.  As we saw with alternation, the parenthesis provide an easy way to get back parts of the token.  For example, using parenthesis, we can capture just the part we are interested in:

```
pattern = r'\s+(s?he)\s+'
```

However, there's more to learn about using parenthesis for group (and non-group) capturing and back references which provides the ability to group different parts of a pattern and then treat the corresponding matching groups differently.  

Be sure to read the documentation: `https://docs.python.org/3.6/library/re.html`

You now have enough knowledge now to responsibly put your regex power to good use: 

**Before you go you should know:**

- how to split, find, replace using regular expressions
- how to limit the greedy

# **Lesson Assignment**

All the code you write will go in `lesson.py`.  The format for the answers will be the same as in parts 1 and 2.  

**Simple Diary**

The following questions will use the text in `story.txt`.  You can use the helper function `read_story()`.

**s_q0:** Find all the references to money.  Money is defined to start with a $ followed by at least one digit, includes a single decimal followed by 2 cents digits.  If the money is $12.123 it is NOT matched

**s_q1:** Write the pattern that can be used to split the diary based on the entry dates.  You can assume the dates will always be in the same format.  The results should NOT include the date itself (but you should know how to do that).

**s_q2:** Find all tokens that include both numbers and letters (e.g. CS101, 1st). 

Hint: 14 total (12 unique)

**Experiment:**

For `s_q1`, take a look at what happens in the following experiments:

1.  `split` the story text based on your regex pattern

2.  use `findall` on the same text and the same regex pattern

3.  `split` the story text, but now update your pattern to capture the dates as well

What things did you notice?

**Test and Submit.**

You should develop and test your regular expressions on small sections of text.  You will have to pass 2/3 tests.  You can use the LessonAutoGrader to help you as well (not required):

```
grader = helper.LessonAutoGrader()
```

```
grader.test_all()
```

You can tag any question on Piazza with **UPRegEx3**


All Rights Reserved

