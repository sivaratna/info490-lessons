# **University 🐍: Nested Loops**

We have already seen that you can place a selection statement (i.e. if/else) inside another:

```
if color == 'red':
```

```
  if time > 18:
```

```
     counter += 1
```

This 'nesting' can also be done with loop structures as well.  That is, you can put one loop inside of another:

```
print("circle your favorite combo")
```

```
for snack in ["Doritos", "Chips", "Pretzels"]:
```

```
  for drink in ["Pepsi", "Coke", "Dew"]:
```

```
    print(snack + ", " + drink)
```

As you can see (hopefully you ran that code), the inner loop (`for` `drink`) runs for each item in the outer loop (`for` `snack`).

You can also **create** nested lists using the same idea:

```
combos = []
```

```
for snack in ["Doritos", "Chips", "Pretzels"]:
```

```
  inner = []
```

```
  for drink in ["Pepsi", "Coke", "Dew"]:
```

```
    inner.append(snack[0]+drink[0])
```

```
  combos.append(inner)
```

```
print(combos)
```

The variable combos is a list of lists:

`[['DP', 'DC', 'DD'],` 

 `['CP', 'CC', 'CD'],` 

 `['PP', 'PC', 'PD']]`

You can even use the same nested structure to unroll the values:

```
for combo in combos:
```

```
  for pair in combo:
```

```
    print(pair)
```

Once you have this list of lists, you can also access each element using the standard index notation:

```
chips = combos[1]
```

```
chips_and_dew = chips[2]
```

It's standard notation to use a combined, shortened syntax version:

```
cd = combos[1][2]
```

In this case, `combs[1]` returns a list and then you index into that list by offset 2.

You can also create nested lists directly:

```
all_pairs = [
```

```
['DP', 'DC', 'DD'], 
```

```
['CP', 'CC', 'CD'], 
```

```
['PP', 'PC', 'PD']]
```

As noted in the Bootcamp lesson on loops, the `for` loop is used in situations where the looping is finite.  It would require a lot more syntax to do nested loops using `while` loops.

**Multiple Dimensions** 

A simple list (e.g. snacks = ["Doritos", "Chips", "Pretzels"]) is known as a one dimensional list. But as we just saw, lists elements can be any valid Python type including another list:

```
scores  = [
  ['Roy', 80,75,85,90,95],
  ['John',75,81,75,85,100],
  ['Dave',80,80,80,90,95]
]
print(scores)
```

The variable `scores` is a list _of lists_.  We would call this a two dimensional list (or matrix).  In this example, scores has 3 rows, each row has 6 elements.  We will discuss matrices in more detail, but for now, scores is 3x6 matrix (3 rows by 6 columns).  

The `len` function on a multi-dimensional list will return the length of the first dimension:

```
print(len(scores))
```

For every dimension, you just need a set a matching square brackets `[]` to access the elements:

```
# what will this print? 
# figure it out before running
print(scores[1][0])
```

Here's the same data, but now the second value in each list is yet another list:

```
scores  = [   
```

```
['Roy', [80,75,85,90,95]],   
```

```
['John',[75,81,75,85,100]],   
```

```
['Dave',[80,80,80,90,95]]
```

```
]
```

In this case, how would you access John's second test score (e.g. 81).  Make sure you understand how and why it works.

There's even a more compact way to create lists (even multiple dimensions) using something called comprehensions which we will see in the next lesson.

**Home on the `range`.**

The `range` function is very useful for generating a 'list' of values that can be used to help build looping (see Listing the Possibilities for a re-refresher):

```
print(list(range(2,10)))
```

# Lesson Assignment:

There are three functions to create and an extra credit one as well.  This lesson is all about nested loops.  Even if you know how to use comprehensions (coming soon), do not use them.

Put all the code in the `lesson.py` tab/module.

**Dice Rolls**

Create a function named `build_roll_permutations` which returns all 36 permutations of rolling two six sided dice.  That is, each dice has a number 1 through 6 on one of its sides.   The return value is a list which contains **tuples**.  Each tuple represents a possible dice roll of two dice.

**Card Deck**

Create a function named `build_deck` that returns a full deck of cards.  The cards are created by a rank and a suit (e.g. 2♡).  The return value is a list of 52 strings.

- The rank must be in A,2,3,4,..,9,10,J,Q,K order
- The suits must be in ♢♣♡♠ order. 

So all the ♢s come first, then ♣, then ♡, then ♠.

**Multiplication Table**

Create a function named `create_multiplication_table` that has two parameters: m, n.  The function returns a table which is simply a list of rows.  Each row is another list with the values.  

The value at table[row][col] is the row*col.  So the following should work:

```
table = create_multiplication_table(6,6)
```

```
print(table[4][3]) # should see 12
```

```
print(table[6][6]) # should see 36
```

**Extra Credit**

If you wish to skip extra credit add the following to the `lesson.py` tab:

```
`skip_extra_credit = True`
```

Create a single function named `create_lower` that has a single parameter, `n`, which is used to create a lower diagonal `nxn` matrix. That is all the elements below the diagonal are the same as the row number (1 based) and all other values are zero:

```
matrix = create_lower(6)
```

```
for m in matrix:
```

```
  print(m)
```

That should print the following:

```
[1, 0, 0, 0, 0, 0]
[2, 2, 0, 0, 0, 0]
[3, 3, 3, 0, 0, 0]
[4, 4, 4, 4, 0, 0]
[5, 5, 5, 5, 5, 0]
[6, 6, 6, 6, 6, 6]
```

The first row should contain one 1

The second row should contain two 2's

and so on.

**Before Submitting:**

Test your functions well.  If you need more help, tag your question on piazza as with **UP02**

**Before you go, you should know:**

- what is nested loop
- when would you need to use nested loops
- how to declare and define a two dimensional array
- how to build a two dimensional array using loops
- how to access a cell in a 2D array


all rights reserved

**Read:**

https://docs.python.org/3/library/functions.html#func-range
