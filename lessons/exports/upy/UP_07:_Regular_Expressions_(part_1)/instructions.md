# **University 🐍:  Regular Expressions (part 1)**

Grab yourself a cup of coffee and settle in; this lesson's a bit longer than most.  If you don't have the time to work through it slowly, reschedule a better time to do this lesson.  However, if there's one thing that will help you parse and wrangle data the most, it's regular expressions. 

We have seen some _methods_ (functions attached to objects) on the string data type that provided some very handy capabilities.  For example `split()` transforms a string into a list of tokens.  Similarly `strip()` and `replace()` gives an easy way to remove or replace unwanted values.  The string has so many useful features that it's **always** worth re-visiting its documentation: `https://docs.python.org/3.6/library/stdtypes.html#string-methods`.  However, even with mastery of _all_ those methods, there are some things that would be extremely tedious if that's all we had to work with.  Let's take a look at some examples.

**Finding Words**

When doing text analysis, the first task is to figure out how to get all the words from a passage of text.  We will use the text from 'The Adventures of Huckleberry Finn' (from Project Gutenberg).  Its contents are in one of the repl.it tabs (`huck.txt`)

Let's take a quick look at how using string's `split()` method to get all the "words" of a book falls short.

```
text  = util.read_huck()
```

```
words = text.split()
```

```
uniq  = sorted(set(words))
```

```
print(len(uniq))
```

We get **13,104** 'words'.  However, in this set, words with different case (You and you) will be considered different words.  We can fix this quickly:

```
def get_uniq_wordset(words):
```

```
  return set([x.lower() for x in words])
```

```
text  = util.read_huck()
```

```
words = text.split()
```

```
uniq  = sorted(get_uniq_wordset(words))
```

```
print(len(uniq))
```

With this improvement, we get **12,615** words.  However if you inspect the contents of `uniq` (e.g `list(uniq)[0:20]`) words, we see a few issues:

1. words have punctuation in them 

2. phrases like hi!--hi! (those without spaces) are treated as a single word

If we pre-clean the text (i.e. before we `split()`) by removing all punctuation except the single quote (so we don't split contractions -- e.g. ain't), we end up with **6,893** unique tokens and **6,326** tokens after case normalization (validation is left to the reader).  However, that punctuation may be valuable in our analysis as well.

**Enter Regular Expressions**

As we have just seen, we need to do a double pass over the tokens, once to remove unwanted punctuation and another to split the text based on whitespace (i.e. `str.split()`). Although efficiency isn't always a goal, it becomes necessary as the datasets grow. However, the goal in this lesson is to see if we can do this by describing what we want to extract from the text, rather than writing a lot of code to do it.  The "tool" we are about to introduce, _**regular expressions**_, provides a language to make it 'easy' to extract patterns from text.  You don't need to use them, but they become very handy to express what you want to extract, rather than writing the code to tell the computer how to do it.  This is essentially the difference between imperative languages (like Python) and declarative languages (like SQL).  Although calling regular expressions a declarative language is a bit of a stretch.  The regular expression capability is provided by the `re` module.  You must include that module at the top of your Python code to use regular expressions:

```
import re
```

If the only thing you were interested in is tokenizing the text, then `split()` would be fine.  But we want the _words_.  Of course we have to define what it means to be a word.  Let's say that a word is any token that contains at least one letter.  With regular expressions, we can capture that _expression_ using a pattern.  Using regular expressions is usually three basic steps:

**1. define the pattern:**

```
pattern = `'**[A-Za-z]+**'`
```

- The pattern is always a string (hence, you need the quotes)
- In this case, we put what we are looking for inside `[]` brackets.  These brackets hold groups of characters (called a character set or character class). So `[A-Z]` means match any uppercase letter, `a-z` matches any lowercase letter.
- the `**+**` means `1` or more of the previous pattern or the thing to its left (in this case the stuff inside the brackets).
- the bracket matches **unordered** characters -- regardless of the order of the characters inside the brackets.
- We would describe this pattern as "one or more characters that are either upper or lower case letters".  So it would attempt to find any token that consists of all letters and any non letter would serve as a split or demarcation point.

 

**2. compile the pattern using the r`e.compile()` method:**

```
 pattern = `'[A-Za-z]+'`
```

```
 regex   = re.**compile**(pattern)
```

This creates a regular expression object that you can use to call different methods on.  In this lesson we will only be looking at the regular expression `findall` method.  The pattern is used to determine what to look for in a body of text.

**3. use the `findall()` method on the object returned by compile.  It's a regular expression object:**

```
import re 
```

```
def find_words(text):
```

```
  pattern = `'[A-Za-z]+'`          #1
```

```
  regex   = re.compile(pattern)  #2
```

```
  return regex.**findall**(text)     #3
```

```
a = find_words(util.read_huck())
```

```
print(len(a))
```

Be sure to run this code (you should see 116,312)

> **Coder's Log:**  This is indeed another lesson that if you don't run each sample code and move to the next one without understanding what you just ran, it will be impossible to learn the nuances being taught.

One small fix we need to do.  The pattern finds words regardless of the case (it's case is insensitive).  So `findall()`will return the words in their original case (it does NOT transform text). We need to be sure words like 'You' and 'you' are treated as the same word. The normalization step is still needed with regular expressions.   Let's fix that:

```
def get_uniq_wordset(words):
```

```
  return set([x.lower() for x in words])
```

```
a = get_uniq_wordset(find_words(util.read_huck()))
```

```
print(len(a))
```

We now get **5,983** 'words' -- any token that consists of all letters.  However, we need to consider to keep tokens that have numbers in them (e.g. 1st) or those that are all numbers (e.g. 10 cents).  We can extend our pattern to include numbers (we use the character class `0-9`):

```
def find_words(text):
```

```
  pattern = '[**0-9**A-Za-z]+'
```

```
  regex   = re.compile(pattern)
```

```
  return regex.findall(text)
```

```
uniq = get_uniq_wordset(find_words(util.read_huck()))
```

```
print(len(uniq))
```

Now the total is **5991** tokens. Can you use the `set` data type and the `difference` method to find what numbers are now captured? (the answer is Yes, and you should do so now).

**Getting Closer**

So the question is why is this NOT **6,326** which we got using `split()`? The issue is that with our `split()` technique we removed all punctuation except the single quote.  So we need to add that in:

```
pattern = '[\'0-9A-Za-z]+'
```

Since pattern is wrapped with single quotes you have to escape the single quote you want to find.  Alternatively, you could use double quotes:

```
pattern = "['0-9A-Za-z]+"
```

Once you run that pattern.  You get the magically **6,326** match!

**Raw Strings**

There's a small issue when a regular expression pattern has these special 'commands' (called escape or control sequences).  We have already been using escape sequences -- the `'\n'` is one of those special characters that is replaced with a newline when it appears in a string.  To tell Python NOT to ignore any control sequences, you preface the string with an `r` -- which means a raw string.  You can see how this will affect the string evaluation in the following:

```
`print( "Hello\nWorld\n", end='')`
```

```
`print(r"Hello\nWorld\n", end='')`
```

In the second `print` statement, the `\n` is printed as opposed to being interpreted to force a return or linefeed.

You can use the same raw string to print out _unicode_ characters.  We will discuss unicode in another lesson.  But you can think of it as a way to provide a representation of all possible characters.  Unicode provides a unique number for every character, no matter what the platform, no matter what the program, no matter what the language.  (more on unicode later).

```
print(r'\U0001f441\U00002764\U0000FE0F\U0001f40d')
print('\U0001f441\U00002764\U0000FE0F\U0001f40d')
```

Once again, you should know how the above two statements are treated differently by prefacing one of the strings with `r`.

With regular expression we ALWAYS use raw strings.

```
def find_words(text):
```

```
  pattern = r'[\'0-9A-Za-z]+'
```

```
  regex   = re.compile(pattern)
```

```
  return regex.findall(text)
```

```
uniq = get_uniq_wordset(find_words(util.read_huck()))
```

```
print(len(uniq))
```

We now have **6,326** unique words (after case normalization).  Yikes. that's where we were with `split()` and some pre processing!  However, the story is not over.  

**Back to Huck Finn.**

At this point, we need to inspect the tokens and decide if we are getting the right values.  For Huckleberry Finn, there's a lot of contractions and hyphenated words.  For example here's the text starting at line 5947:

My breff mos' hop outer me; en I feel so--so--I doan' know HOW I feel.  I crope out, all a-tremblin', en crope aroun' en open de do' easy en slow, en poke my head in behine de chile, sof' en still, en all uv a sudden I says POW!

> **Reader's Log:** You can use the sparknotes service to help translate this passage:  https://www.sparknotes.com/nofear/lit/huckleberry-finn/chapter-23/page_3/

For this specific text, any word with a hyphen (e.g. sugar-hogshead) was split into two (because we either removed it or didn't include it in the regular expression.  But we also did want to split words separated by two hyphens (e.g. Polly--Tom's).

The issue is that in our pattern we are excluding words with a single hyphen in them.  Let's find them using the following pattern:

```
pattern = r"['A-Za-z0-9]+[-]['A-Za-z0-9]+"
```

This finds all the words that have a SINGLE hyphen (`[-]`) with at least one letter before it and at least one letter after it.  There are 604 such words.  Doing that without using regular expressions would be very tedious.

We also can make that single hyphen optional by using the `?` (a special character that means 0 or 1 of the previous pattern).

With a few quick changes, we can quickly see the power of using a regular expression to find different token patterns in the text:

```
def find_words(text):
```

```
  pattern = r"['0-9A-Za-z]+-?['0-9A-Za-z]+"
```

```
  regex   = re.compile(pattern)
```

```
  return regex.findall(text)
```

```
uniq = get_uniq_wordset(find_words(util.read_huck()))
```

```
print(len(uniq))
```

Don't worry, after awhile reading the patterns becomes much easier.  The hardest thing to understand is the `+-?` in the middle.  Here's how you would read the pattern:

"1 or more (that's the `**+**`) characters that are either a single quote, a letter, or a number; FOLLOWED by a hyphen (`-`) that is optional (`?`) FOLLOWED by 1 or more (the very last `**+**`) characters that are either a single quote, a letter or a number."

We now have **6,678** tokens. Note that since the hyphen is the only character we don't have to put it in brackets (e.g. `[-]?` )

The remaining issue is that this pattern forces all words to be at least 2 characters long.  We lose all the single character tokens (e.g.  a, 4, 3, o).  We can fix that in the next lesson.

**A Few Regular Expression Mechanics**

We now have seen enough to realize there's probably a lot of mechanics to learn about using regular expressions.  You won't have to ever memorize them, but you should know what you can do.  You can always look up the syntax later.

The **square brackets `[]`** are used to hold multiple characters or character classes that can occur in any order. 

```
[abc]  matches a or b or c
```

```
[abc]+ matches any combination of the letters: a, b, c 
```

In a character class (the square brackets) **any character** is a literal (meaning it doesn't represent something else) except these four: 

1. `^` 
2. `-` 
3. `]` 
4. `\` 

In other words, if you wanted to match a caret `**^**` you would have to escape it (e.g. `[\^abc]`). 

**The Anti-Match**

If you want to match anything BUT a specific character class, you add the caret as the first item in square brackets:

```
[^abc] matches any thing BUT a or b or c 
```

```
       the caret negates everything that follows
```

**Specific Sequences**

If you wanted to find a specific string, you can just specify the exact order:

```
pattern = r"Aunt\s+Polly"
```

This pattern would read "find the word Aunt followed by at least one white space character (i.e. the `**+**` sign) and then followed by the word Polly".  

**Matching Specific Counts of Characters**

The following shows how to specify **the amount of match counts** that can be used after a pattern (what's immediately to the left):

```
?     0 or 1 time
```

```
*     0 or more times
```

```
+     1 or more times
```

```
{m}   m times
```

```
{m,}  at least m times
```

```
{,n}  0 through n times (inclusive)
```

```
{m,n} m through n times (inclusive)
```

The following can be used to specify matching a character or a set of characters:

```
.  match any character except \n 
```

```
\. match the period
```

```
\? match the question mark
```

```
\s match whitespace \s+ one or more white spaces 
```

```
\S match non whitespace
```

```
\d match digits (same as [0-9])
```

```
\D non digits (same as [^0-9])
```

```
\w same as [a-zA-Z0-9_]+  (word character)
```

```
\W same as [^a-zA-Z0-9_]+ (non word character or non alphanumeric)
```

```
\' match a single quote
```

```
\" match a double quote
```

So the pattern `".o{2}.[ed]"` will match any letter (`.`) followed by 2 o's (`o{2}`) followed by any letter (`.`) and then followed either by an **e** or a **d** (`[ed]`).  So this pattern would match:  looke, hoose, cooke.  Note that these are most likely partial word matches.  But that's correct since we didn't specify any white space or word boundaries (to be discussed later).

**Simplification**

As we have seen, the regular expression pattern can get a bit long and we are always striving to keep the pattern as short and readable as possible.  We can clean up the pattern by telling the compiler of the regular expression to ignore case:

```
def find_words(text):
```

```
  pattern = '[_0-9a-z]+'
```

```
  regex   = re.compile(pattern, **re.IGNORECASE**)
```

```
  return regex.findall(text)
```

Since we want to ignore the case (i.e. case insensitive) for the _entire_ pattern we just pass the `re.IGNORECASE` flag to the compiler.

This is such a popular pattern that Python provides a special character (`\w`) that represents the pattern above (including being case insensitive):

```
def find_words(text):
```

```
  pattern = r'\w+'
```

```
  regex   = re.compile(pattern)
```

```
  return regex.findall(text)
```

So we can shorten our final pattern for Huckleberry Finn tokens as:

```
def find_words(text):
```

```
  pattern = r"['\da-z]+-?['\da-z]+"
```

```
  regex   = re.compile(pattern, re.IGNORECASE)
```

```
  return regex.findall(text)
```

**Greedy Matching ***

One thing (among many) to remember is that the regular expression engine will try to match the longest string possible.  It's called greedy matching.  You can change that, but we will save that for another lesson.  So if you have the pattern:

```
text = "Abra abracadabra"
```

```
pattern = r'ab.*'
```

```
reg_ex = re.compile(pattern, re.IGNORECASE)
```

```
print(reg_ex.findall(text))
```

This will match the entire string (and NOT 3 different `'ab'` substrings).  In general if you have `.*` in your regular expression, it will most likely match more than you want it too.  In many cases, the greed will harm you.

# **More By Example**

**Finding _Italicized_**

As a data scientist, it's important to be very familiar with the data being processed.  In this case after reading some of the raw text we notice that for this book, italicized words or phrases are encoded by surrounding the word with an underscore.  In Huckleberry Finn for example (from Chapter 2, line 243) the text:

    

  'Some thought it would be good to kill the _families_ of boys that told the secrets.'

Get's encoded as follows:

   'Some thought it would be good to kill the \_families\_ of boys that told

the secrets.'

Since we removed those underscores in the first version, and excluded the underscore in the regular expression, those words were never noticed (if you just looked at the returned tokens). 

Here's a quick example to find all italicized words: (those that begin and end with an underscore):

```
def find_words(text):
```

```
  pattern = r"**_**['A-Za-z0-9]+**_**"
```

```
  regex   = re.compile(pattern)
```

```
  return regex.findall(text)
```

```
uniq = get_uniq_wordset(find_words(util.read_huck()))
```

```
print(len(uniq))
```

You should find **330** words that were emphasized in the book.  However, phrases such as  "'Nough!--I _own up!_" would **not** be found.  Can you see why?

**Finding Digits**

To find all the tokens with only digits in them, we just update the pattern:

```
def find_words(text):
```

```
  pattern = r"[0-9]+"
```

```
  regex   = re.compile(pattern)
```

```
  return regex.findall(text)
```

```
uniq = get_uniq_wordset(find_words(util.read_huck()))
```

```
print(len(uniq))
```

We now can easily extract the 8 unique numeric tokens; `['200','300','25','10','1','2','3','4']`

**Experimenting**

When testing regular expressions, it's easier to work with a small sample of text to see if things are working or not.  You can always extract a paragraph of text from your book and test (using set differences) between different patterns, what they match and what they don't.

For example:

```
sentence = "'Deed you _ain't!_  You never said no truer thing 'n that, you bet\nyou." 
```

```
print(find_words(sentence))
```

**Additional Normalization**

You also can decide if you need to post normalize your tokens.  It usually depends on the project's goals and objectives and the regular expression used.  For example, the following could be done with the results from `findall()`:

- removing whitespace before or after the token
- case normalization
- replacing the contractions with the fully spelled set of words (e.g. can't becomes cannot)
- decide on common spelling (e.g. can not becomes cannot)
- removing the plural (e.g songs become song)
- fix spelling errors
- stemming (a topic to be discussed in another lesson) which is similar to extracting the root of a word.

  

We will leave all the tokens alone.

**Before you go, you should know:**

- what is a regular expression
- why are regular expressions useful
- how do you create a regular expression in Python?
- what does `findall` return
- what does . match 
- what does * match
- what does \s match
- what does \d match
- when do you use the `[]` notation 

# **Lesson Assignment**

There is a lot to learn in this lesson.  Be sure to **re-read** it and run all the examples.  For all the questions in this lesson, you don't need to consult external documentation (you can of course, but everything required to solve these puzzles is given to you).  

Notes:  

- If you already know regular expressions and know a way to get a _better_ solution, you still MUST ONLY USE what is taught in this lesson.  Otherwise, you may not pass the tests.
- Do NOT normalize the input or output.  The tests are only looking at the results of the regular expression.
- Use `https://regex101.com` for an easier way to develop/debug a working regular expression
- All your solutions will be in `lesson.py` tab.
- Testing hints are given at the end
- Do **NOT use the 'or' symbol** (e.g the pipe) -- it's something that will be covered in the next lesson.  For any question that asks to find this or that, you need to use a standard regular expression shown in this lesson.  This will be post tested and there is no getting points back.

**Question 0:  Total Sentences**

Write the regular expression to find all the sentences in huck.txt.

Let's assume that all sentences end with one of the following three punctuation marks: `**? ! .**` 

The answer is already done for you!  Take a look at `lesson.py: q0`

```
`def q0():`
```

```
  `pattern = r'[^?.!]+[?.!]+'`  
```

```
  `return re.compile(pattern, re.IGNORECASE)`
```

Which you read as "1 or more of any character that is NOT a terminator followed by at least one terminator. A terminator is one of  (`**?**` or `**.**` or `**!**`)."  

You can test it in main.py:

```
raw = util.read_huck()
```

```
reg_ex = lesson.q0()
```

```
result = reg_ex.findall(raw)
```

```
size = len(result)
```

```
print(size, result[size-3:]) #show the last 3
```

You should get 5960 sentences (based on our definition). If you wanted to capture the ending `**"**` in sentences, you would add the quote:

```
`pattern = r'[^?.!]+[?.!**"**]'` 
```

If you wanted to include any extra punctuation that ends some sentences (sentences that end like this!!!!) you could add the `**+**` at the end:

```
`pattern = r'[^?.!]+[?.!"]**+**'` 
```

Note that the following sentence would be considered 2 sentences:

`Mr. Kean played Richmond.`

So that 5960 is an approximation.  We can do better, but the regular expression required would become very complex and involve more mechanics that we need to learn.

The best way to solve this is to FIRST define some sample text:

```
[sample = "He pulled the lever all the way down to where it said full steam ahead. A bell rang. The motors made a grinding sound and the ferry began to move. The passengers were surprised because the captain was still on deck taling to the Man in the Yellow Hat. Who was running the boat? It was George!!!"](http://www.great-quotes.com/quote/1684325)
```

```
reg_ex = lesson.q0()
```

```
result = reg_ex.findall(sample)
```

```
print(len(result))
```

You get 6 for an answer.  You can verify that by hand.  Once you have it working on sample text, then try it on the full text.

All of your answers should be in the same format as q0.  Return a compiled regular expression (with any flags if necessary).  The ONLY flag (if we use one) will be `re.IGNORECASE`).  There are other flags, but those will be used in subsequent lessons.

Before asking for help.  Be sure to test each part of your answer.  Test it will a few words, a short sentence, a long paragraph.  Now that you know how to slice and dice an array, it's easy to extract sections of text:

```
import helper
```

```
text = helper.read_huck()[0:200]
```

```
print(text)
```

More testing/debugging information is given below.

**Question 1:  !!!**

Define the regular expression to answer:  How many times does an exclamation mark happen 3 times in a row?

Hint: 3

**Question 2:  only numbers please**

Define the regular expression to answer: How many tokens consist of only numbers? 

Hint: 8

**Question 3: cost of numbers**

Define the regular expression to answer: How many tokens represent money (i.e. a value that starts with a $) ?

Hint:  the $ has special meaning, you will need to escape it.

**Question 4:  boom--boom**

Define the regular expression to answer: How many times is there a double dash within a word?  A word consists of only letters.

Hint: 763

**Question 5:  any body or Anybody**

Define the regular expression that finds both: any body as well as **anybody** or **A**ny body.

Hint: 74

**Question 6: quote me**

Define a regular expression that finds single quoted words that contain at least 3 letters.

Hint: 7; a word is only letters

**Question 7: the Dr.,  the Mr. and Mrs.**

Define the regular expression that finds any of the following (includes the period): Dr.  Mr.  Mrs.  (that specific case as well)

Hint: 34

**Question 8:** Aw S**huck**s

Define a regular expression that finds any word that contains **huck**

Note: this is a regular expression where you could use the greedy `*****`

A word consists of only letters and capitalization is not relevant.

Hint: 103

**Question 9: he or she**

Define a regular expression that will find tokens that finds either 'he' or 'she' (without the quotes and capitalization is not relevant) AND each must be surrounded by whitespace so the 'he' in 'them' would not be found. 

Hint: 2110

**Question 10: How many chapters?**

Define the regular expressions to find the chapter markers in Huck Finn.  The result should contain 43 items.

**Testing and Submissions**

You can test each one as shown in question 0.  However, in `helper.py` tab is a testing framework you can use.  You can easily test all your answers by running the following (in `main.py`):

```
tester = util.HuckFinnAutoGrader()
```

```
tester.test_all()
```

This is the same grader that is used in the tests.

You can also just test one question at a time (and show the results):

```
tester = util.HuckFinnAutoGrader()
```

```
# test q1
```

```
tester.test_one(1, show_result_count=100)
```

If your code is taking a long time to run (or if you want to practice on a smaller text sample, you can use the SampleTester):

```
sample = "This! This is!! This is so Fun!!!"
util.SampleTester(lesson.q1, sample, 3)
```

If you want even finer control, you can do something along the lines of the following:

```
text  = util.read_huck()
```

```
regex = lesson.q9()
```

```
s1 = regex.findall(text)
```

```
print(len(s1))
```

**Extra Credit.**

To earn extra credit answer the following 3 questions.  If you wish to skip the extra credit add the following code to `lesson.py`

```
skip_extra_credit = True
```

**Question 11: How many cents?**

Define the regular expression to find a number followed by 'cents' as in "it costs **10 cents**."

Hint: 2

**Question 12: How many cents?**

Define the regular expression to find a spelled out numbers followed by 'cents' as in "it costs **five cents**."

Hint: 19

**Question 13: How many cents?**

Define the regular expression to find a spelled out numbers that include a hyphen followed by 'cents' as in "it costs **twenty-five** **cents**."

Hint: 2

**Food For Thought.**  

If you make the hyphen optional in question 13, how many numbers do you find?  Why isn't the answer 21?  (that is 19 + 2)

You can tag any questions with **RegEx[0-9]+** (that's a regex pun: use the specific number) on piazza.


All Rights Reserved
