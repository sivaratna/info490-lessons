import lesson
import string
import re

def read_huck():
  with open('huck.txt', 'r') as fd:
    txt = fd.read()
  return txt
  
sample0 = "He pulled the lever all the way down to where it said full steam ahead. A bell rang. The motors made a grinding sound and the ferry began to move. The passengers were surprised because the captain was still on deck taling to the Man in the Yellow Hat. Who was running the boat? It was George!!!\nI had 20 dollars and george, $25.00!!! Even CHAPTERS can help us"

sample1 = '''
Miss Watson would say,"Don't put your feet up there, Huckleberry;" and "Don't scrunch up like that, Huckleberry--set up straight;" and pretty soon she would say, "Don't gap and stretch like that, Huckleberry--why don't you try to behave?"  Then she told me all about the bad place,
'''

sample2 = 'Apples--app_les! I love apples'
sample3 = "Richmond............... Mr. Kean."

class AutoGrader(object):
  
  def __init__(self, name, data):
    self.name  = name
    self.data  = data
    self.tests = []
    
  def run_pattern(self, pattern, flag=None, do_uniq=False):
    
    if flag != None:
      regex = re.compile(pattern, flag)  
    else:
      regex = re.compile(pattern)
      
    result = regex.findall(self.data) 
    
    if do_uniq:
      result = set([x.lower() for x in result])
      
    return result
    
  def test_one(self, num, show_result_count=5, data=None):
    return self.run([self.tests[num]], show_result_count, data)
    
  def test_all(self, show_result_count=5):
    return self.run(self.tests, show_result_count, self.data)
    
  def run(self, tests, show_result_count=5, data=None):
    if data is None:
      data = self.data
      
    pass_count = 0
    fail_count = 0
    for i, test in enumerate(tests):
      print("--"*5)
      label, reg_ex, answer_set = test
      result = reg_ex.findall(data)
      
      if answer_set is None:
         pass
      else:
        if len(result) in answer_set:
          print("Test {} PASS ({} found)".format(label, len(result)))
          pass_count += 1
        else:
          found = len(result)
          print("Test {} FAIL".format(label), end=' ')
          if (len(answer_set) == 0):
            print("(no answer key)")
          else:
            print("")
          print("Test {} returned {} values (wanted {})".format(i,found, answer_set))
          print("Test {} regex {}".format(label,reg_ex))
          fail_count += 1
        
      n = min(show_result_count, len(result))
      if n > 0:
        print("Top {} found:".format(n))
        for i in range(0,n):
          r = result[i].strip()
          print(r[:25]) # last 25 characters
    
    if pass_count > 0 or fail_count > 0:
      print ("\n{} correct; {} incorrect".format(pass_count,fail_count))   
    
    return (pass_count,fail_count)

class SampleTester(AutoGrader):
  def __init__(self, test, data, top=5):
    super().__init__('sample', data)
    self.tests = [ ('0', test(), None )]
    self.test_all(show_result_count=top)
  
class HuckFinnDemo(AutoGrader):
  
   def __init__(self):
    super().__init__('huck', read_huck())
    
    r1 = self.run_pattern(r'[A-Za-z]+',  None, False)
    print('1', len(r1))
    
    #5983
    r2 = self.run_pattern(r'[A-Za-z]+',  None, True)
    print('2', len(r2))
    
    #5991
    r3 = self.run_pattern(r'[0-9A-Za-z]+', None, True)
    print('3', len(r3), r3-r2)
    
    #6326
    r6 = self.run_pattern(r"['0-9A-Za-z]+", None, True)
    print('6', len(r6))
    
    # words with hyphens are good
    p1 = r"['0-9A-Za-z]+-?['0-9A-Za-z]+"
    
    r7 = self.run_pattern(p1, None, True)
    print('7', len(r7))
    
    p2   = r"['\da-z]+-?['\da-z]+"
    r7b = self.run_pattern(p2, re.IGNORECASE, True)
    print('7b', len(r7b)) 
  
    # italized
    p2 = r"_['A-Za-z0-9]+_"
    r8 = self.run_pattern(p2, None, True)
    print('8', len(r8)) 
  
    
class HuckFinnAutoGrader(AutoGrader):
  def __init__(self):
    super().__init__('huck', read_huck())
    self.tests = [
      ('0',lesson.q0(), [5960]),
      ('1',lesson.q1(), [3]),
      ('2',lesson.q2(), [8]),
      ('3',lesson.q3(), [1]),
      ('4',lesson.q4(), [763]),
      ('5',lesson.q5(), [74]),
      ('6',lesson.q6(), [7]),
      ('7',lesson.q7(), [34]),
      ('8',lesson.q8(), [103]),
      ('9',lesson.q9(), [2110]),
      ('10',lesson.q10(), [43]),
      
      # uncomment if want to test extra credit
      # ('11',lesson.q11(), [2]),
      # ('12',lesson.q12(), [19]),
      # ('13',lesson.q13(), [2]),
    ]
    

# clean and split example
#util.old_school(util.read_huck())
#demo = util.HuckFinnDemo()

class old_school(object):
  
  def __init__(self, text):
    
    # before split, pre_clean it to remove -- etc, 
    t1_clean = self.pre_clean(text)
    t1 = t1_clean.split()
    
    print("total", len(t1), "uniq", len(set(t1)))
    t1_lower = [x.lower() for x in t1]
    print("after lower")
    print("uniq", len(set(t1_lower)))
    
  def pre_clean(self, t):
    black = string.punctuation
    black = black.replace('\'', "")  # keep single quote
    for r in black:
      t = t.replace(r, ' ')
    return t
    
    