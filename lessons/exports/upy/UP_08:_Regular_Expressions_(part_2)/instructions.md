# **University 🐍:  Regular Expressions Part 2**

This is the second part of using regular expressions to easily find and use patterns in text. As we have seen they are a powerful tool for processing text data by letting you describe a pattern for which data you are interested.  You can use regular expressions for 

- cleaning text: replacing characters, removing characters
- parsing text: splitting text into sentences, words, and tokens (like dates, airport codes, capitalized words, words with hyphens, etc)
- testing text: asking if text has a vowel, is a number, a date, etc
- searching for text that matches a pattern

The effort to learn regular expressions is well worth it -- almost all programming languages support regular expressions, you can even use regular expression in some editors and on the "command line" (something we hope to cover).  

The following reference from part 1 is repeated here:

```
[abc]   matches a or b or c
[abc]+  matches any combination of a, b, c 
[^abc]  matches any thing BUT a or b or c 
        the caret negates everything that follows
^ - ] \ these have special meaning and you will
```

```
        need to escape inside [ ]
```

```
?     0 or 1 time
*     0 or more times
+     1 or more times
{m}   m times
{m,}  at least m times
{,n}  0 through n times (inclusive)
{m,n} m through n times (inclusive)
```

The following can be used to specify matching a character or a set of characters:

```
.  match any character except \n 
\. match the period
\? match the question mark
\s match whitespace; same as [ \t\n\r\f\v]
\S match non whitespace; same as [^ \t\n\r\f\v]
\d match digits; same as [0-9]
\D non digits; same as [^0-9]
\w same as [a-zA-Z0-9_]+  (word character)
\W same as [^a-zA-Z0-9_]+ (non word character or non alphanumeric)
\' match a single quote
\" match a double quote
```

**New in this lesson**

The following is a summary of some of the new topics we will cover in this lesson:

```
ab|bc   Matches either ab **or** bc
```

```
\b      Matches empty string at word boundary
```

```
\B      Matches empty string not at word boundary
```

```
^       Matches the beginning of a line 
```

```
$       Matches the end of the line
```

**This OR that or the other thing**

In the previous lesson you were asked to create a pattern to match either he or she.  The following are possible solutions:

```
pattern = r'\s+[sh]+e\s+'  
```

```
pattern = r'\s+s?he\s+'   
```

Both find 2110 matches.  Both versions required the `\s+` be used to ensure partial words (e.g. sheep) were not matched.  However, the first one is technically wrong since it also matches words like shhhhe.  The second one works because we were able to leverage the common characters.  

With regular expressions you can use the pipe character (`|`) to separate choices.  This is called alternation.  For example:

```
text = "He said breathe and she and the sheep did hehe"
```

```
pattern = r'he|she'
```

```
r = re.findall(pattern, text, re.IGNORECASE)
```

```
print(r)
```

Here are the matches:

```
['He', 'he', 'she', 'he', 'she', 'he', 'he']
```

```
**He** said breat**he** and **she** and t**he** **she**ep did **he**he
```

We can fix the above issues once we learn about word boundaries.

**Word Boundaries**

Let's start this lesson with an attempt to find all words in the text that have two consecutive vowels.  With regular expressions, the task is quite easy.  Let's write a pattern to find any word that contains **two consecutive vowels**:

```
def find_vowels(text):
  pattern = r'[a-z]*[aeiou]{2}[a-z]*'
  regex   = re.compile(pattern, re.IGNORECASE)
  return(regex.findall(text))
```

Here's a description of the parts of pattern:

- `[a-z]` is any letter, a through z
- `*` means zero or more of the previous character class
- `{2}` means 2 of the previous character class (vowels)
- use the `re.IGNORECASE` to keep the pattern simple

You would describe the pattern as trying to match 0 or more letters, followed by exactly 2 vowels followed by 0 or more letters.

```
txt  = helper.read_huck()
uniq = helper.get_uniq_set(find_vowels(txt))
print(uniq[0:20], len(uniq))
```

You should get 1529 words with two consecutive vowels.


How about words that **begin with two consecutive vowels**?  

```
def find_vowels_starts(text):
  pattern = r'[aeiou]{2}[a-z]*'
  regex   = re.compile(pattern, re.IGNORECASE)
  return(regex.findall(text))

v    = find_vowels_starts(helper.read_huck())
uniq = helper.get_uniq_set(v)
print(uniq[0:20], len(uniq))
```

We find 767 matches, but there are some issues here.  We get words like 'oody' and 'oollishness'.  The problem is that we are matching partial words.  We can augment the regular expression with a _**word boundary**_ special character.  In Python, there is an idea of a word boundary which has the following definition:  

_A word boundary **matches the empty string**, but only at the beginning or end of a word. A word is defined as a sequence of alphanumeric or underscore characters, so the end of a word is indicated by whitespace or a non-alphanumeric, non-underscore character._ 

Let's update the pattern so that that it must start with a word boundary:

```
def find_vowels_starts(text):
  pattern = r'**\b**[aeiou]{2}[a-z]*'
  regex   = re.compile(pattern, re.IGNORECASE)
  return regex.findall(text)

v    = find_vowels_starts(helper.read_huck())
uniq = helper.get_uniq_set(v)
print(uniq[0:20], len(uniq))
```

This gives us 65 complete matches.  

The thing to remember about word boundaries is that they match a **_zero-width_** character. They don’t cause the regular expression engine to advance through the string; instead, they consume no characters at all, and simply succeed or fail. 

For example, `\b` is an assertion that the current position is located at a word boundary; the position isn’t changed by the `\b` at all. This means that zero-width assertions should never be repeated, because if they match once at a given location, they will match an infinite number of times.


So a word boundary will find those "words" that at least start with `[a-z0-9_]+` followed by whitespace or `[^0-9a-z_]`.  Once it matches that location, it is up to whatever follows the `\b` in your regular expression to determine a match.


If we wanted to match italicized double vowel words or phrases (e.g. _Aunt Polly!_), we would have to add the underscore to the pattern:

```
pattern = r'\b_[aeiou]{2}.*_'
```

The thing to note is the above expression will _return_ the matching underscore and the set of words it finds will be a subset of the words found by the first regular expression (`r'**\b**[aeiou]{2}[a-z]*'`) and also include words that have non letters before the underscore as well.


We can adjust the pattern to find the 69 words that **end with two consecutive vowels** (e.g kangaroo):

```
def find_vowels_ends(text):
  pattern = r'\b[a-z]*[aeiou]{2}\b'
  regex   = re.compile(pattern, re.IGNORECASE)
  return regex.findall(text)

v    = find_vowels_ends(helper.read_huck())
uniq = helper.get_uniq_set(v)
print(uniq[0:20], len(uniq))
```

**Word Boundaries with he | she**

Now we can address the previous issue with finding he and she:

```
text = "He said breathe and she and the sheep did hehe"
```

```
pattern = r'he|she'
```

```
r = re.findall(pattern, text, re.IGNORECASE)
```

```
print(r)
```

As we saw, these are the matches:

```
['He', 'he', 'she', 'he', 'she', 'he', 'he']
```

```
**He** said breat**he** and **she** and t**he** **she**ep did **he**he
```

If you wanted to restrict the matching using word boundaries (or whitespace) you could do the following:

```
`pattern = r'\bhe|she\b'`  
```

However, we only get two matches:

```
['He', 'she', 'he']
```

```
**He** said breathe and **she** and the sheep did **he**he
```

The alternation precedence is so low that the first \b is assumed to be part of the **he** and the final \b is assumed to follow any **she**.  You can fix this by using parenthesis.  Parenthesis are used to group matches.  When you use them everything in the group is returned as a match:

```
`pattern = r'\b(he|she)\b'`
```

Now we get the words we wanted:

```
['He', 'she']
```

```
**He** said breathe and **she** and the sheep did hehe
```

By using the parenthesis, it gives you the ability add pre or post matching as well: 

```
text = "a telethon of a python coding marathon raised awareness of sleep state misperception"
```

```
pattern = r'\b(py|mara|tele)thon\b'
```

```
r = re.findall(pattern, text, re.IGNORECASE)
```

```
print(r)
```

So if you are searching for a pattern but you want to capture the entire word (i.e. the context) that encapsulates the pattern, use word boundaries.  The complement of \b is \B which means match the empty string at a non word boundary.  If you wanted to find cat in the middle of a word:

```
text = "the cat advocats to concatenate."
```

```
cat = re.findall(r'(\Bcat\B)', text)
```

```
print(cat)
```

**Finding the First and Last**

Sometimes it's handy to be able to match the first item or last item in a body of text.  For example, if we wanted to just to match 'Dogs' in the following poem.  We would do the following:

```
poem = '''\
```

```
I LIKE
```

```
Dogs
```

```
Black Dogs, Green Dogs
```

```
Bad Dogs, Mean Dogs
```

```
All kinds of Dogs
```

```
I like Dogs
```

```
Dogs
```

```
'''
```

```
def find_dogs(text):
```

```
  pattern = r'Dogs'
```

```
  regex   = re.compile(pattern)
```

```
  return regex.findall(text)
```

```
v = find_dogs(poem)
```

```
print("dogs:", len(v))
```

But what if we wanted ONLY those Dogs that begin a line?  The caret ^ can be used to indicate the start of a line of text:

```
poem = "Dogs I like Dogs\nDogs I am"
```

```
def find_dogs(text):
```

```
  pattern = r'^Dogs'
```

```
  regex   = re.compile(pattern)
```

```
  return regex.findall(text)
```

```
v = find_dogs(poem)
```

```
print("dogs:", len(v))
```

The issue is the ^ will ONLY match the first item in a string.  If you want to match all Dogs that begin a line (that is, after a newline), you must use the `re.M` or `re.MULTILINE` flag.

```
poem = "Dogs I like Dogs\nDogs I am"
```

```
def find_dogs(text):
```

```
  pattern = r'^Dogs'
```

```
  regex   = re.compile(pattern, re.M)
```

```
  return regex.findall(text)
```

```
v = find_dogs(poem)
```

```
print("dogs:", len(v))
```

If you need to pass multiple flags to the compile method you use the `|` (the or operator):

```
poem = "Dogs I like Dogs\ndogs I am"
```

```
def find_dogs(text):
```

```
  pattern = r'^Dogs'
```

```
  regex   = re.compile(pattern, re.IGNORECASE | re.M)
```

```
  return regex.findall(text)
```

```
v = find_dogs(poem)
```

```
print("dogs:", len(v))
```

If you want to match words that END a line, you use the `$` character:

```
poem = '''\
```

```
I LIKE
```

```
Dogs
```

```
Black Dogs, Green Dogs
```

```
Bad Dogs, Mean Dogs
```

```
All kinds of Dogs
```

```
I like Dogs
```

```
Dogs
```

```
'''
```

```
def find_dogs(text):
```

```
  pattern = r'Dogs$'
```

```
  regex   = re.compile(pattern, re.IGNORECASE | re.M)
```

```
  return regex.findall(text)
```

```
v = find_dogs(poem)
```

```
print("dogs:", len(v))
```

So to get the Dogs that are the only word on a line:

```
pattern = r'^Dogs$'
```

For analyzing text (including novels in Project Gutenberg) you cannot depend on multiline matching for determining the end of a sentence -- since the placement of the newline characters most likely depends on the format of the book.  For example the pattern that finds any word with two vowels at the end of the line:

```
def find_vowels_ends(text):
```

```
  pattern = r'[a-z]*[aeiou]{2}$'
```

```
  regex   = re.compile(pattern, re.M | re.IGNORECASE)
```

```
  return(regex.findall(text))
```

```
v    = find_vowels_ends(helper.read_huck())
```

```
uniq = helper.get_uniq_set(v)
```

```
print(uniq[0:20])
```

This only finds a few words.  On line 1178 _prairie_ matches the above criteria only because the newline happened to be there.

 

**Easy Practice**

There are several web based tools that you can use to build and test regular expressions easily.  Be sure to check out the following resources:

- `https://pythex.org`
- `https://regex101.com/#python`
- `http://www.pyregex.com`
- Be sure to read the documentation: `https://docs.python.org/3.6/library/re.html`

**Before you go, you should know:**

- how the pipe operator works
- what a word boundary is
- how word boundaries do matching
- the meaning of `^`
- the meaning of `$`
- how `re.MULTILINE` works with `^` and `$`

# **Lesson Assignment**

There are 10 questions to answer.  You need to pass 8 of them.

All the code you write will go in lesson.py.  

The format for the answers will be the same as in part 1 assignment.  

**Huckleberry Finn**

The following questions will use the text inside the tab huck.txt.  You can use the helper function `read_huck()` 

**h_q0:**  How many words start with two consecutive vowels (a, e, i, o, u)

Hint: 65 unique words

**h_q1**: How many total references are there to the days of the week (Monday, Tuesday, etc)?

Hint: 18 (seems like stuff happens on Sunday) -- do not capture the plurals of each day.

**h_q2:** How many female pronoun references are there (she, her, hers, herself, miss, mrs, ms)?

**h_q3:** How many male pronoun references are there (he, his, him, himself, mr)?

**h_q4:** How many lines of complete dialog are there?  

A line of complete dialog is defined as text that starts on its own line and the line starts with a quote (") and the ending quote (") ends a line.  

Take a look at line 8533 for an example.  Note this does not capture all possible lines of dialog.  For example, lines 1510, 8296 and 8331 would not be captured by this definition.

The first match should be line 130: "Who dah?"

2nd match: "Say, who is you?  Whar is you?  Dog my cats ef I didn' hear sumf'n. Well, I know what I's gwyne to do:  I's gwyne to set down here and listen tell I hears it agin."

Hint.  q4 doesn't require a super long or complicated expression.  It might be easiest to test on a small sample of text.  Otherwise, it's very possible that repl.it will hang because the regular expression is taking too long (e.g. too greedy) to execute.

**10000 Words**

The following questions will use the words in `10000.txt`.  These are the 10,000 most common/popular words in all the books in Project Gutenberg:

`https://en.wiktionary.org/wiki/Wiktionary:Frequency_lists#Project_Gutenberg`

For the patterns you write here, using greedy qualifiers (e.g. * ) might work fine here since each word is on a single line.  The same regular expression may not work in a body of text.  You can use the helper function `read_10000()`.

**w_q0:**  Find all the words that start and end with an 's' AND have at least one more 's' between the two. For example **s**i**s**ter**s**

Hint: 24

 

**w_q1:** Using the '$' find all words that end in 'gry'.  Do not use word boundaries.  What happens if you do (besides not passing the tests)?

**w_q2:** How many 5 letter words are there in the 10,000 most common words?

**w_q3:** How many words have the word 'six' somewhere in them?

**w_q4:**  During your break, you are working on a crossword puzzle. You can't get the clue (TV ANTENNAS), but you have the following information:

`▢A▢B▢T`

Find the possible words? that work [that ? is a regular expression pun and not a question]

**Test and Submit.**

You should develop and test your regular expressions on small sections of text.  You will have to pass 8/10 tests.  You can use the LessonAutoGrader to help you as well (not required):

```
grader = helper.LessonAutoGrader()
```

```
grader.test_all()
```

You can tag your question with **REx2Q[hw_0-9]+** (that's a regex pun: use the specific number) on piazza.


All Rights Reserved

