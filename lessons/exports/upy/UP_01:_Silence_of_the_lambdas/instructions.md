# **University 🐍: Silence of the Lambda (and list sorting)**

**Revisiting List Sorting**

As you know, you can sort a list by using the `sorted` function:

```
animals = ["Fox", "Snake", "Octopus", "Bear"]
```

```
print(sorted(animals))
```

What if you wanted to sort the same list based on the length of the animal's name (so 'Fox' would be first)? The `sorted` function actually allows you to specify a way to do custom sorting.  You first need to build a single parameter function.  That parameter will be a single item from the list you are sorting.  The function will return a numeric value that determines it's sort value.  For the above example, the following will work:

```
`def by_length(animal):`
```

```
  `return len(animal)`
```

The function `by_length` just returns how many characters are in the parameter `animal`.  The built-in (i.e. global) `len` function does just that.  Now you pass `by_length` to `sorted` (YES, functions can be parameters) using the `key` parameter.  When you see a parameter being named (as in `key=by_length`), this parameter is called a _keyword or named argument_.

```
animals = ["Fox", "Snake", "Octopus", "Bear"]
```

```
print(sorted(animals, **key=**by_length))
```

The function `by_length` will be called for every value in the animals' list.  The `sorted` function will use the returned values to determine the order. 

Since the sort function just calls the built-in `len` function, you can just use that:

```
`print(sorted(animals, **key=**len))`
```

What if you wanted to sort based on the second letter of the animal's name?  Can you fill in the details of this function:

```
def by_second_letter(animal):
```

```
  # you can assume animal will have at least 2 letters
```

```
  # fix me
```

```
  return 0
```

And now you can use it:

```
animals = ["Fox", "Snake", "Octopus", "Bear"]
```

```
print(sorted(animals, key=by_second_letter))
```

As you may have noticed these sort functions are usually pretty short.  There's a way in Python to create/define a function using a special syntax that allows you to write the entire function in one line of code!  It's called a `**lambda**` function.  Let's see how to create one.

**Sorting Strings as Numbers**

What if you had a list of numbers as strings and you wanted to sort the list based on the numeric value?

```
values = ["1", "10", "2", "20"] 
```

```
print(sorted(values))
```

We can create a sort function to help us:

```
values = ["1", "10", "2", "20"] 
```

```
def by_value(num):
```

```
  return int(num)
```

```
print(sorted(values, key=by_value))
```

 

There is a special shorthand notation that's very useful for passing a function into another function called a lambda function

```
by_value = **lambda x: int(x)**

values = ["1", "10", "2", "20"]
print(sorted(values, key=by_value))
```

- **lambda** is a keyword
- `x` is the parameter, it can be any legal variable name
- `**:**` separates the input from the output
- `int(x)` (all the stuff to the right of the `:`) is what value is returned
- note that there is no return keyword

So everything between `lambda` and the colon is the input and everything after the colon is the output.

**Lambda Silence**

Every time you create a function it's visible to the entire module it's created in.  However, many of these functions are for "one-time" use and there's no need to keep them around after the sorting has finished.  The nice thing about lambda functions is that they can be created in-line with the function call:

```
values = ["1", "10", "2", "20"]
print(sorted(values, **key=**lambda x: int(x)))
```

In this case, since this lambda function has no name, it's referred to as an anonymous function. Anonymous functions were first studied in lambda calculus and that's why the keyword `lambda` is used.

The ability to pass in functions as a parameter to functions (as well as a function that returns another function) is a very powerful concept in languages. This means any function can be treated like a regular type in the language.  It is said that functions are _first-class objects_ in Python.  Be sure to read this paragraph again (it's important).

We will use this capability as the class continues.  This same idea (passing in a function) can also be used in the `sort` method on the list type:

```
items = ["1", "10", "2", "20"]
items.sort(key=lambda x: int(x))
print(items)
```

Since the sort method operates on its own list and doesn't create a new list, it's called an in-place sort.  The sort method does not return anything (unlike the sorted function).


**Not Just for Sorting.**

There are other functions that allow you to pass in a function as well.  For example, to find the longest string in a list of strings you can solve it like this:

```
def find_longest(a_list):
  max_i = 0
  max_len = 0
  for idx, i in enumerate(a_list):
    if len(i) > max_len:
       max_len = len(i)
       max_i = idx
  return a_list[max_i]

words = ["abba", "cadabra", "Abracadabra"]
print(find_longest(words))
```

However, the `max` function also allows you to **pass in a function** to determine what value to use:

```
`print(max(words, key=lambda x: len(x)))`
```

Which can be shortened to:

```
print(max(words, key=len))
```

All that code in `find_longest`, can be replaced with a single line of code using the idea of passing in a function as an argument.

# Lesson Assignment:

**Part 1: Lambda Sorting**

Write a function named `second_sorter` that takes in a list of strings:

```
def second_sorter(strings):
```

```
   # sort the strings alphabetically
```

```
   # based on the 2nd letter
```

```
   return strings
```

and does the following:

- returns the strings sorted by the second character.
- you must use the `sorted` function (not the `sort` method)
- you must use a `lambda` function for the sorter
- you can assume each item in strings will be at least 2 characters long

The following should work after you are done:

```
print(second_sorter(["dog", "cat", "snake"]))
```

**Part 2: Sorting Apples**

Below is a **single** string that contains the name of an apple and its 'sweetness' level:

```
apples = "McIntosh:3,Red Delicious:5,Fuji:8,Gala:6,Ambrosia:7,Honeycrisp:7.5,Granny Smith:1"
```

In this case, the apples are separated by commas, and each apple has two attributes (name and sweetness) separated by a colon.  We will use the power of the string's `split` method to sort this dataset.

**1**. Define a function named `apple_sort` that takes a single string as a parameter, this string will be an "apple".  That is it will be one of the items like 'Honeycrisp:7.5'.  The function will return a numeric value that represents the sweetness of the item.  This function will behave like a custom sorting function in the next step.

**2**. Define a function named `sort_csv`  that takes a single string as a parameter (which represents the complete dataset).  This function will split the incoming dataset into the separate items and then it will sort those items using your `apple_sort` function.  So you will pass in your `apple_sort` function as a parameter to the function that does the sorting.


So after you are done, the following code:

```
print(sort_csv(apples))
```

Should print out:

```
['Fuji:8', 'Honeycrisp:7.5', 'Ambrosia:7', 'Gala:6', 'Red Delicious:5', 'McIntosh:3', 'Granny Smith:1']
```


After you solve this problem, you should feel great!  Getting that sort to work is a testament to how much you have learned (and this example provides an insight into an actual data science task).

Test your functions well.  If you need more help, tag your question on piazza as with **UPLamda**.

**Before you go you should know:**

- what a lambda function is
- what the syntax is for creating a lambda function
- why it's useful to pass functions as arguments
- how are strings sorted by default
- the difference between sorted (the function) and sort (the method)






all rights reserved



Please read:
https://docs.python.org/3/howto/sorting.html

