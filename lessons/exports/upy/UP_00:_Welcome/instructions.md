# University 🐍: Welcome

Welcome to the next phase of your learning journey.  In this classroom we are going to discuss various features of Python.  It builds on the bootcamp as well as discusses new topics.

The knowledge you gain from many of these lessons will be required in order to work on some of the data science lessons (in another classroom).  But since many of these techniques that we will learn can be used for solving a broader range of problems they were put in this separate classroom.

**Still Locked**

The first few lessons in this classroom are locked -- meaning that you can only unlock them by passing all the tests.  Once we finish the first few lessons, the classroom will become unlocked -- meaning you can jump around if necessary.  Right now it's imperative that each assignment requires the solution to be coded for completeness (rather than just thinking "I could do that").

> **Coder's Log:**  Even after the classroom becomes unlocked, there are still lessons that build upon each other and if you get stuck on one lesson and move ahead, you will be putting yourself at risk of not being able to push your brain into a new direction.  Some lessons will give the solution to a previous lesson and if you didn't take the time to solve the problem yourself, you will not be ready when it comes time to building software for your own projects or even getting through a job interview that requires programming skills.  

There's no assignment for this lesson, just hit submit to move to the first lesson!

If you have any questions on this lesson, you can tag it on piazza with pyUP


All rights reserved
