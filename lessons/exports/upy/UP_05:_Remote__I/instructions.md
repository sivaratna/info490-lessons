# **University 🐍: Remote I/O**

**Remote Resources**

In the previous lesson we learned about getting data from a 'local' resource -- specifically a file resource.  We are going to build upon that knowledge to learn how to access resources on the Internet.  As you will see, the steps of opening a resource, reading the resource, and then closing it are still present.  However, we will be using different Python APIs (application programming interfaces) for working with different libraries to access remote resources.  Let's go over some of the terminology for accessing remote content.

**Protocols**

Almost all the resources we will access will be available via HTTP (HyperText Transfer Protocol) and HTTP**S** (Secure).  A protocol is simply a set of rules that each participant in the data exchange must adhere to for communication to happen.  These protocols are built on top of other well established 'Internet' protocols such as TCP (Transfer Control Protocol -- which helps in reliable data delivery) and IP (Internet Protocol -- which helps in routing data) that utilize a engineering technique called layering (much like the data science pipeline) -- where each protocol (or step in the pipeline) can use the protocol it's building upon (or the previous step in the pipeline).  

**Web Services**

Almost any resource that is accessible via HTTP/S has a web server listening to a specific port on a machine.  A port (80 for HTTP and 443 for HTTPS) is just a mechanism for the operating system to know how to hand off incoming network data to the correct process.  A closely related idea is that of a web _service_.  A web service uses the same technologies of a web server, however, the requestor for data to a web service is typically another computer (not a person, but a program).  You may have heard of REST (Representational State Transfer) and SOAP (Simple Object Access Protocol) that further define rules on how to access resources using HTTP (or SMTP for SOAP).  Both of these are typical protocols used by web services -- they need to tightly define the rules for locating a resource and for the encoding of data back to the client.

**Words that Connect**

For every request you make to a server there's some basic terminology that needs to be memorized.  The _**client**_ is the host that is making the _**request**_ for some resource (it's usually the browser, but in our case, it will be your Python code).  

The _**server**_ (who manages the resource) handles the _**request**_ and returns a _**response**_.  So for every connection, there is a client, a server, a request and a response.  The client and server can be the same physical machine as well.

**Request Types**

We will focus most of our attention on simple HTTP GET requests.  There are other types of HTTP requests (e.g. POST).  For now, if a resource can be accessed via the URL (in the browser window) it's a GET request (POST requests puts the data in request message or body and not in the URL).

**The URL**

Of course the most important part of a remote request will be the URL (Uniform Resource Locator).  The URL is a string that identifies a resource and how to access it.  It is made up of several components.  Knowing these components will help you navigate Python documentation that describes how to manipulate different parts of the URL and/or the connection.

**URL by Example**

Let's look at a simple URL:

`http://www.gutenberg.org/files/76/76-0.txt` and look at how it can be broken down into different components.

**The Scheme**

`**http://**www.gutenberg.org/files/76/76-0.txt`

The scheme identifies what communication protocol (HTTP or HTTPS) will be used to access the resource. It is usually HTTP or HTTPS, but can also be something like ftp as well.

**The Host:Port**  

The next item is the hostname followed by a port number.  If the port number is not specified, the default port is used.  Hostnames can be specified via a DNS names (**D**omain **N**ame **S**ervice) or IP addresses:

`http://**www.gutenberg.org**/files/76/76-0.txt`

`http://**www.gutenberg.org:80**/files/76/76-0.txt`

`http://`**173.194.219.113`:80`**

Typically, we use DNS names for identifying the server endpoint.

**The Path**

The path identifies the specific resource the client (e.g. the web browser) wants to access on the server:

`http://www.gutenberg.org**/files/76/76-0.txt**`

In this case it's a specific file, but a resource could be a script (like php) or a program that gets run on the server and whose output is the response (e.g. an SQL or database query).

**The Query String**

The query string is optional but it allows you to pass additional information to the web server to access different aspects of the resource.  For example, the following URL contains the query parameters:


`https://en.wikipedia.org/w/api.php**?action**=parse&**format**=json&**page**=Steve+Jobs&**prop**=text`

The parameters are `action,` `format,` `page,` `prop`.  In this case we are retrieving data from a web service (the output isn't meant for our visual consumption).  The query field starts with a `'?'` and each key/value pair is separated with a `'&'`

**URL encoding**

You may have noticed that the URL used to request a resource cannot contain a space.  There are other characters that are also not allowed as well.  You can see the original specification: https://tools.ietf.org/html/rfc3986#section-2 (the Request for Comments (RFC ) documents has been used by the Internet community to define new standards and share technical information).

For query values that have special characters or spaces, you will need to _encode_ them. URL encoding involves translating unprintable characters or characters with special meaning within URLs to a representation that is unambiguous and universally accepted by web browsers and servers.  In the above query to wikipedia, notice that for the `page` parameter, the space is replaced with a '+' character.  You can also use %20 to encode the space:

https://en.wikipedia.org/w/api.php?action=parse&format=json&page=Steve**%20**Jobs&prop=text

https://www.google.com/search?as_q=apollo**%20**13

Almost all browsers will encode the url for you when you type it in the address bar of the browser; however, the Python libraries will not.  You will have to encode them yourself or with the provided functions. 

**Header Fields**

You can set and read different aspects of the HTTP request and response that define details of the communication between the client and the server. HTTP headers define the operating parameters for the process of data transfer from source to destination. 

They also can provide information about the source and destination, decide the data transmission mode and encoding type. Headers also speak about the content, its type and size, and can be used to restrict the amount and variety of data allowed at both ends of communication. To ensure security, values in the header can specify the time for which data will be valid, mandates receivers to prove their legitimacy and identity, keeps unwanted readers from reading, ensures data integrity with the help of hashes, etc. Also they tell about the client, server, their application handling HTTP communication, environment they working in.

Things like cookies, cache control, authorization, etc are normally handled by setting the correct header fields in a request.  Although we won't be setting or reading the headers, you should know that capability exists.

**Status Types**

When you make a request to a web server (or web service), not only do you get a response, but you also get metadata regarding the response.  The most important is that of the _status code_ (`https://docs.python.org/3.6/library/http.html#http-status-codes`).  When the status code is not `OK` (i.e. not `200`), you should at least log the error and determine how to handle the response.  We have all seen 404 responses before (resource not found) when a resource has been moved or removed from a server.

# **Python Remote I/O**

With all that background, we can start using Python to get access to remote resources.

**urllib module**

One of the earliest Python modules for accessing remote content is the `urllib`. It's a module that is available with almost all Python distributions. If you can't update/change/add Python libraries, this is the most 'raw' way to get a remote resource. 

```
import urllib.request
```

```
def simple_remote_fetch(url):
```

```
  rd = urllib.request.urlopen(url)
```

```
  data = rd.read()
```

```
  rd.close()
```

```
  return str(data)
```

There are a few issues with that function, but for now we will focus on the main components.  Much like local I/O file handling, we **open**, **read**, and **close** the resource.  We are assuming the data can be converted to a string so we return the converted data (this is an assumption that is not always true).

Now we can test it by fetching Emma By Jane Austen from Project Gutenberg:

```
emma = "https://www.gutenberg.org/files/158/158-0.txt"
```

```
text = simple_remote_fetch(emma)
```

```
idx = text.find(r"EMMA\r\n")
```

```
print(text[idx:idx+106])
```

The `r` in front of the string "Emma" tells Python not to change the literal control characters found in the string (the `\r` `\n` are characters that display a new line).  Both of these will be explored further (and explained better) in the lesson on regular expressions.  The 106 constant was chosen so enough of the text could be displayed -- it was chosen only for demonstration purposes.

**A Better Version**

Take a look at the first tab (`v1.py`).  The function `read_remote` shows how to open a connection and read the incoming data.  It also uses the `with` statement so that any resources used are released properly -- even under error conditions.  In general, when you write local or remote I/O functions, you need to cover all the possible cases where the wrong thing happens.  The example also shows how to handle different error conditions.  Normally, you would not print an error condition but rather raise an exception with a clear message (unlike some of the messages that come from the testing framework).

When you read a remote resource over the Internet, many things can go wrong.  The status code also gives some information when the resource requested could not be located.  The Python documentation via https://docs.python.org/3/library/http.html provides a list of possible error conditions.

**Encoding Requests**

You can use the `urlencode` function to ensure your URLs and query parameters are properly encoded -- that is those characters that are not allowed can easily be handled.  The v1 module provides an `encode` function that simply wraps the urllib's function. It allows you to take a set of name/value pairs and properly encode them:

```
map = {'artist': 'Bo Diddley',
```

```
       'song'  : 'Who Do You Love?'
```

```
       }
```

```
print(v1.encode(map))
```

In general the full url is generated as follows:

```
def build_url(base_url, params):
```

```
  url = baseurl + "?" + urllib.parse.urlencode(params) 
```

```
  return url
```

A newer library, `urllb2`, provides additional functionality over `urllib` but is not available for older versions of Python. And if you are using a newer version of Python (e.g. 3.x), there are better libraries that build functionality on top of `urllib`.

**requests module**

One of the more popular ways to request remote resources is with the `requests` module.  The code in the tab `v2.py` also implements the same `read_remote` function but uses the `requests` module.  This example also shows how to check the status code of the response.

For the most part, we are going to keep the functionality of reading a resource very simple -- it will almost never do more than what we have done in the two examples.  Data processing that needs to happen and is specific to remote data (rather than local data) would be one possible exception.

**Before you go, you should know**

- what a url is
- what a query string is
- what's a network protocol
- what's the difference between https and http
- how to download a remote resource using Python

# **Lesson Assignment**

**2 Be or !**¹

**Step 1.** 

Find the text for Hamlet by William Shakespeare at Project Gutenberg EBook.  Since we will be parsing the contents, be sure the url is for the raw text version of the play.

- use the most downloaded text version (ebook #1524)
- create the function `get_hamlet_url` which returns the url (a string) for this play. 

**Step 2:**

Create the function `get_remote_text(url)` that fetches the remote data using the url passed in.  You can decide which library you want to use.  Wrap your entire function with a `try/except` construct.  If any error happens, return `None` otherwise return the result as a string.

- Note.  Hamlet is encoded in UTF-8 (unicode).  We will learn more about that soon, but for now if you decide to use the `urllib.request` library you need to decode the data using the following:

```
    data   = rd.read()
```

```
    output = data.decode('utf-8')
```

Note: If you use the `requests` library, it does this automatically for you in _most_ cases.  You can add `response.encoding` `=` `'utf-8'` in `v2.py`, if you are not passing the tests, before you return the text.

**Step 3:**

Create a function named `extract_passage` that has a single parameter (the text from step 2) and extracts the famous passage from Hamlet (i.e. to be or not to be) using the following restrictions:

- You cannot use any hardcoded numbers (e.g. 1526).  You should use the power of methods on the string type to find these indices (revisit the lesson on Strings or the Python documentation)
- Use Python's slice notation (e.g. `return passage[start:end]` )
- Extract the entire passage.  The passage starts with `**To**` and ends with a period `**.**` (just before OPHELIA speaks).
- You should remove any leading or trailing white space 

**Note:**

If you can't seem to fetch the contents of Hamlet, first try getting it via the browser with which you are using repl.it.  Make sure to test your code before running the submission tests as well.

Submit your code once it passes the tests.  You can tag any questions with **UPRemote** on Piazza.


All Rights Reserved

**Notes:**

**¹** The **`!`** sign is commonly used to represent the boolean NOT operator in many programming languages (except Python).  So it may or may not be a visual pun.
