import urllib.request
import urllib.parse

def encode(dictionary):
  return urllib.parse.urlencode(dictionary)

def read_remote(url):
  try:
    with urllib.request.urlopen(url) as response:
      #status = response.getcode()
      #should check the status == 200 
      data = response.read()
      output = data.decode('utf-8')
      return output
  except urllib.error.HTTPError as e:
    # Return code error (e.g. 404, 501, ...)
    # ...
    print('HTTPError: {}'.format(e.code))
    return None
  except urllib.error.URLError as e:
    # Not an HTTP-specific error (e.g. connection refused)
    print('URLError: {}'.format(e.reason))
    return None