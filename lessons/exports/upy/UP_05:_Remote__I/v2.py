import requests
# see http://docs.python-requests.org/en/master/

def read_remote(url):
  # assumes the url is already encoded (see urllib.parse.urlencode)
  with requests.get(url) as response:
    response.encoding = 'utf-8'
    return response.text
  return None

def read_remote_with_params(url, params): 
  response = requests.get(url, params)
  if response.status_code == requests.codes.ok: # that is 200
    response.encoding = 'utf-8'
    return response.text
  return None