# **University 🐍: Comprehensions**

One of the basic patterns in all of programming is using a for loop to create a list/sequence of items:

```
def normalize(words):
```

```
  out = []
```

```
  for w in words:
```

```
    out.append(w.lower())
```

```
  return out
```

```
  
```

```
sentence = "Remember it's a sin to kill a mockingbird"
```

```
words = sentence.split()
```

```
print(normalize(words))
```

In the above example, the function `normalize` converts a set of words into their lowercase equivalent.  The process of taking input and creating a new transposed output is very common (it's the heart of the data science pipeline). The designers of Python also recognized this and created syntax specifically for this purpose.  It's called **list comprehensions** (or just comprehensions for short) and it looks like this:

```
nw = [ w.lower() **for** w **in** words ]
```

```
print(nw)
```

Take a close look at the new syntax.  You just replaced 5 lines of code (and even more as you will see later) with only one.  The syntax for a list comprehension is as follows:

```
`[ expression **for** item **in** some_list ]`
```

- The brackets `**[]**` surround the comprehension (making it a list)
- the **`for`** and **`in`** are keywords for iteration through `some_list` (or data that can be iterated through)
- `expression` _usually_ transforms the `item` in some way

Using a comprehension you can easily create lists, for example here's the sya powers of two list:

```
p2 = [ 2**n **for** n **in** range(0,12) ]
```

```
print(p2)
```

In this case 2ᴺ (2 raised to the nth power is the `**` operator) is the expression for all values of n from 0 through 11.

**Filtering**

You can even specify a filter inside a comprehension so only values that pass the filter test are added:

```
sentence = "Remember it's a sin to kill a mockingbird"
```

```
words = sentence.split()
```

```
capw = [ w **for** w **in** words **if** w.lower() != w ]
```

```
print(capw)
```

The syntax for this extension is as follows:

```
[ expression **for** item **in** list **if** conditional ]
```

Here's the same power of two table but only for even numbers

```
bytes = [ (n, 2**n) **for** n **in** range(0,20) if n%2==0]
```

```
print(bytes)
```

In this example, you are now creating a list of tuples by using the tuple notation (e.g. `(n, 2**n)`).  Also, those numbers may look familiar to you -- it's the 'math' behind the sizes of computer memory.  

> **Coder's Log**:  as you may know the smallest unit of computer storage is a bit which can hold only 2 values (0 or 1).  A bit's holding capacity is 2¹.  Similarly 2 bits (2 ²) can hold 4 values (00, 01, 10, 11).  A byte is 8 bits (2⁸) which can hold 256 different values.

**Dictionary Comprehensions**

You can also create dictionaries using the same compact syntax (similar to list comprehensions):

```
sentence = "Remember it's a sin to kill a mockingbird"
```

```
words = sentence.split()
```

```
word2len = **{**w:len(w) **for** w **in** words**}**
```

```
print(word2len)
```

Note that you need to **use the curly braces `{}` instead of the brackets `[]`**. Also each item added is a `key:value` pair (e.g. `w:len(w)`)

**Set Comprehension**

Almost the same syntax as the dictionary comprehension is used to create a set of elements:

```
s1 = {x%2 **for** x **in** range(0,100)}
```

```
print(s1)
```

Here the format is `**{**expression **for** each_item **in** data**}**`

Note that the only difference is there's no `key:value` pair.

**Tuple Comprehensions**

There is no way to create a tuple comprehension that's similar to a list comprehension (since tuples are immutable) but the tuple 'function' can receive the same comprehension syntax used to create a sequence of values as a tuple:

```
tl = tuple( 2**n **for** n **in** (1, 2, 3) )
```

```
print(tl)
```

Note that you can leave off the brackets `[]`.

**Multiple Dimensions** 

You have only been working in a single dimension for lists; however, for two dimensions it's just a list of lists:

```
r = [[1, 1, 1], [2, 4, 8], [3, 9, 27], [4, 16, 64]]
```

And you can use the same bracket notation to access a specific item:

```
r2c3 = r[1][2]
```

Sometimes it's easier to visualize a 2 dimensional list as rows and columns:

```
r = [
```

```
[1, 1, 1],  # row 1 (index 0)
```

```
[2, 4, 8],  # row 2
```

```
[3, 9, 27], # row 3
```

```
[4, 16, 64] # row 4
```

```
]
```

You can also use nested comprehensions to create these nested structures:

```
r = [ [x, x*x, x*x*x] **for** x **in** range(1,5) ]
```

```
print(r)
```

```
print(r[2][2])
```

This is very similar to the bytes example that created tuple items inside a list.  However, you can add nested looping for more complex cases:

```
matrix = [[i+j **for** i **in** range(5)] 
```

```
                 **for** j **in** range(5)] 
```

```
print(matrix)
```

You can also combine values from different 'loops' within a comprehension.  For example, if you were tasked to generate all possible combinations of the numbers 1,2,3 with the letters 'a', 'b', 'c' you can build it the standard/traditional way:

```
pairs = []
```

```
for x in [1,2,3]:
```

```
  for y in ['a','b','c']:
```

```
    pairs.append( (x, y) )
```

```
print(pairs)
```

Or you can use nested comprehensions:

```
pairs = [(x, y) **for** x **in** [1,2,3] **for** y **in** ['a','b','c'] ]
```

```
print(pairs)
```

Since Python allows you to iterate over a string to get the individual characters, you can also write the above as follows:

```
pairs = [(x, y) **for** x **in** [1,2,3] **for** y **in** "abc" ]
```

```
print(pairs)
```

**If/Else**

In addition to filtering, you can even specify what to build by including an `if/else` statement in the comprehension:

```
p1 = [ 0 **if** w.upper() == w **else** 1 **for** w **in** "AeIoU"]
```

```
print(p1)
```

And the same using a set comprehension:

```
sentence = "Remember it's a sin to kill a mockingbird"
```

```
words = sentence.split()
```

```
p2 = { w.upper() if w.find('l') >= 0 else w.lower() **for** w **in** words }
```

```
print(p2)
```

**Not all one line.**

Usually the convention is to put the list comprehension on one line.  However, there's no technical reason to do so.  Sometimes it's much easier to read if you organize it across several lines (same example as above but now using dictionary comprehension notation)

```
p2 = {w.upper() if w.find('l') >= 0 
```

```
                else w.lower() 
```

```
      :w.count('l') 
```

```
                for w in words}
```

```
print(p2)
```

**Before you go, you should know**

- what a comprehension is in Python
- what the syntax is for creating a list comprehension
- what the syntax is for creating a set comprehension
- why one chooses the comprehension syntax over standard looping pattern

# **Lesson Assignment**

**Write and Run Your Own Tests First.**

It's important for you to start writing code that tests your own solutions before relying on the testing framework.  For some assignments you will not use repl.it and you won't have the luxury of knowing if your code is working properly or not. 

Before submitting your code, **you must hit 'run' first** so the correct testing framework is installed (each time you have a new session).  This will only be a problem if you don't write testing code yourself and just hit 'run tests'. 

You should place your **testing code** in `main.py` so it doesn't interfere with the actual tests.  

Other than `odd_squared_v1` `and` `is_palindrome`, you must use a comprehension to solve each problem. 

**1**. write a function named `odd_squared_v1` (no parameters) that builds a list of the odd numbers from 1 to 100 (inclusive) squared.  Do NOT use comprehensions.

**2.** write a function named `odd_squared_v2` that builds the same list but uses a list comprehension.

**3.** write a function named `is_palindrome` which returns `True` if the parameter (a string) is a palindrome or `False` if it is not.  A palindrome is a string/word that reads the same forward as backwards (e.g. noon, mom, racecar).  A word is a palindrome even if the case of the letters is different (e.g Civic is still a palindrome). Note this is a helper function which you will use soon (it can also be solved without using comprehensions).

As a note, algorithms for determining if a word is a palindrome are all over the internet.  However, you should solve this using only the syntax you have seen (and most likely using string and list methods https://docs.python.org/3.6/tutorial/datastructures.html#more-on-lists).   It will serve your brain well to solve this yourself.

**4**. write a function named `build_palindrome_map` which takes a sentence and returns a dictionary of the words as keys and a boolean for their value (indicating if the word is a palindrome or not).  

- You should use your `is_palindrome` function from part 3.
- You can assume the sentence does not have any punctuation and words are separated by only whitespace
- The keys in the dictionary should be the unmodified words from the sentence
- Use a comprehension to build the dictionary

After you are done, the following should work:

```
print(build_palindrome_map("Anna rode the Kayak at Noon"))
```

Which should print:

`{'Anna': True, 'rode': False,` 

`'the': False, 'Kayak': True,` 

`'at': False, 'Noon': True}`
   

# **🐍 for Thought:**

The phrase: **Never Odd Or Even** is also a palindrome.  How would you use your function (in part 3) to test a phrase? This not required for passing tests.

**5**. write a function called `build_all_dice_pairs` that generates tuples that represent all permutations of two dice throws.  You must use a single comprehension to solve this.  See if you can get all the pairs in the list to have the lower dice value first (but not required to pass the tests).

There should be 36 values:

```
[(1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 2), (2, 2), (2, 3), (2, 4), (2, 5), (2, 6), (1, 3), (2, 3), (3, 3), (3, 4), (3, 5), (3, 6), (1, 4), (2, 4), (3, 4), (4, 4), (4, 5), (4, 6), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5), (5, 6), (1, 6), (2, 6), (3, 6), (4, 6), (5, 6), (6, 6)]
```

**6. Extra Credit**

**Part 1:** Write a function called `build_dice_pairs_v1` that generates tuples that represents all combinations (not permutations) of two dice throws (using set notation).  Put the lower value of the two dice first.

There should be 21 values (order is not important):

```
{(1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (2, 2), (2, 3), (2, 4), (2, 5), (2, 6), (3, 3), (3, 4), (3, 5), (3, 6), (4, 4), (4, 5), (4, 6), (5, 5), (5, 6), (6, 6)}
```

**Part 2:** write a function called `build_dice_pairs_v2` that does the same as `build_dice_pairs` but uses list notation:

You should get the following 21 values (in this order):

```
[(1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (2, 2), (2, 3), (2, 4), (2, 5), (2, 6), (3, 3), (3, 4), (3, 5), (3, 6), (4, 4), (4, 5), (4, 6), (5, 5), (5, 6), (6, 6)]
```

For Both Parts:

- You must ONLY use only 1 comprehension
- You must solve this using at most 2 Python statements (not including the function definition).  This includes the return statement.

You can skip this test by putting the following at the top of `lesson.py`

```
skip_part6 = True
```

**Hints:**

https://docs.python.org/3/tutorial/datastructures.html may help to refresh, but you have seen everything you need to solve these problems.

If you have any questions on this lesson, you can tag it on piazza with **UPComp**


All rights reserved

=======================

Some fun palindromes:

- Anna 
- Civic 
- Kayak 
- Level 
- Madam 
- Mom 
- Noon 
- Racecar 
- Radar 
- Redder 
- Refer 
- Repaper 
- Rotator 
- Rotor 
- Sagas 
- Solos 
- Stats 
- Tenet 
- Wow
