# **University🐍:  Slicing and Dicing**

This lesson requires a bit of patience and careful reading. By the end of the lesson you may even state:

```
print("hgcvPSyItchgognQ zihsh XACwKeNssoxmKeL"[4::2])
```

**List and String Slicing**

You have seen that if you have a list in Python, you can easily access any element in that list by using the bracket notation (e.g.  `my_list[5]` would access the _sixth_ element). However, one of the most powerful features of Python lists is the ability to use an **extended bracket notation** to access **subsets of elements**.

For text processing, this means you can easily strip off preface, forward, introductions and other front matter, as well as remove any unnecessary content after the body text. For numeric processing, you can readily process different subsets of the values easily.

The notation to access subsets of elements can get complicated quickly, so let's approach this feature with many examples and start out slowly.

**Simple Single Element Access**

As we have seen, lists are accessed using `[]` (brackets).  Index 0 is the first element, index (`len(list) - 1`) is the last element:

```
word = "Apples are Red"
```

```
# first item in the list
print(word[0])
```

```
   
```

```
# last item in a list           
print(word[len(word)-1])  
```

**Substrings and Sublists**

This same notation can be extended to include the starting offset and the ending offset.  Python extends this bracket notation using a '`:`' between the two offsets.  The first number is the start; the second is the end.

So the format is `[start_index **:** end_index]`

Note that the ending index is _exclusive_ (it does not include it) and offsets are always integer values.  This is how we can easily get subsets or subsequences:

```
word = "Apples are Red"
```

```
#       01234567891111
```

```
#                 0123
```

```
#start at index 0 and stop at index 3 
```

```
print(word[0:3])
```

```
#11 through 13 (stop at 14)
```

```
print(word[11:14])
```

Take note of how the comments show the indices of the string.

> **Career Log:**  As another reminder, you must run each of these examples. It will be far better for your learning to actually type in each example (not copy&paste) and try to guess the output before you run the code.  Just reading the examples will make all the assignments extremely difficult.

**Default Indices**

You can also **not** specify the indices (they have defaults) by not using any number:

```
word = "Apples are Red"
```

```
print(word[:6]) # what's the default?
```

```
print(word[2:]) # leaving off the ending number
print(word[:])  # what are the defaults?
```

Be sure you know the defaults.  It's important to easily recognize what's happening when you see something like `word[:4]` and know what that means.

**Stepping and Skipping**
You can even specify a step (also called a skip or stride) number for a third parameter.

```
word = "Apples are Red"
```

```
#       01234567890123
```

```
#                 1111 
print(word[1::2])
print(word[6::3])
```

See if you can figure out what gets printed before running the following:

```
word = "Apples are Red"
```

```
print(word[3:11:3]) 
```

Note the indices (`(3),4,5,(6),7,8,(9)`) are used to pull out the subset of characters.

For the last one we start at index 3 (letter l), stop at 11 (R), take every 3rd letter

```
word = "App**l**es ar**e** Red"
```

```
#       012**3**45**6**78**9**0123
```

```
#                 1111 
```

Remember the end index is exclusive (it does not include it):

```
word = "Apples"
```

```
#       012345
print(word[0:5:1])
```

The general formula for the index slicing is

`[start_index **:** end_index : step ]`

Using this syntax (with the `**:**`) can protect you from going out of bounds:

```
word = "Apples"
```

```
# going beyond the end
print(word[0:15])
```

```
# however, this WILL not work
```

```
print(word[15])
```

**Going Negative**

The _CRAZY_ thing is that you can specify a negative number for each of the parameters.  However, some combinations will not make sense.  To understand what's going on, it's easier to label the indices.  For the string 'Orange' the following will be used to demonstrate:

```
['O', 'r', 'a', 'n', 'g', 'e']  letters
```

```
[ 0    1    2    3    4    5 ]  indices 
```

```
[-6   -5   -4   -3   -2   -1 ]  in reverse
```

Be sure to note that the negative indices start at -1 (at the end of the list) and not 0.

Take a look at the following examples.  Try to figure out what will be printed, before running each example.

```
word = list("Orange")  
```

```
# just to clarify
```

```
#['O', 'r', 'a', 'n', 'g', 'e']  letters
```

```
#[ 0    1    2    3    4    5 ]  indices 
```

```
#[-6   -5   -4   -3   -2   -1 ]  in reverse
```

```
print(word[-1])
```

As mentioned the -1 is the last character (or you can think of it as going back 1 from end).  So the letter 'e' is printed.  

Do each of the following and try to figure out what gets printed:

```
print(word[-5]) 
```

```
# go back 5 from the end, take 'r' letter
```

```
print(word[-5:-2])
```

```
# start at 'r' end before 'g'
```

```
print(word[-4:-3])
```

```
# a single letter: start at 'a', end before 'n'
```

```
print(word[-5:-1])
```

```
# start at 'r' end before the 'e'
```

```
print(word[-5:])
```

```
# start at 'r' end include the final letter
```

**Mixing Numbers**

Although this is frowned upon as being obtuse, sometimes you see "programmers" use both positive and negative indices.  You can always rewrite them so there's no reason for the confusion.  However, you can wade yourself through the trickery by drawing an index chart and just find the indices.

```
word = list("Orange")  
```

```
#['O', 'r', 'a', 'n', 'g', 'e']  letters
```

```
#[ 0    1    2    3    4    5 ]  indices 
```

```
#[-6   -5   -4   -3   -2   -1 ]  in reverse
```

```
# start at 'r' end before 'n'
```

```
print(word[-5:3])
```

```
# the following are essentially the same
```

```
# start at 'r' end before 'g'
```

```
print(word[-5:4])
```

```
# start at 'r' end before 'g'
```

```
print(word[1:-2]) 
```

```
# better solution
```

```
print(word[1:4]) 
```

**Stepping Negative**

Just when you thought it couldn't get more confusing.  You can even specify a negative step number.   It's easiest to think of this as the direction in which you are advancing. 

• a positive step means move from the start index to the end index--> Left to Right

• a negative step means move from end index to the start index --> Right to Left

```
word = list("Orange")  
```

```
#['O', 'r', 'a', 'n', 'g', 'e']  letters
```

```
#[ 0    1    2    3    4    5 ]  indices 
```

```
#[-6   -5   -4   -3   -2   -1 ]  in reverse
```

```
# step is positive, move left to right
```

```
print(word[1:4:2])
```

```
print(word[-5:-2:2]) 
```

```
# step is negative, move right to left
```

```
print(word[-3:-6:-2]) 
```

**Reversing a String**

You can even use negative slicing to reverse a string.  

```
word = list("Orange") 
```

```
#['O', 'r', 'a', 'n', 'g', 'e']  letters
```

```
#[ 0    1    2    3    4    5 ]  indices 
```

```
#[-6   -5   -4   -3   -2   -1 ]  in reverse
```

```
# standard way to reverse a string
```

```
print(word[::-1])
```

```
# same as above, but very confusing
```

```
print(word[-1:-7:-1]) 
```

   

**Behind the Scenes**

There's a little secret behind this slicing functionality.  Python has a function named `slice` which creates an object (a data type) that knows how to iterate through a list.  You can think of it as an object that is responsible for generating indices.  

Here's the function signature.  

```
slice(start, **stop**, step)
```

```
slice(start, **stop**) # step defaults to 1
```

```
slice(**stop**) # start defaults to 0; step to 1
```

Notice that `stop` parameter is required. If you don't pass in any values for the other parameters (or explicitly pass in the value as None, they use the following default values.

SO, the following should work the same as above:

```
word = list("Orange")  
```

```
#['O', 'r', 'a', 'n', 'g', 'e']  letters
```

```
#[ 0    1    2    3    4    5 ]  indices 
```

```
#[-6   -5   -4   -3   -2   -1 ]  in reverse
```

```
# figure out what is printed first
```

```
s1 = slice(-5,-2,2)
```

```
print(word[s1])
```

```
 
```

```
# are they the same ?
```

```
print(word[s1] == word[-5:-2:2]) 
```

```
s2 = slice(-3,-6,-2)
```

```
print(word[s2]) 
```

You can even use slice to reverse the string.  Notice that for the `slice` function, you must pass in `**None**` for any missing parameters:

```
word = list("Orange")
```

```
rev = word[::-1] 
```

```
# defaults for negative strides
```

```
s4 = slice(None, None, -1)
```

```
# defaults for positive strides
```

```
s5 = slice(0, len(word), 1) 
```

```
print(rev, rev == word[s4], word == word[s5])
```

**Summary.**

There's a lot to learn here.  It will just take practice to get comfortable using it.  However, this ability to slice up the data is extremely important.  You don't _need_ to use slicing -- as you could do it with loops and selection statements.  But it is certainly syntactically efficient.  

Here's another way to summarize the slicing capabilities:

```
**sliceable[start:stop:step]**
```

```
**start:** 
```

```
the beginning index of the slice, it will include the element at this index unless it is the same as stop, defaults to 0, i.e. the first index. If it's negative, it means to start n items from the end.
```

```
**stop:** 
```

```
the ending index of the slice, it does not include the element at this index, defaults to length of the sequence being sliced, that is, up to and including the end.
```

```
**step:** 
```

```
the amount by which the index increases, defaults to 1. If it's negative, you're slicing over the iterable in reverse.
```

```
**examples:** (assuming a is a sliceable list)
```

```
# items start through end-1
```

```
a[start:end]
```

```
# items start through the rest of the array 
```

```
a[start:] 
```

```
# items from the beginning through end-1   
```

```
a[:end]
```

```
# a copy of the whole array
```

```
a[:]         
```

```
a[::-1]    # all items in the array, reversed
```

```
a[1::-1]   # the first two items, reversed
```

```
a[:-3:-1]  # the last two items, reversed
```

```
a[-3::-1]  # everything except the last two items, reversed
```

It's important to remember the last element is NOT included:

`[` **<first element to include>** : **<first element to exclude>** : **<step>** ]

**Before you go, you should know:**

- the extended slice format
- the default values when you leave off an index

# **Lesson Assignment**

There are 13 questions regarding slicing.  The first 10 are required to pass.  The last 3 are extra credit.

You will place all your answers in the `lesson.py` tab.  Select that tab and take a look at how it's done.

For your answers, each function (already defined) returns a tuple:

- the first item is the answer using index notation (e.g. `text[16:20]`)
- the second item is the answer using slice notation (e.g. `slice(16,20,None)`)
- the numbers you use for the `slice` function should be the same (you can use `None` for any missing indices to the `slice` function) so don't overthink it!
- when you solve each question, **do NOT use the slice object**.  It's only used for the testing framework for verification. 

You can test your code before submitting by running the following (not required, but helpful for debugging) code:

```
import slicetest
```

```
# run the tests over questions in lesson.py
```

```
print("total correct", slicetest.do_tests())
```

**Question 1 (already finished)**

`Extract: "Data"`

`From:    "Introduction to Data Science"`

```
def question1(text):
```

```
  return text[16:20], (16,20)
```

This is how you can experiment and test it

```
values = question1(`"Introduction to Data Science"`)
```

```
print (values[0] == 'Data')
```

**Notes and hints:**

- 'extract' means return the substring that is to be 'extracted' (i.e. removed) from the 'From' text.
- if you want to debug your answers, just do the following (in `main.py`)

```
import lesson as ans
```

```
result = ans.question1("Introduction to Data Science")
```

```
print(result[0] == 'Data') # looking for True
```

 

- you can also test each part of your answer:

```
expect = "Data"
```

```
word   = "Introduction to Data Science"
```

```
s1 = slice(16,20,None)
```

```
value = word[s1]
```

```
if (value != expect):
```

```
   print("Bad Slice", expect, value)
```

```
value = word[16:20]
```

```
if (value != expect):
```

```
   print("Bad Index", expect, value)
```

- In each of your answers, you should ONLY use the `slice` function as part of the **final return** and you must use the parameter **text** in the return statement.

```
def some_answer(text):    
```

```
  idx = some_calculation()    
```

```
  return (**text**[idx:20], **slice**(idx,20,1))
```

**Bonus:** ideally, you should solve each of these without any hardcoded numbers other than the step amount:

`Extract: "Data"`

`From:    "Introduction to Data Science"`

```
def question1(text):
```

```
  word = 'Data'
```

```
  wl = len(word)
```

```
  idx = text.find(word)
```

```
  return (text[idx:idx+wl], slice(idx, idx+wl, 1))
```

**Question 2:**

`Extract: "ar"`

`From:    "Orange"`

```
def question2(text):
```

```
  return (?,?)
```

**Question 3:**

`Extract: "Oa"`

`From:    "Orange"`

```
def question3(text):
```

```
  return (?,?)
```

**Question 4:**

`Extract: "ence"`

`From:    "Introduction to Data Science"`

```
def question4(text):
```

```
  return (?,?)
```

**Question 5:**

`Extract: "ecne"`

`From:    "Introduction to Data Science"`

```
def question5(text):
```

```
  return (?,?)
```

**Questions 6 through 10** involve manipulating text from the following horror book:

```
My Book, by mike
**Chapter 1**: set up
The little boy in bed was scared.
**Chapter 2**: suspense
"Daddy, daddy there's a monster under my bed."
**Chapter 3**: climax
The father then looked under the bed .. and saw his son.
**Chapter 4**: resolution
"Daddy, daddy, someone's in my bed!"
(the end)
About the author: a first time novelist
```

Notes and Hints:

- the **contents** of the book is everything from 'Chapter 1' until but not including '(the end)'.
- if you find yourself counting the characters, you need to rethink your solution. Use Python to get the indices, not your fingers (see documentation on `find`)
- test each part of your answer (see previous hints)
- you should NOT need to add text to any of these.  If you find yourself wanting to add periods, newlines, etc, you are on the wrong path.
- '**remove**' means return the book but without the part requested to be removed.
- '**extract**' means return the part of the book that is to be 'extracted' (i.e. removed)
- The text is also in the variable `book` (found in the module `slicetest`) if you want to work with the book directly:

```
print(slicetest.book)
```

**Question 6:**

`Remove:  remove the front matter of the book`

`From:    the text of the book`

`the front matter is the stuff before the contents`

```
def question6(text):
```

```
  return (?,?)
```

**Question 7:**

`Remove:  remove the back matter of the book`

`From:    the text of the book`

`the back matter is the stuff after the contents`

```
def question7(text):
```

```
  return (?,?)
```

**Question 8:**

`Extract: the front matter of the book`

`From:    the text of the book`

```
def question8(text):
```

```
  return (?,?)
```

**Question 9:**

`Extract: the back matter of the book (About the author)`

`From:    the text of the book`

`Note:    do NOT include (the end)\n`

```
def question9(text):
```

```
  return (?,?)
```

**Question 10:**

`Extract: the contents of the book`

`From:    the text of the book`

```
def question10(text):
```

```
  return (?,?)
```

**Extra Credit:**

The last 3 questions want you to extract a hidden message from a gibberish of text.

If you want to skip this part put the following line of code in `lesson.py`:

```
skip_extra = True
```

**Question 11:**

`Extract: I Love Python`

`From:    slicetest.mystery11 variable`

```
def question11(text):
```

```
  return (?,?)
```

**Question 12:**

`Extract: Data Science Rocks`

`From:    slicetest.mystery12 variable`

```
def question12(text):
```

```
  return (?,?)
```

**Question 13:**

`Extract: Info 490 is fun`

`From:    slicetest.mystery13 variable`

```
def question13(text):
```

```
  return (?,?)
```

Once all your slices are working hit submit.

You can tag any questions on piazza with **pySliceAndDiceQ<number>**


All rights reserved
