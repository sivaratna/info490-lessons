import lesson as ans

#
# Do NOT change anything in here
# it's a testing framework that you 
# can use to test your code
#

book = "My Book, by mike\n\
Chapter 1: set up\n\
The little boy in bed was scared.\n\
Chapter 2: suspense\n\
\"Daddy, daddy there's a monster under my bed.\"\n\
Chapter 3: climax\n\
The father then looked under the bed .. and saw his son.\n\
Chapter 4: resolution\n\
\"Daddy, daddy, someone's in my bed!\"\n(the end)\n\
About the author: a first time novelist\n"


# find the mystery messages in each of these strings
# using slices
mystery11 = "srbGIE JLWokvQeR DPhyItWhYolnz"
mystery12 = "AgDoSaMqtigaDF FmSAMcgZizueTSnckcYOeUM hzRVVoapceukbasUk"
mystery13 = "PDWQHwnHquOmfVA lTsxJise YV0Vt9wH4Ct qKouPfQgnQLIRvj"



def test_slice(n, text, expected, answer=None, slice_obj=None):
  
  # e.g answer = word[9:12:2]
  # slice_obj = slice(9,12,2)
  
  print("test {}: ".format(n), end='')
  
  if (answer == None or slice_obj == None):
    print("Missing Answer")
    return False
    
  if (expected != answer):
    print("Incorrect index notation\n", expected + "\n<vs>\n"+ answer)
    return False
    
  try:
    check = text[slice_obj]
  except:
    print("Incorrect: use slice() function")
    return False
    
  if check != expected:
    print("Incorrect slice\n", expected + "\n<vs>\n" + check)
    return False

  print("pass")
  return True


def do_tests():
  
  forward = "My Book, by mike\n"
  contents = (""
  "Chapter 1: set up\n"
  "The little boy in bed was scared.\n"
  "Chapter 2: suspense\n"
  "\"Daddy, daddy there's a monster under my bed.\"\n"
  "Chapter 3: climax\n"
  "The father then looked under the bed .. and saw his son.\n"
  "Chapter 4: resolution\n"
  "\"Daddy, daddy, someone's in my bed!\"\n")
  
  post = "(the end)\nAbout the author: a first time novelist\n"
  
  b2      = "{}{}{}".format(forward, contents, post)
  noFront = "{}{}".format(contents, post)
  noEnd   = "{}{}".format(forward, contents)
  
  assert b2 == book, "Book has changed"
  
  # testing framework optional
  tests = [
    ("Introduction to Data Science", "Data", ans.question1),
    ("Orange", "ar", ans.question2),
    ("Orange", "Oa", ans.question3),
    ("Introduction to Data Science", "ence", ans.question4),
    ("Introduction to Data Science", "ecne", ans.question5),
    
    (book, noFront, ans.question6),
    (book, noEnd,   ans.question7),
    (book, forward, ans.question8),
    # just About the Author ... 
    (book, post[10:], ans.question9),
    (book, contents, ans.question10),
    
    (mystery11, "I Love Python", ans.question11),
    
    (mystery12, "Data Science Rocks", ans.question12),
    
    (mystery13, "Info 490 is fun", ans.question13)
  ]

  correct = 0
  for i, t in enumerate(tests):
    txt      = t[0]
    expect   = t[1]
    question = t[2]
    answer = question(txt)
    result = test_slice(i+1, txt, expect, *answer)
    if (result):
      correct +=1
  return correct