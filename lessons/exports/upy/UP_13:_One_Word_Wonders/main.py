# main.py

import re
def is_number(v):
  try:
    float(v)
    return True
  except ValueError:
    return False
    
def is_float(v):
  return len(re.findall(r'[+-]?[0-9]\.[0-9]+', v)) == 1
  
def sum_plus(values, val):
   counts = [0,0,0]
   sum_count = 0
   for v in values:
      if is_number(v):
         counts[0] += 1
      if is_float(v):
         counts[1] += 1
         v = float(v)
         if v <= val:
           # main logic 
           print(v)
           counts[2] += 1
           sum_count += v
      
   return (counts, sum_count)

items = "1,,2.0,a,+0.75, 0.25".split(',')
print(sum_plus(items, 1.5))