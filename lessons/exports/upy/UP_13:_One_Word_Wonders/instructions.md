# **University 🐍: One Word Wonders**

 

_(billboards top 100 one-word songs)_

It's possible that in your own Python exploration, you've come across some syntax that we have not covered yet (e.g. `class`(es)).  There's a few keywords that provide syntactical convenience (they are not necessary) that could be helpful in your future world with Python. This lesson covers the big three: `pass`, `continue`, `break`.  

The first one, `pass`, is used in situations where you need to be syntactically correct but aren't sure what to do.  The latter two, `continue` and `break` help you control looping.  Let's go over these three one word wonders.

# I'll `Pass`

The keyword `pass`, is used in places where Python wants a statement, but the programmer doesn't know what to do yet.  For example, the following is a way to define a function so it is valid Python (the Python parser will be happy) but the implementation is needed.  This can also be used as a placeholder for future code. It can be used it to 'stub' out functionality:

```
def sum_plus(items):
```

```
   pass
```

Sometimes it helps in the following situation.  Imagine writing the following code that counts the number of even numbers in a list:

```
def count_even(numbers):
```

```
  count = 0
```

```
  for n in numbers:
```

```
     if n%2 == 0:
```

```
        count += 1
```

```
     else:
```

```
        count += 1
```

```
        print("not even")
```

```
print(count_even([1,2,3]))
```

You then decide, things are working and want to remove the code under the `else` statement:

```
def count_even(numbers):
```

```
  count = 0
```

```
  for n in numbers:
```

```
     if n%2 == 0:
```

```
        count += 1
```

```
     else:
```

```
        #count += 1
```

```
        #print("not even")
```

```
print(count_even([1,2,3]))
```

What happens when you run it?  Python needs a statement after each `if/else` block.  The keyword `pass` can be added as a way to temporary have valid Python and still keep the structure of your code:

```
def count_even(numbers):
```

```
  count = 0
```

```
  for n in numbers:
```

```
     if n%2 == 0:
```

```
        count += 1
```

```
     else:
```

```
        pass
```

```
        #count += 1
```

```
        #print("not even")
```

Of course, you could argue, to just remove the entire `else` block, but sometimes it's a lot of code that you want to comment out to test something  and eventually you will put everything back.

The `pass` keyword, is sometimes referred to a null block or a no-op (i.e. no operation).

# To `break`

The `break` and `continue` keywords allow you to control the flow within a loop construct (e.g. `for`, `while`).  Let's take a look at a situation that could use a `break`.  

The following code returns the first even number in a list (or `None`):

```
def first_even(numbers):
```

```
  for n in numbers:
```

```
     if n%2 == 0:
```

```
        return n
```

```
  return None
```

Notice this code fails the 'single exit point rule'.  Although since the code logic is so small, it's not that big of a deal.  But in a much longer or complicated function, managing several exit points can be hard to manage.

We could solve the 'single exit point rule' with the following code:

```
def first_even(numbers):
```

```
  
```

```
  first_even = None
```

```
  for n in numbers:
```

```
     if n%2 == 0 and first_even is None:
```

```
        first_even = n
```

```
  
```

```
  return first_even
```

```
print(first_even([1,2,3,4]))
```

This works.  But now the issue is that it's not very efficient.  If someone passes in a list of one million numbers, you will loop through the entire list even if the first even is in slot 0.

The `break` keyword comes to the rescue here:

```
def first_even(numbers):
```

```
  first_even = None
```

```
  for n in numbers:
```

```
     if n%2 == 0:
```

```
        first_even = n
```

```
        **break**
```

```
  return first_even
```

```
print(first_even([1,2,3,4]))
```

The Python **`break`** statement immediately **terminates** a loop entirely. Program execution proceeds to the first statement following the loop body.

As soon as an even number is found, execution will _break_ out of the loop.

Here's another example that uses a `break` statement to get out of a situation where you are waiting for valid input, before moving to the next step:

```
# loop until we get valid input
```

```
value = None
```

```
while True:
```

```
   value = input()  # get user input
```

```
   if value == 'Exit':
```

```
      **break**
```

```
   if value < 10:
```

```
      **break**
```

```
   if value > 20:
```

```
      **break**
```

```
# at this point we have valid input
```

# Or to `continue`

The `continue` keyword is similar to `break`.  But it allows you to immediately **jump to the next iteration** of the loop.  It's a bit harder to come up with a small example to show it's usefulness, but let's try.  

Imagine writing a function (well no need to imagine), that analyzes a set of numbers in a list.  The function `sum_plus` counts the values that are numbers, floats, and values less than 1.5 (or whatever number is passed in).  Be sure you understand what is happening in this function first:

```
def sum_plus(values, val):
```

```
   counts = [0,0,0]
```

```
   sum_count = 0
```

```
   for v in values:
```

```
      if lesson.is_number(v):
```

```
         counts[0] += 1
```

```
      if lesson.is_float(v):
```

```
         counts[1] += 1
```

```
         v = float(v)
```

```
         if v <= val:
```

```
           # main logic 
```

```
           **print(v)**
```

```
           **counts[2] += 1**
```

```
           **sum_count += v**
```

```
      
```

```
   return (counts, sum_count)
```

You can test it like this:

```
items = "1,,2.0,a,+0.75, 0.25".split(',')
```

```
print(sum_plus(items, 1.5))
```

The point to highlight in this code is that the main logic is deep within nested `if` statements.

Sometimes it's easier to write the logic that states, if some condition is not met, then just go to the next iteration of the loop.  That's where we use the keyword `continue`.

The Python **`continue`** statement immediately terminates the current loop iteration. Execution jumps to the top of the loop, and the controlling expression is re-evaluated to determine whether the loop will execute again or terminate.  

The same function is now written using the `continue` keyword:

```
def sum_plus2(values, val):
```

```
  counts = [0,0,0]
```

```
  sum_count = 0
```

```
  for v in values:
```

```
    
```

```
    if not lesson.is_number(v):
```

```
      **continue**
```

```
    
```

```
    counts[0] += 1
```

```
    if not lesson.is_float(v):
```

```
      **continue**
```

```
    
```

```
    counts[1] += 1
```

```
    v = float(v)
```

```
    if v > val:
```

```
      **continue**
```

```
          
```

```
    # all preconditions are now met
```

```
    **counts[2] += 1**
```

```
    **print(v)**
```

```
    **sum_count += v**
```

```
      
```

```
  return (counts, sum_count)
```

Although the code is a bit longer (but that isn't always the case), it reads much easier by putting all the main logic at the end where all the preconditions are met.

You can test this code with the following:

```
items = "1,,2.0,a,+0.75, 0.25".split(',')
```

```
print(sum_plus2(items, 1.5))
```

Here's another quick example of using `continue`:

```
for x in range(0, 23):
```

```
    if x%3 == 0 or x%5 == 0:
```

```
        **continue**
```

```
        # no more code is executed, 
```

```
        # we go to the next number
```

```
        print("never more")
```

```
    print(x)
```

The distinction between `break` and `continue` is demonstrated in the following diagram:

 

**Before you go, you should know:**

- how to use the keywords `pass`, `break`, `continue`

# **Lesson Assignment**

Nothing to write.  

But try to keep these in mind for the future.  Just hit submit.

You can tag any question with **PyOne** with any questions.

References:

- realpython.com (graphic credit)

04.28.20

All Rights Reserved

# **End of Semester, Bonus Material:** 

The following are also Python specific syntax to help 'shorten' code.  

**Interval Comparison**

The standard way in Python (and all other languages) for comparing a number against two endpoints (called an interval comparison) is as follows:

```
`if number >= 10000 and number >= 30000:`
```

```
   `pass`
```

Python also has special syntax to use that can shorten the statement:

```
`if 10000 <= number <= 30000:
    pass`
```

Note that the variable (number) doesn't need to be repeated.  However, I would stay away from this syntax.  No other language supports this (so people coming from other languages will find this difficult to interpret).

Python also has something called chained comparisons that build on this syntax.  I would avoid it unless you can clearly state a good reason to use it and the resulting code is completely understandable.

**InLine if/else**

Python supports a syntax for writing a restricted version of if-else statements in a single line. The following code:

```
**if** num >= 0:
    sign = "positive"
**else**:
    sign = "negative"
```

can be written in a single line as:

```
sign = "positive" **if** num >=0 **else** "negative"
```

This syntax is highly restricted compared to the full “if-elif-else” expressions - no “elif” statement is permitted by this inline syntax, nor are multi-line code blocks within the if/else clauses.

Inline if-else statements can be used anywhere, not just on the right side of an assignment statement, and can be quite convenient:

```
x = 2

_# will store 1 if `x` is non-negative_
_# will store 0 if `x` is negative_
my_list = [1 **if** x >= 0 **else** 0]
print(my_list)
```

