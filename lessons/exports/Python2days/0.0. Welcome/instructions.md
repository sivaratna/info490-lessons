# Welcome -- Don't Panic!

This classroom has over 100 lessons!  But before we start to get queasy let's go over some of expectations. 

1.  Each exercise is small and not very difficult.  Each lesson also has a reference if you want or need more information.  They are all based on the online lessons: https://snakify.org/lessons/

2.  Remember the purpose of all this:  we want to make you a proficient programmer.  You get there by doing this everyday.  Before you know it, you will no longer need to look up syntax and your coding will become a natural extension of your thinking.

3.  Each lesson is meant to be done in sequence, however, they are not locked.  So if you get stuck and want to try the next one, you can.

**Grading**

- Each lesson will be worth 1 point.  Skip a lesson, skip a point.
- If you want more than 100 points (or to make up for a skipped lesson), you can do more than 100 lessons.
- This welcome lesson is worth 1 point too.

**Getting Help**

If you need help on any lesson, tag it on piazza as 2D-XYZ where XYZ is the lesson prefix.

To get started, hit submit.