# Statement

Given a sequence of  integers, where each number is written in a separate line. The sequence ends with 0. Print the average of the sequence. 

# EDIT:  11.21.2019.  The original lesson asked for non-negative integers, but one of the test cases has a negative number. 

# Example input

```
10
```

```
30
```

```
0
```

# Example output

```
20.0
```

# Theory

If you don't know how to start solving this assignment, please, review a theory for this lesson:

https://snakify.org/lessons/while_loop/   

You may also try step-by-step theory chunks:

https://snakify.org/lessons/while_loop/steps/1/