# Statement

Given a sequence of non-negative integers, where each number is written in a separate line. The sequence ends with 0. Print the number of elements of the sequence that are greater than their neighbors above.  

# Example input

```
1
2
3
4
5
0
```

# Example output

```
4
```

# NOTE FROM STUDENTS:

Maybe I am the only one who misinterpreted the directions, but I spent a decent amount of time writing incorrect code for this. 

The directions read, "Print the number of elements of the sequence that are greater than their neighbors above" and I interpreted this as all indexes above the index that you are on instead of just the singular prior index.

 It took me  a while to come up with working code (that only failed the last test case by chance) only to find out that the solution was actually very simple. 

# Theory

If you don't know how to start solving this assignment, please, review a theory for this lesson:

https://snakify.org/lessons/while_loop/   


You may also try step-by-step theory chunks:

https://snakify.org/lessons/while_loop/steps/1/