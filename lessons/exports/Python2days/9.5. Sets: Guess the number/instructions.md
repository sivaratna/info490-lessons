# Statement

Augustus and Beatrice play the following game. Augustus thinks of a secret integer number from 1 to **n**. Beatrice tries to guess the number by providing a set of integers. Augustus answers `YES` if his secret number exists in the provided set, or `NO`, if his number does not exist in the provided set. Then after a few questions Beatrice, totally confused, asks you to help her determine Augustus's secret number.

Given the positive integer **n** in the first line, followed by the a sequence Beatrice's guesses, series of numbers seperated by spaces and Agustus's responses, or Beatrice's plea for `HELP`. When Beatrice calls for help, provide a list of all the remaining possible secret numbers, in ascending order, separated by a space.

# EDIT 10.21.2018

# This is a poorly worded and confusing question.  I actually don't know the intent of this question. 

# Basically the game goes like this:

A chooses a 'secret' number

B then inputs a list of numbers

A says YES or NO if the secret number is in B's LIST

Then it continues on until B figures out there's only one possible number to guess

Here's how my solution runs (bold is input)

```
A: input secret number: N **6**
B: input guess: space separated values OR HELP (hint: set()) **1 2 3**
A: is N in {1, 2, 3} YES/NO **NO**
B: input guess: space separated values OR HELP (hint: set()) **4 5 6 7 8**
A: is N in {4, 5, 6, 7, 8} YES/NO **YES**
B: input guess: space separated values OR HELP (hint: {4, 5, 6, 7, 8}) **5 6**
A: is N in {5, 6} YES/NO **YES**
B: input guess: space separated values OR HELP (hint: {5, 6}) 5
A: is N in {5} YES/NO **NO**
LEFT: 6
```

# Example input

```
6
```

```
1 2 3
```

```
NO
```

```
4 5 6 7 8 
```

```
YES
```

```
5 6
```

```
YES
```

```
5
```

```
NO
```

# Example output.  You need to print 'LEFT: ' followed by the numbers left to guess from if either Help is entered or there's only one number left.

```
LEFT: 6
```

# The numbers following the token 'LEFT:' should be space separated

# Theory

If you don't know how to start solving this assignment, please, review a theory for this lesson:

https://snakify.org/lessons/sets/