import unittest
import sys#EditDelete
import random#EditDelete
import main as lesson#EditDelete
#Click to add an import
class UnitTests(unittest.TestCase):
Advanced
Example test case
def test_prints(self):
# Failure message: 
# only use 3 print statements
self.counter = 0
# org_print = print
def my_print(*args):
  self.counter += 1
  #org_print(*args)
  

lesson.print = my_print
lesson.mad_lib('a','b','c')
self.assertTrue(self.counter == 3, "only 3 calls to print")
def test_mad_lib(self):
# Failure message: 
# unexpected mad_lib
import sys

class MockDevice():
   def __init__(self):
     self.buffer = []
   def write(self, s): 
     self.buffer.append(s)
   def clear(self):
     self.buffer = []
   def flush(self):
     pass
   def get_buffer(self):
     return ''.join(self.buffer)
     
# need to do this again
# remove this, then you get the 'FAIL state'
try:
  lesson.mad_lib('','','')
except AttributeError:
  self.assertRaises(AttributeError, "missing mad_lib()")


org_std = sys.stdout
mock = MockDevice()
sys.stdout = mock 

a = 'a'
b = 'b'
c = str(random.randint(1,101))
lesson.person = 'mike'
lesson.game = 'dice'
lesson.animal = c

lesson.mad_lib(a,b,c)
result = mock.get_buffer()

mock.clear()
self.mad_lib(a,b,c)
answer = mock.get_buffer()

sys.stdout = org_std

self.assertEqual(answer, result)
def test_functions(self):
# Failure message: 
# something went wrong
a = ''
b = ''
c = ''

try:
  a = lesson.get_animal()
  b = lesson.get_person()
  c = lesson.get_game()
  lesson.mad_lib('','','')
except AttributeError:
  self.raiseError("missing funcions")
    
self.assertTrue(len(a) > 1, "bad animal")
self.assertTrue(len(b) > 1, "bad person")
self.assertTrue(len(c) > 1, "bad game")
Create test case