import unittest
import sys#EditDelete
import random#EditDelete
#Click to add an import
class UnitTests(unittest.TestCase):
Advanced
Example test case
def test_def_who_am_i(self):
# Failure message: 
# who_am_i is not defined
try:
  who_am_i('cs',3)
except AttributeError:
  self.assertTrue(False, "missing the function who_am_i()" )
  #assertRaises(AttributeError, "missing who_am_i()")

# valid = True  
# try:
#   who_am_i('CS', 7)
# except:
#   valid = false
# self.assertTrue(valid, "the function who_am_i is missing")
def test_variables(self):
# Failure message: 
# my_major my_caffeine need to be defined
try:
  print(my_major, my_caffeine)
except:
  self.assertTrue(False, "missing variable names" )
  # assertRaises(AttributeError, "missing variable names")
  
msg = "my_caffeine needs to be number"
self.assertTrue(int(my_caffeine) == my_caffeine, msg)
  
def test_who_am_i(self):
# Failure message: 
# who_am_i did not produce the correct result
'''     
with mock.patch('sys.stdout', new=MockDevice()) as fake_out:
            print_poem()
            fack_out.get_buffer
         
            
my_major = 'CS'
my_caffeine = 0
def who_am_i(a,b):
  print("Your major is",a,"and you drink",b, "cups of coffee each day.")

'''

class MockDevice():
   def __init__(self):
     self.buffer = []
   def write(self, s): 
     self.buffer.append(s)
   def flush(self):
     pass
   def get_buffer(self):
     return ''.join(self.buffer)
     
# need to do this again
# remove this, then you get the 'FAIL state'
try:
  who_am_i('cs',3)
except AttributeError:
  self.assertRaises(AttributeError, "missing who_am_i()")

org_std = sys.stdout
mock = MockDevice()
sys.stdout = mock 

cups = random.randint(1,101)
who_am_i('CS', cups)
result = mock.get_buffer()
sys.stdout = org_std

answer = "Your major is CS and you drink "
answer += str(cups) + " cups of coffee each day.\n"

self.assertEqual(answer, result)
Create test case