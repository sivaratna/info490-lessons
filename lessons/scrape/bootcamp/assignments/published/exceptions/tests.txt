import unittest
import statistics as stats#EditDelete
import lesson#EditDelete
#Click to add an import
class UnitTests(unittest.TestCase):
Advanced
Example test case
def test_var(self):
# Failure message: 
# worry_free_var failed
numbers = [1]

a1 = self.worry_free_var(numbers)

valid = False
msg = ""
try:
  a2 = lesson.worry_free_var(numbers)
  valid = a1 is a2 and a2 is None
  if not valid:
    msg ="No None"
except Exception as e:
  msg = "Got an exception " + str(e)
  valid = False

self.assertTrue(valid, "reread the instructions " + msg)
def test_mode(self):
# Failure message: 
# worry_free_mode failed
numbers = []

a1 = self.worry_free_mode(numbers)

valid = False
msg = ""
try:
  a2 = lesson.worry_free_mode(numbers)
  valid = a1 is a2 and a2 is None
  if not valid:
    msg ="No None"
except Exception as e:
  msg = "Got an exception " + str(e)
  valid = False

self.assertTrue(valid, "reread the instructions " + msg)
def test_simple(self):
# Failure message: 
# make sure worry_free_mode is defined
try:
  numbers = [1,1,2]
  a1 = self.worry_free_mode(numbers)
  a2 = lesson.worry_free_mode(numbers)
  valid = a1 == a2
except:
  valid = False

self.assertTrue(valid, "unable to use worry_free_mode")
Create test case