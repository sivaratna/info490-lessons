A 🐍 Coder's Journey
Lesson 18: Modules

This lesson looks a bit different.  Take a look at the editor window.  It now has multiple 'tabs'.  Each tab represents a separate editor (actually a file) where you can write code.  However, when you hit 'run ▶︎'  only the code in the first tab (named main.py) will run.  

Take a look at the tab named 'counter.py' by selecting it.  
It shows a simple way to use a dictionary to keep track of how many times a function was called.
This lesson is all about separating code into different files (or modules) so that you can easily reuse code.  This is essentially how many of the popular Python and data science libraries (numpy, pandas, statistics, etc) are created.  

Once you start building large programs, it helps to put functions into their own module/file in order to organize your code and to easily share it with others.  Once you create a module of your functions, you can import those functions into any program that could use them.  This allows for code reuse.
Coder's Log:  Creating a re-usable Python module for sharing involves more 'work' than we are showing here.  However, the main concepts are exactly the same.
The import statement
In this example, the file main.py imports functions from the module counter.py.  Select the main.py tab to bring it to the forefront.
For our first example, type in the following import statements
from counter import add, get_count

print(add(add(1,3),3), get_count(add))

The import statement has two different syntax styles.  This example uses the from keyword followed by the module name (without the .py suffix) and is importing the functions add and get_add_counts.  So now in your code both of those functions are available to use.  Go ahead and run the code.
In the second example, type the following import statement style:
import counter as calc

print(calc.add(calc.add(1,3),3), calc.get_count(calc.add))

Using this syntax forces us to use the prefix 'calc' for all the functions inside the module.  Be sure to look at the tab "counter.py" to see how add and get_add_count (both of which you wrote before) work.  Personally, I like this style of importing since it serves as a bit of documentation when you are using a function.  It's clear from where it's coming.

You can also skip the 'as' keyword and use the full name of the module:
import counter

print(counter.add(counter.add(1,3),3))
print(counter.get_count(counter.add))
System Libraries
In upcoming lessons, we will rely on using libraries that provide a lot of functionalities.  It would be impractical (nearly impossible) to not use these libraries.  Up until now, we have focused on core Python, but now we will start to explore some of the awesome modules available to us to use. Below is a quick tour of some of the popular libraries used for data analysis.
Math
Python's math module contains many of the functions you might need to help with summing, trigonometry, logarithms, etc.  At the end of this lesson is a list of available math functions that are useful.  A simple example is shown in below:
import math

print(math.pi, math.e)

# sum up the numbers
numbers = [1,2,3,4,4]
print(math.fsum(numbers))

Please see https://docs.python.org/3.6/library/math.html for details (Yes, it is actually important that you read the documentation).  The more you know about the standard libraries, the less code you will need to write -- it's always better to re-use someone's well tested module than attempt to duplicate it's functionality.  The point isn't to memorize them, but to know its general capabilities.  You can always look it up later -- but it helps to remember when an important task is already done for you.
Statistics
Python has its own statistics module, named stats; however, we will also be using the popular SciPy module for a fuller set of statistical functions.  We will have separate lessons for these.  Below shows some simple statistics functions
Please read https://docs.python.org/3.6/library/statistics.html for details.
# statistics
import statistics
numbers = [1,2,3,4,4]
print("ave",  statistics.mean(numbers))
print("std",  statistics.stdev(numbers))
print("mode", statistics.mode(numbers))

Data Science Modules
One of the great things about Python is the plethora of available modules/libraries for working with datasets.  We will have more lessons on each but here's a small sample (we certainly have a lot to cover!):
numPy:  working with arrays (these are more 'array' like rather than lists)
pandas: working with "spreadsheets" of data (built on top of numPy)
matplotlib: visualization library for building graphs
seaborn: visualization library built on top of matplotlib
sciPy: scientific functions (including statistics); numerical and statistical computing
statsmodels: statistical tests and estimate models
from scipy import stats
numbers = [1,2,3,4,4]
print(stats.zscore(numbers))

Although many of the tasks that we ask you to do in this class might be easily solved by using a preexisting library, if any of the lessons ask you NOT to use a module, the intent is to have you learn an important programming task -- not to make busy work.  
Lesson Assignment

Z Scores
A staple calculation in statistics is the z-score.  As a quick reminder, a z-score is a measurement of how many standard deviations below or above a value is over the mean.  The z-score essentially rescales or normalizes your data. We saw an example earlier using the sciPy library to calculate it.  For each value, you subtract the mean and divide by the standard deviation. 
The default z-score calculated in the sciPy library considers the set of numbers to be the entire set from the population.  As you may remember, when you sample data from a population, you need to adjust the standard deviation calculation (dividing by n - 1 instead of n) (we can simulate this later and show that calculating the sample variance using n -1 is indeed an unbiased estimator for the population variance).  
For this lesson we are going to use both the statistics and math libraries to simulate the results of the sciPy library.  If you couldn't use the sciPy library, how would you calculate z-scores?
Create two functions (inside the lesson.py module)
def calculate_sample_zscores(n):
  return None

def calculate_pop_zscores(n):
  return None

1. Each function calculates zscores for the input n -- which is a list of numbers.  One is for a population of values, the other for a sample of values.
2. Both functions return a list of corresponding zscores.
3. Do NOT use the sciPy zscore function (comment out any code that imports it or uses it).
4. You may need to consult the documentation for both math and statistics modules (urls given above).  There's no need to do the full calculation yourself, use the libraries to help you get the mean and standard deviation.
5. You can assume that the parameter is not None.  However, it could be empty.  In this case, return None

Tag any questions with py18 on piazza.

Before you go, you should know:
what a Python module is
how to import a module
why we use modules

All rights reserved

---------------------------------------------------------------------------------
Math Function Description
Although it's not necessary to memorize the available functions, you should know that the math module provides many convenient math functions:

ceil(x) Returns the smallest integer greater than or equal to x.
copysign(x, y) Returns x with the sign of y
fabs(x) Returns the absolute value of x
factorial(x)  Returns the factorial of x
floor(x) Returns the largest integer less than or equal to x
fmod(x, y) Returns the remainder when x is divided by y
frexp(x) Returns the mantissa and exponent of x as the pair (m, e)
fsum(iterable) Returns an accurate floating point sum of values in the iterable
isfinite(x) Returns True if x is neither an infinity nor a NaN (Not a Number)
isinf(x) Returns True if x is a positive or negative infinity
isnan(x) Returns True if x is a NaN
ldexp(x, i) Returns x * (2**i)
modf(x) Returns the fractional and integer parts of x
trunc(x) Returns the truncated integer value of x
exp(x)  Returns e**x
expm1(x) Returns e**x - 1
log(x[, base]) Returns the logarithm of x to the base (defaults to e)
log1p(x) Returns the natural logarithm of 1+x
log2(x) Returns the base-2 logarithm of x
log10(x) Returns the base-10 logarithm of x
pow(x, y)  Returns x raised to the power y
sqrt(x) Returns the square root of x

acos(x) Returns the arc cosine of x
asin(x) Returns the arc sine of x
atan(x) Returns the arc tangent of x
atan2(y, x) Returns atan(y / x)
cos(x)  Returns the cosine of x
hypot(x, y) Returns the Euclidean norm, sqrt(x*x + y*y)
sin(x)  Returns the sine of x
tan(x)  Returns the tangent of x
degrees(x) Converts angle x from radians to degrees
radians(x) Converts angle x from degrees to radians
acosh(x) Returns the inverse hyperbolic cosine of x
asinh(x) Returns the inverse hyperbolic sine of x
atanh(x) Returns the inverse hyperbolic tangent of x
cosh(x) Returns the hyperbolic cosine of x
sinh(x) Returns the hyperbolic cosine of x
tanh(x) Returns the hyperbolic tangent of x


erf(x)  Returns the error function at x
erfc(x) Returns the complementary error function at x
gamma(x) Returns the Gamma function at x
lgamma(x)  Returns the natural logarithm of the absolute value of the Gamma function at x

Constants
pi Mathematical constant, the ratio of circumference of a circle to its diameter (3.14159...)
e Mathematical constant e (2.71828...)
