import unittest
import main as lesson3#EditDelete
#Click to add an import
class UnitTests(unittest.TestCase):
Advanced
Example test case
def test_add(self):
# Failure message: 
# add and divide need to work
self.assertTrue(3 == lesson3.add(2,1))
self.assertTrue(2 == lesson3.divide(4,2))
def test_add_series(self):
# Failure message: 
# add_series failed
# NEXT TIME: call the function add_series instead !!!!

self.add_counter = 0
self.div_counter = 0
ans = lesson3.add_series()

self.assertTrue(1.875 == ans, "wrong answer")
self.assertTrue(3 == self.add_counter, "need to call add() 3 times")
self.assertTrue(3 == self.div_counter, "need to call divide() 3 times")
def test_clean(self):
# Failure message: 
# found variable
with open("main.py") as fd:
    count = 0
    for line in fd:
        idx = line.find('#')
        if idx >= 0:
            line = line[0:idx]
        idx = line.find('print')
        if idx >= 0:
            line = ''
        line = line.replace('==', 'CMP')
        idx = line.find('=')
        if idx >= 0:
            count += 1
    no_vars = (count == 0)
    msg = "You cannot use any variables "
    self.assertTrue(no_vars, msg)
Create test case