// Python Quiz
// GIFT Format; import via moodle via question bank
// get category from file (checked) get context from file (checked)
// https://teaching.unsw.edu.au/moodle-quiz#addqs
// https://docs.moodle.org/33/en/GIFT_format

$CATEGORY: PYTHON_1

::Q01:: [html]
<p>On which line is the example function call made
<pre>
<code>
1. def example(a):
2    for i in range(0,3):
3.     a += i
4.   return a
5. result = example(0)
</code>
</pre>
</p>
{
~1
~2
~3
~4
=5
}


::Q02:: What is the correct way to declare and define a variable named num in Python to hold an integer
{
=num \= 10
~num
~var num \= 10
~var num:int \= 10
~num:int \= 10
}

::Q03:: What is the most accurate description of number in the following def is_even(number):
{
~argument
=formal parameter
~actual argument
~variable
}

::Q04:: If s is a string, how do you get its length
{
~s.length()
~string.len(s)
=len(s)
~length(s)
}

::Q05:: What is the correct way to access the number 3 in the list n = [1,2,3]
{
~n[0]
~n[1]
=n[2]
~n[3]
}

::Q06:: How would I check if two variables are equal in Python
{
~a.equals(b)
~a \= b
~a and b is Equal
=a \=\= b
}

::Q07:: The dictionary is an ordered container of key/value pairs {F}

::Q08:: In what situation is best to use a while loop
{
=if the number of iterations is uncertain
~if the number of iterations is certain
~when iterating over a list of words
~when you need to use nested loops
}

::Q09:: How would you use a module in Python
{
~include module
=import module
~require module
~load module
}

// Lambda functions have been seen
::Q10:: Can a function be used as an argument? {T}

::Q11:: What value does Python use to indicate the absence of a value
{
~null
~False
=None
~NULL
~''
}

::Q12:: [html]
<p>What is the most accurate description of the variable items
<pre>
<code>
items = [(1,2), (3,4), (5,6), (1,2)]
</code>
</pre>
</p>
{
~set of integer pairs
~tuple of lists
~dictionary of integer pairs
=list of tuples
~set of lists
~set of tuples
}


::Q13:: How would you check that a variable n holds either the value 'a' or 'b'
{
~n \=\= 'a' or 'b'
~n \=\= 'a' and 'b'
~n \=\= 'a' and n \=\= 'b'
=n \=\= 'a' or n \=\= 'b'
~n is 'a' or n is 'b'
}

::Q14:: [html]
<p>What does the following function do
<pre>
<code>
def mystery(n):
  idx = 0
  for i in n:
    idx += 1
  return idx
</code>
</pre>
</p>
{
=implements the len function
~calculates the sum of a list of numbers
~finds the maximum value of a list of numbers
~returns the index of the largest value
}



::Q15:: [html]
<p>How many times will 'hello' be printed when you call qX()
<pre>
<code>
def qX():
  for i in range(0,3):
    print("hello")
    return
</code>
</pre>
</p>
{
~0
=1
~2
~3
~4
}

